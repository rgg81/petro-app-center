package br.com.petrobras.appcenter



import grails.test.mixin.*
import static plastic.criteria.PlasticCriteria.*

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(UserApplication)
@Mock([UserApplication,User, Application, BaseApplication, Link, Icon,ApplicationDraft])
class UserApplicationTests {

	def app
	
	void setUp() {
	  app = new Application(name:"Petro App Center", 
								  description: "Loja de aplicacoes",
								  link:new Link(url:"http://www.oglobo.com.br"),
								  icon:new Icon(size:1),
								  currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "SGVT")
	}
	
	
    void testValidUserApplication() {
		def user = new User(key: "uq4e")
		
		def userApp = new UserApplication( user: user, application: app, position: 0 )
		
		assert userApp.validate()
    }
	
	void testUserApplicationWithInvalidUser() {
		def user = null
		
		def userApp = new UserApplication( user: user, application: app, position: 0 )
		
		assert !userApp.validate()

	}
	
	
	void testUserApplicationWithInvalidApplication() {
		def user = new User(key: "uq4e")
		def appNull = null
		
		def userApp = new UserApplication( user: user, application: appNull, position: 0 )
		
		assert !userApp.validate()
	}
	
    void testUserApplicationWithDuplicateApp() {
		
		/*def user = new User(key:'uq4e')
		user.save()
		app.save()

		def userApp1 = new UserApplication( user: user, application: app, position: 0 )
		userApp1.save()
		
		shouldFail{
			def userApp2 = new UserApplication( user: user, application: app, position: 1 )
			userApp2.save()
		}

		
		assert UserApplication.count() == 1
	*/ 
	//TODO: implement me in integration tests
    }
	
	void testUserApplicationWithDuplicatePosition() {
		def user = new User(key:'uq4e')
		
		def app1 = new Application(name:"Petro App Center",
							  	description: "Loja de aplicacoes",
							  	link:new Link(url:"http://www.oglobo.com.br"),
							  	icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VFGT")
		
		def app2 = new Application(name:"Petro App Center2",
								description: "Loja de aplicacoes2",
								link:new Link(url:"http://www.oglobo.com.br"),
								icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVVT")
		
		user.save()
		app1.save()
		app2.save()

		def userApp1 = new UserApplication( user: user, application: app1, position: 0 )
		assert userApp1.save(flush:true)

		shouldFail{
			def userApp2 = new UserApplication( user: user, application: app2, position: 0 )
			userApp2.save(flush:true)
		}
		
		assert UserApplication.count() == 1
	}

	void testUsersSingleApplicationCount() { 
		def userList = [(new User(key:'uq4e').save()),
						(new User(key:'up2h').save()),
						(new User(key:'uq4n').save()),
						(new User(key:'upn1').save())]

		userList.each() {
			(new UserApplication( user: it, application: app, position: 0 )).save()
		}
		assert UserApplication.countByApplication(app) == 4
	}

	void testUsersMultipleApplicationsCountAndRanking() { 
		def user1 = new User(key:'uq4e').save()
		def user2 = new User(key:'up2h').save()
		def user3 = new User(key:'uq4n').save()
		def user4 = new User(key:'upn1').save()

		def app1 = new Application(name:"Petro App Center 1",
							  	description: "Loja de aplicacoes 1",
							  	link:new Link(url:"http://www.oglobo.com.br"),
							  	icon:new Icon(size:1),
								 currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ1").save()
		def app2 = new Application(name:"Petro App Center 2",
							  	description: "Loja de aplicacoes 2",
							  	link:new Link(url:"http://www.oglobo.com.br/1"),
							  	icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ2").save()
		def app3 = new Application(name:"Petro App Center 3",
							  	description: "Loja de aplicacoes 3",
							  	link:new Link(url:"http://www.oglobo.com.br/2"),
							  	icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ3").save()
		def app4 = new Application(name:"Petro App Center 4",
							  	description: "Loja de aplicacoes 4",
							  	link:new Link(url:"http://www.oglobo.com.br/3"),
							  	icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ4").save()
		def app5 = new Application(name:"Petro App Center 5",
							  	description: "Loja de aplicacoes 5",
							  	link:new Link(url:"http://www.oglobo.com.br/4"),
							  	icon:new Icon(size:1),
								currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ5").save()

		new UserApplication( user: user1, application: app1, position: 0).save()
		new UserApplication( user: user1, application: app2, position: 1).save()
		new UserApplication( user: user1, application: app3, position: 2).save()
		new UserApplication( user: user1, application: app4, position: 3).save()

		new UserApplication( user: user2, application: app1, position: 0).save()
		new UserApplication( user: user2, application: app2, position: 1).save()
		new UserApplication( user: user2, application: app3, position: 2).save()

		new UserApplication( user: user3, application: app1, position: 0).save()
		new UserApplication( user: user3, application: app2, position: 1).save()

		new UserApplication( user: user4, application: app1, position: 0).save()

		assert UserApplication.countByApplication(app1) == 4
		assert UserApplication.countByApplication(app2) == 3
		assert UserApplication.countByApplication(app3) == 2
		assert UserApplication.countByApplication(app4) == 1
		assert UserApplication.countByApplication(app5) == 0	

		mockCriteria(UserApplication)
		def c = UserApplication.createCriteria()
		def results = c.list {
			projections {
				groupProperty("application.name")
				rowCount()
			}
		}

		results.sort( {a, b -> b[1] <=> a[1] } )
		assert results.get(0).get(0) == "Petro App Center 1"
		assert results.get(1).get(0) == "Petro App Center 2"
		assert results.get(2).get(0) == "Petro App Center 3"
		assert results.get(3).get(0) == "Petro App Center 4"
	}

}
