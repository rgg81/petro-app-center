package br.com.petrobras.appcenter


import grails.test.mixin.*

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Application)
@Mock([Icon,User, BaseApplication, UserService, Version, Compatibility,InformationSupport,Link, Tag, ApplicationDraft])
class ApplicationTests {
	
	
	def populateValidParamsOnlyRequiredFields() {
		return	 new Application(name:"Petro App Center", 
	    							  description: "Loja de aplicacoes",
	    							  link:new Link(url:"http://www.gloco.com.br"),
	    							  icon:new Icon(size:1L),
									  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XCVG");
	}

	void tearDown() {
		
	}
	
	void testSaveOK() {
		def app = populateValidParamsOnlyRequiredFields()
		app.save()
		assert !app.hasErrors()
	}
	
	void testSaveFail() {
		
		//Seta todas as classes obrigatorias como null
		def app = new Application(name:null,
								  description: null,
								  link:new Link(),
								  icon:new Icon(),
								  currentVersion:new Version())
		try{
			app.save()
			assert false
		}catch(grails.validation.ValidationException validation){
		 	assert true
			assert app.hasErrors()
		}	
	}
	
	
	void testAllValidationOk() {
		def app = populateValidParamsOnlyRequiredFields()
		
		assert app.validate()
		
	}
	
	void testCatalogCodeValidationConstraintWithSame() {
		def app = populateValidParamsOnlyRequiredFields()
		def app2 = populateValidParamsOnlyRequiredFields()
		
		mockForConstraintsTests(Application, [app, app2])
		app.save()
		
		assert !app2.validate()
		
		
	}
	

    void testValidationOnlyName() {
    	def app = new Application(name:"Petro App Center")
    	assert !app.validate()
    }


	void testDeleteApplicationOKOnlyRequiredFields() {

		/* TODO teste para verificar cascade deve ser de integração migrar!!
		def app = populateValidParamsOnlyRequiredFields()
		app.save(flush:true)
		
		assert app.id != null
		assert Version.list().size() == 1
		assert Link.list().size() == 1
		assert Icon.list().size() == 1
		
		app.delete(flush:true)
		
		assert Application.list().size() == 0
		assert Version.list().size() == 0
		assert Link.list().size() == 0
		assert Icon.list().size() == 0
		*/
	}


}
