package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import grails.test.mixin.web.ControllerUnitTestMixin

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Issue)
 //bug GRAILS-11136
@TestMixin(ControllerUnitTestMixin)
class IssueTests {

    void test() {
    	def issue

		/*
		** caso feliz
		*/

		issue = new Issue(description: 'pre-historic', position: 100)
		assert issue.validate()
		issue.save(flush: true)


		/*
		** casos tristes
		*/

		issue = new Issue(position: 200)
		assert !issue.validate()
		assert issue.errors.getFieldError('description').getCode() == 'nullable'

		issue = new Issue(description: '', position: 200)
		assert !issue.validate()
		assert issue.errors.getFieldError('description').getCode() == 'nullable'

		issue = new Issue(description: 'pre-historic')
		assert !issue.validate()
		assert issue.errors.getFieldError('description').getCode() == 'unique'

		
		issue = new Issue(description: 'uncomfortable', position: 100)
		assert !issue.validate()
		assert issue.errors.getFieldError('position').getCode() == 'unique'

    }

}
