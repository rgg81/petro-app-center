package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse
import grails.converters.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(HeartBeatController)
@Mock([User])
class HeartBeatControllerTests {

	def user

    @Before
    void setUp() {
    	user = new User(key:'Zé Colméia').save(flush: true, failOnError:true)

        controller.shiroService = new Expando()
    }

    @After
    void tearDown() {
        user.delete(flush: true)

        controller.shiroService = null
    }

	void testIsAliveSessionUp() {
        def expectedResponseText = ([isAlive: false] as JSON) as String

        controller.shiroService.metaClass.loggedUser = { -> null}

        controller.isAlive()

        assert response.status == HttpServletResponse.SC_OK 
        assert response.contentType.startsWith("application/json")
        assert response.text == expectedResponseText
	}

	void testIsAliveSessionDown() {
        def expectedResponseText = ([isAlive: true] as JSON) as String

        controller.shiroService.metaClass.loggedUser = { -> user}

        controller.isAlive()

        assert response.status == HttpServletResponse.SC_OK 
        assert response.contentType.startsWith("application/json")
        assert response.text == expectedResponseText
	}

}
