package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(AdminApplication)
@Mock([User, Application, ApplicationDraft])
class AdminApplicationTests {

    void testConstraints() {
		def app = new Application(name:"Petro App Center", 
	    							  description: "Loja de aplicacoes",
	    							  link:new Link(url:"http://www.gloco.com.br"),
	    							  icon:new Icon(size:1L),
									  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XPTO")
		app.save()
		
		def user = new User(key:'upn1')
		user.save()
									  
		def existingAdminApplication = new AdminApplication(application:app, admin:user)
		def nullableAdminApplication = new AdminApplication(application: null, admin: null)
		
		mockForConstraintsTests(AdminApplication, [existingAdminApplication, nullableAdminApplication])		

		def newAdminApplication = new AdminApplication(application:app, admin:user)

		// unique
		assert !newAdminApplication.validate()
		assert "unique" == newAdminApplication.errors["admin"]

		// nullable
		assert !nullableAdminApplication.validate()
		assert "nullable" == nullableAdminApplication.errors["application"]
		assert "nullable" == nullableAdminApplication.errors["admin"]

    }
}
