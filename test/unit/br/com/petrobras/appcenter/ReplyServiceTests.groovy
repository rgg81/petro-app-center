package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ReplyService)
@Mock([User, Reply, Comment,Application,ApplicationDraft,AdminApplication])
class ReplyServiceTests {
	def commentAuthor, replyAuthor, reply, other,comment,adminapp1,adminapp2

	void setUp() {
		
		commentAuthor = new User(key: "uq4e").save(flush:true)
		replyAuthor = new User(key: "uq4n").save(flush:true)
		adminapp1 = replyAuthor
		adminapp2 = new User(key: "up23").save(flush:true)
		other = new User(key: "uq4p").save(flush:true)

		def app = new Application(name:"Petro App Center", 
								  description: "Loja de aplicacoes",
								  link:new Link(),
								  icon:new Icon(), 
								  catalogCode:"XCVG",
								  currentVersion:new Version(number: 1, description: "v1", date: new Date())
								  )
		app.save(flush:true)

		def ap1 = new AdminApplication(admin:adminapp1, application:app).save(flush:true)
		def ap2 = new AdminApplication(admin:adminapp2, application:app).save(flush:true)



		comment = new Comment(title: "Comentario title", text: "Comentario text", user: commentAuthor, application: app, rating: 1).save(flush:true)
    	
    	service.shiroService = new Expando()

	}

	void assertCauseExceptionMessage(String msg, Closure c){
		def error
    	try{
    		c.call()
    	}catch(Exception e){ if(e?.cause?.message) {error = e.cause.message} else{throw e} }
    	assertTrue  "Deveria ter dado erro" , error == msg
	}

	//importante
	void testSaveReplyWithLoggedUserDiferentFromReplyAuthor () {
    	assertCauseExceptionMessage("Usuario nao permitido",{
    		service.shiroService.metaClass.loggedUser = { -> adminapp2}
        	service.saveReply(new Reply(user:adminapp1,comment:comment, text:"text"))
    	})    
    }

    //importante
	void testSaveReplyWithLoggedUserNotAdmin () {
    	assertCauseExceptionMessage("Usuario nao permitido",{
    		service.shiroService.metaClass.loggedUser = { -> other}
        	service.saveReply(new Reply(user:other,comment:comment, text:"text"))
    	})    
    }

    void testSaveReplyNoLoggedUser(){
    	assertCauseExceptionMessage("Usuario Invalido",{
    		service.shiroService.metaClass.loggedUser = { -> null}
    		service.saveReply(new Reply(user:replyAuthor,comment:comment, text:"text"))
    	})   	
    }

 	void testSaveReplyInvalidReply(){
 		assertCauseExceptionMessage("Replica Invalida",{
		 		service.shiroService.metaClass.loggedUser = { -> replyAuthor}
		    	service.saveReply(null)
	    })
    }

    void testSaveReplyWithoutText(){
    	assertCauseExceptionMessage("Replica Invalida",{
		    	service.shiroService.metaClass.loggedUser = { -> replyAuthor}
		    	service.saveReply(new Reply(user:replyAuthor,comment:comment, text:""))
		})
    }

 	void testDeleteReplyInvalidReply(){
    	assertCauseExceptionMessage("Replica Invalida",{
    		service.shiroService.metaClass.loggedUser = { -> replyAuthor}
    		service.deleteReplyAdmin(null)
    	})
    }

    void testDeleteReplyNoLoggedUser(){
    	def replyToDelete = new Reply(user:replyAuthor,comment:comment, text:"text").save()
    	assertCauseExceptionMessage("Usuario Invalido",{
    		service.shiroService.metaClass.loggedUser = { -> null}
    		service.deleteReply(replyToDelete)
    	})
    }
	
	//importante
    void testDeleteReplyOnlyByAdminApps(){
    	def replyToDelete = new Reply(user:adminapp1,comment:comment, text:"text").save()
		assertCauseExceptionMessage("Usuario nao permitido",{
    		service.shiroService.metaClass.loggedUser = { -> other}
    		service.deleteReplyAdmin(replyToDelete)
    	})
    }

    void testDeleteReplyOk(){
    	def replyToDelete = new Reply(user:adminapp1,comment:comment, text:"text").save()
   		service.shiroService.metaClass.loggedUser = { -> adminapp2}
   		service.deleteReply(replyToDelete)
    }

}
