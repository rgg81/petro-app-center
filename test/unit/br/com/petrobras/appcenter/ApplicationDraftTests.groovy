package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ApplicationDraft)
class ApplicationDraftTests {

   void testConstraints() {
	   def verDraft = new Version(number:"2",description:"descricao da versao 1.0",date:new Date())
	   def appDraftWithOutApplication = new ApplicationDraft(name:"Draft App",
		   description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
		   link:new Link(url:"http://www.google.com.br/q=222222222"),
		   icon:new Icon(size:1), currentVersion: verDraft, catalogCode:"XPXO")
	   
	   def app = new Application(name:"Draft App",
		   description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
		   link:new Link(url:"http://www.google.com.br/q=222222222"),
		   icon:new Icon(size:1), currentVersion: verDraft,catalogCode:"XPYO")
	   //application null
	   assert appDraftWithOutApplication.validate()
	   
	   assert appDraftWithOutApplication.save()
	   
	   def appDraftWithApplication = new ApplicationDraft(name:"Draft App",
		   description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
		   link:new Link(url:"http://www.google.com.br/q=222222222"),
		   icon:new Icon(size:1), currentVersion: verDraft, application:app, catalogCode:"XPZO")
	   
	   assert appDraftWithApplication.validate()
	   assert appDraftWithApplication.save()
    }
}
