package br.com.petrobras.appcenter

import grails.plugin.mail.MailService;
import grails.test.mixin.*
import org.codehaus.groovy.grails.plugins.testing.GrailsMockHttpServletRequest
import org.codehaus.groovy.grails.web.json.JSONObject
import org.junit.*

import javax.servlet.http.HttpServletResponse

import grails.converters.*
import grails.validation.ValidationException

import org.springframework.validation.*

import au.com.bytecode.opencsv.CSVWriter
import groovy.mock.interceptor.MockFor

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ReviewController)
@Mock([Application, User, Comment,ApplicationDraft,ReviewMailService,ReviewsService])
class ReviewControllerTests {

    def user, application, comment, likes, newReview, updatedReview
    List invalidStringDomain, invalidRatingDomain, invalidIntegerDomain

    final shouldFailWithCause = new GroovyTestCase().&shouldFailWithCause

    @Before
    void setUp() {
    	user = new User(key:'uq4n').save(flush: true, failOnError:true)
        
        def mockcontrol = mockFor(ShiroService)
        mockcontrol.demand.loggedUser(0..1000){ -> user }

        controller.shiroService = mockcontrol.createMock()
        controller.reviewsService = new ReviewsService()
        controller.reviewsService.shiroService = mockcontrol.createMock()
        controller.reviewsService.reviewMailService = new ReviewMailService()

        application = new Application(name:"Petro App Center", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon(), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()),catalogCode: "SCNV")
        application.save(flush: true, failOnError:true)

        comment = new Comment(title: "Comment title", text: "Comment text", application:application, user:user, rating: 2)
        comment = comment.save(flush: true, failOnError:true)

        likes = new Expando()
        likes.metaClass.size = { 10 }
        comment.metaClass.getLikes = { likes }
        comment.metaClass.getDislikes = { likes }
        //bug GRAILS-11855
        comment.metaClass.getLastUpdated = { new java.util.Date().getTime() }

        newReview = [ rating: 3, title: "Comment title", text: "comment.text", application: application]

        updatedReview = [
            rating: 3,
            title: "Comment title", 
            text: "Comment text",
            id: 1
        ]

        invalidIntegerDomain = [null, "1", "0", "6", new Date()]
        invalidStringDomain = [null, 1, 1.1, new Date()]
        invalidRatingDomain = [null, -1, 0, 6, 10000]

        User.metaClass.getName = { "Zé Colméia" }
        User.metaClass.getDepartment = { [acronym: "US/Yellostone"] }

        ReviewMailService.metaClass.sendMailNewReview = { Comment comment, String host ->
            println "sending email to All"
        }
    }

    private void invalidAppplication(Closure closure, status) {     
        for (app_code in [null, "0", "-1"]) {
            params["applicationId"] = app_code as String

            closure()

            assert response.status == status
            assert response.text.length() > 0
            response.reset()
        }
    }

	void testListReviewsWithMostLikedCommentInvalidAppplication() {
        params["limit"] = 1

        invalidAppplication({controller.mostLiked()}, HttpServletResponse.SC_BAD_REQUEST)
    }

	void testListReviewsWithMostLikedComment() {		
        params["applicationId"] = "1"
        params["limit"] = "5"

        controller.reviewsService.metaClass.listMostLikedReviews = {  applicationid, limit ->
            [comment]
        }

        controller.mostLiked()

        println response.text
        assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.contains("text/json")
    }

    void testListRecentReviewsInvalidAppplication() {
        params["offset"] = "2"
        params["limit"] = "2"

        invalidAppplication({controller.recent()}, HttpServletResponse.SC_BAD_REQUEST)
    }

    void testListRecentReviews() {
        params["applicationId"] = "1"

        controller.reviewsService.metaClass.listRecentReviews = { Application application, int offset, int limit -> 
            [1,2,3,4,5].collect{ 
                [comment: comment, rating: it]
            }
        }

        for (offset in [null, -1, 0, 2, 5]) {
            println "offset: " + offset
            params["offset"] = offset as String
            params["limit"] = "2"

            controller.recent()

            assert response.status == HttpServletResponse.SC_OK
            assert response.contentType.contains("text/json")
            response.reset()
        }

        for (limit in [null, -1, 0, 2, 5]) {
            println "limit: " + limit
            params["offset"] = "2"
            params["limit"] = limit as String

            controller.recent()

            assert response.status == HttpServletResponse.SC_OK
            assert response.contentType.contains("text/json")
            response.reset()
        }
    }

    void testListReviewsByRating() {
        params["applicationId"] = "1"
        params["rating"] = "5"
        params["offset"] = "1"
        params["limit"] = "10"
        
        def mockcontrol = mockFor(ShiroService)
        mockcontrol.demand.loggedUser(1..5) { -> user }
        mockcontrol.demand.isModerator(1..5) { -> true }

        controller.shiroService = mockcontrol.createMock()

        controller.reviewsService.metaClass.listByRating = { Application application, double rating, int offset, int limit -> 
            [1,2,3,4,5].collect{ 
                [stars: it, user: [key: user.key, name: user.name, department: user.department.acronym]] 
            }
        }
        
        controller.byRating()
        assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.contains("text/json")
    }

    void testAddReviewInvalidAppplicationInJSON() {

        newReview.applicationId = null
        request.JSON = newReview
        request.method = 'POST'
        controller.insert()
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
        response.reset()

        newReview.applicationId = "1"
        request.JSON = newReview
        request.method = 'POST'
        controller.insert()
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
        

    }

   void testAddReviewInvalidReviewInternalServerError() {
        request.JSON = newReview

        controller.reviewsService.metaClass.addReview = { newReview, String host ->
            throw new RuntimeException("Failed!!")
        }
        request.method = 'POST'
        controller.insert()

        assert response.status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR  
        assert response.text.length() > 0
    }

   void testAddReviewInvalidReviewBadRequestFromService() {
        request.JSON = newReview

        def mockControl = mockFor(Errors,true)
        
        mockControl.demand.getAllErrors(2) {  -> new ArrayList<ObjectError>() }
        def errors =  mockControl.createMock()

        controller.reviewsService.metaClass.addReview = { newReview, String host ->
            try{
                throw new ValidationException("Failed with ValidationException!!", errors) 
            } catch (Exception e) {
                throw new RuntimeException("Failed with RuntimeException!!", e)
            }
        }
        request.method = 'POST'
        controller.insert()

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.text.length() > 0
    }

    private void propertyTest(String propertyName, List testDomain, Closure closure, review) {

        for (prop in testDomain) {
            request.JSON = review

            request.JSON[propertyName] = prop
            println "request.JSON[propertyName]: " + request.JSON[propertyName]

            closure()

            assert response.status == HttpServletResponse.SC_BAD_REQUEST 
            assert response.text.length() > 0
        }
    }

    void testAddReviewInvalidReviewBadRequestFromControllerTitle() {
        propertyTest("title", invalidStringDomain, { 
            request.method = 'POST'
            controller.insert()}, newReview)
    }

    void testAddReviewInvalidReviewBadRequestFromControllerText() {
        propertyTest("text", invalidStringDomain, {
            request.method = 'POST'
            controller.insert()}, newReview)
    }

    void testAddReviewInvalidReviewBadRequestFromControllerRating() {
        propertyTest("rating", invalidIntegerDomain, {
            request.method = 'POST'
            controller.insert()}, newReview)
        propertyTest("rating", invalidRatingDomain, {
            request.method = 'POST'
            controller.insert()}, newReview)
    }


    void testAddReview() {

        def application = new Application(name:"Petro App Center",
                description: "Loja de aplicacoes",
                link:new Link(url: "http://www.google.com.br"),
                icon:new Icon(),
                currentVersion: new Version(number: 1, description: "v1", date: new Date()),catalogCode: "SCNV")
        application.save(flush: true, failOnError:true)

        def comment = new Comment(title: "Comment title", text: "Comment text", application:application, user:user, rating: 2)

        request.JSON = comment as JSON
        request.method = 'POST'
        controller.insert()

        assert response.status == HttpServletResponse.SC_CREATED
        assert response.text.contains(comment.title)
        assert response.text.contains("5")

    }

    void testLikeDislikeReview() {

        controller.reviewsService.metaClass.likeReview = { long comentarioId->
            new Like()
        }
        controller.reviewsService.metaClass.dislikeReview = { long comentarioId ->
            new Like()
        }
        request.contentType = "text/json"
        request.JSON = updatedReview
        request.method = 'PUT'
        controller.like()
        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_OK

        request.method = 'PUT'
        controller.dislike()
        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_OK
    }

    void testLikeDislikeReviewWithoutReview() {
        def noComment = [id: null]
        request.JSON = noComment

        controller.reviewsService.metaClass.likeReview = { Long comentarioId -> 
            try{
                throw new ValidationException("no comment specified", noComment)
            } catch (Exception e) {
                throw new RuntimeException("Failed!!", e)
            }
        }
        controller.reviewsService.metaClass.dislikeReview = { Long comentarioId -> 
            try{
                throw new ValidationException("no comment specified", noComment)
            } catch (Exception e) {
                throw new RuntimeException("Failed!!", e)
            }
        }
        request.method = 'PUT'
        controller.like()

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.text.length() > 0

        request.method = 'PUT'
        controller.dislike()

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.text.length() > 0
    }
    
    void testUpdateReviewOK() {

        verifySendMailOnce({

            request.contentType = "text/json"
            
            request.method = 'PUT'
            controller.update()

            assert response.contentType.contains("text/json")
            assert response.status == HttpServletResponse.SC_OK
    		assert response.text.contains(comment.title)
    		assert response.text.contains("5")
        },updatedReview)
    }


    void testUpdateReviewWithIllegalArgumentException() {
        controller.reviewsService.metaClass.editReview = { JSONObject review, String domain ->
            throw new RuntimeException("Error runtime",new IllegalArgumentException("Erro cause"))
        }

        request.contentType = "text/json"
        request.JSON = updatedReview
        request.method = 'PUT'
        controller.update()

        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
        assert response.text.contains("Erro cause")        
    }

    void testUpdateReviewJSONInvalid() {

        request.contentType = "text/json"
        request.JSON = [ rating: 3, title: null, text: "comment.text", application: application]
        request.method = 'PUT'
        controller.update()

        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
       
    }

    void testUpdateReviewWithException() {
        controller.reviewsService.metaClass.editReview = { JSONObject review, String domain ->
            throw new Exception("Erro cause")
        }

        request.contentType = "text/json"
        request.JSON = updatedReview
        request.method = 'PUT'
        controller.update()

        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR
        assert response.text.contains("Erro cause")      

    }

    void testUpdateReviewWithInvalidTitle() {
        propertyTest("title", invalidStringDomain, {
            request.method = 'PUT'
            controller.update()}, updatedReview)
    }

    void testUpdateReviewWithInvalidText() {
        propertyTest("text", invalidStringDomain, {
            request.method = 'PUT'
            controller.update()}, updatedReview)
    }

    void testUpdateReviewWithInvalidId() {
        propertyTest("id", invalidIntegerDomain, {
            request.method = 'PUT'
            controller.update()}, updatedReview)
    }

    void testUpdateReviewWithInvalidRating() {
        propertyTest("rating", invalidIntegerDomain, {
            request.method = 'PUT'
            controller.update()}, updatedReview)
        propertyTest("rating", invalidRatingDomain, {
            request.method = 'PUT'
            controller.update()}, updatedReview)
    }

    void testDeleteReviewOK() {
        controller.reviewsService.metaClass.deleteReview = { Long reviewId ->
            "ok"
        }

        request.contentType = "text/json"
        params["id"] = "${comment.id}"
        request.method = 'DELETE'
        controller.delete()

        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_OK
        
    }

    void testDeleteReviewInvalidParam() {

        request.contentType = "text/json"
        params["id"] = "" 
        request.method = 'DELETE'
        controller.delete()

        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
       
    }

    void testMyReviewOK() {

        request.contentType = "text/json"
        params["applicationId"] = "" + application.id + ""
        controller.my()

        assert response.contentType.contains("text/json")
        assert response.text.contains("2")
        assert response.text.contains(comment.id.toString())
        assert response.status == HttpServletResponse.SC_OK
        
    }


    void testMyReviewNotLogged() {
        def mockcontrol = mockFor(ShiroService)
        mockcontrol.demand.loggedUser(1..1000) { -> null }

        controller.shiroService = mockcontrol.createMock()
        controller.my()
       
        assert response.status == HttpServletResponse.SC_FORBIDDEN
        
    }

    void testMyReviewInvalidParam() {
        controller.reviewsService.metaClass.myReview = { Application application ->
            [comment: comment, rating: 3] 
        }
        request.contentType = "text/json"
       
        invalidAppplication({controller.my()}, HttpServletResponse.SC_BAD_REQUEST)
           
    }

    void testMyReviewEmpty() {
        Comment.metaClass.'static'.findByApplicationAndUser = {application, User user ->
            null
        }

        request.contentType = "text/json"
        params["applicationId"] = "" + application.id + ""
        controller.my()

        assert response.status == HttpServletResponse.SC_NO_CONTENT        
    }

    void testMetadata() {

        controller.metadata()

        assert response.status == HttpServletResponse.SC_OK
    }

    private void verifySendMailOnce(Closure closure,review){

        request.JSON = review

        def mockControl = mockFor(ReviewMailService)
        mockControl.demand.sendMailNewReview(1) { Comment comment, String host ->
            println "sending email"
        }

        controller.reviewsService.reviewMailService = mockControl.createMock();
        closure()
        mockControl.verify()       
    }

    private void verifyDontSendMailWithoutComment(Closure closure,review){

        review.title = null
        review.text = null

        request.JSON = review

        def mockControl = mockFor(ReviewMailService)
        mockControl.demand.sendMailNewReview(0) { }

        controller.reviewsService.reviewMailService = mockControl.createMock();
        closure()
        mockControl.verify()       
    } 

    void testDontSendMailWhenNewReviewWithoutComment(){
        verifyDontSendMailWithoutComment({
            request.method = 'POST'
            controller.insert()
        },newReview);
    }

    void testSendMailOnceWhenUpdateReview(){
        verifySendMailOnce({
            request.method = 'PUT'
            controller.update()
        },updatedReview);
    }


    void testFillReportWithZeroValues(){
        def reportByDay = [:]
        controller.fillReportWithZeroValues("13/07/2014","16/07/2014",reportByDay)

        assert reportByDay["12/07/2014"] == null

        assert reportByDay["13/07/2014"].count == 0
        assert reportByDay["13/07/2014"].reviews.size == 0

        assert reportByDay["14/07/2014"].count == 0
        assert reportByDay["14/07/2014"].reviews.size == 0

        assert reportByDay["15/07/2014"].count == 0
        assert reportByDay["15/07/2014"].reviews.size == 0

        assert reportByDay["16/07/2014"].count == 0
        assert reportByDay["16/07/2014"].reviews.size == 0

        assert reportByDay["17/07/2014"] == null
        
    }

    void testRenderReviewsAsCSVEmptyAdmins(){

        def reviews = [1,2,3,4,5].collect{
            [ rating: it, title: "Comment title", text: "comment.text", application: application, user: user]
        }
        
        

        AdminApplication.metaClass.'static'.findAllByApplication = { Application application -> [] }

        controller.renderReviewsAsCSV(response,request,reviews)

        assert response.status == 200
        assert response.header("Content-Type") == "application/vnd.ms-excel:ISO-8859-1"
        assert response.header("Content-Disposition") == "attachment;filename=avaliacoes.csv"

        AdminApplication.metaClass.'static'.findAllByApplication = { Application application -> [new AdminApplication(admin:user,application:application)] }

        def mock = new MockFor(CSVWriter)
        mock.demand.writeNext(6) { String[] a -> assert a[a.length-1].contains(user.key) || a[a.length-1] == "admins"}
        mock.demand.flush{}
        mock.demand.close{}

        mock.use {   
            controller.renderReviewsAsCSV(response,request,reviews)
        }
        assert response.status == 200
        assert response.header("Content-Type") == "application/vnd.ms-excel:ISO-8859-1"
        assert response.header("Content-Disposition") == "attachment;filename=avaliacoes.csv"

    }

}