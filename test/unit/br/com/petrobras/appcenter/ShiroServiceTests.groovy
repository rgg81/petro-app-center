package br.com.petrobras.appcenter

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

import org.apache.shiro.SecurityUtils

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(ShiroService)
@Mock([User])
class ShiroServiceTests {

    void setUp() {
        // Setup logic here
    }

    void tearDown() {
        // Tear down logic here
    }

    void testIsAuthenticated() {
        def subject = null 

        SecurityUtils.metaClass.'static'.getSubject = { subject } 

		assert (!service.isAuthenticated())

        subject = new Expando()

		subject.isAuthenticated = { -> false }
        assert (!service.isAuthenticated())

		subject.isAuthenticated = { -> true }
        assert (service.isAuthenticated())

    }

    void testLoggedUser() {
        service.metaClass.getUserInfo = { null }
		assert service.loggedUser() == null

		new User(key:'CATATAU').save(flush:true, failOnError:true)		

		def user
        ["catatau", "CATATAU"].each() { login ->
	        service.metaClass.getUserInfo = { new br.com.petrobras.security.model.User(login) }

	        user = service.loggedUser()
	        assert user != null 
	    	assert user instanceof User
	        assert user.key == "CATATAU" 
        }
    }

    void testGetUserInfo() {
        def subject = null 

        SecurityUtils.metaClass.'static'.getSubject = { subject } 

		assert service.userInfo == null

        def principals = new Expando()
        principals.oneByType = { Class c -> new br.com.petrobras.security.model.User("catatau") }

        subject = new Expando()
        subject.metaClass.getPrincipals = { principals }

        assert service.userInfo != null 
        assert service.userInfo instanceof br.com.petrobras.security.model.User
    }

    void testIsModerator() {
        def subject = null 

        SecurityUtils.metaClass.'static'.getSubject = { subject } 

		assert (!service.isModerator())

        subject = new Expando()

        subject.hasRole = { roleId -> false }
        assert (!service.isModerator())

        subject.hasRole = { roleId -> true }
        assert (service.isModerator())

    }

}