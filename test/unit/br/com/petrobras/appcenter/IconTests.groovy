package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Icon)
@Mock([Application, ApplicationDraft])
class IconTests {
    def app

    void setUp() {
      app = new Application(name:"Petro App Center", 
                    description: "Loja de aplicacoes",
                    link:new Link(),
                    icon:new Icon(),
                    currentVersion: new Version(), catalogCode:"XCVG")
     // app.save(flush:true)
    }

    void testeValidIcon() {
    	def icon = new Icon(width:100, application:new Application(), height:100, size:50000000)
     	assert icon.validate()
    }

    void testNullApplication() {
       def icon = new Icon(width:100, height:100, size:50000000)
       assert !icon.validate()
    }

    void testRelativePath() {
      def icon = new Icon(width:100, application:app, height:100, size:50000000)
      assert icon.relativePath() == "icons/icon_${app.id}.jpg"
    }
	
	void testRelativePathByApplication() {
		def ver = new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
			
		def app = new Application(name:"Petrobras Aplicação. Avalie, comente e favorite as aplicações mais utilizadas por você, desse modo você estará dando feedback e ajudando o ",	
			 	description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
			 	link:new Link(url:"http://www.google.com.br/q=23s"),
			 	icon:new Icon(size:1), currentVersion: ver, catalogCode:"XCVZ")
		app.save(flush:true)
		assert Icon.relativePathByApplication(app) == "icons/icon_${app.id}.jpg?1"
	}

    void testsysAbsolutePath() {

      def icon = new Icon(width:100, application:app, height:100, size:50000000)

      icon.grailsApplication = new Expando()
      icon.grailsApplication.config = [imageDir: '/testpath']

      assert icon.sysAbsolutePath() == "/testpath/icons/icon_${app.id}.jpg"
    }
}

