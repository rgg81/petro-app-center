package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UrlController)
@Mock([Application,Link,ApplicationDraft,BaseApplication])
class UrlControllerTests {

	void setUp() {
//		Link.metaClass.static.findByUrl = { String param ->
//			if(param == "http://www.google.com")
//				new Link()
//			else
//				null
//		}
		
		def app = new Application(name:"App",
			description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
			link:new Link(url:"http://www.google.com.br/"),
			icon:new Icon(size:1), currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode:"XPYO")
		app.save()
	}
    void testNotUniqueUrl() {
      
	   controller.request.JSON.val = 'http://www.zzzz.com' 
	   controller.check()
	   assert response.json == [isUnique:true]

    }
	
	void testPassByValidationUrl() {
		
		def app = new Application(name:"App",
			description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
			link:new Link(url:"http://192.168.56.101:8080/petro-app-center/accessUrl"),
			icon:new Icon(size:1), currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode:"XPYO")
		app.save()
		
		 controller.request.JSON.val = 'http://192.168.56.101:8080/petro-app-center/accessUrl'
		 controller.check()
		 assert response.json == [isUnique:true]
  
	  }
	
	void testUniqueUrl() {
		
		 controller.request.JSON.val = 'http://www.google.com.br/'
		 controller.check()
		 assert response.json == [isUnique:false]
  
	  }
	
}
