package br.com.petrobras.appcenter

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import static plastic.criteria.PlasticCriteria.* // mockCriteria() method
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(ReviewsService)
class ReviewServiceSpec extends Specification {

	static ahost = "localhost:8080"

	def review = Mock(Comment) 
	def catatau = Mock(User)
	def reply = Mock(Reply)

    def setup() {
    }

    def cleanup() {
    }

    void "addReview: review or host null"() {
		setup: 
		def e

		when: 
		service.addReview(null, null)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.addReview(null, ahost)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.addReview(review, null)

		then: 
		1* review.shouldSendMailNewReview() >> true
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
    } 

	void "addReview: sending mail or not"() {
		setup: 
		service.reviewMailService = Mock(ReviewMailService)

		when: 
		service.addReview(review, ahost)

		then: 
		2* review.shouldSendMailNewReview() >> true
		1* service.reviewMailService.sendMailNewReview(review, ahost)
		1* review.save() >> null

		when: 
		service.addReview(review, null)

		then: 
		2* review.shouldSendMailNewReview() >> false
		0* service.reviewMailService.sendMailNewReview(review, ahost)
		1* review.save() >> null
	}

	void "likeReview: must call evaluateReview with LikeEnum.Like.value as argument"() {
		setup: 
		def _reviewId = 0
		def _value = 0
		def count = 0
		service.metaClass.evaluateReview = { long reviewId, Integer value -> 
			count++
			_reviewId = reviewId
			_value = value
		}

		when: 
		service.likeReview(1)

		then: 
		count == 1
		_reviewId == 1
		_value == LikeEnum.Like.value
	}

	void "dislikeReview: must call evaluateReview with LikeEnum.Dislike.value as argument"() {
		setup: 
		def _reviewId = 0
		def _value = 0
		def count = 0
		service.metaClass.evaluateReview = { long reviewId, Integer value -> 
			count++
			_reviewId = reviewId
			_value = value
		}

		when: 
		service.dislikeReview(1)

		then: 
		count == 1
		_reviewId == 1
		_value == LikeEnum.Dislike.value
	}

	void "evaluateReview: review must exist"() {
		setup: 
		GroovyMock(Comment, global: true)
		def reviewId = 1
		def e 

		when: 
		service.evaluateReview(reviewId, LikeEnum.Like.value)

		then: 
		service.shiroService = Mock(ShiroService){
			1* loggedUser()
		}
		1* Comment.get(reviewId) >> null
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "evaluateReview: update or save a like"() {
		setup: 
		GroovyMock(Comment, global: true)
		GroovyMock(Like, global: true)		
		def reviewId = 1
		def value
		service.shiroService = Mock(ShiroService){
			2* loggedUser() >> catatau
		}
		2* Comment.get(reviewId) >> review
		def like = Mock(Like)

		when: 
		value = 1
		service.evaluateReview(reviewId, value)

		then: 
		1* Like.findOrCreateByUserAndComment(catatau, review) >> like
		1* like.setProperty('value', 1) 
		1* like.save() 

		when: 
		value = -1
		service.evaluateReview(reviewId, value)

		then: 
		1* Like.findOrCreateByUserAndComment(catatau, review) >> like
		1* like.setProperty('value', -1) 
		1* like.save() 
	}

	void "editReview: null reviewMap or null reviewMap.id"() {
		setup: 
		def e

		when: 
		service.editReview(null, null)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.editReview([id: null], null)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "editReview: got an id for the review but none was found!?"() {
		setup: 
		def e
		GroovyMock(Comment, global: true)

		when: 
		service.editReview([id: 1], null)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> null 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "editReview: should send email but host is null"() {
		setup: 
		def e
		GroovyMock(Comment, global: true)

		when: 
		service.editReview([id: 1], null)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review] 
		1* review.shouldSendMailNewReview() >> true
		0* review.reply >> null
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.editReview([id: 1], null)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review] 
		1* review.shouldSendMailNewReview() >> false
		1* review.reply >> reply
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "editReview: there is no logged user OR it is not his review"() {
		setup: 
		def e
		GroovyMock(Comment, global: true)		
		service.shiroService = Mock(ShiroService)

		when: 
		service.editReview([id: 1], ahost)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review] 
		1* review.shouldSendMailNewReview() >> true
		0* review.reply >> null
		1* review.user >> catatau
		1* service.shiroService.loggedUser() >> null
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.editReview([id: 1], ahost)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review] 
		1* review.shouldSendMailNewReview() >> false
		1* review.reply >> reply
		1* review.user >> catatau
		1* service.shiroService.loggedUser() >> Mock(User)
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "editReview: should email someone, if a reply exists or if review says so"() {
		setup: 
		def e
		4* review.user >> catatau
		GroovyMock(Comment, global: true)
		service.shiroService = Mock(ShiroService)
		4* service.shiroService.loggedUser() >> catatau
		service.replyMailService = Mock(ReplyMailService)
		service.reviewMailService = Mock(ReviewMailService)
		review.evaluations >> Mock(java.util.Set)
		4* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review] 
		4* review.evaluations.clear()
		4* review.save([flush: true])

		when: 		
		service.editReview([id: 1], ahost)

		then: 
		2* review.shouldSendMailNewReview() >> false
		0* service.replyMailService.sendMailUpdateReviewWithReply(review, ahost)
		0* service.reviewMailService.sendMailNewReview(review, ahost)

		when: 		
		service.editReview([id: 1], ahost)

		then: 
		2* review.shouldSendMailNewReview() >> false
		2* review.reply >> reply
		1* service.replyMailService.sendMailUpdateReviewWithReply(review, ahost)
		0* service.reviewMailService.sendMailNewReview(review, ahost)

		when: 
		service.editReview([id: 1], ahost)

		then: 
		2* review.shouldSendMailNewReview() >> true
		0* service.replyMailService.sendMailUpdateReviewWithReply(review, ahost)
		1* service.reviewMailService.sendMailNewReview(review, ahost)

		when: 		
		service.editReview([id: 1], ahost)

		then: 
		2* review.shouldSendMailNewReview() >> true
		1* review.reply >> reply
		1* service.replyMailService.sendMailUpdateReviewWithReply(review, ahost)
		1* service.reviewMailService.sendMailNewReview(review, ahost)
	}

	void "deleteReview: reviewId and review must be not null"() {
		setup: 
		def e
		def reviewId = 1
		GroovyMock(Comment, global: true)

		when: 
		service.deleteReview(null, null)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.deleteReview(reviewId, null)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> null
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "deleteReview: there must be a host, if review was replied"() {
		setup: 
		def e
		def reviewId = 1
		GroovyMock(Comment, global: true)

		when: 
		service.deleteReview(reviewId, null)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		1* review.reply >> reply
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}
	
	void "deleteReview: only the reviewer may delete his piece"() {
		setup: 
		def e
		def reviewId = 1
		GroovyMock(Comment, global: true)

		when: 
		service.deleteReview(reviewId, ahost)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		1* review.reply >> reply
		1* review.user >> catatau
		service.shiroService = Mock(ShiroService) {
			1* loggedUser() >> Mock(User)
		}
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "deleteReview: should email someone, if a reply exists"() {
		setup: 
		def reviewId = 1
		GroovyMock(Comment, global: true)
		service.replyMailService = Mock(ReplyMailService)

		when: 
		service.deleteReview(reviewId, ahost)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		2* review.reply >> null
		1* review.user >> catatau
		service.shiroService = Mock(ShiroService) {
			1* loggedUser() >> catatau
		}
		0* service.replyMailService.sendMailDeleteReviewWithReply(review, ahost) 
		1* review.delete()

		when: 
		service.deleteReview(reviewId, ahost)

		then: 
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		2* review.reply >> reply
		1* review.user >> catatau
		service.shiroService = Mock(ShiroService) {
			1* loggedUser() >> catatau
		}
		1* service.replyMailService.sendMailDeleteReviewWithReply(review, ahost) 
		1* review.delete()
	}

	void "moderateReview: reviewId not be null, user must be moderator, retrieved review must not be null and there must be a host if exists a reply"() {
		setup: 
		def e
		def reviewId = 1
		GroovyMock(Comment, global: true)

		when: 
		service.moderateReview(null, null)

		then: 
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.moderateReview(reviewId, null)

		then: 
		service.shiroService = Mock(ShiroService){
			1* isModerator() >> false
		}
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.moderateReview(reviewId, null)

		then: 
		service.shiroService = Mock(ShiroService){
			1* isModerator() >> true
		}
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> null
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException

		when: 
		service.moderateReview(reviewId, null)

		then: 
		service.shiroService = Mock(ShiroService){
			1* isModerator() >> true
		}
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		1* review.reply >> reply
		e = thrown(RuntimeException)
		e.cause.class == IllegalArgumentException
	}

	void "moderateReview: should email someone, if a reply exists"() {
		setup: 
		def reviewId = 1
		GroovyMock(Comment, global: true)
		service.replyMailService = Mock(ReplyMailService)
		service.reviewMailService = Mock(ReviewMailService)

		when: 
		service.moderateReview(reviewId, ahost)

		then: 
		service.shiroService = Mock(ShiroService){
			1* isModerator() >> true
		}
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		2* review.reply >> reply
		1* service.reviewMailService.sendMailDeleteReview(review, ahost)
		1* service.replyMailService.sendMailDeleteReviewWithReply(review, ahost)
		1* review.delete()

		when: 
		service.moderateReview(reviewId, ahost)

		then: 
		service.shiroService = Mock(ShiroService){
			1* isModerator() >> true
		}
		1* Comment.findAllById(1, [fetch: [application: "eager", user: "eager", reply: "eager"]]) >> [review]
		2* review.reply >> null
		1* service.reviewMailService.sendMailDeleteReview(review, ahost)
		0* service.replyMailService.sendMailDeleteReviewWithReply(review, ahost)
		1* review.delete()
	}

	@IgnoreRest
	@Unroll
	void "listReviewsFromDayToDay: if one of #arguments is null an #exception must be thrown"(arguments, exception) {
		setup: 
		def e = getException(service.&listReviewsFromDayToDay, *arguments)

		expect:
		exception == e?.class

		where: 
		arguments | exception
		[null, null] | java.lang.IllegalArgumentException
		['a', null] | java.lang.IllegalArgumentException
		[null, 'b'] | java.lang.IllegalArgumentException		
	}

	Exception getException(closure, ...args){
		try{
			closure.call(args)
			return null
		} catch(any) {
			return any
		}
	}

}
