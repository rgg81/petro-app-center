package br.com.petrobras.appcenter

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.junit.Test


/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(SearchAppController)
@Mock([Application,BaseApplication,ShiroService])
class SearchAppControllerTests extends GsonTestsLegacy {

	@Test
	void 'Search application empty parameters'() {
		params.q = ''
		def ret = controller.index()
		assert ret == [:]
	}
	
	@Test
	void 'Search application no highlight'() {
		def tag1 = new Expando()
		tag1.metaClass.getName = {"tag1"}

		def tag2 = new Expando()
		tag2.metaClass.getName = {"tag2"}
	      
		Application.metaClass.static.search = { Closure q, Map opts ->
		   	return [
		   		total:1,
		   		searchResults:[
		   						[	
		   							name:"app1", 
		   							description:"app1 description",
		   							tags:[tag1,tag2]
		   						]
		   		], 
		   		highlight:[]
		   	]
	   }
	   params.q = 'app1'
	   def ret = controller.index()
	   assert ret.result.total == 1
	   assert ret.result.searchResults.size() == 1
	   assert ret.result.searchResults[0].name == "app1"
	   assert ret.result.searchResults[0].description == "app1 description"
	   assert ret.result.searchResults[0].tags[0].name == "tag1"
	   assert ret.result.searchResults[0].tags[1].name == "tag2"
	   assert ret.result.searchResults[0].tags[0].key == "tag1"
	   assert ret.result.searchResults[0].tags[1].key == "tag2"

	}
	
	@Test
	void 'Search application highlight'() {
		def tag1 = new Expando()
		tag1.metaClass.getName = {"tag1"}

		def tag2 = new Expando()
		tag2.metaClass.getName = {"tag2"}

		def htag2 = new Expando()
		htag2.metaClass.string = {"<b>tag2</b>"}

		Application.metaClass.static.search = { Closure q, Map opts ->
		   	return [
		   		total:1,
		   		searchResults:[
		   						[	
		   							name:"app1",
		   							description:"app1 description",
		   							tags:[tag1,tag2]
		   						],

		   		],
		   		highlight:[
		   					[	
		   						name:[fragments:["happ1"]],
		   						description:[fragments:["happ1 description"]],
		   						tagsBusca:[fragments:[htag2]]
		   					]
		   		]
		   	]
		}

		params.q = 'app1'
		def ret = controller.index()
		assert ret.result.searchResults[0].name == "happ1"
		assert ret.result.searchResults[0].description == "happ1 description"
		assert ret.result.searchResults[0].tags[0].name == "tag1"
		assert ret.result.searchResults[0].tags[1].name == "<b>tag2</b>"
		assert ret.result.searchResults[0].tags[0].key == "tag1"
		assert ret.result.searchResults[0].tags[1].key == "tag2"
	}

	@Test
	void 'Search application description truncate 300 chars'() {

		def outOfLimitchars = ""

		for(i in 1..400){
			outOfLimitchars += "c"
		}

		Application.metaClass.static.search = { Closure q, Map opts ->
		   return [total:1,searchResults:[[name:"app1", description:outOfLimitchars]],highlight:[]]
		}
	   
		params.q = 'app1'
		def ret = controller.index()
		assert ret.result.searchResults[0].description.size() == 300 
	}

	@Test
	void 'Autocomplete application empty parameters'() {
		params.q = ''
		def ret = controller.autocomplete()
		assert ret == [:]
	}

	@Test
	void 'Autocomplete application with max of 5 results avoiding duplication'() {

		Icon.metaClass.static.relativePathByApplication = { app ->
			return "path${app.id}"
		} 

		def options = []

		for(i in 1..4){

			def text = new Expando()
			def text_s = "app${i}"
			text.metaClass.string = { text_s}

			def s = new Expando()
			s.metaClass.getText = {text}

			def pl = new Expando()
			def appid = i 
			pl.metaClass.getAppid = {appid}

			def icon_s = "path${i}"
			pl.metaClass.getIconPath = {icon_s}

			s.metaClass.getPayloadAsMap = { pl }

			options.push(s)
		}

		Application.metaClass.static.search = { Closure q, Map opts ->
		   	return [
		   		suggest:[[[ options:options ]]],
		   		searchResults:[
		   						[name:"app2",id: 2L],
		   						[name:"app6",id: 6L],
		   						[name:"app7",id: 7L],
		   						[name:"app8",id: 8L],
		   		],
		   		highlight:[
		   					[name:[fragments:[methodString("happ2")]]],
		   					[name:[fragments:[methodString("happ6")]]],
		   					[name:[fragments:[methodString("happ7")]]],
		   					[name:[fragments:[methodString("happ8")]]]
		   		]
		   	]
		}
		params.q = 'app'
		controller.autocomplete()
		assert response.GSON.size() == 5
		
		assert response.GSON.get(0).value.getAsInt() == 1
		assert response.GSON.get(1).value.getAsInt() == 2
		assert response.GSON.get(2).value.getAsInt() == 3
		assert response.GSON.get(3).value.getAsInt() == 4
		assert response.GSON.get(4).value.getAsInt() == 6

		assert response.GSON.get(0).name.getAsString() == "app1"
		assert response.GSON.get(1).name.getAsString() == "app2"
		assert response.GSON.get(2).name.getAsString() == "app3"
		assert response.GSON.get(3).name.getAsString() == "app4"
		assert response.GSON.get(4).name.getAsString() == "app6"
		assert response.GSON.get(4).highlight.getAsString() == "happ6"

	}

	private Expando methodString(String s){
		def e = new Expando()
		e.metaClass.string = {s}
		return e
	}

}
