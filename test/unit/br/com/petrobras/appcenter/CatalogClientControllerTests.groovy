package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CatalogClientController)
@Mock([User])
class CatalogClientControllerTests {

    void testFetch() {
        def user = new User(key:'uq4n').save(flush: true, failOnError:true)
        def mockcontrol = mockFor(ShiroService)
        mockcontrol.demand.loggedUser(0..1000){ -> user }

        controller.shiroService = mockcontrol.createMock()

    	//BaseController.metaClass.secure = { Closure closure -> closure() } 

    	params.key = "fred"

    	controller.catalogClientService = new Expando()	
		controller.catalogClientService.metaClass.fetch = { String appCode -> [cry: "Diiiiiino!!!!"] }

        controller.fetch()

	    assert response.text.length() > 0
        assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.startsWith("text/json")
    }
}
