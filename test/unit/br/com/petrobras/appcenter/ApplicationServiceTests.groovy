package br.com.petrobras.appcenter



import grails.test.mixin.*
import grails.test.mixin.support.GrailsUnitTestMixin

import org.junit.*
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.springframework.web.multipart.MultipartFile
import org.springframework.mock.web.MockMultipartFile

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */

@TestFor(ApplicationService)
@Mock([Application,Icon,User,UserApplication,Link, Tag,AdminApplication,BaseApplication,ApplicationCompatibilities,ApplicationDraft, Report])
class ApplicationServiceTests {

	def tagNames

	void setUp() {

		service.metaClass.transferFile = { File file, String toPath ->
			println "transferFile .... "
		}
		Icon.metaClass.sysAbsolutePath = {
			return "/testpath"
		}
		tagNames = ['médico','medicina']
		Application.metaClass.unindex = {
			return true;
		}

		service.accessControlService = new Expando()
		service.accessControlService.metaClass.authorizedUsers = { id -> [] }
		service.accessControlService.metaClass.updateRoleAndGrants = { application -> }
		service.accessControlService.metaClass.deleteAutorizationInformationValueInApplication = { application -> }
	}

    void testSaveApplication() {
		
		Application app = new Application(
        	'name' : 'app1',
	        'description' : 'app1 description',
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon('url' : 'http://www.google.com'), catalogCode:"XPTO")

		def file = new File('test/img-app-ok.jpg')

		app.adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]

		service.saveApplication(app,file,null,null,null,null)

		assert Application.list().size == 1

    }
	
	void testDeleteApplication() {
		Application app = new Application(
			'name' : 'app1',
			'description' : 'app1 description',
			'link' : new Link('url' : 'http://www.google.com'),
			'icon' : new Icon('url' : 'http://www.google.com'), catalogCode:"XPZO")

		def file = new File('test/img-app-ok.jpg')

		app.adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]

		service.saveApplication(app,file,null, null,null,null)
		service.deleteApplication(app)
		
		assert Application.count() == 0
		
		
	}

    void testNullFileApplicationRollback() {
		
		Application app = new Application(
        	'name' : 'app1',
	        'description' : 'app1 description',
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon('url' : 'http://www.google.com'), catalogCode:"XPAO")

		def file = null
		try {			
			service.saveApplication(app,file,null,null,null,null)
		}catch(e) {
			assert e.class == RuntimeException
		}

		assert Application.list().size() == 0
    }

     void testInvalidApplicationRollback() {
		
		Application app = new Application(
        	'name' : null,
	        'description' : null,
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon('url' : 'http://www.google.com'), catalogCode:"APTO")

		def file = new File('test/img-app-ok.jpg')
		try{
			service.saveApplication(app,file,null,null,null,null)	
		} catch (Exception e){
			assert e.class == RuntimeException
		}
		assert Application.list().size() == 0
    }

    void testFailedToSaveFileRollback() {
		
		Application app = new Application(
        	'name' : "app1",
	        'description' : "app1 desc",
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon('url' : 'http://www.google.com'),
			catalogCode:"XVTO")
    	
		service.metaClass.transferFile = { File file, String toPath ->
			throw new FileNotFoundException() 
		}

		def file = new File('test/img-app-ok.jpg')
		try{
			service.saveApplication(app,file,null,null,null,null)
		} catch (Exception e){
			assert e.class == RuntimeException
		}
		// Esse assert abaixo nao funciona mas o rollback funciona corretamente no tomcat
		//@Transactional nao funciona no no unit test TODO: verificar integration test
		//assert Application.list().size() == 0
    }

    void testUniqueDepartments(){
    	def apps = []
    	def user = new User(key:'uq4n').save(flush:true)
    	createApps(10,user)
    	def deps = service.listDistinctDepartments()

    	assert deps.size() == 5    
    }

    private createApps(total,user){

    	def apps = []
    	for(i in 1..total){
    		def app = new Application(
        	name : "app${i}",
	        description : "app${i} desc",
	        link : new Link('url' : 'http://www.google.com'),
	        icon : new Icon('url' : 'http://www.google.com'),
			catalogCode:"XVT${i%total}",
			department:"TIC/COM${i%5}")

			def file = new File('test/img-app-ok.jpg')
			app.adminsApp = [new AdminApplication(admin:user , application:app)]

			apps.push(service.saveApplication(app,file,null, null,null,null))
    	}
    	return apps
    }

    void testSearchByDepartment(){
    	def user = new User(key:'uq4n').save(flush:true)
		user.metaClass.getName = {"NOME"}

    	def mockSearchResult = new Expando()
    	mockSearchResult.metaClass.getSearchResults = { createApps(9,user) }

    	Application.metaClass.'static'.search = { Closure query, java.util.LinkedHashMap params ->
    		assert params.from == 3
    		assert params.size == 9
    		mockSearchResult
    	}
    	
    	def result = service.searchByDepartment('TIC',[from:3,size:9]); 
    	assert result.apps*.admins.unique()[0] == 'NOME - uq4n'
    	assert result.apps[0].name ==  "app1"
    }

	void testAsCSVStructureNoTags(){
		def user = new User(key:'uq4n').save(flush:true)
		user.metaClass.getName = {"NOME"}
		def apps = createApps(10,user) 
		
		//bug GRAILS-11855
		Application.metaClass.getLastUpdated =  {-> new Date()}

		def csv = service.asCSVStructure(apps,new java.util.Locale("pt", "BR"))

		assert csv[0] == ["data ultima modificacao","nome","descrição","url","tags", "administradores", "informacoes de suporte"]
		assert csv[1][1] == "app1"
		assert csv[2][1] == "app2"
		assert csv[10][1] == "app10"
    	
    }

    
}
