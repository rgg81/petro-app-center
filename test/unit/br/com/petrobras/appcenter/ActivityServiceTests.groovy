package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ActivityService)
@Mock([User,Application,ApplicationDraft,UserActivity])
class ActivityServiceTests {

    def app
    def user
    

    void setUp() {

        user = new User(key:"uq4n")
        user.save(failOnError:true,flush:true)

        app = new Application(
            'name' : 'app1',
            'description' : 'app1 description',
            'link' : new Link('url' : 'http://www.google.com'),
            'icon' : new Icon('url' : 'http://www.google.com'),currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()), catalogCode:"RRRR")


        app.adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]

        app.save(failOnError:true,flush:true)

    }


    void testRegisterAccessAppActivity() {
    
    	def activity = service.registerActivity(user,app,UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE, false)
    	
    	assert UserActivity.count() == 1
        assert activity.userkey == user.key
        assert activity.userId == user.id
        assert activity.itemId == app.id
        assert activity.itemName == app.name
        assert activity.context == UserActivity.MYAPPS_WHERE
        assert activity.ratingValue == 0
        assert activity.type == UserActivity.ACCESS_APPLICATION
        
    }

    void testRegisterRatingAppActivity() {
    
        def activity = service.registerRatingActivity(user,app,UserActivity.RATING_APPLICATION,4,UserActivity.MYAPPS_WHERE,false)
        
        assert UserActivity.count() == 1
        assert activity.userkey == user.key
        assert activity.userId == user.id
        assert activity.itemId == app.id
        assert activity.itemName == app.name
        assert activity.context == UserActivity.MYAPPS_WHERE
        assert activity.ratingValue == 4
        assert activity.type == UserActivity.RATING_APPLICATION
        
    }


    void testRegisterActivityWithNullUser() {
    
        def activity = service.registerActivity(null,app,UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE, false)
        
        assert UserActivity.count() == 1
        assert activity.userkey == "anonymous"
        assert activity.userId == 0
        assert activity.itemId == app.id
        assert activity.itemName == app.name
        assert activity.context == UserActivity.MYAPPS_WHERE
        assert activity.ratingValue == 0
        assert activity.type == UserActivity.ACCESS_APPLICATION
        
    }


    void testRegisterRatingAppActivityWithNullUser() {
        shouldFail{
            def activity = service.registerRatingActivity(null,app,UserActivity.RATING_APPLICATION,4,UserActivity.MYAPPS_WHERE,false)
        }
    }


    void testShouldFailWithInvalidActivityType() {
        try{
            def activity = service.registerActivity(user,app, null,null,false)
        }catch(Throwable e ){
            println e
            assert e.cause.class == grails.validation.ValidationException
        }
    }
}
