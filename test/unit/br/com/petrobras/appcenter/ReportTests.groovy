package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Report)
class ReportTests {

    void test() {
		def pir8 = new Application(name:"Petro App Center", 
							  description: "Loja de aplicacoes",
							  link:new Link(url:"http://www.gloco.com.br"),
							  icon:new Icon(size:1L),
							  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XCVG");

    	def dino = new User(key: 'dino')

    	def report, issues

		/*
		** casos felizes
		*/

		issues = [new Issue(description: 'pre-historic', position: 100), new Issue(description: 'dinossaurs', position: 200)]
		report = new Report(application: pir8, user: dino, description: 'description', issues: issues)
		assert report.validate()

		report = new Report(application: pir8, user: dino, description: 'description')
		assert report.validate()

		report = new Report(application: pir8, user: dino, description: 'description', issues: [])
		assert report.validate()

		issues = [new Issue(description: 'pre-historic', position: 300)]
		report = new Report(application: pir8, user: dino, issues: issues)
		assert report.validate()

		/*
		** casos tristes
		*/

		report = new Report(user: dino, description: 'description', issues: [])
		assert !report.validate()
		assert report.errors.getFieldError('application').getCode() == 'nullable'

		report = new Report(application: pir8, description: 'description', issues: [])
		assert !report.validate()
		assert report.errors.getFieldError('user').getCode() == 'nullable'

		report = new Report(application: pir8, user: dino)
		assert !report.validate()
		assert report.errors.getFieldError('description').getCode() == 'report.must.have.description.or.issue'
		assert report.errors.getFieldError('issues').getCode() == 'report.must.have.description.or.issue'

		report = new Report(application: pir8, user: dino, issues: [])
		assert !report.validate()
		assert report.errors.getFieldError('description').getCode() == 'report.must.have.description.or.issue'
		assert report.errors.getFieldError('issues').getCode() == 'report.must.have.description.or.issue'

    }

}
