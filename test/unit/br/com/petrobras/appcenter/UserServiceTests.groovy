package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserService)
@Mock([Application,BaseApplication,Icon,User,UserApplication,Link,ApplicationDraft])
class UserServiceTests {

	def user

	void setUp() {
		user = new User(key: 'savedUser')
		user.save()
	}

    void testSuccessAddUserApplication(){
   	
    	def app = new Application(
        	'name' : "app1",
	        'description' : "app1 desc",
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon(size:1), 
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ1")
		
		app.save()

		service.addApplication(app, user)

		assert app.userApplications?.size() == 1
		assert User.count() == 1
		assert Application.count() == 1

    }

    void testSuccessAddTwoUserApplication(){
    	def app1 = new Application(
        	'name' : "app1",
	        'description' : "app1 desc",
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon(size:1),
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ2")

		app1.save()

    	def app2 =  new Application(
        	'name' : "app2",
	        'description' : "app2 desc",
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon(size:1),
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ3")

		app2.save()


		service.addApplication(app1, user)
		service.addApplication(app2, user)

		assert app1.userApplications?.size() == 1
		assert app2.userApplications?.size() == 1
		assert user.myApplications?.size() == 2

    }
	
	void testUpdateUserApplicationPosition(){
		def app1 = new Application(
			'name' : "app1",
			'description' : "app1 desc",
			'link' : new Link('url' : 'http://www.google.com'),
			'icon' : new Icon(size:1),
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ2")

		app1.save()

		def app2 =  new Application(
			'name' : "app2",
			'description' : "app2 desc",
			'link' : new Link('url' : 'http://www.google.com'),
			'icon' : new Icon(size:1),
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ3")

		app2.save()


		service.addApplication(app1, user)
		service.addApplication(app2, user)
		
		assert UserApplication.findByApplicationAndUser(app1,user).position == 0
		assert UserApplication.findByApplicationAndUser(app2,user).position == 1
		
		service.updateUserApplicationPosition(UserApplication.findByApplicationAndUser(app1,user), 1)
		service.updateUserApplicationPosition(UserApplication.findByApplicationAndUser(app2,user), 0)
		
		assert UserApplication.findByApplicationAndUser(app1,user).position == 1
		assert UserApplication.findByApplicationAndUser(app2,user).position == 0
		
	}

    void testSuccessRemoveUserApplication(){
    	def app = new Application(
        	'name' : "app1",
	        'description' : "app1 desc",
	        'link' : new Link('url' : 'http://www.google.com'),
	        'icon' : new Icon(size:1),
			currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),catalogCode: "VVZ4")

		app.save()

		service.addApplication(app, user)
		
		assert app.userApplications?.size() == 1
		assert User.count() == 1
		assert Application.count() == 1
		assert UserApplication.count() == 1

		service.removeApplication(app, user)


		println "userApps: ${app.userApplications} | application : ${app.userApplications['application']} | user : ${app.userApplications['user']}"

		assert UserApplication.count() == 0
    }

    void testSaveNewUser() {
    	def fredKey = "Fred"

    	def fred = User.findByKey(fredKey)
    	assert fred == null

    	service.saveNewUser(fredKey)
    	fred = User.findByKey(fredKey)
    	assert fred != null

    	service.saveNewUser(fred.key)
    }

    void testShareApplication() {
    	service.shareUserApplications(user)
    	assert user.sharesMyApplications == true
    }

    void testUnshareApplication() {
    	service.unshareUserApplications(user)
    	assert user.sharesMyApplications == false
    }
}
