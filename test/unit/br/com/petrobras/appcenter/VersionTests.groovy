package br.com.petrobras.appcenter



import java.util.Date;

import grails.test.mixin.*

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Version)
@Mock(Application)
class VersionTests {

   def app
	
	void setUp() {
	  app = new Application(name:"Petro App Center",
					description: "Loja de aplicacoes",
					link:new Link(),
					icon:new Icon()
					)
	//  app.save(flush:true)
	}
	
   void testeValidVersion() {
	   def version = new Version(number:"1", application:app, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: new Date())
     	assert version.validate()
    }

    void testInvalidNumberOfVersion() {
       def version = new Version(number:"", application:app, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: new Date())
       assert !version.validate()
    }
	
	void testInvalidDescription() {
		def version = new Version(number:"1", application:app, description:"",date: new Date())
		assert version.validate()
	 }
	
	void testInvalidDate() {
		def version = new Version(number:"1", application:app, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: "")
		assert !version.validate()
	 }
	
    void testNullNumberOfVersion() {
       def version = new Version(number:null, application:app, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: new Date())
       assert !version.validate()
    }
	
	void testNullDate() {
		def version = new Version(number:"1", application:app, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: null)
		assert !version.validate()
	 }

	void testNullDescription() {
		def version = new Version(number:"1", application:app, description:null,date: new Date())
		assert version.validate()
	 }
	
    void testNullApplication() {
       def version = new Version(number:"1", application:null, description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: new Date())
       assert !version.validate()
    }
	
	
}
