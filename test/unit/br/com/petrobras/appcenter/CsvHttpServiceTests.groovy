package br.com.petrobras.appcenter
import java.io.ByteArrayOutputStream
import javax.servlet.http.HttpServletResponse


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(CsvHttpService)
class CsvHttpServiceTests {

    void testRender() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream()
        def response =  new Expando()

        response.metaClass.setHeader = {String a, String b -> 
        	if(a == 'Content-Type'){ assert b == "application/vnd.ms-excel:ISO-8859-1"}
        	if(b == 'Content-Disposition'){ assert b == "Content-Disposition","attachment;filename=arquivo.csv"}
        }

        response.metaClass.getOutputStream = { stream }

        def csv = [['h1','h2','h3'],['a1','a2','a3'],['b1','b2','b3']]
        service.render(response, csv, "arquivo")

        def r = new String( stream.toByteArray(), "UTF-8" )
        assert  r.contains('sep=,') 
        assert  r.contains('"h1","h2","h3"') 
        assert  r.contains('"a1","a2","a3"') 
        assert  r.contains('"b1","b2","b3"')
    }
}
