package br.com.petrobras.appcenter


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Compatibility)
@Mock([Application, ApplicationDraft])
class CompatibilityTests {
	
	def app

    void setUp() {
      app = new Application(name:"Petro App Center", 
                    description: "Loja de aplicacoes",
                    link:new Link(),
                    icon:new Icon(),
                    currentVersion:new Version(), catalogCode:"XCVG")
      app.save(flush:true)
    }
	
	 void testeValidCompatibility() {
		 def compatibilidade = new Compatibility(nameImage:"internetExplorer.png", description:"Internet Explorer 8.0")
		 compatibilidade.save(flush:true)
	     assert compatibilidade.validate()
		 
    }

	void testConstraints() {
		def nullCompatibilidade = new Compatibility( nameImage: null, description:null)
		assert !nullCompatibilidade.validate()
		assert "nullable" == nullCompatibilidade.errors["nameImage"].code
		assert "nullable" == nullCompatibilidade.errors["description"].code
		
		def compatibilidade = new Compatibility(nameImage:"internetExplorer.png", description:"Internet Explorer 8.0")
		
		mockForConstraintsTests(Compatibility, [compatibilidade])
		
		def newCompatibilidade = new Compatibility(nameImage:"internetExplorer.png", description:"Internet Explorer 11.0")
		
		assert !newCompatibilidade.validate()
		assert "unique" == newCompatibilidade.errors["nameImage"]
		
	 }
	
}
