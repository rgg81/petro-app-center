package br.com.petrobras.appcenter

import grails.test.mixin.*

import javax.servlet.http.HttpServletRequest;

import org.junit.*
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse



/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ImageUploadController)
class ImageUploadControllerTests {

    void setUp(){
		controller.metaClass.createTemporaryFile = { String filename->
			new File('./test.jpg')
		}
	}
	
	void testUploadInvalidImageWithWrongDimension() {
		params["qqfile"] = "test-img.jpg"
		params["guid"] = "abslkd-alksdl-blksl"
		
		controller.metaClass.selectInputStream = { HttpServletRequest req ->
			def file = new File("test/img-app-notok.jpg")
			def inputStream = new FileInputStream(file)
			(InputStream)inputStream
		}
		
		controller.upload()
		assert response.json == [message:"Dimensão da imagem está diferente (119x119)", success:false]
		
    }
	
	void testUploadInvalidImageWithWrongExtension() {

		params["qqfile"] = "test-img.bmp"
		params["guid"] = "abslkd-alksdl-blksl"
		
		controller.metaClass.selectInputStream = { HttpServletRequest req ->
			def file = new File("test/img-app-notok.jpg")
			def inputStream = new FileInputStream(file)
			(InputStream)inputStream
		}
		
		controller.upload()
		assert response.json == [message:"Extensão não permitida", success:false]
		
	}
	
	void testUploadValidImage() {
		
		params["qqfile"] = "test-img.jpg"
		params["guid"] = "abslkd-alksdl-blksl"
		
		controller.metaClass.selectInputStream = { HttpServletRequest req ->
			def file = new File("test/img-app-ok.jpg")
			def inputStream = new FileInputStream(file)
			(InputStream)inputStream
		}
		
		controller.upload()
		assert response.json == [success:true]
		
	}

	void tearDown() {
		new File('./test.jpg').delete()
	}
}
