package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ApplicationCompatibilities)
@Mock([Application, Compatibility, ApplicationDraft])
class ApplicationCompatibilitiesTests {

    void testConstraints() {
		def app = new Application(name:"Petro App Center", 
	    							  description: "Loja de aplicacoes",
	    							  link:new Link(url:"http://www.gloco.com.br"),
	    							  icon:new Icon(size:1L),
									  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()),catalogCode:"XPTO")
		app.save()
		

		def compat = new Compatibility(nameImage: "G", description: "Google")
		compat.save()

		def uniqueAppCompat = new ApplicationCompatibilities(application: app, compatibility: compat)
		def nullableAppCompat = new ApplicationCompatibilities(application: null, compatibility: null)

		mockForConstraintsTests(ApplicationCompatibilities, [uniqueAppCompat, nullableAppCompat])

		def redundantAppCompat = new ApplicationCompatibilities(application: app, compatibility: compat)

		// unique
		assert !redundantAppCompat.validate()
		assert "unique" == redundantAppCompat.errors["compatibility"]

		// nullable
		assert !nullableAppCompat.validate()
		assert "nullable" == nullableAppCompat.errors["application"]
		assert "nullable" == nullableAppCompat.errors["compatibility"]

    }
}
