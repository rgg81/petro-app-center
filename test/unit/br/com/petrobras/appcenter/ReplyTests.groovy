package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*
import grails.test.mixin.web.ControllerUnitTestMixin

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
 //bug GRAILS-11136
@TestMixin(ControllerUnitTestMixin)
@TestFor(Reply)
@Mock([User, Reply, Comment])
class ReplyTests {
	def user, app, comment

	void setUp() {
		user = new User(key: "uq4e")
		app = new Application(name:"Petro App Center", 
								  description: "Loja de aplicacoes",
								  link:new Link(),
								  icon:new Icon(), catalogCode:"XCVG")
		comment = new Comment(title: "Comentario title", text: "Comentario text", user: user, application: app)
	}

	void testValidReply(){
		def text = ""
		for(i in 1..1000){
			text += "a"
		}
		def reply = new Reply(user:user,comment:comment, text:text)
		assert reply.validate()
	}

    void testEmptyText() {
		def reply = new Reply(text: "",user:user,comment:comment)
		assert !reply.validate()
		assert "nullable" == reply.errors["text"].code
	}

	void testRepplyWithNoComment(){
		def reply = new Reply(user:user,text:"Replyyyyyy")
		assert !reply.validate()
	}

	void testReplyWithNoUser(){
		def reply = new Reply(comment:comment, text:"Replyyyyyy")
		assert !reply.validate()
	}

	void testReplyWithOverlimitText(){
		def text = ""
		for(i in 1..1001){
			text += "a"
		}
		def reply = new Reply(user:user,comment:comment, text:text)
		assert !reply.validate()
		assert "maxSize.exceeded" == reply.errors["text"].code
	}
}
