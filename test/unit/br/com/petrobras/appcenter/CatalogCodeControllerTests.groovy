package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(CatalogCodeController)
@Mock([BaseApplication,ApplicationDraft,Application])
class CatalogCodeControllerTests {

    void testUnique() {
		def app = new Application(name:"Petro App Center", 
	    							  description: "Loja de aplicacoes",
	    							  link:new Link(url:"http://www.gloco.com.br"),
	    							  icon:new Icon(size:1L),
									  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XCVG").save();
       controller.request.JSON.val = 'popb' 
	   controller.check()
	   assert response.json == [isUnique:true]
    }
	
	
	void testNotUnique() {
		def app = new Application(name:"Petro App Center",
									  description: "Loja de aplicacoes",
									  link:new Link(url:"http://www.gloco.com.br"),
									  icon:new Icon(size:1L),
									  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XCVG").save();
	   controller.request.JSON.val = 'XCVG'
	   controller.check()
	   assert response.json == [isUnique:false]
	}
}
