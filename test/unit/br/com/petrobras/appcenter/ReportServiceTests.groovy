package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import grails.validation.ValidationException

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ReportService)
@Mock([Report])
class ReportServiceTests {

    void test() {
		def pir8 = new Application(name:"Petro App Center", 
							  description: "Loja de aplicacoes",
							  link:new Link(url:"http://www.gloco.com.br"),
							  icon:new Icon(size:1L),
							  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), catalogCode:"XCVG");

    	def dino = new User(key: 'dino')

    	def report, issues

		/*
		** casos felizes
		*/

		issues = [new Issue(description: 'pre-historic', position: 100), new Issue(description: 'dinossaurs', position: 200)]
		report = new Report(application: pir8, user: dino, description: 'description', issues: issues)

		service.save(report)
		assert Report.get(report.id) != null


		report = new Report(application: pir8, user: dino, description: 'description')
		service.save(report)
		assert Report.get(report.id) != null

		report = new Report(application: pir8, user: dino, description: 'description', issues: [])
		service.save(report)
		assert Report.get(report.id) != null

		issues = [new Issue(description: 'pre-historic', position: 300)]
		report = new Report(application: pir8, user: dino, issues: issues)
		service.save(report)
		assert Report.get(report.id) != null


		/*
		** casos tristes
		*/

		report = new Report(user: dino, description: 'description', issues: [])
    	shouldFail(ValidationException){ service.save(report) }

		report = new Report(application: pir8, description: 'description', issues: [])
		shouldFail(ValidationException){ service.save(report) }

		report = new Report(application: pir8, user: dino)
		shouldFail(ValidationException){ service.save(report) }

		report = new Report(application: pir8, user: dino, issues: [])
		shouldFail(ValidationException){ service.save(report) }

    }
}
