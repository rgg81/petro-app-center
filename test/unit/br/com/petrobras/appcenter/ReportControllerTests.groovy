package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse
import grails.converters.*
import grails.plugin.gson.converters.GSON
import grails.plugin.mail.MailService;
import com.google.gson.GsonBuilder
import com.google.gson.JsonPrimitive
import com.google.gson.JsonObject
import com.google.gson.JsonArray

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ReportController)
@Mock([Report, Issue, User, Application, ApplicationDraft, MailService, AdminApplication])
class ReportControllerTests {

	def yogibear, booboobear, pir8, fewpicnicbaskets, huntseason, lowqualitysandwiches, report

    @Before
    void setUp() {
    	yogibear = new User(key:'yogi').save(flush: true, failOnError:true)
		booboobear = new User(key:'boob').save(flush:true, failOnError:true)

        controller.shiroService = new Expando()
        controller.shiroService.metaClass.loggedUser = { -> yogibear }

        User.metaClass.getName = { "Yogi Bear" }
        User.metaClass.getDepartment = { [acronym: "US/Yellostone"] }

		pir8 = new Application(name:"Petro App Center", 
							  description: "Loja de aplicacoes",
							  link:new Link(url:"http://www.gloco.com.br"),
							  icon:new Icon(size:1L),
							  currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), 
							  catalogCode:"XCVG").save(flush: true, failOnError:true)

		fewpicnicbaskets = new Issue(description: 'not enough picnic baskets!', position: 100).save(flush: true)
		huntseason = new Issue(description: 'bear hunt season!!', position: 200).save(flush: true)
		lowqualitysandwiches = new Issue(description: 'low quality sandwiches!!', position: 300).save(flush: true)

		report = new Report(application: pir8, 
							user: yogibear, 
							description: 'More carbs!!!', 
							issues: [fewpicnicbaskets, huntseason, lowqualitysandwiches]).save(flush: true, failOnError:true)

		new AdminApplication(admin: booboobear, application: pir8).save(flush:true, failOnError:true)
    }

    void testListIssues() {
    	controller.listIssues()

    	def issues = JSON.parse(response.text)

    	assert issues[0].id == fewpicnicbaskets.id
    	assert issues[1].id == huntseason.id
    	assert issues[2].id == lowqualitysandwiches.id

    	assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.contains("text/json")
    }

    void testSave() {
		def issues = new JsonArray()

		def fewpicnicbaskets = new JsonObject()
		fewpicnicbaskets.addProperty("description", "not enough picnic baskets!")
		fewpicnicbaskets.add("position", new JsonPrimitive(100))
		issues.add(fewpicnicbaskets)

		def huntseason = new JsonObject()
		huntseason.addProperty("description", "bear hunt season!!")
		huntseason.add("position", new JsonPrimitive(200))
		issues.add(huntseason)

		def lowqualitysandwiches = new JsonObject()
		lowqualitysandwiches.addProperty("description", "low quality sandwiches!!")
		lowqualitysandwiches.add("position", new JsonPrimitive(300))
		issues.add(lowqualitysandwiches)

		def req = new JsonObject()
		req.add("applicationId", new JsonPrimitive(pir8.id))
		req.add("description", new JsonPrimitive("More carbs!!!"))
		req.add("issues", issues)

    	request.GSON = req
        request.method = 'POST'

		controller.reportService = [save: {}]
		controller.gsonBuilder = new GsonBuilder()
		controller.metaClass.sendMailNewReport = {}
    	controller.save()

    	assert response.status == HttpServletResponse.SC_CREATED 
    }

    //Teste inútil enquanto nenhuma exception for lançada
	void testSendMailNewReport() {
    	controller.mailService = new Expando()
    	controller.mailService.sendMail = { c -> c() }
    	controller.metaClass.async = { async -> 
    		assert async == true
    	}
    	controller.metaClass.to = { to ->
    		assert to == ['boob@petrobras.com.br', 'yogi@petrobras.com.br']
    	}
    	controller.metaClass.subject = { subject -> 
    		assert subject == "[Aplicações Petrobras] - Notificação de erros de informação"
    	}
    	controller.metaClass.html = { map -> 
    		assert map.view == "/mail/mailShell"
    		assert map.model.template == "/mail/newReport"
    		assert map.model.serverNameWithPort == "${request.serverName}:${request.serverPort}"
    	}

    	controller.sendMailNewReport(report)


    	// Deve falhar silenciosamente mesmo que a mensagem não seja enviada
    	controller.mailService = new Expando()
    	controller.mailService.sendMail = { c -> throw new Exception('Falhe silenciosamente!') }
    	controller.sendMailNewReport(report)
	}

	void testList() {
		assert controller.list() == []
		assert response.text == ""
    	assert response.status == HttpServletResponse.SC_OK 
	}

	void testAllEmpty() {
		controller.reportService = [ listReportsAndIssues: { [] } ]

		controller.all() 
		
    	assert JSON.parse(response.text) == []
    	assert response.status == HttpServletResponse.SC_OK 
    	assert response.contentType.contains("text/json")
	}

	void testAll() {
        // bug GRAILS-11855
        Report.metaClass.getDateCreated = { -> new Date()}

		controller.reportService = [ listReportsAndIssues: { Report.findAll() } ]

		controller.all() 
		
    	def list = JSON.parse(response.text)

        assert response.status == HttpServletResponse.SC_OK 
        assert response.contentType.contains("text/json")
    	assert list.size() == 1
    	assert list[0].issues.size() == 3
	}
}
