package br.com.petrobras.appcenter

import static org.junit.Assert.*
import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

import biz.petrobras.services.ti.aplicacao._1.AplicacaoPT
import biz.petrobras.services.ti.aplicacao._1.AplicacaoSoapService
import biz.petrobras.schemata.ti.aplicacao._1.Geral
import biz.petrobras.schemata.ti.aplicacao._1.ServiceData
import biz.petrobras.schemata.ti.aplicacao._1.AplicacaoResponse
import biz.petrobras.schemata.ti.aplicacao._1.LiderProjeto
import biz.petrobras.schemata.common.status._1.SyncReturnType
import javax.xml.ws.soap.SOAPFaultException
import javax.xml.soap.SOAPFault
import javax.xml.ws.BindingProvider

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(CatalogClientService)
@TestMixin(GrailsUnitTestMixin)
class CatalogClientServiceTests {

    final shouldFailWithCause = new GroovyTestCase().&shouldFailWithCause

    void setUp() {}

    void tearDown() {
		def remove = GroovySystem.metaClassRegistry.&removeMetaClass
        remove AplicacaoSoapService
        remove BindingProvider
    }

	void testFetch() {
		service.metaClass.getCatalogoAplicacaoSoapPort = { 
			def liderProjeto = [ getChave: { "fred" } ] as LiderProjeto

			def palavras = [ getPalavraChave: { [ "rocky", "bedrock" ] } ] as biz.petrobras.schemata.ti.aplicacao._1.Aplicacao.Palavras

			def geral = [ getNome: { "flintstones" }, getDescricao: { "bedrock rulez!" }, getCentroDeAgilidade: { "TIC/HATERS-BR" },getLiderProjeto: { liderProjeto }, getSituacao: { "Produção" } ] as Geral

			def aplicacao = [ getGeral: { -> geral }, getPalavras: { -> palavras } ] as biz.petrobras.schemata.ti.aplicacao._1.Aplicacao

			def serviceData = [ getAplicacao: { [aplicacao] } ] as ServiceData

			def response = [ getServiceData: { -> serviceData } ] as AplicacaoResponse

			def aplicacaoSoapPort = new Expando(getRequestContext: { [:] }) as BindingProvider
			aplicacaoSoapPort.metaClass.buscaAplicacaoPorCodigo = { [ response ] }
			aplicacaoSoapPort 
		}

		def applicationDataFromCatalog = service.fetch("dino")

		assert applicationDataFromCatalog.name == "flintstones"
		assert applicationDataFromCatalog.description == "bedrock rulez!"
		assert applicationDataFromCatalog.department == "TIC/HATERS-BR"
		assert applicationDataFromCatalog.tags == [ "rocky", "bedrock" ]
		assert applicationDataFromCatalog.admins == ["fred"]
	}

	void testFetchFailsWithInvalidApplicationCode() {
		service.metaClass.getCatalogoAplicacaoSoapPort = { 
			def serviceStatus = [ getCode: { 601 as java.math.BigInteger } ] as SyncReturnType

			def response = [ getServiceStatus: { serviceStatus } ] as AplicacaoResponse

			def aplicacaoSoapPort = new Expando(getRequestContext: { [:] }) as BindingProvider
			aplicacaoSoapPort.metaClass.buscaAplicacaoPorCodigo = { [ response ] }
			aplicacaoSoapPort 
		}

		shouldFail(IllegalArgumentException) { service.fetch("dino") }		
	}

	void testFetchFails() {
		service.metaClass.getCatalogoAplicacaoSoapPort = { 
			def aplicacaoSoapPort = new Expando(getRequestContext: { [:] }) as BindingProvider
			aplicacaoSoapPort.metaClass.buscaAplicacaoPorCodigo = { throw new Exception() }
			aplicacaoSoapPort 
		}

		shouldFail(RuntimeException) { service.fetch("dino") }		
	}

}
