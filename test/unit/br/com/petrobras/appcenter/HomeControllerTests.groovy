package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(HomeController)
@Mock([User, ApplicationService, Application])
class HomeControllerTests {


	void testMyApplicationsNotLogged() {

		controller.shiroService = new Expando()
		controller.shiroService.metaClass.isAuthenticated = { -> return false	}

		def result = controller.index()

		assert !result?.containsKey('apps')
	}

    void testMyApplicationsLogged() {

		def user = new User(key: 'savedUser')
		user.save()

		controller.shiroService = new Expando()
        controller.shiroService.metaClass.isAuthenticated = { -> return true	}
        controller.shiroService.metaClass.loggedUser = { -> User.findByKey('savedUser')}

		def result = controller.index()

		assert result?.containsKey('apps')
    }
	
}
