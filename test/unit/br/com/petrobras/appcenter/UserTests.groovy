package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(User)
@Mock([Application, UserApplication, User])
class UserTests {

    void testValidUser() {
       def user = new User(key:"uq4e")
	   
	   assert user.validate()
    }
	
	void testInvalidUserKey() {
		assert ! (new User().validate());
	}
	
	void testUniqueUser() {
		def user1 = new User(key:"uq4e")
		def user2 = new User(key:"uq4e")
		
		user1.save(flush:true)
		shouldFail{
			user2.save(flush:true)
		}
 
	}
}
