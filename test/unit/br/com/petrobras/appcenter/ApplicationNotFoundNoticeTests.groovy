package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import grails.test.mixin.web.ControllerUnitTestMixin

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
 //bug GRAILS-11136
@TestMixin(ControllerUnitTestMixin)
@TestFor(ApplicationNotFoundNotice)
class ApplicationNotFoundNoticeTests {

    void test() {
    	def dino = new User(key: 'dino')

    	def notice

		/*
		** casos felizes
		*/
		notice = new ApplicationNotFoundNotice(user: dino, description: 'description', url: 'http://google.com')
		assert notice.validate()

		notice = new ApplicationNotFoundNotice(user: dino, description: 'description', url: '')
		assert notice.validate()

		notice = new ApplicationNotFoundNotice(user: dino, description: 'description')
		assert notice.validate()

		/*
		** casos tristes
		*/

		notice = new ApplicationNotFoundNotice(description: 'description', url: 'http://google.com')
		assert !notice.validate()
		assert notice.errors.getFieldError('user').getCode() == 'nullable'

		notice = new ApplicationNotFoundNotice(user: dino, description: '', url: '')
		assert !notice.validate()
		assert notice.errors.getFieldError('description').getCode() == 'nullable' 

		notice = new ApplicationNotFoundNotice(user: dino, url: '')
		assert !notice.validate()
		assert notice.errors.getFieldError('description').getCode() == 'nullable'

    }

}
