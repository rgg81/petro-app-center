package br.com.petrobras.appcenter



import grails.test.mixin.*
import grails.test.mixin.domain.DomainClassUnitTestMixin;

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TagController)
@TestMixin(DomainClassUnitTestMixin)
class TagControllerTests {

	void setUp() {

	}
	void testFindTagJson() {
		Tag.metaClass.static.findAllByNameLike = {String param ->
			[new Tag(name:"tag1 tag2"), new Tag(name:"tag2 tag3")]			
		}		
		params.name = 'med'
		controller.find()
		assert response.json == [[value:'tag1 tag2', tokens:['tag1', 'tag2']], [value:'tag2 tag3', tokens:['tag2', 'tag3']]]	   
    }
}
