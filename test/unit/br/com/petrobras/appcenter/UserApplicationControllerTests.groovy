package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UserApplicationController)
@Mock([User,UserApplication,Application])
class UserApplicationControllerTests {

    void testUpdateAppPosition() {
		controller.shiroService = new Expando()
		def fred = new User(key:'FRED').save(flush: true, failOnError:true)
		controller.shiroService.metaClass.loggedUser = { -> fred}
		
		request.JSON = "[{\"position\":0,\"applicationId\":15},{\"position\":1,\"applicationId\":12},{\"position\":2,\"applicationId\":14},{\"position\":3,\"applicationId\":18},{\"position\":4,\"applicationId\":7},{\"position\":5,\"applicationId\":13}]"
		controller.updateAppPosition()
		
		assert response.json.success == true
		
    }
}
