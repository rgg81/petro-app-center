package br.com.petrobras.appcenter

import grails.test.mixin.*

import org.junit.*

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import com.google.gson.JsonNull
import com.google.gson.JsonParser
import com.google.gson.GsonBuilder
import grails.plugin.gson.adapters.*
import grails.plugin.gson.api.ArtefactEnhancer
import grails.plugin.gson.spring.GsonBuilderFactory
import grails.plugin.gson.support.proxy.DefaultEntityProxyHandler
import org.codehaus.groovy.grails.web.converters.Converter
import grails.plugin.gson.adapters.GrailsDomainDeserializer
import grails.plugin.gson.adapters.GrailsDomainSerializer
import grails.plugin.gson.converters.GsonParsingParameterCreationListener

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
class GsonTestsLegacy {


	def doWithSpring = {

		proxyHandler DefaultEntityProxyHandler
		domainSerializer GrailsDomainSerializer, ref('grailsApplication'), ref('proxyHandler')
		domainDeserializer GrailsDomainDeserializer, ref('grailsApplication')
		gsonBuilder GsonBuilderFactory
		jsonParsingParameterCreationListener GsonParsingParameterCreationListener, ref('gsonBuilder')

	}

	@Before
	void gsonSetup(){
		def gsonBuilder = applicationContext.getBean('gsonBuilder', GsonBuilder)
		def domainDeserializer = applicationContext.getBean('domainDeserializer', GrailsDomainDeserializer)

		def enhancer = new ArtefactEnhancer(grailsApplication, gsonBuilder, domainDeserializer)
		enhancer.enhanceControllers()
		enhancer.enhanceDomains()
		enhancer.enhanceRequest()

		def parser = new JsonParser()

		HttpServletRequest.metaClass.setGSON = { json ->
			if (json instanceof Converter) {
				json = json.toString()
			}
			if (json instanceof CharSequence) {
				json = parser.parse(json.toString())
			}
			delegate.contentType = 'application/json'
			delegate.content = gsonBuilder.create().toJson(json).getBytes('UTF-8')
		}

		HttpServletResponse.metaClass.getGSON = {->
			parser.parse delegate.contentAsString
		}
	}
}