package br.com.petrobras.appcenter

import grails.test.mixin.*

import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */

@TestFor(GreetedUserTagLib)
class GreetedUserTagLibTests {
  
  def mockControl

	void setUp() {

    mockControl = mockFor(ShiroService)
    
	}

   	void testShowNameUserAuthenticated() {
   		mockControl.demand.loggedUser(1..1) {
   			-> [shortName: "User1 Surname"]
   		}

      tagLib.shiroService = mockControl.createMock()

   		def output = tagLib.showName().toString()
      assert output.trim() != ""
    }

    void testShowNameUserNotAuthenticated() {

    	for (user in [null, []]) {
	   		mockControl.demand.loggedUser(1..1) {
	   			-> user
	   		}
        tagLib.shiroService = mockControl.createMock()

	   		def output = tagLib.showName().toString()

	        assert output.trim() == ""		    		
    	}
    }

}
