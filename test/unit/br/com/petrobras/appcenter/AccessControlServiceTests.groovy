package br.com.petrobras.appcenter

import grails.test.mixin.*
import grails.test.runtime.FreshRuntime
import org.junit.*
import org.springframework.transaction.annotation.Transactional
import br.com.petrobras.security.ISecurityContext
import br.com.petrobras.security.exception.InvalidArgumentException
import br.com.petrobras.security.exception.PermissionDeniedException
import br.com.petrobras.security.exception.ObjectNotFoundException
import br.com.petrobras.security.exception.ConstraintViolationException
import br.com.petrobras.security.exception.InvalidOperationException
import br.com.petrobras.security.management.information.IInformationValueManager
import br.com.petrobras.security.management.authorization.IUserInformationValueAuthorizationManager
import br.com.petrobras.security.model.InformationValueClassification
import br.com.petrobras.security.model.InformationValueClassificationEnum
import br.com.petrobras.security.model.InformationValue
import br.com.petrobras.security.model.Information
import br.com.petrobras.security.model.authorization.information.UserInformationValueAuthorization
import br.com.petrobras.security.management.authorization.IUserRoleAuthorizationManager
import org.apache.shiro.SecurityUtils

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AccessControlService)
@FreshRuntime
@Mock([Application, User, AdminApplication, ApplicationCompatibilities, Compatibility, BaseApplication, ApplicationDraft])
class AccessControlServiceTests {

	def barney, fred, betty, wilma 
	def context, userManager, informationValueManager, userInformationValueAuthorizationManager, userRoleAuthorizationManager, informationValueAuthorizer
	def revokees, grantees
	def grailsApplication

	Application application

    final shouldFailWithCause = new GroovyTestCase().&shouldFailWithCause

	void setUp() {
		new User(key:'barney').save(flush:true, failOnError:true)
		new User(key:'fred').save(flush:true, failOnError:true)
		new User(key:'betty').save(flush:true, failOnError:true)
		new User(key:'wilma').save(flush:true, failOnError:true)

		barney = new br.com.petrobras.security.model.User("barney")
		fred = new br.com.petrobras.security.model.User("fred")
		betty = new br.com.petrobras.security.model.User("betty")
		wilma = new br.com.petrobras.security.model.User("wilma")

		context = new Expando()
		ISecurityContext.metaClass.'static'.getContext = { context }

		grailsApplication = [config:[app:[role:[adminapp: 'AdminApp']]]]
	}

	void setUpFindUser() {
		userManager = new Expando()

		context.metaClass.getUserManager = { -> userManager }
	}

	void setUpUpdateRoleAndGrants() {
        application = new Application(name:"Petro App Center", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon(), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()), catalogCode:"XPTO")
        application.save(flush: true, failOnError:true)
		
		revokees = [barney, fred]
		println "revokees: $revokees"
		grantees = [betty, wilma]
		println "grantees: $grantees"

		grantees.each() {
			println "login: $it"
			println "login: ${it.login}"
			println "id: " + User.findByKey(it.login)
			new AdminApplication(admin: User.findByKey(it.login), application: application).save(flush:true)	
		}

		informationValueManager = new Expando()
		userInformationValueAuthorizationManager = new Expando()
		userInformationValueAuthorizationManager.revoke = new Expando()
		userRoleAuthorizationManager = new Expando()

		context.metaClass.getUserRoleAuthorizationManager = { -> userRoleAuthorizationManager }
		context.metaClass.getInformationValueManager = { -> informationValueManager }
		context.metaClass.getUserInformationValueAuthorizationManager = { -> userInformationValueAuthorizationManager }
	}

	@After
	void tearDown() {
		def remove = GroovySystem.metaClassRegistry.&removeMetaClass
        remove ISecurityContext
	}

	void testFindUserSuccess() {
		setUpFindUser()

		userManager.find = { String login -> 
			assert login == barney.login
			barney 
		}

		def user = service.findUser(barney.login)

		assert user != null
		assert user == barney
	}

	void testFindUserFailUserNotFound() {
		setUpFindUser()
		
		def user

		userManager.find = { String login -> throw new ObjectNotFoundException("Failed!!") }

    	shouldFailWithCause(ObjectNotFoundException) { service.findUser("any login") }
	}

	void testFindUserFailWithException() {
		setUpFindUser()
		
		def user

		userManager.find = { String login -> throw new Exception("Failed!!") }

        shouldFail() { service.findUser("any login") }
	}

	void testUpdateRoleAndGrantsSuccess() {
		setUpUpdateRoleAndGrants()

		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> 
			revokees.collect { 
				new UserInformationValueAuthorization(it, new InformationValue())
			}
		}

		userInformationValueAuthorizationManager.revoke = { userLogin, informationId, informationValueId -> 
			assert userLogin in revokees*.login
			assert informationId == "application"
			assert informationValueId == application.id as String
		}

		userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> 
			assert userLogin in grantees*.login
			assert roleIdz == grailsApplication.config.app.role.adminapp
		}

		userInformationValueAuthorizationManager.grant = { userLogin, informationId, informationValueId -> 
			assert userLogin in grantees*.login
			assert informationId == "application"
			assert informationValueId == application.id as String
		}

		/*
		Caso feliz em que nenhuma exception é lançada. Esse caso é uma alteração, o valor da informação já existe. 
		*/
		service.updateRoleAndGrants(application)

		/*
		Caso em que o valor de informação ainda não existe, criação da aplicação. 
		*/
		informationValueManager.save = { informationValue -> 
			assert informationValue.id == application.id as String
			assert informationValue.enabled
		}
		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue ->  throw new ObjectNotFoundException() }
		service.updateRoleAndGrants(application)

		/*
		**
		Os casos abaixo testam o tratamento de possíveis inconsistências no controle de acesso. Essas podem surgir a partir de erros na aplicação seguidos de rollback. 
		**
		*/

		/*
		Ao tentar conceder autorização a um usuário em um valor de informação, mas o usuário já possui tal privilégio. 
		*/
		userInformationValueAuthorizationManager.grant = { userLogin, informationId, informationValueId -> throw new InvalidOperationException() }
		service.updateRoleAndGrants(application)

		/*
		Ao tentar conceder um papel a usuário que já o possui.  
		*/
		userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> throw new InvalidOperationException() }
		service.updateRoleAndGrants(application)

		/*
		Ao tentar revogar autorização a um usuário em um valor de informação, mas o usuário não possui tal privilégio. 
		*/
		userInformationValueAuthorizationManager.revoke = { userLogin, informationId, informationValueId -> throw new InvalidOperationException() }
		service.updateRoleAndGrants(application)

		/*
		Ao tentar salvar um valor de informação já existente.  
		*/
		informationValueManager.save = { informationValue -> throw new ConstraintViolationException() }
		service.updateRoleAndGrants(application)

	}

	void testUpdateRoleAndGrantsFailureSaveApplication() {
		setUpUpdateRoleAndGrants()

		/*
		Falha ao recuperar autorizações em valor de informação
		*/
		[InvalidArgumentException, PermissionDeniedException, SecurityException].each() {
			userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue ->  throw new ObjectNotFoundException() }

		/*
		Falha ao salvar valor de informação
		*/
		[InvalidArgumentException, InvalidOperationException, ObjectNotFoundException, PermissionDeniedException, SecurityException].each() {
			informationValueManager.save = { informationValue -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		informationValueManager.save = { informationValue -> 
			assert informationValue.id == application.id as String
			assert informationValue.enabled
		}

		/*
		Falha ao autorizar usuário em papel
		*/
		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> 
			assert userLogin in grantees*.login
			assert roleIdz == grailsApplication.config.app.role.adminapp
		}

		/*
		Falha ao conceder autorização a usuário em valor de informação
		*/
		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userInformationValueAuthorizationManager.grant = { String userLogin, String informationId, String informationValueId -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

	}

	void testUpdateRoleAndGrantsFailureUpdateApplication() {
		setUpUpdateRoleAndGrants()
	
		/*
		Falha ao recuperar autorizações em valor de informação
		*/
		[InvalidArgumentException, PermissionDeniedException, SecurityException].each() {
			userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> 	
			revokees.collect { 
				new UserInformationValueAuthorization(it, new InformationValue())
			}
		}

		/*
		Falha ao revogar autorização a um usuário em valor de informação
		*/
		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userInformationValueAuthorizationManager.revoke = { String userLogin, String informationId, String informationValueId -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		userInformationValueAuthorizationManager.revoke = { String userLogin, String informationId, String informationValueId -> 
			assert userLogin in revokees*.login
			assert informationId == "application"
			assert informationValueId == application.id as String
		}

		/*
		Falha ao autorizar usuário em papel
		*/
		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

		userRoleAuthorizationManager.grant = { String userLogin, String roleIdz -> 
			assert userLogin in grantees*.login
			assert roleIdz == grailsApplication.config.app.role.adminapp
		}

		/*
		Falha ao conceder autorização a usuário em valor de informação
		*/
		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userInformationValueAuthorizationManager.grant = { String userLogin, String informationId, String informationValueId -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.updateRoleAndGrants(application) }
		}

	}

//deleteAutorizationInformationValueInApplication

	void setUpDeleteAutorizationInformationValueInApplication() {
		userInformationValueAuthorizationManager = new Expando()
		context.metaClass.getUserInformationValueAuthorizationManager = { -> userInformationValueAuthorizationManager }

		informationValueManager = new Expando()
		context.metaClass.getInformationValueManager = { -> informationValueManager }
	}

	void testDeleteAutorizationInformationValueInApplicationSuccess() {
		setUpDeleteAutorizationInformationValueInApplication()

		def application = [id: 609]

		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> [] }

		informationValueManager.remove = { informationValue -> }

		service.metaClass.revokeAutorizationInformationValue = { List<String> revokees, InformationValue informationValue, manager -> }
		
		service.deleteAutorizationInformationValueInApplication(application) 

		// Outro caso de sucesso: Valor de informação já não existia no momento da exclusão da aplicação
		informationValueManager.remove = { informationValue -> throw new ObjectNotFoundException() }
		service.deleteAutorizationInformationValueInApplication(application) 

		// Outro caso de sucesso: Autorização referente ao Valor de informação já não existia no momento da exclusão da aplicação
		userInformationValueAuthorizationManager.revoke = { String userLogin, String informationId, String informationValueId -> throw new InvalidOperationException() } 
		service.deleteAutorizationInformationValueInApplication(application) 

		// Outro caso de sucesso: Não há usuários autorizados para o valor de informação no momento da exclusão da aplicação
		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> throw new ObjectNotFoundException() }
		service.deleteAutorizationInformationValueInApplication(application) 
	}

	void testDeleteAutorizationInformationValueInApplicationFailure() {
		setUpDeleteAutorizationInformationValueInApplication()

		def application = [id: 609]

		/*
		Falha ao buscar usuário com autorização em valor de informação
		*/
		[InvalidArgumentException, PermissionDeniedException, SecurityException].each() {
			userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> throw it.newInstance() }
			shouldFail(it) { service.deleteAutorizationInformationValueInApplication(application) }
		}
		
		/*
		Falha ao revogar autorizações de usuário no valor de informação
		*/
		userInformationValueAuthorizationManager.findAllWithInformationValue = { informationValue -> [ [user: [login: 'fred']], [user: [login: 'betty']] ] }

		[PermissionDeniedException, ObjectNotFoundException, InvalidArgumentException, SecurityException].each() {
			userInformationValueAuthorizationManager.revoke = { String userLogin, String informationId, String informationValueId -> throw it.newInstance() } 
			shouldFailWithCause(it) { service.deleteAutorizationInformationValueInApplication(application) }
		}

		/*
		Falha ao remover valor de informação
		*/
		//AccessControlService.metaClass.'static'.revokeAutorizationInformationValue = { List<String> revokees, InformationValue informationValue, manager -> }

		userInformationValueAuthorizationManager.revoke = { String userLogin, String informationId, String informationValueId -> } 

		[InvalidArgumentException, ConstraintViolationException, InvalidOperationException, PermissionDeniedException, SecurityException].each() {
			informationValueManager.remove = { informationValue -> throw it.newInstance() }
			shouldFail(it) { service.deleteAutorizationInformationValueInApplication(application) }
		}
		
	}

	void setUpIsAuthorized() {
		informationValueAuthorizer = new Expando()

		context.metaClass.getInformationValueAuthorizer = { -> informationValueAuthorizer }
	}

	void testIsAuthorizedSuccess() {
		setUpIsAuthorized()

		SecurityUtils.metaClass.'static'.hasRole = { roleId -> true } 

		informationValueAuthorizer.isAuthorized = { String informationId, String informationValueId -> true }
		
		assert service.isAuthorized('nth-bowling-match', '100000') == true
	}

	void testIsAuthorizedFailure() {
		setUpIsAuthorized()

		[true, false].each() { input -> 
			SecurityUtils.metaClass.'static'.hasRole = { roleId -> input } 

			[InvalidArgumentException, ObjectNotFoundException, PermissionDeniedException, InvalidOperationException, SecurityException].each() {
				informationValueAuthorizer.isAuthorized = { String informationId, String informationValueId -> throw it.newInstance() }

				shouldFailWithCause(it) { service.isAuthorized('nth-bowling-match', '100000') }
			}
		}
	}

	void setUpGetModeratorsEmails() {
		userRoleAuthorizationManager = new Expando()

		context.metaClass.getUserRoleAuthorizationManager = { -> userRoleAuthorizationManager }
	}

	void testGetModeratorsEmailsSuccess() {
		setUpGetModeratorsEmails()

		userRoleAuthorizationManager.findAllWithRole = { String roleId -> 
			[[user: [email: 'brubble@bedrock']], [user: [email: 'wilma.rubble@bedrock']]]
		}
		
		assert service.getModeratorsEmails() == ['brubble@bedrock', 'wilma.rubble@bedrock']
	}

	void testGetModeratorsEmailsFailure() {
		setUpGetModeratorsEmails()

		[PermissionDeniedException, InvalidArgumentException, ObjectNotFoundException, SecurityException].each() {
			userRoleAuthorizationManager.findAllWithRole = { String roleId -> throw it.newInstance() }
			shouldFailWithCause(it) { service.getModeratorsEmails() }
		}

	}

}