package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import grails.test.mixin.web.ControllerUnitTestMixin
/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Comment)
@Mock([User, Application, Comment])
//bug GRAILS-11136
@TestMixin(ControllerUnitTestMixin)
class CommentTests {

	User user
	Application app
	String title
	String text

	@Before
	void setUp() {
		user = new User(key: "uq4e")
		app = new Application(name:"Petro App Center", 
								  description: "Loja de aplicacoes",
								  link:new Link(),
								  icon:new Icon(), catalogCode:"XCVG")
		title = "Comment title" 
		text = 'a' * 1000
	}

	void testValidComment() {
		def comment = new Comment(title: title, text: text, user: user, application: app, rating: 1)
		assert comment.validate()
		assert !comment.hasErrors()
	}

	void testInvalidUser() {
		def comment = new Comment(title: title, text: text, user: null, application: app)
		assert !comment.validate()
		assert "nullable" == comment.errors["user"].code

		comment = new Comment(title: title, text: text, application: app)
		assert !comment.validate()
		assert "nullable" == comment.errors["user"].code
	}

	void testInvalidApplication() {
		def comment = new Comment(title: title, text: text, user: user, application: null)
		assert !comment.validate()
		assert "nullable" == comment.errors["application"].code

		comment = new Comment(title: title, text: text, user: user)
		assert !comment.validate()
		assert "nullable" == comment.errors["application"].code
	}

	void testInvalidTitleAndText() {
		def comment = new Comment(title: "", text: text, user: user, application: app)
		assert !comment.validate()
		assert "blank" == comment.errors["title"].code

		comment = new Comment(title: title, text: "", user: user, application: app)
		assert !comment.validate()
		assert "blank" == comment.errors["text"].code
	}

	void testInvalidText() {
		def comment = new Comment(title: title, text: "b$text", user: user, application: app)
		assert !comment.validate()
		assert "maxSize.exceeded" == comment.errors["text"].code
	}

}