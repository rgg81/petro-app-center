package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse
import grails.converters.*
import grails.validation.ValidationException
import org.springframework.validation.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(BaseController)
@Mock([User, ApplicationDraft])
class BaseControllerTests {

	def user

    @Before
    void setUp() {
    	user = new User(key:'Zé Colméia').save(flush: true, failOnError:true)

        controller.shiroService = new Expando()

    }

    @After
    void tearDown() {
        User.findByKey('Zé Colméia').delete(flush: true)

        controller.shiroService = null
    }

	void testSecureForbidden() {
        def expectedResponseText = ([message: "Unauthorized access!"] as JSON) as String

        controller.shiroService.metaClass.loggedUser = { -> null}

        controller.secure({"oi"})

        assert response.status == HttpServletResponse.SC_FORBIDDEN 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
	}

    void testTryClosureIllegalArgumentException() {
        def errorMessage = "IllegalArgumentException"
        def expectedResponseText = ([ message:errorMessage, errors:[errorMessage] ] as JSON) as String

        controller.tryClosure({throw new IllegalArgumentException(errorMessage)})

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
    }

    void testTryClosureRuntimeExceptionCausedByIllegalArgumentException() {
        def errorMessage = "IllegalArgumentException"
        def expectedResponseText = ([ message:errorMessage, errors:[errorMessage] ] as JSON) as String

        controller.tryClosure({
            try {
                throw new IllegalArgumentException(errorMessage)
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(errorMessage, e)
            }
        })

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
    }

    void testTryClosureRuntimeExceptionCausedByValidationException() {
        def expectedResponseText

        controller.tryClosure({
            try {
                new User().save(flush:true)
            } catch (ValidationException e) {
                expectedResponseText = e.errors.allErrors.collect {
                    controller.message(error:it, encodeAs:'JavaScript')
                }

                throw new RuntimeException("", e)
            }
        })

        expectedResponseText = ([message: expectedResponseText.join(';\n') + '.', errors: expectedResponseText] as JSON) as String

        assert response.status == HttpServletResponse.SC_BAD_REQUEST 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText 
    }

    //TODO: testar com e sem caused by
    void testTryClosureRuntimeException() {
        def errorMessage = "RuntimeException"
        def expectedResponseText = ([ message:errorMessage ] as JSON) as String

        controller.tryClosure({throw new RuntimeException(errorMessage)})

        assert response.status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
    }

    void testTryClosureRuntimeExceptionCausedByException() {
        def errorMessage = "RuntimeException"
        def causedByErrorMessage = "perpetrator!"
        def expectedResponseText = ([ message:errorMessage ] as JSON) as String

        controller.tryClosure({
            try {
                throw new Exception(causedByErrorMessage)
            } catch(Exception e) {
                throw new RuntimeException(errorMessage, e)
            }
        })

        assert response.status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
    }

    void testTryClosureException() {
        def errorMessage = "Exception"
        def expectedResponseText = ([ message:errorMessage ] as JSON) as String

        controller.tryClosure({throw new RuntimeException(errorMessage)})

        assert response.status == HttpServletResponse.SC_INTERNAL_SERVER_ERROR 
        assert response.contentType.startsWith("text/json")
        assert response.text == expectedResponseText
    }    

}