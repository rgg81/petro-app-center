package br.com.petrobras.appcenter

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@TestFor(Comment)
//@Mock([BaseApplication, User])
class ReviewSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "test for #rating as rating validator should be #success"(rating, success) {
		def comment = new Comment(
			title: "Comment title", 
			text: "Comment text", 
			date: new Date(), 
			application: new Application(), 
			user: new User(), 
			rating: rating) 

        expect: 
        success == comment.validate()

        where: 
        rating	| success
        -1 		| false
         0 		| false
         1 		| true
         2 		| true
         4 		| true
         6 		| false
    }

    void "test shouldSendMailNewReview"(title,text,result) {
        def comment = new Comment(
                title: title,
                text: text,
                date: new Date(),
                application: new Application(),
                user: new User(),
                rating: 2)


        expect:
        result == comment.shouldSendMailNewReview()

        where:
        title	  |  text        | result
        '' 		  |   ''         | false
        'not null'|   ''         | false
        ''        |   'not null' | false
        null      |   null       | false
        null      |   'not null' | false
        'not null'|   null       | false
        'not null'|   'not null' | true

    }
}
