package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Link)
class LinkTests {

    void testeValidLink() {
    	def link = new Link(url:"http://google.com", application:new Application())
     	assert link.validate()
    }

    void testNullURL() {
       def link = new Link(url:null, application:new Application())
       assert !link.validate()
    }

	void testBlankURL() {
		def link = new Link(url:"", application:new Application())
		assert !link.validate()
	 }
	
	
    void testNullApplication() {
       def link = new Link(url:"http://google.com")
       assert !link.validate()
    }
}
