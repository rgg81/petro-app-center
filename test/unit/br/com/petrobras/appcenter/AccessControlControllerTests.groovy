package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import br.com.petrobras.security.ISecurityContext
import br.com.petrobras.security.exception.ObjectNotFoundException
import javax.servlet.http.HttpServletResponse
import grails.converters.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AccessControlController)
@Mock([Application, User, ApplicationDraft])
class AccessControlControllerTests {
	
	Application application
	final shouldFailWithCause = new GroovyTestCase().&shouldFailWithCause

	@Before
	void setUp() {
        application = new Application(name:"Petro App Center", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon(), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()), catalogCode: "XPTO")
        application.save(flush: true, failOnError:true)

		BaseController.metaClass.secure = { Closure closure -> closure() } 

		controller.accessControlService = new Expando()		
	}

	@After
	void tearDown() {
		def remove = GroovySystem.metaClassRegistry.&removeMetaClass
        remove BaseController
	}

    void testFindExistingModelUser() {
    	params.key = "Fred"
		def user1 = new User(key:'uq4e').save(flush: true, failOnError:true)

		def responseText = [login: "upn1"]

		controller.accessControlService.metaClass.findUser = { String key -> responseText }
		User.metaClass.static.findByKey = {key -> 
			user1
		}

        controller.findUser()

        assert response.text.length() > 0
        assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.startsWith("text/json")
    }
	
	void testFindNotExistingModelUser() {
		params.key = "Fred"
		def responseText = [login: "upn1"]

		controller.accessControlService.metaClass.findUser = { String key -> responseText }
		User.metaClass.static.findByKey = {key ->
			null
		}

		controller.findUser()

		assert response.text.length() > 0
		assert response.status == HttpServletResponse.SC_OK
		assert response.contentType.startsWith("text/json")
		assert User.count() == 1
	}

}