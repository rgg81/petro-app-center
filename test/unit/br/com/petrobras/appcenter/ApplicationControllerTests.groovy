
package br.com.petrobras.appcenter

import com.google.gson.JsonNull
import grails.converters.JSON
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.test.runtime.FreshRuntime
import org.springframework.mock.web.MockMultipartHttpServletRequest

import javax.servlet.http.HttpServletResponse

@TestFor(ApplicationController)
@FreshRuntime
@Mock([Application,BaseApplication,UserApplication,ApplicationCompatibilities,AdminApplication,ApplicationDraft,Icon,User, Version, Compatibility,InformationSupport,Link, ShiroService, Report])
class ApplicationControllerTests extends GsonTestsLegacy{
	def fred
	def app, applicationTest
	
    void setUp() {


    	fred = new User(key:'FRED').save(flush: true, failOnError:true)
    	def wilma = new User(key:'wilm').save(flush: true, failOnError:true)

        Icon.metaClass.sysAbsolutePath = {
            return '/testpath'
        }
		
		
		controller.applicationService = new ApplicationService()
		controller.applicationService.accessControlService = new Expando()
		controller.applicationService.accessControlService.metaClass.deleteAutorizationInformationValueInApplication = { application ->
			println "revoking access from controle de acesso"
		}
        
        controller.shiroService = new Expando()
        controller.shiroService.metaClass.loggedUser = { -> fred}
		User.metaClass.getName = { -> "Fred Flinstone"}
        		
		controller.userService = new UserService()

		app = new Application(
			name: "Modespe - Modelo Otimizador do Desembolso com Participações Especiais",
			description: "Apoiar o Tributário e as UOs do E&P na programação trimestral dos desembolsos com participações especiais através da otmização da utilização de créditos. \nSistema de apoio à decisão de pagamento de Participação Especial.",
			link: new Link(url:"http://www.google.com"),
			icon: new Icon(width:119, height:119, size:5191),
			currentVersion: new Version(number:"1", description:"Descrição da versão",date: new Date()), 
			adminsApp: new AdminApplication(admin: wilma, application: app),
			catalogCode:"XPZO"
		).save(flush: true, failOnError:true)

		applicationTest = new Application(name:"Petro App Center",
			description: "Loja de aplicacoes",
			link:new Link(url:"http://www.google.com"),
			icon:new Icon(size:1),
			currentVersion: new Version(number:"1", description:"Primeira versão do sistema de atualizaçãoo de informações pessoais",date: new Date()),
			catalogCode:"YPTO"
			)
		applicationTest.metaClass.unindex = { -> println "unindex app ..."}		
    }

    def populateValidParams(params, controller) {
        assert params != null
		assert request != null
		
        params["name"] = 'app1'
		params["icon"] = "img-app-ok.jpg"
        params["description"] = 'app1 description'
        params["link.url"] = 'http://www.google.com'
		params["icon.size"] = "1"
        params["icon.url"] = 'http://www.google.com/1'
		params["currentVersion"] = "1"
		params["descriptionVersion"] = "descricao da versao 1.0"
		params["dateVersion_day"] ="10"
		params["dateVersion_month"] ="05"
		params["dateVersion_year"] ="2013"
		params["tagNames[]"]=[]
		params["compatibility[]"]=[]
		params["information.url[]"]=[]
		params["information.description[]"]=[]
		
		controller.metaClass.iconPath = { -> "test/"}
		controller.metaClass.request = new MockMultipartHttpServletRequest()
		
		
    }

    void testIndex() {
        controller.index()
        assert "/application/list" == response.redirectedUrl
    }

    void testList() {

		params.max = 10
        def model = controller.list()
		
        assert model.applicationInstanceList.size() <= 10 
        assert model.applicationInstanceTotal == Application.list().size()
    }

    void testCreate() {
        def model = controller.create()
        assert model.applicationInstance != null
    }

	void testPublishNewApplicationOk() {
		request.GSON = "{\"class\":\"br.com.petrobras.appcenter.Application\",\"id\":${JsonNull.INSTANCE},\"name\":\"dadasdasdasd\",\"description\":\"dadasfasfa\",\"department\":\"TIC/HATERS\",\"isDownloadable\":true,\"catalogCode\":\"AAAA\",\"link\":{\"url\":\"http://google.com?q=dasdsda\"},\"icon\":{\"width\":null,\"height\":null,\"size\":null,\"id\":null,\"relativePath\":null,\"name\":\"burger.jpg\"},\"currentVersion\":{\"class\":\"br.com.petrobras.appcenter.Version\",\"id\":null,\"number\":\"1\",\"description\":\"dasdas\",\"date\":\"23/12/2013\"},\"adminsApp\":[{\"admin\":{\"class\":\"br.com.petrobras.appcenter.User\",\"id\":18,\"name\":\"Leandro Da Silva Borges\",\"key\":\"Y1R4\"}}],\"tags\":[],\"informationsSupport\":[],\"compatibilities\":[{\"id\":2,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"}],\"allcompatibilities\":[{\"id\":1,\"nameImage\":\"compatibility_ie.png\",\"description\":\"Internet Explorer\"},{\"id\":2,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"},{\"id\":3,\"nameImage\":\"compatibility_chrome.png\",\"description\":\"Google Chrome\"},{\"id\":4,\"nameImage\":\"compatibility_safari.png\",\"description\":\"Safari\"},{\"id\":5,\"nameImage\":\"compatibility_opera.png\",\"description\":\"Opera\"},{\"id\":6,\"nameImage\":\"compatibility_iphone-web.png\",\"description\":\"Iphone (Web)\"},{\"id\":7,\"nameImage\":\"compatibility_android-web.png\",\"description\":\"Android (Web)\"}]}"

        controller.applicationService.metaClass.saveApplication = { application, File iconFile, tagNames, informations, appDraft,versions ->
			application.icon.width = 199
			application.icon.height = 199
			application.icon.size = 2
            application.save(flush:true, failOnError:true)

            return application
        }
        request.method = 'POST'
		controller.publish()
		
		assert response.status == HttpServletResponse.SC_CREATED
		assert response.contentType.contains("text/json")

        def uri = controller.g.createLink(controller: 'application', action: 'show', id: response.GSON.application.id.asInt, absolute: true)
        assert response.GSON.uri.asString == uri

        def message = controller.message(
        	code: 'default.created.message', 
        	args: [controller.message(code: 'application.label'), request.GSON.name.asString])
        assert response.GSON.message.asString == message

        def gson = controller.gsonBuilder.create()
        def application = gson.fromJson(response.GSON.application, Application.class)
        assert application.department == 'TIC/HATERS'
		assert application.isDownloadable == true
        assert application.class == Application
	}


	void testSaveDraftFirstTime() {
		//request.GSON = "{\"class\":\"br.com.petrobras.appcenter.Application\",id:${JsonNull.INSTANCE},\"name\":\"dadasdasdasd\",\"description\":\"dadasfasfa\",\"department\":\"TIC/HATERS\",\"catalogCode\":\"AAAA\",\"link\":{\"url\":\"http://google.com?q=dasdsda\"},\"icon\":{\"width\":null,\"height\":null,\"size\":null,\"id\":null,\"relativePath\":null,\"name\":\"burger.jpg\"},\"currentVersion\":{\"class\":\"br.com.petrobras.appcenter.Version\",\"id\":null,\"number\":\"1\",\"description\":\"dasdas\",\"date\":\"23/12/2013\"},\"adminsApp\":[{\"admin\":{\"class\":\"br.com.petrobras.appcenter.User\",\"id\":18,\"name\":\"Leandro Da Silva Borges\",\"key\":\"Y1R4\"}}],\"tags\":[],\"informationsSupport\":[],\"compatibilities\":[{\"id\":2,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"}],\"allcompatibilities\":[{\"id\":1,\"nameImage\":\"compatibility_ie.png\",\"description\":\"Internet Explorer\"},{\"id\":2,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"},{\"id\":3,\"nameImage\":\"compatibility_chrome.png\",\"description\":\"Google Chrome\"},{\"id\":4,\"nameImage\":\"compatibility_safari.png\",\"description\":\"Safari\"},{\"id\":5,\"nameImage\":\"compatibility_opera.png\",\"description\":\"Opera\"},{\"id\":6,\"nameImage\":\"compatibility_iphone-web.png\",\"description\":\"Iphone (Web)\"},{\"id\":7,\"nameImage\":\"compatibility_android-web.png\",\"description\":\"Android (Web)\"}]}"
		request.GSON = "{\"class\":\"br.com.petrobras.appcenter.Application\",\"id\":null,\"name\":\"Portal Petrobras\",\"description\":\"Um portal voltado para o público interno que apresente a Companhia como uma empresa integrada de\nenergia. Um ambiente integrado de trabalho, comunicação e relacionamento. \",\"department\":\"TIC/TIC-CORP/TIC-JCAA\",\"isDownloadable\":true,\"catalogCode\":\"POPB\",\"link\":{\"url\":\"http://devopsreactions.tumblr.com/\"},\"icon\":{\"width\":null,\"height\":null,\"size\":null,\"id\":null,\"relativePath\":null,\"name\":\"popb.jpg\"},\"versions\":[{\"number\":\"1.0\",\"date\":\"30/01/2015\"}],\"adminsApp\":[{\"admin\":{\"key\":\"UQ4N\",\"name\":\"Fernando Luiz Valente De Souza\",\"id\":3,\"class\":\"br.com.petrobras.appcenter.User\"}},{\"admin\":{\"class\":\"br.com.petrobras.appcenter.User\",\"id\":18,\"name\":\"Cecilia Felix De Souza\",\"key\":\"Y4YU\"}}],\"tags\":[{\"name\":\"petrobras\"},{\"name\":\"portal\"}],\"informationsSupport\":[],\"compatibilities\":[],\"allcompatibilities\":[{\"id\":7,\"nameImage\":\"compatibility_android-web.png\",\"description\":\"Android\"},{\"id\":3,\"nameImage\":\"compatibility_chrome.png\",\"description\":\"Google Chrome\"},{\"id\":1,\"nameImage\":\"compatibility_ie.png\",\"description\":\"Internet Explorer\"},{\"id\":5,\"nameImage\":\"compatibility_notes.png\",\"description\":\"Lotus Notes\"},{\"id\":2,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"},{\"id\":4,\"nameImage\":\"compatibility_safari.png\",\"description\":\"Safari\"},{\"id\":6,\"nameImage\":\"compatibility_iphone-web.png\",\"description\":\"iOS\"}],\"currentVersion\":{\"number\":\"\",\"date\":\"\",\"description\":\"\"}}"
        controller.applicationService.metaClass.saveApplication = { application, File iconFile, tagNames, informations, appDraft,versions ->
        	assert application.department == 'TIC/TIC-CORP/TIC-JCAA'
			assert application.isDownloadable == true
			application.icon.width = 199
			application.icon.height = 199
			application.icon.size = 2
            application.save(flush:true, failOnError:true)

            return application
        }
		request.method = 'POST'
		controller.save()
		
		assert response.status == HttpServletResponse.SC_CREATED
		assert response.contentType.contains("text/json")

        def uri = controller.g.createLink(controller: 'application', action: 'edit', id: response.GSON.application.id.asInt, absolute: true)
        assert response.GSON.uri.asString == uri

        def message = controller.message(
        	code: 'default.created.message', 
        	args: [controller.message(code: 'application.label'), request.GSON.name.asString])

        assert response.GSON.message.asString == message
        assert request.GSON.department.asString == 'TIC/TIC-CORP/TIC-JCAA'

	}
	
	void testShowFail() {
		controller.show()
		assert flash.message != null
		assert response.redirectedUrl == '/application/list'

	}
		
	
    void testShowOk() {
		//Salva a aplicacao
		applicationTest.save()
		assert applicationTest.id != null
		
		//Recupera e apresenta a aplicacao
        params.id = applicationTest.id
        def model = controller.show()
        assert model.applicationInstance == applicationTest
    }

	void testEditFail() {
		controller.edit()

		assert flash.message != null
		assert response.redirectedUrl == '/application/list'
	}
	
    void testEditOk() {
      
		//Salva a aplicacao
		applicationTest.save()
		assert applicationTest.id != null
		
		
        def model = controller.edit(applicationTest.id)
		def expectedJson = applicationTest as JSON

        assert model.appJson.toString() == (expectedJson.toString())
    }


	void testSaveExistingApplicationOk() {
		request.GSON = "{\"class\":\"br.com.petrobras.appcenter.Application\",\"id\":1,\"name\":\"Novo nome \",\"catalogCode\":\"AAZZ\",\"isDownloadable\":true,\"description\":\"Apoiar o Tributário e as UOs do E&P na programação trimestral dos desembolsos com participações especiais através da otmização da utilização de créditos. \nSistema de apoio à decisão de pagamento de Participação Especial.\",\"link\":{\"class\":\"br.com.petrobras.appcenter.Link\",\"id\":3517,\"application\":{\"class\":\"Application\",\"id\":3514},\"url\":\"http://www.fernanda.com.br/teste\"},\"icon\":{\"width\":119,\"height\":119,\"size\":5191,\"id\":3516,\"relativePath\":\"icons/icon_3514.jpg\"},\"versions\":[{\"class\":\"br.com.petrobras.appcenter.Version\",\"id\":3515,\"number\":\"1\",\"description\":\"Descrição da versão\",\"date\":\"13/12/2013\"}],\"adminsApp\":[{\"class\":\"br.com.petrobras.appcenter.AdminApplication\",\"id\":3521,\"application\":{\"id\":3514},\"version\":0,\"admin\":{\"key\":\"Y1B7\",\"name\":\"Maria Fernanda Goncalves Ferreira\",\"id\":3506,\"class\":\"br.com.petrobras.appcenter.User\"}}],\"tags\":[{\"name\":\"e&p\"},{\"name\":\"participação especial\"},{\"name\":\"tributos\"}],\"informationsSupport\":[],\"compatibilities\":[],\"allcompatibilities\":[{\"id\":3498,\"nameImage\":\"compatibility_ie.png\",\"description\":\"Internet Explorer\"},{\"id\":3499,\"nameImage\":\"compatibility_firefox.png\",\"description\":\"Mozilla Firefox\"},{\"id\":3500,\"nameImage\":\"compatibility_chrome.png\",\"description\":\"Google Chrome\"},{\"id\":3501,\"nameImage\":\"compatibility_safari.png\",\"description\":\"Safari\"},{\"id\":3502,\"nameImage\":\"compatibility_opera.png\",\"description\":\"Opera\"},{\"id\":3503,\"nameImage\":\"compatibility_iphone-web.png\",\"description\":\"Iphone (Web)\"},{\"id\":3504,\"nameImage\":\"compatibility_android-web.png\",\"description\":\"Android (Web)\"}]}"
		log.info "request.GSON.id: ${request.GSON.id.asInt}"

        controller.applicationService.metaClass.saveApplication = { application, File iconFile, tagNames, informations, appDraft, versions ->
            application.save(flush:true, failOnError:true)

            return application
        }

        request.method = 'POST'
		controller.save()
		
		assert response.status == HttpServletResponse.SC_CREATED
		assert response.contentType.contains("text/json")

        def uri = controller.g.createLink(controller: 'application', action: 'edit', id: request.GSON.id.asInt, absolute: true)
        assert response.GSON.uri.asString == uri

        def message = controller.message(
        	code: 'default.created.message', 
        	args: [controller.message(code: 'application.label'), request.GSON.name.asString])
        assert response.GSON.message.asString == message

        def gson = controller.gsonBuilder.create()
        def application = gson.fromJson(response.GSON.application, Application.class)
        assert application.class == Application
	}
	

	void testDeleteFail() {
		request.method = 'POST'
		controller.delete()
		assert response.status == HttpServletResponse.SC_BAD_REQUEST
		assert response.contentType.contains("text/json")
		assert response.json.message == "application not found"
		
		response.reset()
		request.method = 'POST'
		controller.delete(4543543)
		assert response.status == HttpServletResponse.SC_BAD_REQUEST
		assert response.contentType.contains("text/json")
		assert response.json.message == "application not found"

	}
	
    void testDeleteOk() {
		
		//Salva a aplicacao
		applicationTest.save()
		assert applicationTest.id != null
		
		params.id = applicationTest.id
		request.method = 'POST'
		controller.delete()

        assert Application.get(applicationTest.id) == null
        assert response.status == HttpServletResponse.SC_CREATED
		assert response.contentType.contains("application/json")
		assert response.json.success == true
		
    }
	
	
    void testAddUserApplication() {
        
		controller.userService.metaClass.addApplication { Application app, User user ->
			return true
		}
		
		applicationTest.save(flush:true)
		assert applicationTest.id != null
		
		params.id = applicationTest.id
		request.method = 'POST'
        controller.addUserApplication()
        assert response.status == HttpServletResponse.SC_CREATED
		assert response.json.success == true
        //erro na adicao
        response.reset()

        controller.userService.metaClass.addApplication { Application app, User user ->
            return false
        }
		request.method = 'POST'
        controller.addUserApplication(4543543)
		assert response.status == HttpServletResponse.SC_BAD_REQUEST
		assert response.contentType.contains("text/json")
		assert response.json.message == "application not found"

    }

    void testRemoveUserApplication() {
        
		controller.userService.metaClass.removeApplication { Application app, User user ->
			return true
		}
		
		applicationTest.save()
		assert applicationTest.id != null
		
		params.id = applicationTest.id
		request.method = 'POST'
		controller.removeUserApplication()
        
		
        response.reset()
        request.method = 'POST'
        controller.removeUserApplication(54234523)
        assert response.status == HttpServletResponse.SC_BAD_REQUEST
		assert response.contentType.contains("text/json")
		assert response.json.message == "application not found"

    }
	
	void testListUserApplications() {
		applicationTest.save()
		assert applicationTest.id != null
		
		params.id = applicationTest.id
		request.method = 'POST'
		controller.addUserApplication()
		assert response.json.success == true
		//erro na adicao
		response.reset()
		request.method = 'GET'
		controller.listUserApplications("fred")
		
		assert response.json == [[appName:"Petro App Center", appImage:"http://localhost:80/icons/icon_"+applicationTest.id+".jpg", applink:"http://www.google.com"]]
		
	}

	void testPagination() {
		params.offset = null
		params.limit = null
		def pagination = controller.pagination()
		assert pagination.offset == 0
		assert pagination.limit == 10

		params.offset = 0
		params.limit = 0
		pagination = controller.pagination()
		assert pagination.offset == 0
		assert pagination.limit == 10

		params.offset = 1
		params.limit = 1		
		pagination = controller.pagination()
		assert pagination.offset == 1
		assert pagination.limit == 1
	}

	void testApplicationTile() {
		def tile = ApplicationHelper.applicationTile([])
		assert tile == []

		Application.metaClass.getAverageRating = { 2 }
		
		tile = ApplicationHelper.applicationTile(app)
		
		assert tile[0].id == app.id
		assert tile[0].name == app.name
		assert tile[0].description == app.description
		assert tile[0].iconPath == "/${app.icon.relativePath()}?${app.icon.size}"
		assert tile[0].averageRating == 2
		assert tile[0].url == app.link.url
	}

	void testPopular() {
		controller.applicationService.metaClass.popular = { int offset, int limit -> [app] }
		controller.applicationService.metaClass.popularCount = { 1 }
		Application.metaClass.getAverageRating = { 2 }
		
		controller.popular()
		
		assert response.json.applications != null
		assert response.json.count == 1
        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_OK
	}

	void testBestRated() {
		controller.applicationService.metaClass.bestRated = { int offset, int limit -> [apps: [app], total: 1] }
		controller.applicationService.metaClass.bestRatedCount = { 1 }
		Application.metaClass.getAverageRating = { 2 }
		
		controller.bestRated()
		
		assert response.json.applications != null
		assert response.json.count == 1
        assert response.contentType.contains("text/json")
        assert response.status == HttpServletResponse.SC_OK
	}

}