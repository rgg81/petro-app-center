package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*
import org.springframework.mock.web.MockMultipartFile
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */

@Mock([User,Application,UserApplication,Link,Icon, ApplicationDraft])
class MetaClassTests {
	@Test
     void testSomething1() {

		Object.metaClass.transferTo = { File f-> println "File: Calling mocked TranferTo" }
		Object obj = new Object();


		obj.transferTo(new File("/tmp/teste"))

    }
    @Test
    void testSomething2(){
    	/* BEGIN: MOCKING USERS AND APPLICATIONS */
    	def user1 = new User(key:'uq4e').save(flush: true)
		def user2 = new User(key:'up2h').save(flush: true)
		def user3 = new User(key:'uq4n').save(flush: true)
		def user4 = new User(key:'upn1').save(flush: true)
		def user5 = new User(key:'ab9w').save(flush: true)

		def users = [user1,user2,user3,user4,user5]


		def apps = []
		for(i in 1..10){
			def app = new Application(name:"Petro App Center ${i}",
			 	description: "Loja de aplicacoes ${i}",
			 	link:new Link(url:"http://www.google.com.br/q=${i}"),
			 	icon:new Icon(size:1),
				currentVersion: new Version(), catalogCode: "CV${i}")

			apps.add(app.save(flush:true))
		}

		users.each{ user ->
			apps.eachWithIndex { app, i ->
				new UserApplication( user: user, application: app, position: i).save()
			}	
		}
		assert UserApplication.count() == users.size() * apps.size()
    	/* END: MOCKING USERS AND APPLICATIONS */ 
    }
}
