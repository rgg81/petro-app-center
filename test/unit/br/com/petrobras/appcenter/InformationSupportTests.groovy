package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(InformationSupport)
@Mock([Application, ApplicationDraft])
class InformationSupportTests {

	def app
	
	void setUp() {
	  app = new Application(name:"Petro App Center",
					description: "Loja de aplicacoes",
					link:new Link(),
					icon:new Icon(),
					informationsSupport:new ArrayList(),
					currentVersion:new Version(), catalogCode: "CVFG")
	  app.save(flush:true)
	}
	
   void testeValidInformationSupport() {
    	def information = new InformationSupport(url:"http://google.com", application:app, description:"Sistema 1")
     	assert information.validate()
    }
	
	void testInvalidDescription() {
		def information = new InformationSupport(url:"http://google.com", application:app, description:"")
		assert !information.validate()
	 }
	
    void testNullURL() {
       def information = new InformationSupport(url:null, application:app, description:"Sistema 1")
       assert !information.validate()
    }

	void testNullDescription() {
		def information = new InformationSupport(url:"http://google.com", application:app, description:null)
		assert !information.validate()
	 }
	
    void testNullApplication() {
       def information = new InformationSupport(url:"http://google.com", description:"Sistema 1")
       assert !information.validate()
    }
	
	void testSameApplicationManyInformationsSupport() {
		
		def information = new InformationSupport(url:"http://google.com", application:app, description:"Google")
		def information_1 = new InformationSupport(url:"http://oglobo.com", application:app, description:"O globo")
		
		information.save(flush:true)
		information_1.save(flush:true)
		assertEquals 2, InformationSupport.list().size()
	 }
	
	
}
