package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.Cookie

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ActivityController)
@Mock([User,ShiroService,Application,ApplicationDraft])
class ActivityControllerTests {
	def fred
  def app
  def barn
	
	void setUp() {
		fred = new User(id:1,key:'FRED',tracking:"fred_tracking_cookie").save(flush: true, failOnError:true)
    barn = new User(id:2,key:'BARN').save(flush:true, failOnError:true)
    app = new Application(
    'name' : 'app1' ,
    'description' : 'app1description',
    'link' : new Link('url' : 'http://www.google.com'),
    'icon' : new Icon('url' : 'http://www.google.com',size:10,height:10,width:10),
    currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()), catalogCode:"RRR1")

    
    app.adminsApp = [new AdminApplication(admin:barn, application:app)]

    app.save(failOnError:true,flush:true)

    controller.shiroService = new Expando()
    controller.shiroService.metaClass.loggedUser = { -> null} //default not logged user
    controller.activityService = new Expando()
   
	}

  void defaultParams(){

    params["type"] = "accessapp"
    params["where"] = "showapp"
    params["appid"] = "1"
    params["url"] = "http://www.google.com"

  }

  void testInvalidParameters() {
 		controller.proxy()
 		assert response.status == HttpServletResponse.SC_BAD_REQUEST
    assert response.text.length() > 0
 	
  }

  void testLoggedUser(){

    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u.key == fred.key
      assert isReliable == true
    }

    defaultParams()
    controller.shiroService.metaClass.loggedUser = { -> fred}
    controller.proxy()
    assert response.status == 302
  }

  void testTrackingCookieWithMatchingUserParam(){

    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u != null
      assert u.key == fred.key
      assert isReliable == false
    }

    defaultParams()
    Cookie cookie = new Cookie("PETRO_TRACKING","fred_tracking_cookie")
    request.cookies = [cookie].toArray()
    params["userkey"] = "FRED"

    controller.proxy()
    assert response.status == 302
  }

  void testTrackingCookieWithWrongUserParam(){

    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u == null
      assert isReliable == true
    }

    defaultParams()
    Cookie cookie = new Cookie("PETRO_TRACKING","fred_tracking_cookie")
    request.cookies = [cookie].toArray()
    params["userkey"] = "BARN"

    controller.proxy()
    assert response.status == 302
  }

  void testInvalidTrackingCookie(){

    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u == null
      assert isReliable == true
    }

    defaultParams()
    Cookie cookie = new Cookie("PETRO_TRACKING","invalid_tracking_cookie")
    request.cookies = [cookie].toArray()
    controller.proxy()
    assert response.status == 302
  }

  void testNoTrackingCookie(){
    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u == null
      assert isReliable == true
    }

    defaultParams()
    controller.proxy()
    assert response.status == 302
  }

  void testTrackingCookieWithoutUserParam(){
    controller.activityService.metaClass.registerActivity = { User u, Application application, String type,String where, boolean isReliable -> 
      assert u != null
      assert u.key == fred.key
      assert isReliable == false
    }

    defaultParams()

    Cookie cookie = new Cookie("PETRO_TRACKING","fred_tracking_cookie")
    request.cookies = [cookie].toArray()

    controller.proxy()
    assert response.status == 302
  }

  void tearDown() {
    //app.adminsApp.delete() 
    app.delete()
    barn.delete()
    fred.delete()
  }

}
