package br.com.petrobras.appcenter



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(RatingScoreTagLib)
class RatingScoreTagLibTests {

    void testAppNull() {
    	try{
        	def output = tagLib.draw(bean:null).toString()
        	assert false
        }catch(RuntimeException  e){
        	assert true
        }
    }

     void testAppOk() {
  		
     	  def app = new Application(name:"Petro App Center", 
    							  description: "Loja de aplicacoes",
    							  link:new Link(url:"http://www.google.com"),
    							  icon:new Icon(size:1),
								  currentVersion:new Version(number:"1", description:"Primeira vers�o do sistema de atualiza��o de informa��es pessoais",date: new Date())
								  )
		   
        app.metaClass.getRatingScore = { ->
          [1:20, 2:10, 3:30, 4:5, 5:27]
        }

  		assert tagLib.total([bean: app]) == '92'
  		assert tagLib.mean([bean: app]) == String.format("%.2f",3.10)
  		
  		def currStar = 5;
  		
  		tagLib.customDraw([bean: app]) {
			assert it.stars == currStar
			assert it.ratings == app.ratingScore[currStar]
			assert it.total == 92
			
			currStar--
		}
    }

}
