package br.com.petrobras.appcenter

import org.junit.*

import grails.converters.JSON;
import grails.plugin.gson.test.GsonUnitTestMixin
import grails.test.GrailsUnitTestCase
import grails.test.mixin.*

import java.text.ParseException

import javax.servlet.http.HttpServletResponse

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UserController)
@Mock([Application,BaseApplication,UserApplication,ApplicationCompatibilities,ApplicationDraft,ApplicationService,User,UserService,AccessControlService,ShiroService])

class UserControllerTests {
	def fred, wilma
	def app, applicationTest

  def doWithSpring = {
    userService UserService
  }

    void setUp() {
       fred = new User(key:'FRED').save(flush: true, failOnError:true)
       wilma = new User(key:'WILM').save(flush: true, failOnError:true)
       controller.shiroService = new Expando()
       controller.shiroService.metaClass.loggedUser = { -> fred}
	   User.metaClass.getName = { -> "Fred Flinstone"}
        		
	   app = new Application(
	   	name: "Modespe - Modelo Otimizador do Desembolso com Participações Especiais",
		 description: "Apoiar o Tributário e as UOs do E&P na programação trimestral dos desembolsos com participações especiais através da otmização da utilização de créditos. \nSistema de apoio à decisão de pagamento de Participação Especial.",
		link: new Link(url:"http://www.google.com"),
		icon: new Icon(width:119, height:119, size:5191),
		currentVersion: new Version(number:"1", description:"Descrição da versão",date: new Date()), 
		adminsApp: new AdminApplication(admin: wilma, application: app),
		catalogCode:"XPZO"
	   ).save(flush: true, failOnError:true)

	  new UserApplication( user: fred, application: app, position: 0 ).save(flush:true, failOnError:true)
	  new UserApplication( user: wilma, application: app, position: 0).save(flush:true, failOnError:true)
    }
    @Test
    void testUserSharingApplication() {
    	controller.share()
    	assert "FRED" == response.json.user
    	assert true == response.json.shareUserApplications
    }
    @Test
    void testUserUnSharingApplication() {
    	controller.unshare()
    	assert "FRED" == response.json.user
    	assert false == response.json.shareUserApplications
    }
}
