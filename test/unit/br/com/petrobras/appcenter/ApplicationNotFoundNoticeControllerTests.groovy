package br.com.petrobras.appcenter

import grails.test.mixin.*
import org.junit.*
import javax.servlet.http.HttpServletResponse
import grails.converters.*
import grails.plugin.gson.converters.GSON
import grails.plugin.mail.MailService;
import com.google.gson.JsonPrimitive
import com.google.gson.JsonObject
import com.google.gson.JsonElement
//import com.google.gson.JsonArray

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ApplicationNotFoundNoticeController)
@Mock([ApplicationNotFoundNotice, User])
class ApplicationNotFoundNoticeControllerTests {

	def yogibear, booboobear

    @Before
    void setUp() {
    	yogibear = new User(key:'yogi').save(flush:true, failOnError:true)
    	booboobear = new User(key:'boob').save(flush:true, failOnError:true)

        controller.shiroService = new Expando()
        controller.shiroService.metaClass.loggedUser = { -> yogibear }

        User.metaClass.getName = { "Yogi Bear" }
        User.metaClass.getDepartment = { [acronym: "US/Yellostone"] }
    }

    void testSave() {
		def req = new JsonObject()
		req.add("url", new JsonPrimitive('http://yellowstone.gov.us'))
		req.add("description", new JsonPrimitive("app what??"))

    	request.GSON = req
        request.method = 'POST'
		controller.save()

    	assert response.status == HttpServletResponse.SC_CREATED 
    }

    void testList() {
    	controller.list()

    	response == []
    }
    
    void testAllEmpty() {
    	controller.all()
        assert JSON.parse(response.text) == []
		assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.contains("text/json")
	}

    void testAll() {
		def a = new ApplicationNotFoundNotice(user: yogibear, url: 'http://yellowstone.gov.us', description: 'where\'s that picknick basket?').save(flush:true, failOnError:true)
		new ApplicationNotFoundNotice(user: booboobear, url: 'http://nps.gov/yell', description: 'Where\'s Yogi??').save(flush:true, failOnError:true)
        
        //bug GRAILS-11855
        ApplicationNotFoundNotice.metaClass.getDateCreated = { -> new Date()}
        println "ApplicationNotFoundNotice.metaClass.getDateCreated : "  + a.dateCreated
    	
        controller.all()
        
        def list = JSON.parse(response.text)
        println list

        assert list[0].user.key == 'yogi'
        assert list[0].url == 'http://yellowstone.gov.us'
        assert list[0].description == 'where\'s that picknick basket?'

        assert list[1].user.key == 'boob'
        assert list[1].url == 'http://nps.gov/yell'
        assert list[1].description == 'Where\'s Yogi??'

		assert response.status == HttpServletResponse.SC_OK
        assert response.contentType.contains("text/json")
    }

    void testSendMailNewApplicationNotFoundNotice() {
    	def notice = new ApplicationNotFoundNotice(user: booboobear, url: 'http://nps.gov/yell', description: 'Where\'s Yogi??').save(flush:true, failOnError:true)

    	controller.accessControlService = new Expando()
    	controller.accessControlService.getModeratorsEmails = { ['yogi@nps.gov'] }

    	controller.mailService = new Expando()
    	controller.mailService.sendMail = { c -> c() }
    	controller.metaClass.async = { async -> 
    		assert async == true
    	}
    	controller.metaClass.to = { to -> 
    		assert to == 'boob@petrobras.com.br'
    	}
        controller.metaClass.bcc = { bcc -> 
            assert bcc == ['yogi@nps.gov']
        }
    	controller.metaClass.subject = { subject -> 
    		assert subject == "[Aplicações Petrobras] - Notificação de aplicação não encontrada"
    	}
    	controller.metaClass.html = { map -> 
    		assert map.view == "/mail/mailShell"
    		assert map.model.template == "/mail/newApplicationNotFoundNotice"
    		assert map.model.applicationNotFoundNotice == notice
    	}

    	controller.sendMailNewApplicationNotFoundNotice(notice)


    	// Deve falhar silenciosamente mesmo que a mensagem não seja enviada
    	controller.mailService = new Expando()
    	controller.mailService.sendMail = { c -> throw new Exception('Falhe silenciosamente!') }
    	controller.sendMailNewApplicationNotFoundNotice(notice)
    }
}
