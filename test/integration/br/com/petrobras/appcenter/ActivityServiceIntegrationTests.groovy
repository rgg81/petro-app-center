package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin
import io.searchbox.client.config.HttpClientConfig
import io.searchbox.client.http.JestHttpClient
import io.searchbox.client.JestClientFactory

@TestMixin(IntegrationTestMixin)
class ActivityServiceIntegrationTests {
    def users = []
    def apps = []
    
    def elasticSearchService
    def elasticSearchAdminService
    def activityService
    def applicationService

    
    @Before
    void setUp() {
        User.executeUpdate("delete from Comment")
        User.executeUpdate("delete from User")
        ["uq4n","upn1","hacm"].each{ it->
            def user = new User(key:it)
            user.save(failOnError:true,flush:true)
            users.push(user)
        }
        def barn = new User(key:'barn').save(flush:true) 
        def fred = new User(key:'fred').save(flush:true)

        [1,2,3,4,5,6,7,8,9,10].each{ it ->
            def app = new Application(
            'name' : 'app' + it,
            'description' : 'app${it} description',
            'link' : new Link('url' : 'http://www.google.com'),
            'icon' : new Icon('url' : 'http://www.google.com',size:10,height:10,width:10),
            currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()), catalogCode:"RRR" + it)

            
            app.adminsApp = [new AdminApplication(admin:barn, application:app), new AdminApplication(admin:fred, application:app)]

            app.save(failOnError:true,flush:true)
            apps.push(app)
        }
        
        def factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                        .Builder("http://localhost:9200")
                        .multiThreaded(true)
                        .connTimeout(1500)
                        .readTimeout(3000)
                        .build());

        activityService.client.shutdownClient() //closing regular client
        activityService.client = (JestHttpClient)factory.getObject();

    }

    @After
    void tearDown() {
        apps.each{ app ->

            AdminApplication.findAllByApplication(app).each {
                it.delete(flush:true)
            }
            ApplicationCompatibilities.findAllByApplication(app).each {
                it.delete(flush:true)
            }
            UserApplication.findAllByApplication(app).each {
                it.delete(flush:true)
            }
            app.delete(flush:true)
        }

        users.each{ user ->
            user.delete(flush:true)
        }
        
    }

    @Test
    void testAppsAggregation(){
        def activities =[]
        for(i in 1..4){
            activities.push(activityService.registerActivity(users[0],apps[0],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))    
        }

        for(i in 1..10){
            activities.push(activityService.registerActivity(users[1],apps[0],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[1],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[2],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[3],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[4],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[5],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[6],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[7],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))  
            activities.push(activityService.registerActivity(users[1],apps[8],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
            activities.push(activityService.registerActivity(users[1],apps[9],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
        }

        for(i in 1..6){
            activities.push(activityService.registerActivity(users[0],apps[1],UserActivity.ACCESS_APPLICATION,UserActivity.MYAPPS_WHERE,true))
        }

        assert activities.size() == 110

        activities.each { it ->
            elasticSearchService.index(it)    
        }
        elasticSearchAdminService.refresh()
        
        def ids = activityService.aggregatedApps(users[0].key,UserActivity.ACCESS_APPLICATION, 10, 8)

        assert ids == [apps[1].id,apps[0].id]

        ids = activityService.aggregatedApps(users[1].key,UserActivity.ACCESS_APPLICATION, 10, 8)

        assert ids.size() == 8

        def idsAndCounts = activityService.topApps(1000)
        assert idsAndCounts.size() == 10

        idsAndCounts.each { it ->
            assert it.key != null
            assert it.value >= 0
        }

        activities.each { it ->
            elasticSearchService.unindex(it)    
        }
        elasticSearchAdminService.refresh()

        UserActivity.executeUpdate("delete from UserActivity")

    }
}
