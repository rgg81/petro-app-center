package br.com.petrobras.appcenter
import static org.junit.Assert.*
import org.junit.*
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin

@TestMixin(IntegrationTestMixin)
class RateableIntegrationTests {

	static transactional = false
	def reviewsService
	def applicationService
	
	@Before
	void setUp() {
	}
	void setUpBestRatedAppsTest(){

		def user1 = new User(key:'uq4e').save(flush: true)
		def user2 = new User(key:'up2h').save(flush: true)
		def user3 = new User(key:'uq4n').save(flush: true)
		def user4 = new User(key:'upn1').save(flush: true)
		def user5 = new User(key:'ab9w').save(flush: true)
		def user6 = new User(key:'y2aq').save(flush: true)
		def users = [
			user1,
			user2,
			user3,
			user4,
			user5,
			user6
		]
		def apps = []
		for(i in 1..20){
			def app = new Application(name:"Petro App Center ${i}",
			description: "Loja de aplicacoes ${i}",
			link:new Link(url:"http://www.google.com.br/q=${i}"),
			icon:new Icon(size:1), currentVersion: new Version(number:"${i}",description:"descricao da versao 1.0",date:new Date()), catalogCode: "SD${i}")
			apps.add(app.save(flush:true))
		}

		apps[0, 3, 6, 9].eachWithIndex { Application app, i ->
			users[0..5].each{ User user ->
				reviewsService.addReview(new Comment(application: app, rating: 5-i, user: user),"localhost")
			}
		}

		apps[1, 4, 7, 10].eachWithIndex { Application app, i ->
			users[0..2].each{ User user ->
				reviewsService.addReview(new Comment(application: app, rating: 5-i, user: user),"localhost")
			}
		}

		apps[2, 5, 8, 11].eachWithIndex { Application app, i ->
			users[0].each{ User user ->
				reviewsService.addReview(new Comment(application: app, rating: 5-i, user: user),"localhost")
			}
		}
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	@Test
	void testBestRatedApps() {
		setUpBestRatedAppsTest()
		def bestApps = applicationService.bestRated(0, 1000).apps
		bestApps.eachWithIndex { Application app, i ->
			if (i < (bestApps.size()-1)) {
				assert app.averageRating >= bestApps[i+1].averageRating
			}	
		}
	}

	@Test
	void testApplicationScore(){
		Application app = new Application(name:"Petro App Center Score Test",
			description: "Loja de aplicacoes Score",
			link:new Link(url:"http://www.google.com.br/q=score"),
			icon:new Icon(size:1),
			 currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()),
			 catalogCode: "SDNL"
			)

		app.save(flush:true,failOnError:true)
		def users = []
		for(i in 1..15){
			users.add(new User(key:"u${i}").save(flush: true,failOnError:true))
		}
		//5 ratings 1 stars 
		users[0..4].each { User user ->
			println "app [id:${app.id} , name : ${app.name}] | user[id:${user.id} , key : ${user.key}]"

			reviewsService.addReview(new Comment(application: app, rating: 1, user: user),"localhost")
		}
		//4 ratings 5 stars
		users[5..8].each { User user ->
			println "app [id:${app.id} , name : ${app.name}] | user[id:${user.id} , key : ${user.key}]"
			reviewsService.addReview(new Comment(application: app, rating: 5, user: user),"localhost")
		}
		//3 ratings 2 stars 
		users[9..11].each { User user ->
			println "app [id:${app.id} , name : ${app.name}] | user[id:${user.id} , key : ${user.key}]"
			reviewsService.addReview(new Comment(application: app, rating: 2, user: user),"localhost")
		}

		//1 ratings 4 stars
		reviewsService.addReview(new Comment(application: app, rating: 4, user: users[14]),"localhost")

		def score = app.ratingScore;
       	assert score[1] == 5
        assert score[2] == 3
        assert score[3] == 0
        assert score[4] == 1
        assert score[5] == 4

		//2 ratings 3 stars
		users[12..13].each { User user ->
			println "app [id:${app.id} , name : ${app.name}] | user[id:${user.id} , key : ${user.key}]"
			reviewsService.addReview(new Comment(application: app, rating: 3, user: user),"localhost")
		}

		score = app.ratingScore;

        assert score[1] == 5
        assert score[2] == 3
        assert score[3] == 2
        assert score[4] == 1
        assert score[5] == 4
 

    }

//	//TODO Mover para a classe de integra��o quando tivermos fazendo a p�gina de aplica��o
//	@Test
//	void testDeleteApp() {
//		def controller = new ApplicationController()
//		def totalAps = Application.count()
//		controller.params.id = 1
//		controller.delete()
//		assert Application.count() == (totalAps - 1)
//	}

}
