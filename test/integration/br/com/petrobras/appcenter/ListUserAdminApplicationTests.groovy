package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin

@TestMixin(IntegrationTestMixin)
class ListUserAdminApplicationTests {

	def applicationService
	
    @Before
    void setUp() {
        // Setup logic here
    }

    @After
    void tearDown() {
        // Tear down logic here
    }

    @Test
    void testListApps() {
		applicationService.accessControlService = new Expando()
		applicationService.accessControlService.metaClass.authorizedUsers = { id -> [] }
		applicationService.accessControlService.metaClass.updateRoleAndGrants = { application -> }
		
		def app = new Application(
			'name' : 'Application teste 1',
			'description' : 'app2 description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "CVFG")
		
		def app2 = new Application(
			'name' : 'Application teste 2',
			'description' : 'app2 description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "CLKJ")
		
		
		def user1 = new User(key:'zzzy').save(flush:true)
		def user2 = new User(key:'y7yz').save(flush:true)
		
		app.adminsApp = [new AdminApplication(admin:user1, application:app), new AdminApplication(admin:user2, application:app)]
		app2.adminsApp = [new AdminApplication(admin:user1, application:app2), new AdminApplication(admin:user2, application:app2)]
		
		applicationService.copyFile(new File('test/img-app-ok.jpg'), 'target/img-app-ok.jpg')
		def file = new File('target/img-app-ok.jpg')
		
		applicationService.saveApplication(app, file, [], [], null,[])
		
		def appDraft = new ApplicationDraft(
			name : 'Application Draft teste 1',
			description : 'app3 description draft',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), application:app, catalogCode: "BMGD")
		
		def appDraft2 = new ApplicationDraft(
			name : 'Application Draft teste 2',
			description : 'app3 description draft',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "VFDF")
		
		appDraft.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		appDraft2.adminsApp = [new AdminApplication(admin:user1, application:appDraft2), new AdminApplication(admin:user2, application:appDraft2)]
		
		applicationService.saveApplication(appDraft, file, [], [], null,[])
		
		applicationService.copyFile(new File('test/img-app-ok.jpg'), 'target/img-app-ok.jpg')
		applicationService.saveApplication(appDraft2, file, [], [], null,[])
		
		applicationService.copyFile(new File('test/img-app-ok.jpg'), 'target/img-app-ok.jpg')
		applicationService.saveApplication(app2, file, [], [], null,[])
		
		def results = applicationService.userApplicationsAdmins(user1)
		
		assert results.size == 3
		assert results*.status == ["Rascunho", "Rascunho","Publicado"]
		assert results*.application.name == ["Application Draft teste 1", "Application Draft teste 2", "Application teste 2"]
		
		println results
		
		
    }
}
