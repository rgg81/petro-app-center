package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import org.springframework.dao.DataIntegrityViolationException
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin

@TestMixin(IntegrationTestMixin)
class LikeTests {

    Comment comment
    User commentOwner, likeOwner
    Application app

    @Before
    void setUp() {    
        commentOwner = new User(key: "zxpa")
        likeOwner = new User(key: "wafg")

        app = new Application(name:"Petro App Center - LikeTests", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon(size:1), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()), catalogCode: "CVFG")

        def title = "Comment title" 
        def text = "Comment text"

        //app.addToComments(comment)
        app.save(flush: true, failOnError:true)

        //commentOwner.addToComments(comment)
        commentOwner.save(flush: true, failOnError:true)
        likeOwner.save(flush: true, failOnError:true)

        comment = new Comment(title: title, text: text, date:new Date(),user:commentOwner,application:app, rating: 1)

        comment.save(flush:true)
        
        assert Application.findAllByName('Petro App Center - LikeTests').size() == 1
    }

    @After
    void tearDown() {
        // Tear down logic here
    }

    void testNewEvaluationFromTheSameUser() {
        Like like1 = new Like(user: likeOwner, value: LikeEnum.Like.value, comment: comment).save(flush: true)

        shouldFail(grails.validation.ValidationException){
            Like like2 = new Like(user: likeOwner, value: LikeEnum.Dislike.value, comment: comment).save(flush: true)
        }

    }

   void testDislikeLike() {
        Like like1 = new Like(user: likeOwner, value: LikeEnum.Like.value, comment: comment).save(flush: true)
        comment.refresh()
        assert comment.evaluations.size() == 1
        assert comment.likes.size() == 1
        assert comment.dislikes.size() == 0

    }


}