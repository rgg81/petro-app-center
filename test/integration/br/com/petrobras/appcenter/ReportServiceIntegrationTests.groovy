package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import br.com.petrobras.appcenter.*

class ReportServiceIntegrationTests {

    def yogibear, pir8, report
    def reportService

    @Before
    void setUp() {
        yogibear = new User(key:'yogi').save(flush: true, failOnError:true)

        User.metaClass.getName = { "Yogi Bear" }
        User.metaClass.getDepartment = { [acronym: "US/Yellostone"] }

        pir8 = new Application(name:"Petro App Center", 
                              description: "Loja de aplicacoes",
                              link:new Link(url:"http://www.gloco.com.br"),
                              icon:new Icon(size:1L),
                              currentVersion:new Version(number:"1", description:"Primeira versão do sistema de atualização de informações pessoais",date: new Date()), 
                              catalogCode:"XCVG").save(flush: true, failOnError:true)

        println "Report.findAll(): ${Report.findAll()}"
        report = new Report(application: pir8, 
                            user: yogibear, 
                            description: 'More carbs!!!', 
                            issues: [Issue.get(0), Issue.get(2), Issue.get(3), Issue.get(5)]).save(flush: true, failOnError:true)

        new AdminApplication(admin: yogibear, application: pir8)
    }

    @After
    void tearDown() {        
        pir8.delete()
        yogibear.delete()
        report.delete()
    }

    @Test
    void testListReportsAndIssues() {
        def list = reportService.listReportsAndIssues() 
        
        assert list.size() == 1
        assert list[0].issues.size() == 4
        assert list[0].application.id == pir8.id
        assert list[0].user.key == yogibear.key
        assert list[0].description == 'More carbs!!!'
    }
}
