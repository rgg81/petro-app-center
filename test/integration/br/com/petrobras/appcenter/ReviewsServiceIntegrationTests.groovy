package br.com.petrobras.appcenter

import grails.test.mixin.TestMixin
import grails.test.mixin.integration.IntegrationTestMixin
import org.codehaus.groovy.grails.commons.ApplicationAttributes
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.junit.Before

import java.text.SimpleDateFormat

@TestMixin(IntegrationTestMixin)
class ReviewsServiceIntegrationTests {

    def user,application,comment,rating
    def reviewsService
    final shouldFailWithCause = new GroovyTestCase().&shouldFailWithCause

    @Before
    void setUp() {
        user = new User(key:'userx').save()

        reviewsService.shiroService = new Expando()
        reviewsService.shiroService.metaClass.isAuthenticated = { -> return true	}
		reviewsService.shiroService.metaClass.isModerator = { -> return false }
        reviewsService.shiroService.metaClass.loggedUser = { -> User.findByKey('userx')}
        reviewsService.reviewMailService.metaClass.sendMailNewReview = { review,host ->
            println("sending new review mail")
        }
        reviewsService.reviewMailService.metaClass.sendMailDeleteReview = { review,host ->
            println("sending review delete mail")
        }


        application = new Application(name:"Petro App Center", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon([size: 1]), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()), catalogCode: "SGVT")
        application = application.save(flush: true, failOnError:true)

        comment = new Comment(title: "Comment title", text: "Comment text", application: application, user: user, rating: 1)
        
        rating = 5

    }
    
    def loadComments(){
        def comments = []
        for(i in 1..5){
            def user = new User(key:"commenter${i}").save()
            comments.add(new Comment(title: "Comment title ${i}", text: "Comment text ${i}", user: user, application:application, rating: i).save())
        }
        return comments

    }

    void likeComments(comments, like=LikeEnum.Like.value){
        comments.eachWithIndex{ Comment comment, j  -> 
            for(i in j..4){
                def user = User.findByKey("liker${i}")
                if(!user) user = new User(key:"liker${i}").save()
                new Like(user: user, value: like, comment: comment).save()
            }
        }
    }

    void loadCommentsWithDiferentDates(){
        def comments = []
        
        for(i in 0..4){
            def user = new User(key:"commenter${i}").save()
            comments.add(new Comment(title: "Comment title ${i}", text: "Comment text ${i}", user: user, 
                application:application, rating:(i + 1)).save())

        }
    }

    void testListMostPopularComments(){
      
        likeComments(loadComments())
        def mostLikedComments  = application.getMostLikedReviews(2)
        assert mostLikedComments.size() == 2

        assert mostLikedComments[0].user.key == "commenter1"
        assert mostLikedComments[1].user.key == "commenter2"
    }

    //TODO: no futuro deveria ser testListTwoReviewsMostLiked
    void testListTwoReviewsWithMostLikedComment() {
        def comments = loadComments()
        likeComments(comments)

        def reviews = reviewsService.listMostLikedReviews(application.id,2)

        assert reviews.size() == 2

        assert reviews[0].rating == 1.0
        assert reviews[1].rating == 2.0

        assert reviews[0].user.key == "commenter1"
        assert reviews[1].user.key == "commenter2"


        //disliking index 0 and  1  10 times
        comments[0..1].each{ comment -> 
            for(i in 1..10){
                def user = User.findByKey("disliker${i}")
                if(!user) user = new User(key:"disliker${i}").save()
                new Like(user: user, value: LikeEnum.Dislike.value , comment: comment).save()
            }
        }

        def reviews2 = reviewsService.listMostLikedReviews(application.id,3)

        assert reviews2.size() == 3

        assert reviews2[0].rating == 3.0
        assert reviews2[1].rating == 4.0
        assert reviews2[2].rating == 5.0

        assert reviews2[0].user.key == "commenter3"
        assert reviews2[1].user.key == "commenter4"
        assert reviews2[2].user.key == "commenter5"

    }

    void testListReviewsWithMostLikedCommentNoLikes() {
        def comments = loadComments()

        def reviews = reviewsService.listMostLikedReviews(application.id,2)

        assert reviews.size() == 0

        likeComments(comments, LikeEnum.Dislike.value)

        reviews = reviewsService.listMostLikedReviews(application.id,2)

        assert reviews.size() == 0
    }



    void testLikeDisLikeReview() {
        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 2).save()
        reviewsService.likeReview(comment1.id)
        comment1.refresh()
        assert comment1.likes.size() == 1
        assert comment1.dislikes.size() == 0

        def user2 = new User(key:'usery').save()
        def comment2 = new Comment(title: "Comment title 2", text: "Comment text 2", application:application,user:user2, rating: 2).save()
        reviewsService.dislikeReview(comment2.id)
        comment2.refresh()
        assert comment2.likes.size() == 0
        assert comment2.dislikes.size() == 1
    }

    
    void testEditReview() {
        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 5)
        shouldFail{ //comentario nao esta salvo
            reviewsService.editReview(comment1,"localhost")
        }   
        //review salva
        comment1.save()

        //edit ok
        comment1.rating = 3
        def editedReview = reviewsService.editReview(comment1,"localhost")

        application.refresh()
        assert application.averageRating == 3
        assert application.totalRatings == 1
        assert application.comments.size() == 1
		assert editedReview.text == comment1.text
		assert editedReview.title == comment1.title
		assert editedReview.rating == 3

    }

    void testEditReviewDiferentUser() {
        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 5)
        comment1.save()


        def user2 = new User(key:'usery').save()
        reviewsService.shiroService.metaClass.loggedUser = { -> User.findByKey('usery')}

        shouldFailWithCause(IllegalArgumentException){
            reviewsService.editReview(comment1,"localhost")
        }

    }

    void testEditReviewWithLikes() {
        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 5)

        //review salva
        comment1.save()

        def like = LikeEnum.Like.value
        def user = User.findByKey("liker1")
        if(!user) user = new User(key:"liker1").save()
        new Like(user: user, value: like, comment: comment1).save()

        //edit ok
        comment1.refresh()	// Needs to refresh

        assert comment1.evaluations?.size() > 0

        comment1.rating = 3
        comment1 = reviewsService.editReview(comment1,"localhost")

        comment1.refresh()

        assert comment1.evaluations?.size() == 0
    }

    void testDeleteReview() {

        def application2 = new Application(name:"Petro App Center2", 
            description: "Loja de aplicacoes2",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon([size:1]), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()), catalogCode: "CCFA")
        application2 = application2.save(flush: true, failOnError:true)

        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 5).save()

        def comment2 = new Comment( application:application2,user:user, rating: 3).save()

        reviewsService.deleteReview(comment1.id,"localhost")
        assert Comment.get(comment1.id) == null
    }

    void testDeleteReviewDiferentUser() {

        def comment1 = new Comment(title: "Comment title", text: "Comment text", application:application,user:user, rating: 5).save()
    
        def user2 = new User(key:'usery').save()
        reviewsService.shiroService.metaClass.loggedUser = { -> User.findByKey('usery')}

        shouldFailWithCause(RuntimeException){
            reviewsService.deleteReview(comment1.id, "localhost")
        }
        
    }


    void loadCommentsModifiedInDiferentDays(dates){
        def comments = []
        
        for(i in 1..5){
            def user = new User(key:"commenter${i}").save()
            def comment = new Comment(title: "Comment title ${i}", text: "Comment text ${i}", user: user, 
                application:application, dateCreated : dates[i],lastUpdated:dates[i], rating: i)

            toggleAutoTimestamp(comment, false)
            comments.add(comment.save())
            toggleAutoTimestamp(comment, true)

        }
    }

    def toggleAutoTimestamp(target, enabled) {
        def applicationContext = (ServletContextHolder.getServletContext()                                                           
                                  .getAttribute(ApplicationAttributes.APPLICATION_CONTEXT))

        def closureInterceptor = applicationContext.getBean("eventTriggeringInterceptor")
        def datastore = closureInterceptor.datastores.values().iterator().next()
        def interceptor = datastore.getEventTriggeringInterceptor()

        def listener = interceptor.findEventListener(target)
        listener.shouldTimestamp = enabled
        null
    }

    def testListReviewsFromDayToDay(){
        def format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        
        def dates = [
            format.parse("10/07/2014 00:00:00"), 
            format.parse("11/07/2014 00:00:00"),
            format.parse("12/07/2014 00:00:00"),
            format.parse("13/07/2014 00:00:00"),
            format.parse("14/07/2014 23:30:00")
        ]; 

        loadCommentsModifiedInDiferentDays(dates);

        def reviews = reviewsService.listReviewsFromDayToDay("10/07/2014","14/07/2014")

        assert reviews.size() == 5

    }

}
