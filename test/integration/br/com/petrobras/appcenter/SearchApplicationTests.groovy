package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin

@TestMixin(IntegrationTestMixin)
class SearchApplicationTests {

	def elasticSearchService
	def elasticSearchAdminService
	
    @Before
    void setUp() {
        // Setup logic here
    }

    @After
    void tearDown() {
        def dataFolder = new File('./data')
        println "dataFolder:" + dataFolder
        if (dataFolder.isDirectory()) {
            dataFolder.deleteDir()
        }
    }

    @Test
    void testSearchApplication() {
		def app1 = new Application(name:"Petro App Center zteste", 
            description: "Loja de aplicacoes",
            link:new Link(url: "http://www.google.com.br"),
            icon:new Icon(size:1), 
            currentVersion: new Version(number: 1, description: "v1", date: new Date()),catalogCode: "SGVT")
		['abafa', 'abafado', 'curtir', 'curtição'].each { value ->
			app1.addToTags(new Tag(name:value))
		}
		def app2 = new Application(name:"Petro App Center 2 zteste",
			description: "Loja de aplicacoes 2",
			link:new Link(url: "http://www.google.com.br"),
			icon:new Icon(size:1),
			currentVersion: new Version(number: 1, description: "v1", date: new Date()),catalogCode: "SVVT")
		['abafa', 'abafado', 'curtir', 'curtição'].each { value ->
			app2.addToTags(new Tag(name:value))
		}
		def app3 = new Application(name:"Petro App Center 3 zteste",
			description: "Loja de aplicacoes 3",
			link:new Link(url: "http://www.google.com.br"),
			icon:new Icon(size:1),
			currentVersion: new Version(number: 1, description: "v1", date: new Date()),catalogCode: "SQQT")
		['abafa', 'abafado', 'curtir', 'curtição'].each { value ->
			app3.addToTags(new Tag(name:value))
		}
		
		app1.save(flush:true)
		app2.save(flush:true)
		app3.save(flush:true)

		elasticSearchService.index(app1)
		elasticSearchService.index(app2)
		elasticSearchService.index(app3)

		elasticSearchAdminService.refresh()

		def ret = elasticSearchService.search('zteste')
		assert ret.total == 3
		
		def anotherRet = elasticSearchService.search('abafa')
		assert anotherRet.total == 3
		
    }
}
