package br.com.petrobras.appcenter

import static org.junit.Assert.*
import org.junit.*
import org.springframework.transaction.annotation.Transactional;
import org.springframework.mock.web.MockMultipartFile
import org.springframework.mock.web.MockMultipartHttpServletRequest
import org.springframework.web.multipart.MultipartFile
import grails.validation.ValidationException
import br.com.petrobras.security.configuration.StandaloneSecurityConfigurer
import br.com.petrobras.security.configuration.SecuritySettings
import br.com.petrobras.security.exception.ConfigurationException
import grails.test.mixin.integration.IntegrationTestMixin
import grails.test.mixin.TestMixin

@TestMixin(IntegrationTestMixin)
class ApplicationServiceIntegrationTests {

    def applicationService
    def reviewsService
	def tag1
	def tag2
	def tag3
	def infoSupport1
	def infoSupport2
	def infoSupport3
	def version1
	def version2
	def compatib1
	def compatib2
	def file,file2
	def appDraft
	def app
	def user1,user2
	def userService
	

	@Before
    void setUp() {
        Icon.metaClass.sysAbsolutePath = {  ->
            return "target/icon_${application.id}.jpg"
        }
		
		applicationService.copyFile(new File('test/img-app-ok.jpg'), 'target/img-app-ok.jpg')
		applicationService.copyFile(new File('test/img-app-ok-draft.jpg'), 'target/img-app-ok-draft.jpg')

        file = new File('target/img-app-ok.jpg')
		file2 = new File('target/img-app-ok-draft.jpg')
		user1 = new User(key:'zxpt').save(flush:true)
		user2 = new User(key:'vfgh').save(flush:true)
		
		compatib1 = Compatibility.get(1)
		compatib2 = Compatibility.get(2)
		
		if (!compatib1) compatib1 = new Compatibility(id: 1, nameImage:"compatibility_ie.png", description: "Internet Explorer").save(flush:true, failOnError: true)
		if (!compatib2) compatib2 = new Compatibility(id: 2, nameImage:"compatibility_firefox.png", description: "Mozilla Firefox").save(flush:true, failOnError: true)

        //loadAccessControl()

        applicationService.accessControlService = new Expando()
        applicationService.accessControlService.metaClass.authorizedUsers = { id -> [] }
        applicationService.accessControlService.metaClass.updateRoleAndGrants = { application -> }
		applicationService.accessControlService.metaClass.deleteAutorizationInformationValueInApplication = { application -> }
		
		tag1 = new Tag(name:'medico')
		tag2 = new Tag(name:'medicos')
		tag3 = new Tag(name:'existir')
		
		infoSupport1 = new InformationSupport(url:'http://www.google.com', description:'Desc1')
		infoSupport2 = new InformationSupport(url:'http://www.google.com', description:'Desc2')
		infoSupport3 = new InformationSupport(url:'http://www.google.com', description:'Desc3')
		
		version1= new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
		version2= new Version(number:"2",description:"descricao da versao 2.0",date:new Date())
		
		

    }

    void loadAccessControl() {
        println "\n\n\n\n ****************Configurando o CA ********************** \n\n\n\n"
        Properties properties = new Properties()
        try {
            properties.load(new FileInputStream("dominio/ljap/config.properties"))
            SecuritySettings.load(new StandaloneSecurityConfigurer(), properties, true)
        } catch (IOException e) {
            println "Não foi possível ler o arquivo de configurações do CA."
            throw e;
        } catch(ConfigurationException ce) {
            println "Não foi possível carregar as configurações do CA."
            throw ce;
        }
    }

    @After
    void tearDown() {
        // Tear down logic here
    }
	
	void testCreateDraftWithNoLiveApp() {
		appDraft = new ApplicationDraft(
			'name' : 'app1 draft',
			'description' : 'app1 description draft',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "ZSDF")
		
		appDraft.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		appDraft.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(appDraft, file2, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1,version2])
		assert ApplicationDraft.count == 1
		
	}
	
	void testCreateAppWithDraft(){
		app = new Application(
			'name' : 'app1-testeCreateAppWithDraft',
			'description' : 'app1 description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "ZSDA")
		
		app.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		app.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(app, file, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], appDraft,[version1,version2])
		
		assert Application.findAllByName('app1-testeCreateAppWithDraft').size == 1
		assert ApplicationDraft.count == 0
		
	}
	
	void testCreateAppWithNoDraft(){
		def app2 = new Application(
			'name' : 'app2',
			'description' : 'app2 description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "ZZZZ")
		
		app2.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		app2.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(app2, file, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1,version2])
		
		assert Application.findByName('app2')
		assert ApplicationDraft.count == 0
		
	}

    void testUpdateDraftWithNoLiveApp() {
		def appDraft2 = new ApplicationDraft(
			name : 'app2 draft',
			description : 'app2 description draft',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "ZDFG")
		
		appDraft2.adminsApp = [new AdminApplication(admin:user1, application:appDraft2), new AdminApplication(admin:user2, application:appDraft2)]
		appDraft2.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(appDraft2, file, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1,version2])
		
		def app = ApplicationDraft.findByName("app2 draft")

        assert app != null
		
		println "tags ${app.tags}"
        assert app.tags*.name.contains('medico')
        assert app.tags*.name.contains('medicos')
        assert app.tags*.name.contains('existir')
		assert app.versions.size() == 2
		assert Tag.list().size() == 3
		
		def appID = app.id
		
		app.name = "app1b"
		

		def tag1 = new Tag(name:'medico')
		def tag2 = new Tag(name:'medicos')
		
		def version1= new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
		
		
		def compatib1 = Compatibility.get(1)
		app.appCompatib = [compatib1]

		def adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]
		app.adminsApp = adminsApp		

        applicationService.saveApplication(app, file2, [tag1, tag2], null, null,[version1])

		app.refresh()

        assert app != null
		assert Tag.list().size() == 2
        assert app.tags.size() == 2
		assert app.adminsApp.size() == adminsApp.size()
		assert app.appCompatib.size() == 1
		assert app.versions.size() == 1
        assert app.tags*.name.contains('medico')
        assert app.tags*.name.contains('medicos')
    }
	
	void testUpdateDraftWithLiveApp() {
		def appDraft3 = new ApplicationDraft(
			name : 'app3 draft',
			description : 'app3 description draft',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), application:app, catalogCode: "ZPPF")
		
		appDraft3.adminsApp = [new AdminApplication(admin:user1, application:appDraft3), new AdminApplication(admin:user2, application:appDraft3)]
		appDraft3.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(appDraft3, file2, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1])
		
		def app = ApplicationDraft.findByName("app3 draft")

		assert app != null
		
		println "tags ${app.tags}"
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
		assert app.tags*.name.contains('existir')
		assert app.versions*.description.contains('descricao da versao 1.0')
		assert Tag.list().size() == 3
		
		def appID = app.id
		
		app.name = "app1b"
		

		def tag1 = new Tag(name:'medico')
		def tag2 = new Tag(name:'medicos')
		
		def compatib1 = Compatibility.get(1)
		app.appCompatib = [compatib1]

		def adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]
		app.adminsApp = adminsApp		

		applicationService.saveApplication(app, file, [tag1, tag2], null, null,[version2])

		app.refresh()

		assert app != null
		assert Tag.list().size() == 2
		assert app.tags.size() == 2
		assert app.adminsApp.size() == adminsApp.size()
		assert app.appCompatib.size() == 1
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
		assert app.versions*.description.contains('descricao da versao 2.0')
	}
	
	void testUpdateAppWithNoDraft() {
		def app3 = new Application(
			name : 'app3',
			description : 'app3 description',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "PPPP")
		
		app3.adminsApp = [new AdminApplication(admin:user1, application:app3), new AdminApplication(admin:user2, application:app3)]
		app3.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(app3, file, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1,version2])
		
		def app = Application.findByName("app3")

		assert app != null
		
		println "tags ${app.tags}"
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
		assert app.tags*.name.contains('existir')
		assert Tag.list().size() == 3
		
		def appID = app.id
		
		app.name = "app1b"
		

		def tag1 = new Tag(name:'medico')
		def tag2 = new Tag(name:'medicos')
		
		def version1= new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
		def version2= new Version(number:"2",description:"descricao da versao 2.0",date:new Date())
		
		def compatib1 = Compatibility.get(1)
		app.appCompatib = [compatib1]

		def adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]
		app.adminsApp = adminsApp

		applicationService.saveApplication(app, file2, [tag1, tag2], null, null,[version1,version2])

		app.refresh()

		assert app != null
		assert Tag.list().size() == 2
		assert app.tags.size() == 2
		assert app.adminsApp.size() == adminsApp.size()
		assert app.appCompatib.size() == 1
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
	}
	
	void testUpdateAppWithDraft() {
		
		def app4 = new Application(
			name : 'app4',
			description : 'app4 description',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "POIU")
		
		def compatib3 = new Compatibility(nameImage:"compatibility_ie2.png", description: "Internet Explorer").save(flush:true, failOnError: true)
		def compatib4 = new Compatibility(nameImage:"compatibility_firefox2.png", description: "Mozilla Firefox").save(flush:true, failOnError: true)
		
		app4.adminsApp = [new AdminApplication(admin:user1, application:app4), new AdminApplication(admin:user2, application:app4)]
		app4.appCompatib = [compatib3, compatib4]
		
		applicationService.saveApplication(app4, file, [tag1, tag2, tag3], [], null,[])
		
		def appDraft4 = new ApplicationDraft(
			name : 'app3',
			description : 'app3 description',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), application:app4, catalogCode: "ZPLF")
		
		appDraft4.adminsApp = [new AdminApplication(admin:user1, application:appDraft4), new AdminApplication(admin:user2, application:appDraft4)]
		appDraft4.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(appDraft4, file2, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1,version2])

		def app = Application.findByName("app4")
		assert app != null
		
		println "tags ${app.tags}"
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
		assert app.tags*.name.contains('existir')
		assert Tag.list().size() == 6
		
		def appID = app.id
		
		app.name = "app1b"

		def tag1 = new Tag(name:'medico')
		def tag2 = new Tag(name:'medicos')
		
		def version1= new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
		def version2= new Version(number:"2",description:"descricao da versao 2.0",date:new Date())
		
		def compatib1 = Compatibility.get(1)
		app.appCompatib = [compatib1]

		def adminsApp = [new AdminApplication(admin:new User(key:'barn').save(flush:true), application:app), new AdminApplication(admin:new User(key:'fred').save(flush:true), application:app)]
		app.adminsApp = adminsApp		

		applicationService.saveApplication(app, file, [tag1, tag2], null, appDraft4,[version1,version2])

		app.refresh()

		assert app != null
		assert Tag.list().size() == 2
		assert app.tags.size() == 2
		assert app.adminsApp.size() == adminsApp.size()
		assert app.appCompatib.size() == 1
		assert app.tags*.name.contains('medico')
		assert app.tags*.name.contains('medicos')
	}

	//TODO descobrir pq no teste integrado a aplicacao nao é indexada quando salva
   /* void testUnindexWhenCreatingInvalidApp() {
       def app = new Application(
			'name' : 'UnindexApp',
			'description' : 'UnindexApp description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1),currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()), catalogCode: "ZZZZ")
		
		app.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		app.appCompatib = [compatib1, compatib2]
		shouldFail(java.lang.RuntimeException){
			applicationService.saveApplication(app, null, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null)
		}
		elasticSearchAdminService.refresh()


		assert Application.search("UnindexApp").total == 0

    }

    void testIndexWhenCreatingApp() {
		def app = new Application(
			'name' : 'indexApp',
			'description' : 'indexApp description',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1),currentVersion: new Version(number:"1",description:"descricao da versao 1.0",date:new Date()), catalogCode: "ZZZZ")

		app.adminsApp = [new AdminApplication(admin:user1, application:appDraft), new AdminApplication(admin:user2, application:appDraft)]
		app.appCompatib = [compatib1, compatib2]
		
		app = applicationService.saveApplication(app, file, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null)

		elasticSearchAdminService.refresh() 

		assert Application.findAllByName('indexApp').size == 1
		assert Application.search("indexApp").total == 1

    }*/
	
	void testDeleteDraftWithNoLiveApp() {
		def appDraftToTest = new ApplicationDraft(
			'name' : 'app draft to delete',
			'description' : 'app delete description draft',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "PLF")
		
		appDraftToTest.adminsApp = [new AdminApplication(admin:user1, application:appDraftToTest), new AdminApplication(admin:user2, application:appDraftToTest)]
		appDraftToTest.appCompatib = [compatib1, compatib2]
		
		applicationService.saveApplication(appDraftToTest, file2, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version2])
		
		def app = ApplicationDraft.findByName("app draft to delete")
		
		assert app != null
		applicationService.deleteApplication(app)
		
		assert ApplicationDraft.findByName("app draft to delete") == null
		
	}
	
	void testDeleteLiveAppWithNoDraft() {
		
		def appLive = new Application(
			name : 'app live to delete',
			description : 'app live to delete description',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "QWER")
		
		def compatib3 = new Compatibility(nameImage:"compatibility_ie2.png", description: "Internet Explorer").save(flush:true, failOnError: true)
		def compatib4 = new Compatibility(nameImage:"compatibility_firefox2.png", description: "Mozilla Firefox").save(flush:true, failOnError: true)
		
		appLive.adminsApp = [new AdminApplication(admin:user1, application:appLive), new AdminApplication(admin:user2, application:appLive)]
		appLive.appCompatib = [compatib3, compatib4]
		
		applicationService.saveApplication(appLive, file, [tag1, tag2, tag3], [], null,[version1])
		
		def app = Application.findByName("app live to delete")
		
		assert app != null
		applicationService.deleteApplication(app)
		
		assert Application.findByName("app live to delete") == null
		
		
	}
	
	void testDeleteLiveAppWithDraft() {
	
		def appLive = new Application(
			name : 'app live to delete',
			description : 'app live to delete description',
			link : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), catalogCode: "LKAQ")
		
		def compatib3 = new Compatibility(nameImage:"compatibility_ie2.png", description: "Internet Explorer").save(flush:true, failOnError: true)
		def compatib4 = new Compatibility(nameImage:"compatibility_firefox2.png", description: "Mozilla Firefox").save(flush:true, failOnError: true)
		
		appLive.adminsApp = [new AdminApplication(admin:user1, application:appLive), new AdminApplication(admin:user2, application:appLive)]
		appLive.appCompatib = [compatib3, compatib4]
		
		applicationService.saveApplication(appLive, file, [tag1, tag2, tag3], [], null,[version1,version2])
		
		def appDraftToTest = new ApplicationDraft(
			'name' : 'app draft to delete',
			'description' : 'app delete description draft',
			'link' : new Link(url : 'http://www.google.com'),
			icon:new Icon(size:1), application:appLive, catalogCode: "ZMFG")
		
		appDraftToTest.adminsApp = [new AdminApplication(admin:user1, application:appDraftToTest), new AdminApplication(admin:user2, application:appDraftToTest)]
		appDraftToTest.appCompatib = [compatib1, compatib2]
		
		def version1= new Version(number:"1",description:"descricao da versao 1.0",date:new Date())
		
		
		applicationService.saveApplication(appDraftToTest, file2, [tag1, tag2, tag3], [infoSupport1, infoSupport2, infoSupport3], null,[version1])
		
		def app = Application.findByName("app live to delete")
		def appDraft = ApplicationDraft.findByName("app draft to delete")
		
		assert app != null
		assert appDraft != null
		
		applicationService.deleteApplication(app)
		
		assert Application.findByName("app live to delete") == null
		assert ApplicationDraft.findByName("app draft to delete") == null
			
	}

	private def createUsers(n) {
		def users = []

		for (i in 1..n) { 
			users.add(new User(key:"use$i").save(flush:true, failOnError: true))
		}
		
		users
	}
	
	private def createApplications(n, users) {
		def app, apps = []

		for (i in 1..n) {
			app = new Application(
				name : "app$i name",
				description : "app$i description",
				link : new Link(url : "http://www.google.com/$i"),
				icon:new Icon(size:1), 
				catalogCode: "CAT$i")
			
			app.adminsApp = [new AdminApplication(admin:users[i%users.size()], application:app), new AdminApplication(admin:users[(i+1)%users.size()], application:app)]
			
			applicationService.saveApplication(app, file, null, [], null,[])

			apps.add(app)
		}

		apps
	}

	private void makeThemPopular(userApps) {
		userApps.each() { userApp ->
			userApp.apps.each() { app -> 
				userService.addApplication app, userApp.user
			}
		}
	}

	void testPopular() {
		Application.list().each() { applicationService.deleteApplication it }
		User.list().each() { it.delete() }

		def users = createUsers(10)
		def apps = createApplications(10, users)
		def userApps
		
	
		/*
		*** Sem mais populares
		*/
		userApps = []
		assert applicationService.popular(0, 10) == []
		assert applicationService.popular(1, 2) == []
		
		assert applicationService.popularCount() == 0
	

		/*
		*** Mais populares - mesmo peso para todas, desempate pelo código
		*/
		userApps = []
		userApps.add([user: users[1], apps: [apps[2]]])
		userApps.add([user: users[4], apps: [apps[5], apps[6]]])
		
		makeThemPopular userApps

		assert applicationService.popular(0, 10) == [ apps[2], apps[5], apps[6] ]

		assert applicationService.popular(1, 1) == [ apps[5] ]

		assert applicationService.popularCount() == 3


		/*
		*** Mais populares - pesos diferentes
		*/
		userApps = []
		userApps.add([user: users[1], apps: [apps[4]]])
		userApps.add([user: users[4], apps: [apps[3]]])
		userApps.add([user: users[8], apps: [apps[1], apps[9], apps[5]]])

		makeThemPopular userApps

		assert applicationService.popular(0, 10) == [ apps[5], apps[1], apps[2], apps[3], apps[4], apps[6], apps[9] ]

		assert applicationService.popular(5, 10) == [ apps[6], apps[9] ]

		assert applicationService.popularCount() == 7
	}

	private void rateIt(application, ratings) {
		ratings.each() { rating ->
			reviewsService.addReview(new Comment(application: application, text: "", title: "", user: rating.rater, rating: rating.starRating),"localhost")
		}
	}

	void testBestRated() {
		Application.list().each() { applicationService.deleteApplication it }
		User.list().each() { it.delete() }

		def users = createUsers(10)
		def apps = createApplications(10, users)


		/*
		*** Nenhuma aplicação avaliada
		*/
		assert applicationService.bestRated(0, 10) == [apps:[],total:0]
		assert applicationService.bestRated(1, 2) == [apps:[],total:0]


		/*
		*** Algumas com mesma média e número de avaliações
		*/
		rateIt apps[2], [ [rater: users[6], starRating: 5], [rater: users[3], starRating: 2], [rater: users[1], starRating: 1] ]	// média: 8/3: 2.67 
		rateIt apps[4], [ [rater: users[4], starRating: 3], [rater: users[6], starRating: 4] ]										// média: 7/2: 3.5 
		rateIt apps[7], [ [rater: users[8], starRating: 2], [rater: users[0], starRating: 4], [rater: users[1], starRating: 2] ]	// média: 8/3: 2.67	
		rateIt apps[9], [ [rater: users[2], starRating: 1] ]																		// média: 1 

		assert applicationService.bestRated(0, 10) == [apps:[ apps[4], apps[2], apps[7] ], total: 3]
		assert applicationService.bestRated(1, 2) == [ apps: [apps[2], apps[7]], total: 3 ]




	}

}