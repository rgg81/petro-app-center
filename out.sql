-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: changelog.groovy
-- Ran at: 24/09/15 10:21
-- Against: LJAP@jdbc:oracle:thin:@//vipprtlcrpha.petrobras.com.br:1521/prtlcrph.petrobras.com.br
-- Liquibase version: 2.0.5
-- *********************************************************************

-- Lock Database
-- Changeset changelog.groovy::1441143558483-1::leandro (generated)::(Checksum: 3:ad59cfbf36978b63760c088d961e00db)
ALTER TABLE pcomment ADD rating NUMBER(10,0) DEFAULT '1' NOT NULL;

INSERT INTO DATABASECHANGELOG (AUTHOR, COMMENTS, DATEEXECUTED, DESCRIPTION, EXECTYPE, FILENAME, ID, LIQUIBASE, MD5SUM, ORDEREXECUTED) VALUES ('leandro (generated)', '', SYSTIMESTAMP, 'Add Column', 'EXECUTED', 'changelog.groovy', '1441143558483-1', '2.0.5', '3:ad59cfbf36978b63760c088d961e00db', 142);

-- Changeset changelog.groovy::ratings-migration::y1r4::(Checksum: 3:22d4bd77bd67957492b1eaeefb491bee)
update pcomment c set rating = 0;

update pcomment c set rating = (select r.stars from rating_link rl, rating r where rl.rating_id = r.id and rl.rating_ref = c.application_id and r.rater_id = c.user_id);

INSERT INTO DATABASECHANGELOG (AUTHOR, COMMENTS, DATEEXECUTED, DESCRIPTION, EXECTYPE, FILENAME, ID, LIQUIBASE, MD5SUM, ORDEREXECUTED) VALUES ('y1r4', '', SYSTIMESTAMP, 'Custom SQL', 'EXECUTED', 'changelog.groovy', 'ratings-migration', '2.0.5', '3:22d4bd77bd67957492b1eaeefb491bee', 143);

-- Release Database Lock
