--------------------------------------------------------
--  DDL for Table APPLICATION
--------------------------------------------------------

  CREATE TABLE "LJAP"."APPLICATION" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"DESCRIPTION" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR)
   );
--------------------------------------------------------
--  DDL for Table ICON
--------------------------------------------------------

  CREATE TABLE "LJAP"."ICON" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"APPLICATION_ID" NUMBER(19,0), 
	"HEIGHT" NUMBER(10,0), 
	"IMGSIZE" NUMBER(19,0), 
	"WIDTH" NUMBER(10,0)
   )  ;
--------------------------------------------------------
--  DDL for Table LINK
--------------------------------------------------------

  CREATE TABLE "LJAP"."LINK" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"APPLICATION_ID" NUMBER(19,0), 
	"URL" VARCHAR2(255 CHAR)
   )  ;
--------------------------------------------------------
--  DDL for Table RATING
--------------------------------------------------------

  CREATE TABLE "LJAP"."RATING" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"DATE_CREATED" TIMESTAMP (6), 
	"LAST_UPDATED" TIMESTAMP (6), 
	"RATER_CLASS" VARCHAR2(255 CHAR), 
	"RATER_ID" NUMBER(19,0), 
	"STARS" FLOAT(126)
   ) ;
--------------------------------------------------------
--  DDL for Table RATING_LINK
--------------------------------------------------------

  CREATE TABLE "LJAP"."RATING_LINK" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"RATING_ID" NUMBER(19,0), 
	"RATING_REF" NUMBER(19,0), 
	"TYPE" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table TEST_DOMAIN
--------------------------------------------------------

  CREATE TABLE "LJAP"."TEST_DOMAIN" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"NAME" VARCHAR2(255 CHAR)
   )  ;
--------------------------------------------------------
--  DDL for Table TEST_RATER
--------------------------------------------------------

  CREATE TABLE "LJAP"."TEST_RATER" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table USER_APPLICATION
--------------------------------------------------------

  CREATE TABLE "LJAP"."USER_APPLICATION" 
   (	"USER_ID" NUMBER(19,0), 
	"APPLICATION_ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"POSITION" NUMBER(10,0)
   )  ;
--------------------------------------------------------
--  DDL for Table USERDOMAIN
--------------------------------------------------------

  CREATE TABLE "LJAP"."USERDOMAIN" 
   (	"ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"USRKEY" VARCHAR2(255 CHAR)
   ) ;
   
--------------------------------------------------------
--  DDL for Table TAG
--------------------------------------------------------

  CREATE TABLE "LJAP"."TAG" 
   (	"NAME" VARCHAR2(255 CHAR), 
	"APPLICATION_ID" NUMBER(19,0), 
	"VERSION" NUMBER(19,0), 
	"NORMALIZED_NAME" VARCHAR2(255 CHAR)
   );
   
   
  
--------------------------------------------------------
--  DDL for Index SYS_C001589416
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589416" ON "LJAP"."APPLICATION" ("ID") ;
--------------------------------------------------------
--  DDL for Index SYS_C001589431
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589431" ON "LJAP"."ICON" ("ID") ;
--------------------------------------------------------
--  DDL for Index SYS_C001589432
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589432" ON "LJAP"."ICON" ("APPLICATION_ID")  ;
--------------------------------------------------------
--  DDL for Index SYS_C001589437
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589437" ON "LJAP"."LINK" ("ID") ;
--------------------------------------------------------
--  DDL for Index SYS_C001589438
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589438" ON "LJAP"."LINK" ("APPLICATION_ID")  ;
--------------------------------------------------------
--  DDL for Index SYS_C001589446
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589446" ON "LJAP"."RATING" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C001589452
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589452" ON "LJAP"."RATING_LINK" ("ID")  ;
--------------------------------------------------------
--  DDL for Index SYS_C001589456
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589456" ON "LJAP"."TEST_DOMAIN" ("ID")  ;
--------------------------------------------------------
--  DDL for Index SYS_C001589460
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589460" ON "LJAP"."TEST_RATER" ("ID") ;
--------------------------------------------------------
--  DDL for Index SYS_C001589502
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589502" ON "LJAP"."USER_APPLICATION" ("USER_ID", "APPLICATION_ID")  ;
--------------------------------------------------------
--  DDL for Index SYS_C001589503
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589503" ON "LJAP"."USER_APPLICATION" ("USER_ID", "POSITION");
--------------------------------------------------------
--  DDL for Index SYS_C001589464
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001589464" ON "LJAP"."USERDOMAIN" ("ID")  ;
  
--------------------------------------------------------
--  DDL for Index SYS_C001760113
--------------------------------------------------------

  CREATE UNIQUE INDEX "LJAP"."SYS_C001760113" ON "LJAP"."TAG" ("NAME", "APPLICATION_ID") ;

--------------------------------------------------------
--  Constraints for Table APPLICATION
--------------------------------------------------------

  ALTER TABLE "LJAP"."APPLICATION" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."APPLICATION" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."APPLICATION" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."APPLICATION" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."APPLICATION" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ICON
--------------------------------------------------------

  ALTER TABLE "LJAP"."ICON" ADD UNIQUE ("APPLICATION_ID");
  ALTER TABLE "LJAP"."ICON" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."ICON" MODIFY ("WIDTH" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."ICON" MODIFY ("IMGSIZE" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."ICON" MODIFY ("HEIGHT" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."ICON" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."ICON" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."ICON" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LINK
--------------------------------------------------------

  ALTER TABLE "LJAP"."LINK" ADD UNIQUE ("APPLICATION_ID");
  ALTER TABLE "LJAP"."LINK" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."LINK" MODIFY ("URL" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."LINK" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."LINK" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."LINK" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RATING
--------------------------------------------------------

  ALTER TABLE "LJAP"."RATING" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."RATING" MODIFY ("STARS" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("RATER_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("RATER_CLASS" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("LAST_UPDATED" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("DATE_CREATED" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RATING_LINK
--------------------------------------------------------

  ALTER TABLE "LJAP"."RATING_LINK" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."RATING_LINK" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING_LINK" MODIFY ("RATING_REF" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING_LINK" MODIFY ("RATING_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING_LINK" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."RATING_LINK" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TEST_DOMAIN
--------------------------------------------------------

  ALTER TABLE "LJAP"."TEST_DOMAIN" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."TEST_DOMAIN" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TEST_DOMAIN" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TEST_DOMAIN" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TEST_RATER
--------------------------------------------------------

  ALTER TABLE "LJAP"."TEST_RATER" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."TEST_RATER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TEST_RATER" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TEST_RATER" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USER_APPLICATION
--------------------------------------------------------

  ALTER TABLE "LJAP"."USER_APPLICATION" ADD UNIQUE ("USER_ID", "POSITION");
  ALTER TABLE "LJAP"."USER_APPLICATION" ADD PRIMARY KEY ("USER_ID", "APPLICATION_ID");
  ALTER TABLE "LJAP"."USER_APPLICATION" MODIFY ("POSITION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."USER_APPLICATION" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."USER_APPLICATION" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."USER_APPLICATION" MODIFY ("USER_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERDOMAIN
--------------------------------------------------------

  ALTER TABLE "LJAP"."USERDOMAIN" ADD PRIMARY KEY ("ID");
  ALTER TABLE "LJAP"."USERDOMAIN" MODIFY ("USRKEY" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."USERDOMAIN" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."USERDOMAIN" MODIFY ("ID" NOT NULL ENABLE);
  
--------------------------------------------------------
--  Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "LJAP"."TAG" ADD PRIMARY KEY ("NAME", "APPLICATION_ID");
  ALTER TABLE "LJAP"."TAG" MODIFY ("NORMALIZED_NAME" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TAG" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TAG" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
  ALTER TABLE "LJAP"."TAG" MODIFY ("NAME" NOT NULL ENABLE);
  
  
--------------------------------------------------------
--  Ref Constraints for Table ICON
--------------------------------------------------------

  ALTER TABLE "LJAP"."ICON" ADD CONSTRAINT "FK313C79BDC89C87" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "LJAP"."APPLICATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LINK
--------------------------------------------------------

  ALTER TABLE "LJAP"."LINK" ADD CONSTRAINT "FK32AFFABDC89C87" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "LJAP"."APPLICATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RATING_LINK
--------------------------------------------------------

  ALTER TABLE "LJAP"."RATING_LINK" ADD CONSTRAINT "FK1827315C45884E64" FOREIGN KEY ("RATING_ID")
	  REFERENCES "LJAP"."RATING" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table USER_APPLICATION
--------------------------------------------------------

  ALTER TABLE "LJAP"."USER_APPLICATION" ADD CONSTRAINT "FKFFA4F6DCB86A09AD" FOREIGN KEY ("USER_ID")
	  REFERENCES "LJAP"."USERDOMAIN" ("ID") ENABLE;
  ALTER TABLE "LJAP"."USER_APPLICATION" ADD CONSTRAINT "FKFFA4F6DCBDC89C87" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "LJAP"."APPLICATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table TAG
--------------------------------------------------------

  ALTER TABLE "LJAP"."TAG" ADD CONSTRAINT "FK1BF9ABDC89C87" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "LJAP"."APPLICATION" ("ID") ENABLE;

    
--------------------------------------------------------
--  DDL for Sequence HIBERNATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "LJAP"."HIBERNATE_SEQUENCE";

--------------------------------------------------------
--  GRANTs TO LJAP_APLICACAO
--------------------------------------------------------   
   
   grant all on "LJAP"."APPLICATION" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."ICON" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."LINK" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."RATING" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."RATING_LINK" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."TEST_DOMAIN" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."TEST_RATER" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."USER_APPLICATION" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."USERDOMAIN" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."HIBERNATE_SEQUENCE" to "LJAP_APLICACAO" ;
   grant all on "LJAP"."TAG" to "LJAP_APLICACAO" ;

