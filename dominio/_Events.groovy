eventClasspathStart = { 
	addResourceBundlesToClasspath() 
} 

eventConfigureTomcat = {tomcat -> 
 	def context = tomcat.addWebapp('/' , '/path/to/domain/images') 
	tomcat.addUser("upn1", "12345678")
	tomcat.addRole("upn1", "everyone")
} 

private def addResourceBundlesToClasspath(){ 
	classpathSet = false 
	File externalDir = new File("/path/to/domain/classpath" ) 
	rootLoader.addURL( externalDir.toURI().toURL() ) 
}