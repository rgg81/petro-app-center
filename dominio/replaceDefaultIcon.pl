#!/usr/bin/perl
use strict;
use warnings;

sub md5($){
	my $file = shift;
	my $result = `md5 -q $file`;
	$result =~ s/\n//;
	return $result;
}


my $num_args = $#ARGV + 1;
if ($num_args != 1) {
  print "\nUsage: replaceDefaultIcon.pl pathToIcons\n";
  exit;
}

my $newicons = {'69a7c6f6eed2aac33a3efe89333fb0b6' => 'icone-padrao-petrobras1.jpg' , # icone substitui => icone com hash
				'c87a9b99bc09c11aa63fe1ec2e6ccf94' => 'icone-padrao-petrobras2.jpg',
				'10e0f89743f58dc2a4bf15229b21cc35' => 'icone-padrao-petrobras3.jpg'};
my $pathToIcons=$ARGV[0];

opendir(my $dh, $pathToIcons) || die "can't opendir $pathToIcons: $!";
my @files = grep { -f "$pathToIcons/$_" } readdir($dh);
closedir $dh;

my @filestoReplace; 

foreach(@files) {
	my $result = `md5 -q $pathToIcons$_`;
	$result =~ s/\n//;
	if($newicons->{md5("$pathToIcons$_")}){
		#print md5("$pathToIcons$_") . "\n\n";
		print "<" . $_ . "> will be replaced by <" . $newicons->{md5("$pathToIcons$_")} . ">\n" ;
		push @filestoReplace, $_;
	}
}
die "Sem ícones para serem substituidos" unless scalar(@filestoReplace) > 0;

print "Os ícones acima serão sobrescritas pelo novo icone padrão. Confirma?(Sim/Nao)";
my $ret = <STDIN>;

if($ret eq "Sim\n"){
	print "Backing up files ...\n";
	system "mkdir $pathToIcons/backup" and die "failed to create backup";

	foreach(@filestoReplace) {	
		system "cp $pathToIcons$_ $pathToIcons/backup/" and die "failed to copy file";
	}
	print "Back up done.\n";
	print "Replacing files ...\n";
	foreach(@filestoReplace) {
		my $tofile = $newicons ->{md5("$pathToIcons$_")};
	  	system "cp $tofile $pathToIcons$_" and die "failed to copy file";
		print "file $pathToIcons$_ replaced by $tofile\n"; 
	}	
}else{
	print "Exiting...\n";
}









