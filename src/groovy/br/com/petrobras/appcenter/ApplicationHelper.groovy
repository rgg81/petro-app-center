package br.com.petrobras.appcenter

public class ApplicationHelper {	

	public static applicationTile(applications) {
    	return applications.collect { it -> 
	    	[
	    		id: it.id, 
	    		name: it.name, 
	    		description: it.description,
	    		iconPath: "/${it.icon.relativePath()}?${it.icon.size}", 
	    		averageRating: Application.get(it.id).averageRating,
				isDownloadable: it.isDownloadable,
				tooltip:it.isDownloadable? 'Baixe esta aplicação.' : 'Acesse esta aplicação.',
	    		url: it.link.url
	    	]
    	}
    }


}