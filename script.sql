-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: changelog.groovy
-- Ran at: 2/27/14 2:05 PM
-- Against: LJAP@jdbc:oracle:thin:@//glaucius.petrobras.com.br:1521/prtlcrpt
-- Liquibase version: 2.0.5
-- *********************************************************************

-- Lock Database
-- Changeset changelog.groovy::addnotnull-catalogcode-in-application::UPN1 (generated)::(Checksum: 3:0da8bdda94c1422096ab93fc0bb09b11)
ALTER TABLE base_application MODIFY catalog_code NOT NULL;

INSERT INTO DATABASECHANGELOG (AUTHOR, COMMENTS, DATEEXECUTED, DESCRIPTION, EXECTYPE, FILENAME, ID, LIQUIBASE, MD5SUM, ORDEREXECUTED) VALUES ('UPN1 (generated)', '', SYSTIMESTAMP, 'Add Not-Null Constraint', 'EXECUTED', 'changelog.groovy', 'addnotnull-catalogcode-in-application', '2.0.5', '3:0da8bdda94c1422096ab93fc0bb09b11', 102);

-- Release Database Lock
