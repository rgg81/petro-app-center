@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7' )

import groovyx.net.http.HTTPBuilder
import static groovyx.net.http.ContentType.HTML
import org.apache.http.impl.client.AbstractHttpClient
import org.apache.http.params.BasicHttpParams
import java.util.concurrent.*
import br.com.petrobras.appcenter.*

def task = { user, appid ->

	def http = new HTTPBuilder('http://aplicacoespetrobrash.petrobras.com.br/')


	AbstractHttpClient ahc = http.client
	BasicHttpParams params = new BasicHttpParams();
	params.setParameter("http.protocol.handle-redirects",false)
	ahc.setParams(params)

	http.get(path:'/petro-app-center/activity/proxy',
				query : [type:'accessapp', appid: appid, where: 'myapps', url:'http://mobile.petrobras.com.br/apps/', userkey: user.key ],
				headers : [Cookie: 'PETRO_TRACKING='+user.tracking+';'] ) { resp, html ->
	    	println 'got async response => ' + resp.status + ' - Thread =>' +  Thread.currentThread().name
	    	return resp
		}
	http.shutdown()
}

def pool = Executors.newFixedThreadPool(10)




def users = []//s[[key:'UQ4N',tracking:'2852ac9ce68516421908cd5ee125997a'],[key:'BEK7',tracking:'07de6cd6fef2a38e8d600275f903a1c2']]

new File("./scripts/md5s.txt").eachLine { 
	line -> m = ( line =~ /(.*)\t(.*)/ ) 
	users .add([key:m[0][1],tracking:m[0][2]])
}

//println users

def appids =[]

new File("./scripts/appids.txt").eachLine { 
	line -> appids.add(line)
}

def random = new Random()
users.each{ user ->
	(0..5).each {
		pool.submit {
			task (user,appids[random.nextInt(appids.size())])
		}
	}
}
pool.shutdown()

/*def missing = requestsDone(futures)

while ( missing != 0 ) {
	printf "processing... missing %10d\r", missing
    Thread.sleep(300)
	missing = requestsDone(futures)
}*/
//println "Finish  missing " +  missing


/*
def long requestsDone(f){
	def missing = f.size()
	f.each{
		if(it.done){
			missing--
		}
	}
	return missing;
}*/





