includeTargets << grailsScript("_GrailsInit")

target(main: "Script para realizar deploy no jboss") {
    depends(parseArguments)

    def jboss_home, controller, target_group, user, password, host, server_config

    controller = ant.antProject.properties.controller ?: argsMap["controller"]
    if (!controller) {
        println "Parâmetro controller não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "controller: ${controller}"

    jboss_home = ant.antProject.properties.jboss_home ?: argsMap["jboss_home"]
    if (!jboss_home) {
        println "Parâmetro jboss_home não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "jboss_home: ${jboss_home}"


    target_group = ant.antProject.properties.target_group ?: argsMap["target_group"]
    if (!target_group) {
        println "Parâmetro target_group não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "target_group: ${target_group}"

    host = ant.antProject.properties.host ?: argsMap["host"]
    if (!host) {
        println "Parâmetro host não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "host: ${host}"

    server_config = ant.antProject.properties.server_config ?: argsMap["server_config"]
    if (!server_config) {
        println "Parâmetro server_config não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "server_config: ${server_config}"

    user = ant.antProject.properties.user ?: argsMap["user"]
    if (!user) {
        println "Parâmetro user não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "user: ${user}"

    password = ant.antProject.properties.password ?: argsMap["password"]
    if (!password) {
        println "Parâmetro password não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "password: ${password}"


    if(!new File("${basedir}/target/petro-app-center.war").exists()){
        println "Pacote nao encontrado. Favor execute grails war"
        return
    }

    def pathWar = "${basedir}/target/petro-app-center.war"

    def process = "env JBOSS_HOME=${jboss_home} env USER=${user} env PASSWORD=${password} env CONTROLLER=${controller} env TARGET_GROUP=${target_group} env NODE_NAME=${host} env SERVER_CONFIG=${server_config}  ${basedir}/scripts/deploy-jboss.sh ${pathWar}".execute()

    process.in.eachLine { line -> println line }

    process.waitFor()   // Wait for the command to finish

    // Obtain status and output
    println "return code: ${ process.exitValue()}"
    println "stderr: ${process.err.text}"


}

setDefaultTarget(main)