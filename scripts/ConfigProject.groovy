includeTargets << grailsScript("_GrailsInit")
/**
 * Esse script faz o seguinte:
 * 1- solicita do usuario o caminho dos diretorios "libs", "logs" e "appfiles"
 * 2- copia o arquivo template/ljap-env.groovy para a raiz da home do usuario (ex.: C:\Users\<chave>)
 * 3- pega os valores do passo 1 e joga no arquivo USER_HOME/ljap-env.groovy
 * 4- copia os arquivos contidos em /dominio/ljap/ e copia para os diretorios adequados, de acordo com o que o usuario
 *    informou no passo 1
 * 5- os arquivos copiados no passo 4 sao entao manipulados e configurados automaticamente, e a pasta uploaded_images eh criada
 * 6- descompacta o icones.zip dentro de appfiles/ljap/images/
 *
 * A partir dai, ao executar grails run-app, o script scripts/_Events.groovy usa as propriedades configuradas em
 * USER_HOME/ljap-env.groovy pra configurar o classpath do Tomcat e mapear o context root ("/")
 * pro diretorio /..../appfiles/ljap/images
 */
target(main: "Script para configurar automaticamente o ambiente local do desenvolvedor") {

    def propsFileName = "ljap-env.groovy"

    // passo 1
    ant.input(message: "Qual o caminho do diretorio libs (aka path/to/domain/classpath)? [Default: D:/Projetos/libs]",
            defaultvalue: "D:/Projetos/libs",
            addproperty: "ljap.libs")
    def libsPath = ant.antProject.properties."ljap.libs"

    ant.input(message: "Qual o caminho do diretorio logs? [Default: D:/Projetos/logs]",
            defaultvalue: "D:/Projetos/logs",
            addproperty: "ljap.logs")
    def logsPath = ant.antProject.properties."ljap.logs"

    // passo 1
    ant.input(message: "Qual o caminho do diretorio appfiles? [Default: D:/Projetos/appfiles]",
            defaultvalue: "D:/Projetos/appfiles",
            addproperty: "ljap.appfiles")
    def appfilesPath = ant.antProject.properties."ljap.appfiles"
	
	// passo 1
	ant.input(message: "Qual sua chave de acesso ao proxy? [Default: xxxx]",
			defaultvalue: "xxxx",
			addproperty: "ljap.proxyUser")
	def proxyUser = ant.antProject.properties."ljap.proxyUser"
	
	// passo 1
	ant.input(message: "Qual sua senha de acesso ao proxy? [Default: xxxx]",
			defaultvalue: "xxxx",
			addproperty: "ljap.proxyPassword")
	def proxyPassword = ant.antProject.properties."ljap.proxyPassword"

    // passo 2
    ant.copy(file: "${basedir}/scripts/template/${propsFileName}",
            todir: "${userHome}", overwrite: true)

    // passo 3
    ant.replace(file: "${userHome}/${propsFileName}", token: "@libs@", value: libsPath)
    ant.replace(file: "${userHome}/${propsFileName}", token: "@logs@", value: logsPath)
    ant.replace(file: "${userHome}/${propsFileName}", token: "@appfiles@", value: appfilesPath)
	ant.replace(file: "${userHome}/${propsFileName}", token: "@proxyUser@", value: proxyUser)
	ant.replace(file: "${userHome}/${propsFileName}", token: "@proxyPassword@", value: proxyPassword)

    // passo 4
    def ljapLibsPath = libsPath + "/ljap"

    if(!new File(ljapLibsPath).exists()){
        ant.mkdir(dir: ljapLibsPath)
    }

    ant.copy(file: "${basedir}/dominio/ljap/config.properties",
            todir: new File(ljapLibsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/ljap/log4j.properties",
            todir: new File(ljapLibsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/libs/mysql-connector-java-5.1.26-bin.jar",
            todir: new File(libsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/libs/ojdbc5.jar",
            todir: new File(libsPath), overwrite: true)

    ant.copy(file: "${basedir}/dominio/ljap/elasticsearch.json",
            todir: new File(ljapLibsPath), overwrite: true)

    // passo 5
    def logFilePath = logsPath + "/ljap/ljap.log"
    def imageDirFilePath = appfilesPath + "/ljap/uploaded_images"


    if(!new File(imageDirFilePath).exists()){
        ant.mkdir(dir: imageDirFilePath)
    }

    ant.replace(file: "${ljapLibsPath}/config.properties", token: "@log4jpath@", value: "${ljapLibsPath}/log4j.properties")
    ant.replace(file: "${ljapLibsPath}/config.properties", token: "@imageDir@", value: imageDirFilePath)
    ant.replace(file: "${ljapLibsPath}/log4j.properties", token: "@logpath@", value: logFilePath)

    // passo 6
    def iconsDirPath = imageDirFilePath + "/icons"

    if(!new File(iconsDirPath).exists()){
        ant.mkdir(dir: iconsDirPath)
    }

    ant.unzip(src: "${basedir}/dominio/icons.zip", dest: new File(iconsDirPath), overwrite: true)
}

setDefaultTarget(main)
