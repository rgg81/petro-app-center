includeTargets << grailsScript("_GrailsInit")

target(main: "Script para realizar deploy no weblogic") {
    depends(parseArguments)

    def bea_home, adminurl, targets, user, password

    adminurl = ant.antProject.properties.adminurl ?: argsMap["adminurl"]
    if (!adminurl) {
        println "Parâmetro adminurl não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "adminurl: ${adminurl}"

    targets = ant.antProject.properties.targets ?: argsMap["targets"]
    if (!targets) {
        println "Parâmetro targets não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "targets: ${targets}"

    bea_home = ant.antProject.properties.bea_home ?: argsMap["bea_home"]
    if (!bea_home) {
        println "Parâmetro bea_home não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "bea_home: ${bea_home}"

    user = ant.antProject.properties.user ?: argsMap["user"]
    if (!user) {
        println "Parâmetro user não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "user: ${user}"

    password = ant.antProject.properties.password ?: argsMap["password"]
    if (!password) {
        println "Parâmetro password não informado! Use --parametro=valor na linha de comando ou inclua uma propriedade na tarefa. "
        return
    }
    println "password: ${password}"

    if(!new File("${basedir}/target/petro-app-center.war").exists()){
        println "Pacote nao encontrado. Favor execute grails war"
        return
    }

    def pathWar = "${basedir}/target/petro-app-center.war"

    ant.path(id:'weblogic-deploy-path'){
        ant.pathelement(location:"${bea_home}/wlserver/server/lib/weblogic.jar")
        ant.pathelement(location:"${bea_home}/modules/com.bea.core.utils.full_1.9.0.1.jar")
        ant.pathelement(location:"${bea_home}/modules/com.bea.core.i18n_1.8.0.0.jar")
        ant.pathelement(location:"${bea_home}/modules/com.bea.core.weblogic.rmi.client_1.10.0.0.jar")
        ant.pathelement(location:"${bea_home}/modules/javax.enterprise.deploy_1.2.jar")
    }

    ant.taskdef(name:'wldeploy', classname:'weblogic.ant.taskdefs.management.WLDeploy', classpathref:'weblogic-deploy-path')
    
    def params = [verbose:'true',debug:'true', name:'petro-app-center', user:user,password:password,adminurl:adminurl,targets:targets]

    ant.wldeploy(params + [action:'stop',graceful:'true',ignoresessions:'true',failonerror:'false'])
   
    ant.wldeploy(params + [action:'undeploy', failonerror:'false', allversions:'true'])

    ant.wldeploy(params + [action:'deploy',upload:'true',source:pathWar, failonerror:'true'])

    ant.wldeploy(params + [action:'start', failonerror:'true'])

}

setDefaultTarget(main)