#!/usr/bin/env bash

WAR_FILE="$1"

commandJboss() {
    $JBOSS_HOME/bin/jboss-cli.sh --connect --controller="$CONTROLLER" --user="$USER" --password="$PASSWORD" --commands="$1"
}

deployWar() {
    commandJboss "deploy $WAR_FILE --server-groups=$TARGET_GROUP"
}
commandJboss "/server-group=$TARGET_GROUP:restart-servers"
STATUS=`commandJboss "/host=$NODE_NAME/server-config=$SERVER_CONFIG:read-resource(include-runtime=true)" | grep \"status\" | grep STARTED`
echo "$STATUS"

while [ -z "$STATUS" ]
do
    echo "Server is restarting..."
    STATUS=`commandJboss "/host=$NODE_NAME/server-config=$SERVER_CONFIG:read-resource(include-runtime=true)" | grep \"status\" | grep STARTED`
done

echo "Server restarted"
echo "Deploy war file $WAR_FILE"
DEPLOY_MESSAGE=`deployWar`

if [[ "$DEPLOY_MESSAGE" == *"already exists in the deployment repository"* ]]
then
  echo "Application already exists. Undeploying...";
  commandJboss "undeploy $(basename "$WAR_FILE") --server-groups=$TARGET_GROUP"
fi

echo "Deploying..."
deployWar
echo "Success!"







