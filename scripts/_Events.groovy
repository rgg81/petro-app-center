String appCode = getConfig().appCode
String libs = getEnvProperties().petrobras.libs.dir
String appfiles = getEnvProperties().petrobras.appfiles.dir + "/" + appCode
String imagesDir = appfiles + "/uploaded_images"

System.properties.setProperty('ELASTICSEARCH_PATH', appfiles + "/elasticsearch")

eventSetClasspath = { rootLoader ->
    println "add classpath"
    addResourceBundlesToClasspath(libs,rootLoader)

    // Adiciona todos os *.jar
    def folder = new File(libs)
    folder.eachFileMatch (~/.*.jar/)  { jarFile ->
        println "Adicionando lib ${jarFile}..."
        addResourceBundlesToClasspath(jarFile,rootLoader)
    }
}

eventConfigureTomcat = {tomcat ->
	println "configure tomcat"
	tomcat.addWebapp('/' , imagesDir)
	def loader = tomcat.class.classLoader
	addResourceBundlesToClasspath(libs,loader)
	addResourceBundlesToClasspath("${libs}/mysql-connector-java-5.1.26-bin.jar",loader)
	addResourceBundlesToClasspath("${libs}/ojdbc5.jar",loader)
}

private def addResourceBundlesToClasspath(String strDir, rootLoader) {
    addResourceBundlesToClasspath(new File(strDir), rootLoader)
}

private def addResourceBundlesToClasspath(File externalDir, rootLoader){

    if (grailsSettings.grailsEnv != "production") {
        classpathSet = false
        rootLoader.addURL( externalDir.toURI().toURL() )
    }
}

private def getConfig(){
    return getConfFile("Config")
}

/**
 * {@see http://stackoverflow.com/questions/5654416/access-settings-in-buildconfig}
 * @param fileName
 * @return
 */
private def getConfFile(String fileName){
    String configPath = "${basedir}${File.separator}grails-app${File.separator}conf${File.separator}${fileName}.groovy"
    return new ConfigSlurper(grailsSettings.grailsEnv).parse(new File(configPath).toURI().toURL())
}

private def getEnvProperties(){
    return new ConfigSlurper().parse(new File("${userHome}/ljap-env.groovy").toURI().toURL())
}
