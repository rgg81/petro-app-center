[ 
		{
			"id":0,
			"name":"Viagens",
			"image":"app-travel.png",
			"description": "Primeiramente você solicita a viagem ao seu gerente. Em seguida, a viagem deve ser criada com todos os detalhes fornecidos por você, passando então pela aprovação do gerente. Feita a aprovação do gerente você receberá as reservas e instruções para a sua viagem. É importante lembrar de fornecer os dados necessários para a aprovação da prestação de contas ao fim da viagem.</p><img src=\"img/app-travel-ss.jpg\"></p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":1,
			"name":"Rodízio de Fornecedores",
			"image":"app-rodizio.png",
			"description": "Uma descrição de exemplo para rodízio de fornecedores. A ideia é que essa seja um pouco mais curta e sem imagem</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":2,
			"name":"Carrinho de compras",
			"image":"app-carrinho.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":3,
			"name":"PAN - Plano Anual de Negócios",
			"image":"app-pan.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		},
		{
			"id":3,
			"name":"PAN - Plano Anual de Negócios",
			"image":"app-pan.png",
			"description": "O carrinho para compras pode ser utilizado para fazer feira ou para ir ao supermercado. Resistente e de fácil transporte, serve para carregar alimentos e compras até a sua casa. Conceito ecológico, não precisa utilizar as bolsas plásticas de supermercado.</p>",
			"comments": [
				{
					"sender": "João Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 180 
				},
				{
					"sender": "Maria Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 108 
				},
				{
					"sender": "José Silva",
					"text": "Comentário aleatório falando sobre a aplicação. Critica seguida por sugestão e elogio",
					"rating": 45 
				}

			] 

		}
]
