var toolbeltApp = angular.module("toolbeltApp", ['mainApp', 'animations', 'ngSanitize']);

toolbeltApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});

toolbeltApp.controller('toolbeltCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
	$scope.infos = null;

	$scope.loadHomeInfo = function() {
			httpNoCache.get("/petro-app-center/toolbelt/indexInfo").success(function(infos) {
				$scope.infos = infos;
                $("#toolbelt-title-loading").hide();
                $("#applications-panel").css("visibility","visible");
				drawPie([["Aplicações com média acima de 3", infos.averageAboveThree], ["Aplicações com média abaixo de 3", infos.averageBelowThree]])

			//$scope.updateHasMore(reviews);			
		}).error(function(errorData) {
			console.log("Erro ao obter informações de aplicações: " + errorData);
		});
	}

	function drawPie(share) {
		Highcharts.setOptions({colors: ['#7CB5EC', '#ff6666']});
		$('#averageChart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            spacingTop:0,
            spacingBottom: 0,
        },
        title: {
            text: null
        },
        tooltip: {
    	    pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: false,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits : {
        	enabled: false
        },
        series: [{
            type: 'pie',
            data: share
        }]
    });
	}

	$scope.loadHomeInfo();

}]);


toolbeltApp.controller('allApplicationsCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
    $scope.applications = null;
    $scope.loading = true;
    $scope.loadHomeInfo = function() {
    	$scope.url = "/petro-app-center/toolbelt/allApplications";
		$scope.urlCsv =  $scope.url + "?format=csv";    
    	httpNoCache.get($scope.url,{headers: {'Accept': 'application/json'}}).success(function(applications) {
    		$scope.loading = false;    
    		$scope.applications = replaceByValue(applications, "admins", ",","<br>");
                $("#toolbelt-title-loading").hide();
                $('#my-final-table').dynatable({
                  dataset: {
                    records: applications
                  },
                  inputs: {
                    processingText: "Carregando...",
                    searchText: "Pesquisar: ",
                    perPageText: "Mostrar: ",
                    paginationPrev: "Anterior",
                    paginationNext: "Próxima",
                    recordCountText: "Mostrando "
                  },
                  params: {
                    records: "registros"
                  }
                });
               // $("#dynatable-search-my-final-table").html(function () { return $(this).html().replace("Search", "Pesquisar"); });

            //$scope.updateHasMore(reviews);            
        }).error(function(errorData) {
            console.log("Erro ao obter aplicações: " + errorData);
        });
    }

    $scope.loadHomeInfo();

}]);

toolbeltApp.controller('averageXUseCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
    $scope.average = 3;
    $scope.aboveAverage = new Array();
    $scope.belowAverage = new Array();

    $scope.loadHomeInfo = function() {
            httpNoCache.get("/petro-app-center/toolbelt/applicationAverageAndUse").success(function(apps) {
                $("#toolbelt-title-loading").hide();
                for (i in apps) {
                    if (apps[i].y > $scope.average) {
                        $scope.aboveAverage.push(apps[i]);       
                    } else {
                        $scope.belowAverage.push(apps[i]);  
                    }

                }
                drawScatter();

            //$scope.updateHasMore(reviews);            
        }).error(function(errorData) {
            console.log("Erro ao obter informações de aplicações: " + errorData);
        });
    }
    $scope.loadHomeInfo();

    function drawScatter() {
        Highcharts.setOptions({
            lang: {
                resetZoom: "Tirar Zoom"
            }
        });
        $('#scatter-chart').highcharts({
            chart: {
                type: 'scatter',
                zoomType: 'xy'
            },
            title: {
                text: null
            },
            subtitle: {
                text: '<strong>Clique e arraste para ver a área escolhida em detalhes</strong>'
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: '<strong>Número de vezes adicionada por usuários</strong>'
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true
            },
            yAxis: {
                title: {
                    text: '<strong>Média das avaliações feitas</strong>'
                }
            },
            plotOptions: {
                scatter: {
                    allowPointSelect: true,
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            $window.open("/petro-app-center/application/show/" + this.id,"_blank");
                        }
                    }
                },
                showInLegend: false,
                    marker: {
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '<b>{point.name}</b><br>Média: {point.y}, Adicionada: {point.x} vezes<br>'
                    }
                }
            },
            credits : {
                enabled: false
            },
            series: [{
                color: '#7CB5EC',
                data: $scope.aboveAverage
            },
            {
                color: '#ff6666',
                data: $scope.belowAverage
            }]
        });
    }

}]);


toolbeltApp.controller('ratingXAccessCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
    $scope.average = 3;
    $scope.aboveAverage = new Array();
    $scope.belowAverage = new Array();
    $scope.xCaption = " - Total"
    $scope.loadHomeInfo = function(timespan) {
            var oneDay = 24*60*60*1000
            httpNoCache.get("/petro-app-center/toolbelt/applicationAverageAndAccess" + (timespan? "?timespan="+timespan:"")).success(function(result) {
                $scope.aboveAverage = new Array();
                $scope.belowAverage = new Array();
                $("#toolbelt-title-loading").hide();
                for (i in result["apps"]) {
                    if (result["apps"][i].rating > $scope.average) {
                        $scope.aboveAverage.push(result["apps"][i]);       
                    } else {
                        $scope.belowAverage.push(result["apps"][i]);  
                    }

                }
                if (timespan) {                    
                    $scope.xCaption = " de " + result["startDate"] + " a " + result["endDate"];
                } else {
                    $scope.xCaption = " - Total"
                }
                drawScatter();

            //$scope.updateHasMore(reviews);            
        }).error(function(errorData) {
            console.log("Erro ao obter informações de aplicações: " + errorData);
        });
    }
    $scope.loadHomeInfo();

    function drawScatter() {
        Highcharts.setOptions({
            lang: {
                resetZoom: "Tirar Zoom"
            }
        });
        $('#scatter-chart').highcharts({
            chart: {
                type: 'scatter',
                zoomType: 'xy'
            },
            title: {
                text: null
            },
            subtitle: {
                text: '<strong>Clique e arraste para ver a área escolhida em detalhes</strong>'
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: '<strong>Número de acessos '+ $scope.xCaption +'</strong>'
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true
            },
            yAxis: {
                title: {
                    text: '<strong>Média das avaliações feitas</strong>'
                }
            },
            plotOptions: {
                scatter: {
                    allowPointSelect: true,
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            $window.open("/petro-app-center/application/show/" + this.id,"_blank");
                        }
                    }
                },
                showInLegend: false,
                    marker: {
                        radius: 5,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: false
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '',
                        pointFormat: '<b>{point.name}</b><br>Média: {point.y}, Acessada: {point.x} vezes<br>'
                    }
                }
            },
            credits : {
                enabled: false
            },
            series: [{
                color: '#7CB5EC',
                data: $scope.aboveAverage
            },
            {
                color: '#ff6666',
                data: $scope.belowAverage
            }]
        });
    }

}]);


String.prototype.replaceAll = function(de, para){
    var str = this;
    var pos = str.indexOf(de);
    while (pos > -1){
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}

function replaceByValue( json, field, oldvalue, newvalue ) {
    for( var k = 0; k < json.length; ++k ) {
        if(json[k][field].indexOf(oldvalue) > -1 ) {
            json[k][field] = json[k][field].replaceAll(oldvalue,newvalue) ;
        }
    }
    return json;
}