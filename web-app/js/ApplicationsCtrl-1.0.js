var applicationsApp = angular.module("applicationsApp", ['mainApp','uiSwitch']);

applicationsApp.controller('ApplicationsCtrl', ['$scope', '$http', '$window', '$modal', 'httpNoCache', 'ljapEmpty', function($scope, $http, $window, $modal,httpNoCache, ljapEmpty) {
	$scope.application;
	$scope.key = '';
	$scope.tag = '';
	$scope.triggerkeyvalidation = false;
	$scope.triggertagvalidation = false;
	$scope.triggersuportinfovalidation = false;
	$scope.catalogAppNotFound = false;
	$scope.fetchingCatalogData = false;
	$scope.catalogAppNotUnique = false;
	$scope.suportInfo = new Object();
	$scope.suportInfo.description = '';
	$scope.suportInfo.url = '';
	$scope.adminFromCatalogCode = []
	$scope.DEFAULT_ICON_RELATIVE_PATH = "/petro-app-center/static/images/img-app-example.png"; 
	$scope.dataFromCatalog;
	$scope.shouldShowAddVersion = false;
	$scope.currentVersionToEdit;
	$scope.referenceVersionEdited;

	$scope.addSuportInfo = function() {
		if ($scope.suportInfo.description != '' && $scope.suportInfo.url != ''){
			if($scope.form.suportInfoUrl.$valid){
				if (jQuery.inArray($scope.suportInfo.url, $scope.application.informationsSupport.map(function(s){return s.url})) == -1 && jQuery.inArray($scope.suportInfo.description, $scope.application.informationsSupport.map(function(s){return s.description})) == -1) {
					var newSuportInfo = new Object();
					newSuportInfo.url = $scope.suportInfo.url;
					newSuportInfo.description = $scope.suportInfo.description;
					$scope.application.informationsSupport.push(newSuportInfo);
					$scope.suportInfo.description = '';
					$scope.suportInfo.url = '';
				} else {
					$scope.duplicatesuportinfo = true
				}
			} else {
				$scope.triggersuportinfovalidation = true;
			}
			
		}
		else {
			$scope.invalidsuportinfo = true
		}
	}
	
	$scope.removeSuportInfo = function(suportInfo) {
		$scope.application.informationsSupport.splice($scope.application.informationsSupport.indexOf(suportInfo),1);
	};
	
	$scope.checkForm = function(){
		return (!$scope.form.$valid || (($('input[name="icon"]').val() == '') && ($scope.application.icon.relativePath == null)) || $scope.application.adminsApp.length == 0 || $scope.application.versions.length == 0)
	};
	
	$scope.checkCatalogForm = function(){
		return (!$scope.application.catalogCode || $scope.fetchingCatalogData);
	};
	
	$scope.cleanSuportInfoMsg = function() {
	    $scope.invalidsuportinfo = false;
		$scope.triggersuportinfovalidation = false;
		$scope.duplicatesuportinfo = false;
	};
	
	$scope.addTag = function(tag) {
		if (tag != ''){
			if (jQuery.inArray(angular.lowercase(tag), $scope.application.tags.map(function(s){return s.name})) == -1) {
				var newTag = new Object();
				newTag.name = angular.lowercase(tag);
				$scope.application.tags.push(newTag);
				$scope.tag = '';
			}
			else {
				$scope.duplicatetag = true
			}
			
		}
		else {
			$scope.invalidtag = true
		}
		//not a good practice wire ui with controllers
		$('input[name="tag"]').focus();
	}
	
	$scope.checkClassOpacity = function(compatibility) {
		return (jQuery.inArray(compatibility.id, $scope.application.compatibilities.map(function(s){return s.id})) == -1)
	}
	
	$scope.toogleCompatibility = function(compatibility) {
		if (jQuery.inArray(compatibility.id, $scope.application.compatibilities.map(function(s){return s.id})) == -1) {
			$scope.application.compatibilities.push(compatibility);
		}
		else {
			$scope.application.compatibilities.splice($scope.application.compatibilities.map(function(s){return s.id}).indexOf(compatibility.id),1);
		}
	}
	
	$scope.removeTag = function(tag) {
		$scope.application.tags.splice($scope.application.tags.indexOf(tag),1);
	};
	
	$scope.getTags = function(value) {
		return httpNoCache.get('/petro-app-center/tag/find?name=' + value).then(function(response){ 
			var tagsDescription = [];
			
			angular.forEach(response.data, function(tag, key){
				this.push(tag.value);
			}, tagsDescription);
			return tagsDescription;
		}); 
	};
	
	$scope.cleanTagMsg = function() {
	    $scope.invalidtag = false;
		$scope.triggertagvalidation = false;
		$scope.duplicatetag = false;
	};

	$scope.addAdmin = function(key) {
		if (key != ''){
			if ($scope.form.key.$valid) {
				if (jQuery.inArray(angular.lowercase(key), $scope.application.adminsApp.map(function(s){return angular.lowercase(s.admin.key)})) == -1) {
					httpNoCache.get("/petro-app-center/accessControl/findUser?key=" + key).success(function(toBeAdmin) {
						var adminApp = new Object();
						adminApp.admin = toBeAdmin;
						$scope.application.adminsApp.push(adminApp); 
						$scope.key = '';
					}).error(function(errorData) {
						console.log("Erro ao obter lista de reviews: " + errorData);
						if ( errorData.errors ) {
							$scope.usernotexist = true;					
						} else {
							alert(errorData.message);
						}
					});
				}
				else {
					$scope.duplicatekey = true
				}
			}
			else {
				$scope.triggerkeyvalidation = true;
			}
			
		} else {
			$scope.invalidkey = true;	
		}
		//not a good practice wire ui with controllers
		$('input[name="key"]').focus();
	}
	
	$scope.cleanKeyMsg = function() {
	    $scope.invalidkey = false;
		$scope.triggerkeyvalidation = false;
		$scope.usernotexist = false;
		$scope.duplicatekey = false;
	};
	

	$scope.init = function(application) {
		$scope.application = application;
	};

	$scope.fetchCatalogData = function(){		
		$http.post("/petro-app-center/catalogCode/check", {'val': $scope.application.catalogCode, 'applicationId': $scope.application.id} ).success(function(data, status, headers, config) {
			if(data.isUnique) {
				$scope.fetchingCatalogData = true;
				$scope.catalogAppNotFound = false;

				httpNoCache.get("/petro-app-center/catalogClient/fetch?appCode=" + angular.uppercase($scope.application.catalogCode)).success(function(data) {
					if (data.status == 'Homologação') {
						$scope.dataFromCatalog = data;

					    var modalInstance = $modal.open({templateUrl: 'confirmCatalogImportModal.html', controller: ModalInstanceCtrl});

					    modalInstance.result.then(function (selectedItem) {
							$scope.updateFormWithCatalogData();
					    }, function () {
							$scope.fetchingCatalogData = false;
							$scope.cleanSomeData();
					    });
					} else {
						$scope.updateFormWithCatalogData(data); 
					}

				}).error(function(errorData) {
					$scope.catalogAppNotFound = true;
					$scope.catalogMsg = errorData.message;
					$scope.fetchingCatalogData = false;
					console.log("Erro ao obter dados do catálogo de aplicações: " + errorData);
				});
			} else {
				$scope.catalogAppNotUnique = true;
			}
		}).error(function(errorData) {
			console.log("Erro ao conferir duplicidade do codigo do catalogo: " + errorData);
			alert(errorData.message);
		});
	};

	$scope.updateFormWithCatalogData = function(data) {
		if (typeof data == 'undefined') data = $scope.dataFromCatalog;
		
		$scope.application.name = data.name;
		$scope.application.description = data.description;
		$scope.application.department = data.department;
		console.log($scope.application.department);
		angular.forEach(data.tags, function(v,k){
			$scope.addTag(v);
		});
		//quando a aplicacao é importada do catalogo, as vezes existem tags duplicadas. Para não aparecer a mensagem quando a aplicação for importada.
		$scope.duplicatetag = false;
		$scope.adminFromCatalogCode = data.admins;
		angular.forEach(data.admins, function(v,k){
			$scope.addAdmin(v);
		});

		$scope.fetchingCatalogData = false;
		$('input[name="url"]').focus();
	};

	$scope.cleanCatalogCodeMsg = function() {
	    $scope.catalogAppNotFound = false;
	    $scope.catalogAppNotUnique = false;
	};
	
	
	$scope.cleanSomeData = function() {
		$scope.application.catalogCode = null;
		$scope.application.name = null;
		$scope.application.description = null;
		$scope.application.department = null;
		$scope.application.tags= [];
		$scope.application.adminsApp= $scope.application.adminsApp.filter(function(i) {
			return $scope.adminFromCatalogCode.indexOf(i.admin.key) < 0;
			}
		);
		$scope.catalogAppNotUnique = false;
		$scope.catalogAppNotFound = false;
	}
	
	$scope.removeAdmin = function(admin) {
		$scope.application.adminsApp.splice($scope.application.adminsApp.indexOf(admin),1);
	};

	$scope.saveOrPublish = function(typeOfOperation) {
		// TODO: resolver o problema do datepicker com o angular. Datepicker define o value mas o modelo do angular não é atualizado. 
 		var today = new Date();
		$('#button-'+typeOfOperation+'-app').button('loading');
		$('.btn').prop("disabled",true);
		
		$scope.application.icon.name = $("input[name='icon']").attr("value");
		
		var ModalInstanceCtrl = function ($scope, $modalInstance) {
		  $scope.ok = function () {
		    $modalInstance.close('close');
		  };
		};

		$http.post("/petro-app-center/application/" + typeOfOperation + "/" + $scope.application.id, $scope.application).success(function(data, status, headers, config) {
			$('#button-'+ typeOfOperation + '-app').button('reset');
			$('.btn').prop("disabled",false);
			if (status == 201) {
				$('#' + typeOfOperation + 'MessageModal').on('hidden.bs.modal', function () {
					$window.location.href = data.uri;
				});
				
				$('#' + typeOfOperation + 'MessageModal').modal('show');
			}
		}).error(function(errorData) {
			console.log("Erro ao obter dados da aplicação: " + errorData);
			alert(errorData.message);
		});
	};

	$scope.iconPath = function() {		
		return $scope.application.icon.relativePath ? "/" + $scope.application.icon.relativePath + "?" + $scope.application.icon.size: $scope.DEFAULT_ICON_RELATIVE_PATH;
	};
	
	$scope.newVersion = function(){
		if (!$scope.application.currentVersion)
			$scope.application.currentVersion = new Object();
		
		if(!$scope.application.currentVersion.number) {
			$scope.versionNumberRequired = true;
			return;
		}
		
		if(!$scope.application.currentVersion.date) {
			$scope.versionDateRequired = true;
			return;
		}
		
		if ($scope.application.versions.length === 0){
			$scope.application.versions.push(angular.copy($scope.application.currentVersion)); 
		} else {
			$scope.application.versions.unshift(angular.copy($scope.application.currentVersion)); 
		}
		
		$scope.application.currentVersion.number = '';
		$scope.application.currentVersion.date = '';
		$scope.application.currentVersion.description = '';
		$('input[name="versionNumber"]').focus();
	}; 
	
	$scope.cleanVersionMsg = function() {
	    $scope.versionDateRequired = false;
	    $scope.versionNumberRequired = false;
	    $scope.editVersionNumberRequired = false;
	    $scope.editVersionDateRequired = false;
	};
	
	$scope.editVersion = function(version){

		$scope.referenceVersionEdited = version;
		$scope.currentVersionToEdit = angular.copy(version);
		$('#modal-edit-version').modal('show');
	}
	
	$scope.deleteVersion = function(version){
		var index = $scope.application.versions.indexOf(version);
		if(index != -1)
			$scope.application.versions.splice(index,1);
		
	}
	
	$scope.okEditVersion = function(){
		if (!$scope.currentVersionToEdit)
			$scope.currentVersionToEdit = new Object();
		
		if(!$scope.currentVersionToEdit.number) {
			$scope.editVersionNumberRequired = true;
			return;
		}
		
		if(!$scope.currentVersionToEdit.date) {
			$scope.editVersionDateRequired = true;
			return;
		}
		$scope.referenceVersionEdited.description = $scope.currentVersionToEdit.description;
		$scope.referenceVersionEdited.number = $scope.currentVersionToEdit.number;
		$scope.referenceVersionEdited.date = $scope.currentVersionToEdit.date;
		$('#modal-edit-version').modal('hide');
	}

	

}]);

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.
var ModalInstanceCtrl = function ($scope, $modalInstance) {
  $scope.yes = function () {
    //$modalInstance.close($scope.selected.item);
    $modalInstance.close();
  };

  $scope.no = function () {
    //$modalInstance.dismiss('cancel');
    $modalInstance.dismiss();
  };
};
