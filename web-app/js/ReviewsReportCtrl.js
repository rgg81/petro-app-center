var trend = angular.module("trendApp", ['mainApp','ngSanitize']);

trend.controller('reviewsReportCtrl', ['$scope', '$http', 'httpNoCache', function($scope, $http , httpNoCache) {
	$scope.day = null;
	$scope.loading = false;
	$scope.url = null;

	$scope.list = function(start,end,accept) {
		if(!accept){
			accept = 'application/json';
		}
		$scope.loading = true;
		$scope.day = null;
		$scope.url = "/petro-app-center/review/groupedByDay" + (start ? "?start=" + start : "") + (end ? "&end=" + end : "");
		$scope.urlCsv =  $scope.url + "&format=csv";

		httpNoCache.get($scope.url,{headers: {'Accept': accept}}).success(function(data) {

 			var points = [];
	        for(i in data){
	        	var x = moment(i + " 00:00:00 +0000", "DD/MM/YYYY h:mm:ss Z");
	            points.push([x.valueOf(),data[i].count]);
	        }
	        
			$scope.data = data;
			drawReport(points,start,end); 
			$scope.loading = false;

		}).error(function(errorData) {
			console.log("Erro ao obter lista de erros de cadastros: " + errorData);
			$scope.loading = false;
		});
	};

	$scope.onDaySelected = function(dayInMilli){
		var mDay = moment(dayInMilli).zone("00:00");
		$scope.day  = mDay.format("DD/MM/YYYY");
		$scope.$apply();
	}

}]);
