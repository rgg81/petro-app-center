var reindexApps = angular.module("searchAdminApp", ['mainApp', 'animations', 'ngSanitize']);

reindexApps.controller('searchAdminCtrl', ['$scope', '$http', '$interval', function($scope, $http, $interval) {
	$scope.indexes; 
	$scope.message = ""; 

	$scope.deleteIndex = function(indexName) {
		//TODO: pedir confirmação
		$http.delete("/petro-app-center/SearchAdmin/deleteIndex/" + indexName).success(function(data, status, headers, config) {
			logAll(data, status, headers, config)

			$scope.list(); 
		}).error(function(errorData) {
			console.log("Erro ao excluir índice %s: %s", indexName, errorData);
			alert(errorData.message);
		});
	}; 

	$scope.installMappings = function(indexName) {
		$scope.message = "creating index " + indexName

		//TODO: pedir confirmação
		$http.post("/petro-app-center/SearchAdmin/installMappings/" + indexName).success(function(data, status, headers, config) {
			logAll(data, status, headers, config)

			$scope.message = ""; 

			$scope.list(); 
		}).error(function(errorData) {
			$scope.message = ""; 
			console.log("Erro ao excluir índice %s: %s", indexName, errorData);
			alert(errorData.message);
		});
	}; 

	$scope.indexDocs = function(index, classes) {
		$scope.message = "indexing " + index + ":" + classes.map(function(e){ return e.clazz }).join(separator=", "); 
console.log($scope.message);
console.log(index)
console.log(classes)

		var handle = $interval(function(){
			console.log("atualizando tela"); 
			$scope.list(); 
		}, 1000); 
		//TODO: pedir confirmação
		$http.post("/petro-app-center/SearchAdmin/indexDocs", classes).success(function(data, status, headers, config) {
			//logAll(data, status, headers, config)

			$scope.message = ""; 

			$interval.cancel(handle); 

			$scope.list(index); 
		}).error(function(errorData) {
			$scope.message = ""; 

			$interval.cancel(handle); 
			console.log("Erro ao excluir índice %s: %s", indexName, errorData);
			alert(errorData.message);
		});
	}; 

	$scope.list = function(index) {
		$http.get("/petro-app-center/SearchAdmin/list?index=" + index).success(function(data, status, headers, config) {
			logAll(data, status, headers, config)
			$scope.indexes = data.indexes; 
		}).error(function(errorData) {
			console.log("Erro ao obter lista índices: " + errorData);
			alert(errorData.message);
		});
	}; 

	function logAll(data, status, headers, config) {
		console.log(data)
		//console.log(status)
		//console.log(headers)
		//console.log(config)
	}

	$scope.list();
}]);
