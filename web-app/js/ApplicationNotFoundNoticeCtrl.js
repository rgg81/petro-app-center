var applicationNotFoundNoticeApp = angular.module("applicationNotFoundNoticeApp", ['mainApp']);

applicationNotFoundNoticeApp.controller('applicationNotFoundNoticeCtrl', ['$scope', '$http', 'httpNoCache', function($scope, $http , httpNoCache) {
	$scope.notices; 

	$scope.list = function() {
		httpNoCache.get("/petro-app-center/applicationNotFoundNotice/all").success(function(notices) {
			$scope.notices = notices;
		}).error(function(errorData) {
			console.error("Erro ao obter lista de aplicações não encontradas. ", errorData);
		});
	};

	$scope.init = function() {
		$scope.list();	
	};

	$scope.init();

}]);
