'use strict';

/* Controllers */
function AppListCtrl($scope, $http) {
  $http.get('data/apps.js').success(function(data) {
	$scope.apps = data;
	var apps2 = new Array()
	for (var i = 0; i < 8; i++) {
		apps2.push(data[i]);
	}

	$scope.apps2 = apps2;
  });
 
  $scope.orderProp = 'id';
}