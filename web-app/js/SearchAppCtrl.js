var searchApp = angular.module("searchApp", ['mainApp']);

searchApp.controller('searchAppCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.applicationNotFoundNotice = { url: "", description: ""};
	$scope.disableButton = false;
	
	var originalApplicationNotFoundNotice = angular.copy($scope.applicationNotFoundNotice);

	$scope.save = function() {
		$scope.disableButton = true
		$http.post("/petro-app-center/ApplicationNotFoundNotice/save", $scope.applicationNotFoundNotice).success(function(data, status, headers, config) {
			if (status == 201) {
				$('#application-not-found').modal('hide');			
				
				$('#alertSuccessfullySubmittedApplicationNotFoundNotice').modal('show');
				
				$scope.applicationNotFoundNotice = angular.copy(originalApplicationNotFoundNotice);
				
				$scope.addApplicationNotFoundNoticeForm.$setPristine();
				$scope.disableButton = false;
			}
		}).error(function(errorData) {
			console.log("Erro: " + errorData);
			alert(errorData.message);
			$scope.disableButton = false;
		});
	}

}]);