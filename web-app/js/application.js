if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

// Avoid `console` errors in browsers that lack a console.
(function() {
	var method;
	var noop = function () {};
	var methods = [
	'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
	'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
	'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
	'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});
	while (length--) {
		method = methods[length];
		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

function addAutoCompleteToTag(){
	$('.autocomplete-tag').typeahead([
	{
		name: 'best-picture-winners',
		remote: '/petro-app-center/tag/find?name=%QUERY'
	}
	]);
}
addAutoCompleteToTag();

$("#edit-button").click(function() {	
	var reordered = function($elements) {
		var jsonArr = [];
		for (var i=0;i<$elements.length;i++) { 
			jsonArr.push({
		        applicationId: $($elements[i]).data('applicationid'),
		        position: i
		    });
		}
		
		$.ajax({
			  type: "POST",
			  url: "/petro-app-center/userApplication/updateAppPosition",
			  data: JSON.stringify(jsonArr),
			  processData: false,
			  contentType: "application/json; charset=UTF-8" 
		}).done(function( msg ) {
		    console.log( msg );
		});	
	};
	
	$('.gridly .petro-app').css({"margin-top":"0","margin-bottom":"0"});
	$('.gridly .petro-icon-crop').addClass("tossing");
	$('.gridly .petro-remove-button').fadeIn('slow');

	if (!$('.gridly .petro-app').hasClass('petro-edit-my-apps')) {
		$('.gridly').gridly({
			base: 131, // px 
			gutter: 15, // px
			columns: 8,
			callbacks:{reordered: reordered}
		}).gridly('draggable', 'on');
		
		$('.gridly .petro-app').addClass("petro-edit-my-apps");
		$("#edit-button").fadeOut(function() {
			$(this).text("sair")
		}).fadeIn('fast');
	} else {
		$('.gridly').gridly('draggable', 'off');
		$('.gridly .petro-app').removeClass("petro-edit-my-apps");
		$('.gridly .petro-icon-crop').removeClass("tossing");
		$('.gridly .petro-remove-button').fadeOut('slow');
		$("#edit-button").fadeOut(function() {
			$(this).text("editar");
			$(this).blur();
		}).fadeIn('fast');
	}
});

