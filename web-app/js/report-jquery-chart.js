var drawReport = function(points,start,end){
    var one_day = 24 * 3600 * 1000;
    
    var startDate = moment(start+ " 00:00:00 +0000", "DD/MM/YYYY h:mm:ss Z");
    var endDate = moment(end+ " 23:59:59 +0000", "DD/MM/YYYY h:mm:ss Z");

    //console.log(startDate.toDate());
    //console.log(endDate.toDate());
    var xrange = endDate.diff(startDate, 'days'); 
    Highcharts.setOptions({
        lang: {
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',  'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        },
        tooltip:{
            xDateFormat: '%A, %d %B %Y'
            //headerFormat: '<span style="font-size: 10px">{point.x}</span><br/>' 
        }
    });

    $('#graph').highcharts({
        
        credits :{
            enabled: false
        },
        title: {
            text: 'Avaliações'
        },

        subtitle: {
            text: 'Período: ' + start + ' - ' + end
        },

        xAxis: {
            type: 'datetime',
            //tickInterval: (xrange < 7 ? 1 : 7) * one_day, // one week
            tickWidth: 0,
            gridLineWidth: 1,
            minTickInterval: 1 * one_day,         
            labels: {
                align: 'left',
                x: 3,
                y: -3
            },
            //startOnTick: false,
            min: startDate.subtract(1,'day').valueOf(),
            max: endDate.valueOf()

        },

        yAxis: [{ // left y axis
            title: {
                text: null
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false,
            startOnTick: false,
            min:0

        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            floating: true,
            borderWidth: 0
        },

        tooltip: {
            shared: true,
            crosshairs: true
        },

        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            angular.element('#reviewsReportCtrl').scope().onDaySelected(this.x);
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                },
                events:{          
                    legendItemClick: function () {
                        return false;
                    }
                },
                dataLabels: { enabled: true }
            }       
        },      

        series: [{
            name: 'Número de Avaliações',
            lineWidth: 4,
            marker: {
                radius: 4
            },
            data: points
        }]
    });
}
