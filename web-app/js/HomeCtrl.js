var homeApp = angular.module("homeApp", ['mainApp', 'animations', 'ngSanitize']);

//controller somente do "Minhas Aplicações"
homeApp.controller('MyAppsCtrl', ['$rootScope','$scope', '$http', '$window', '$element', 'userAppService', 'httpNoCache', function($rootScope, $scope, $http, $window, $element, userAppService,httpNoCache) {
	//action myapps
	$rootScope.hashMyApps = new Object();
	$rootScope.myApps = null;
	$scope.sharesApps = false;
	$scope.list = {}
	$scope.list.action = "myApps";
	$scope.elementTooltip = null;

	
	$scope.loadMyApps = function(opts) {		
		httpNoCache.get('/petro-app-center/user/myApplications', 
					{headers: {'Content-Type': 'utf-8'}}).success(function(result) {
			$rootScope.myApps = result.applications;
			for (i in result.applications) {
				$rootScope.hashMyApps[result.applications[i].id] = result.applications[i];
			}
			if (opts && opts.init) {
				$rootScope.$broadcast('myAppsLoaded');
			} 
			
		}).error(function(errorData) {
			console.log("Erro ao obter minhas aplicações: " + errorData);
		});
	}

	$scope.checkHeight = function() {
		if ($("#myapps > ul").css("height") > "0px") {
			$("#myapps > ul").css("height","auto");
		}
	}
	$scope.$on('reloadMyApps', function(event) {
		$scope.loadMyApps();
		$scope.checkHeight();
		$('.gridly .petro-app').removeClass("petro-edit-my-apps");		
		$("#edit-button").fadeOut(function() {
			$(this).text("editar");
			$(this).blur();
		}).fadeIn('fast');	

	});

	

 	// action myapps
	$scope.removeUserApplication = function(appId,event) {
		userAppService.remove(appId).success(function(data, status, headers, config) {
			if (status == 201) {
				delete $rootScope.hashMyApps[appId];
				//NAO HA remoção no $rootScope.myApps (logo, não há sincronismo entre hashMyApps e myApps), então a remoção é feita VISUALMENTE APENAS
				//NOT TO DO: não estamos afim de fazer um loop pra isso por considerar pointless, não gostou, azar o seu.
				$(event.target).closest('.brick').fadeOut('slow').remove();	
		    	$('.gridly').gridly('layout');
		    	if ($.isEmptyObject($rootScope.hashMyApps)) {
					$rootScope.$broadcast('reloadMyApps');
					$("#myapps > ul").css("height","0px");
				}
		    	
			}
		}).error(function(errorData) {
			console.log("Erro ao adicionar aplicação: " + errorData);
		});

	}

	$scope.getShareInformation = function() {
		httpNoCache.get('/petro-app-center/user/checkShare', 
			{headers: {'Content-Type': 'utf-8'}}).success(function(result) {
				$scope.sharesApps = result.shareUserApplications;
			}).error(function(errorData) {
				console.log("Erro obtendo informacoes sobe compartilhamento de aplicacoes " + errorData)

			});


	}

	$scope.shareMyApplications = function() {
		if ($scope.sharesApps) {
			httpNoCache.get('/petro-app-center/user/share', 
			{headers: {'Content-Type': 'utf-8'}}).success(function(result) {
				$scope.sharesApps = result.shareUserApplications;
			}).error(function(errorData) {
				console.log("Erro ao compartilhar Minhas Aplicacoes " + errorData)

			});
		} else {
			httpNoCache.get('/petro-app-center/user/unshare', 
			{headers: {'Content-Type': 'utf-8'}}).success(function(result) {
				$scope.sharesApps = result.shareUserApplications;
			}).error(function(errorData) {
				console.log("Erro ao suspender o compartilhamento de Minhas Aplicacoes " + errorData)

			});
		}

	}
	$scope.loadMyApps({init:true});
	$scope.getShareInformation();


}]);


//Controller para os demais casos...
homeApp.controller('HomeCtrl', ['$rootScope','$scope', '$http', '$window', '$element', 'userAppService', 'httpNoCache', function($rootScope, $scope, $http, $window, $element, userAppService,httpNoCache) {
	/*
	offset: 
	limit: número de registros a ser recuperado, usado como tamanho da página nesse caso.
	current: página corrente. 
	last: última página. 
	*/
	
	$scope.list = {id:null, pagination: {offset:0, limit:0, count:0, current: 0, last: 0}, action:null, user: null, applications:null};



	$scope.init = function(actionName, listId, limit, user) {
		if (!actionName) alert("actionName is required!!")

		$scope.list.action = actionName;

		//TODO: Usar o element do controller ao invés do id
		if (!listId) alert("listId is required!!");

		$scope.list.id = listId;

		$scope.list.pagination.limit = limit ? limit : 16;
		
		//TODO: Não funciona!
		$scope.getShowMoreButton().button('loading');
		if (actionName == "userapps") {
			$scope.list.user = user;
		}
	}


	$scope.$on('myAppsLoaded', function(event) {
		$scope.loadMore();

	});


	
	$scope.loadMore = function() {
		var url = null;
		if ($scope.list.user) {
			url = '/petro-app-center/user/userApps/' + $scope.list.user;
		} else if($scope.list.action == "myMostAccessed"){
			url = '/petro-app-center/user/mostAccessedApps';
		}else {
			url = '/petro-app-center/application/' + $scope.list.action;
		}
		httpNoCache.get(url + '?limit=' + $scope.list.pagination.limit + '&offset=' + $scope.list.pagination.offset, 
				{headers: {'Content-Type': 'utf-8'}}).success(function(result) {
			$scope.list.applications = $scope.list.applications ? $scope.list.applications.concat(result.applications) : result.applications;
			$scope.list.pagination.count = result.count;

			$scope.list.pagination.last = Math.floor(result.count / $scope.list.pagination.limit) + ((result.count % $scope.list.pagination.limit == 0) ? 0 : 1);

			$scope.list.pagination.current++;

			var element = $scope.getShowMoreButton();
			if ($scope.list.pagination.current == $scope.list.pagination.last ) {
				element.remove();
			} else {
				element.button('reset');
			}

			$scope.list.pagination.offset += $scope.list.pagination.limit; 
		}).error(function(errorData) {
			console.log("Erro ao obter mais aplicações: " + errorData);
		});
	}

	$scope.getShowMoreButton = function() {
		//TODO: Usar o element do controller ao invés do id
		return $('#' + $scope.list.id + ' > .showmore')		
	}
	
	$scope.more = function() {
		$scope.getShowMoreButton().button('loading');

		$scope.loadMore();
	}
	
	//actions mais populares e melhor avaliados
	$scope.addUserApplication = function(appId,event) {
		userAppService.add(appId).success(function(data, status, headers, config) {
			if (status == 201) {
				if(!Modernizr.cssanimations) {
					$(event.target).parent().parent().nextAll(".petro-app-added").removeClass("hidden").css("opacity",0).animate({ opacity: 1 }, 1500).animate({ opacity: 1 }, 1000).animate({ opacity: 0 }, 2500);
				}
				else {
					$(event.target).parent().parent().nextAll(".petro-app-added").removeClass("hidden").addClass("show").addClass("fadeIn");
				}
				$rootScope.$broadcast('reloadMyApps');
			}
		}).error(function(errorData) {
			console.log("Erro ao adicionar aplicação: " + errorData);
		});
	}

}]);

function showMiniButtons(element) {
	element.find(".petro-app-minibuttons").first().removeClass("slide");
}

function hideMiniButtons(element) {
	element.find(".petro-app-minibuttons").first().addClass("slide");
}

homeApp.directive('hoverApp', function ($compile,$templateCache) {
    return {
        link: function ($scope, element, attrs) {
			element.hoverIntent( 
					function(){
						showMiniButtons(element);						
					}, 						
					function() {
						if ($scope.elementTooltip) {
							if (document.getElementsByClassName("tooltipster-base").length == 0) {
								hideMiniButtons(element);
							}
						} else {
							hideMiniButtons(element);
						}
					}
			);			
        }
    };
});

homeApp.directive('helpApp', function ($compile,$templateCache) {
    return {
        link: function ($scope, element, attrs) {
        	element.tooltipster({
				functionInit : function(origin, content) {
			   		return $('<span>' + $(origin).attr('title') + '</span><div class="petro-more-help"><a href="/petro-app-center/help#' + $(origin.context).data('helpref')+ '">Posso ajudar?</a></div>');
				},
		       animation: 'fade',
			   delay: 200,
			   theme: 'tooltipster-default',
			   touchDevices: true,
			   trigger: 'hover',
			   interactive: 'boolean',
			   functionBefore: function(origin,continueTooltip) {
			   		continueTooltip();
			   		$scope.elementTooltip = this.closest(".petro-app")
			   },
			   functionAfter: function(origin){

			   	if (!$scope.elementTooltip.is(":hover"))
			   		hideMiniButtons($scope.elementTooltip);
            	}

		    });
        }
    };
});

