var reportApp = angular.module("reportApp", ['mainApp', 'animations', 'ngSanitize']);

reportApp.controller('reportCtrl', ['$scope', '$http', 'httpNoCache', function($scope, $http , httpNoCache) {
	$scope.reports; 

	$scope.list = function() {
		httpNoCache.get("/petro-app-center/report/all").success(function(reports) {
			$scope.reports = reports;
		}).error(function(errorData) {
			console.log("Erro ao obter lista de erros de cadastros: " + errorData);
		});
	};

	$scope.init = function() {
		$scope.list();	
	};

	$scope.init();

}]);
