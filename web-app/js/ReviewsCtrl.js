var reviewsApp = angular.module("reviewsApp", ['mainApp', 'animations', 'ngSanitize']);

reviewsApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
reviewsApp.controller('replyCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
	$scope.title;
	$scope.text;

	
}]);

reviewsApp.controller('reportCtrl', ['$scope', '$http', '$window', 'httpNoCache', function($scope, $http, $window, httpNoCache) {
	$scope.issues; 
	$scope.description; 

	$scope.loadIssues = function() {
		httpNoCache.get("/petro-app-center/report/listIssues").success(function(issues) {
			$scope.issues = issues;
		}).error(function(errorData) {
			console.log("Erro ao obter lista de problemas: " + errorData);
		});
	}

	$scope.save = function() {
		var report = {};
		report.applicationId = $scope.appId;
		report.description = $scope.description;

		report.issues = $scope.issues.filter(function(value){
			return value.checked;
		}).map(function(value){
			delete value.checked;
			return value;
		});

		$http.post("/petro-app-center/report/save", report).success(function(data, status, headers, config) {
			if (status == 201) {
				$('#report-issue').modal('hide');			
				
				$scope.description = "";

				$('#alertSuccessfullySubmittedReport').modal('show');
			}
		}).error(function(errorData) {
			console.log("Erro: " + errorData);
			alert(errorData.message);
		});
	}

	$scope.isFormInvalid = function(){
		return !(((typeof $scope.description != 'undefined') && ($scope.description.length > 0)) ||  ((typeof $scope.issues != 'undefined') && $scope.issues.some(function(value){
			return (typeof value.checked != 'undefined') && value.checked
		})));
	};
	
	$scope.loadIssues();	
}]);

reviewsApp.controller('userApplicationCtrl', ['$scope', '$http', '$window', function($scope, $http, $window) {
	$scope.operation = 'undefined'; 

	$scope.shouldShow = function(isMyApplication, operation){
		if ($scope.operation === 'undefined')
			$scope.operation = (isMyApplication ? "remove" : "add"); 

		return $scope.operation === operation; 
	}; 
	$scope.addUserApplication = function(e) {
		$('#addUserApplicationMessageModal').on('hidden.bs.modal', function () {
			$scope.operation = "remove"; 
			// Necessário pelo jquery acima, não é um evento controlado pelo angular
			$scope.$apply(); 
		});
		$http.post("/petro-app-center/application/addUserApplication/" + $scope.appId).success(function(data, status, headers, config) {
			if (status == 201) {
				$('#addUserApplicationMessageModal').modal('show');
			}
		}).error(function(errorData) {
			console.log("Erro ao obter dados da aplicação: " + errorData);
			alert(errorData.message);
		});
	}
	
	$scope.removeUserApplication = function(e) {
		$('#removeUserApplicationMessageModal').on('hidden.bs.modal', function () {
			$scope.operation = "add"; 
			// Necessário pelo jquery acima, não é um evento controlado pelo angular
			$scope.$apply(); 
		});
		$http.post("/petro-app-center/application/removeUserApplication/" + $scope.appId).success(function(data, status, headers, config) {
			if (status == 201) {
				$('#removeUserApplicationMessageModal').modal('show');
			}
		}).error(function(errorData) {
			console.log("Erro ao obter dados da aplicação: " + errorData);
			alert(errorData.message);
		});
	}
}]);

reviewsApp.controller('ReviewsCtrl', ['$scope', '$http', '$window', 'userAppService','httpNoCache',function($scope, $http, $window, userAppService,httpNoCache) {
	// TODO: Atualizar lastReviews conforme incluir/alterar/excluir myReview
	$scope.lastReviews = [];
	$scope.mostLiked = [];
	$scope.myReview = null;
	$scope.stars = [5, 4, 3, 2, 1];
	$scope.metadata = [];
	$scope.pagination = {offset:0, limit:5};
	$scope.hasMore = true;
	$scope.myReply = null;
	$scope.editingReply = null;
	$scope.creatingReply = null;

	$scope.reviewsByRating = [];
	$scope.reviewsByRatingPagination = {offset: 0, limit: 5};
	$scope.reviewsByRatingHasMore = true;
	$scope.numberOfStars = [1,2,3,4,5,6]

	$scope.init = function(id,key) {
		$scope.loggedUser.id = id;
		$scope.loggedUser.key = key;
	}

	// Obtém as reviews mais recentes ("lastReviews")
	$scope.list = function() {
		var limit = $scope.pagination.limit + $scope.pagination.offset
		httpNoCache.get("/petro-app-center/review/recent?applicationId=" + $scope.appId + "&limit=" + limit).success(function(reviews) {
			$scope.lastReviews = reviews;

			$scope.updateHasMore(reviews);
			$scope.editingReply = null;	
			$scope.creatingReply = null;		
		}).error(function(errorData) {
			console.log("Erro ao obter lista de reviews: " + errorData);
		});
	}
	
	// Obtém os melhores reviews ("mostliked")
	$scope.loadMostLiked = function() {
		httpNoCache.get("/petro-app-center/review/mostLiked?applicationId=" + $scope.appId).success(function(reviews) {
			$scope.mostLiked = reviews;
		}).error(function(errorData) {
			console.log("Erro ao obter melhores reviews: " + errorData);
		});
	}

	$scope.loadMore = function() {
		$scope.pagination.offset = $scope.pagination.offset + $scope.pagination.limit; 

		httpNoCache.get("/petro-app-center/review/recent?applicationId=" + $scope.appId + "&limit=" + $scope.pagination.limit + "&offset=" + $scope.pagination.offset).success(function(reviews) {
			$scope.lastReviews = $scope.lastReviews.concat(reviews)

			$scope.updateHasMore(reviews);			
		}).error(function(errorData) {
			console.log("Erro ao obter mais reviews: " + errorData);
		});
	}

	$scope.updateHasMore = function(reviews) {
		if (reviews.length < $scope.pagination.limit) {
			$scope.hasMore = false;
		} else {
			$scope.more();
		}		
	}

	$scope.more = function() {
		var offset = $scope.pagination.offset + $scope.pagination.limit; 

		httpNoCache.get("/petro-app-center/review/recent?applicationId=" + $scope.appId + "&limit=1" + "&offset=" + offset).success(function(reviews) {
			$scope.hasMore = (reviews.length > 0);
		}).error(function(errorData) {
			console.log("Erro ao obter mais reviews: " + errorData);
		});
	}

	// Obtém a review do usuário ("myReview")
	$scope.loadMy = function() {
		httpNoCache.get("/petro-app-center/review/my?applicationId=" + $scope.appId).success(function(myReview) {
			if (status != 204){
				$scope.myReview = myReview;
			}
		}).error(function(errorData) {
			console.log("Erro ao obter my-review: " + errorData);
		});
	}

	$scope.$watch('myReview', function(newValue, oldValue) {
		$scope.loadMostLiked();
		$scope.list();
	}, true);

	$scope.addReview = function() {
		//if (! $scope.myNewReview ) {
			$scope.myNewReview = {"rating": 0, "title":"", "text":"", errors: null};
			$scope.cleanStars();
		//}
	}
	
	$scope.saveNewReview = function() {
		if ( $scope.myNewReview.id ) {
			// Editar
			$scope.myNewReview.application = $scope.appId
			$http.put("/petro-app-center/review/" +  $scope.myNewReview.id, $scope.myNewReview).success(function(myReview) {
				$scope.myReview = myReview;
				$scope.myNewReview = null ;
				$("#newReview").modal("hide");
			}).error(function(errorData) {
				console.log("Erro ao editar o review: " + errorData);
				
				if ( errorData.errors ) {
					$scope.myNewReview.errors = errorData.errors; 					
				} else {
					alert(errorData.message);
				}
			});
		} else {
			// Inserir
			$scope.myNewReview.application = $scope.appId
			$http.post("/petro-app-center/review", $scope.myNewReview).success(function(myReview) {
				$scope.myReview = myReview;
				$scope.myNewReview = null ;
				$("#newReview").modal("hide");
			}).error(function(errorData) {
				console.log("Erro ao inserir novo review: " + errorData);
				
				if ( errorData.errors ) {
					$scope.myNewReview.errors = errorData.errors; 					
				} else {
					alert(errorData.message);
				}
			});
		}
	}
	
	$scope.editMyReview = function() {
		$scope.myNewReview = {
				id: $scope.myReview.id,
				rating: $scope.myReview.rating,
				title: $scope.myReview.title,
				text: $scope.myReview.text
		}
		$scope.showStars($scope.myNewReview.rating, null);
	}
	
	$scope.removeMyReview = function(sucessCallback, failureCallback) {
		if ( $scope.myReview ) {
			$http.delete("/petro-app-center/review/" + $scope.myReview.id).success(function(myReview) {
				$scope.myReview = null;
		//		$scope.myNewReview = null;
				$scope.cleanStars();

				if (!(typeof sucessCallback === "undefined")) sucessCallback();
			}).error(function(errorData) {
				console.log("Erro ao excluir my-review: " + errorData);

				if (!(typeof failureCallback === "undefined")) failureCallback();
			});
		}
	}

	$scope.remove = function(review, sucessCallback, failureCallback) {
		if (!(typeof review === "undefined")) {
			if ($scope.myReview && (review.id == $scope.myReview.id)) {
				$scope.removeMyReview (sucessCallback, failureCallback); 
			} else {
				$http.delete("/petro-app-center/review/moderate/" + review.id).success(function() {
					var index = $scope.lastReviews.indexOf(review);
	
					if (index > -1) $scope.lastReviews.splice(index, 1);
	
					if ($scope.mostLiked.some(function(currentValue, index, array) {return (this.user.key == currentValue.user.key)}, review)) {
						$scope.loadMostLiked();
					}

					sucessCallback();
				}).error(function(errorData) {
					console.log("Erro ao excluir review: " + errorData);
	
					failureCallback(errorData);
				});
			}
		}

	}

	$scope.dislike = function(review, event, hideModal) {
		$http.put("/petro-app-center/review/dislike", review).success(function(myReview) {
			$scope.loadMy();
			$scope.loadMostLiked();
			$scope.list();

		}).error(function(errorData) {
			console.log("Erro ao gostar de review: " + errorData);
			alert(errorData.message)
		});
	}

	$scope.like = function(review, event, hideModal) {
		$http.put("/petro-app-center/review/like", review).success(function(myReview) {
			$scope.loadMy();
			$scope.loadMostLiked();
			$scope.list();
		}).error(function(errorData) {
			console.log("Erro ao gostar de review: " + errorData);
			alert(errorData.message)
		});
	}
	
	$scope.rate = function(numberOfStars, event) {
		$scope.myNewReview.rating = numberOfStars;

		$(event.target.parentNode).children().each(function(index) {			
			if (index > (4 - numberOfStars)) {
				$(this).addClass("petro-big-rating-on");
			} else {
				$(this).removeClass("petro-big-rating-on");
			}
		});
	}

	$scope.cleanStars = function() {
		$("div.petro-big-rating > span").removeClass("petro-big-rating-on");		
	}

	$scope.showStars = function(numberOfStars, element) {
		if (!element) {
			element = $("div.petro-big-rating > span");
		}

		element.each(function(index) {			
			if (index > (4 - numberOfStars)) {
				$(this).addClass("petro-big-rating-on");
			} else {
				$(this).removeClass("petro-big-rating-on");
			}
		});
	}

	$scope.loadMy();

	$scope.loadMetadata = function() {
		httpNoCache.get("/petro-app-center/review/metadata").success(function(metadata) {
			$scope.metadata = metadata;
		}).error(function(errorData) {
			console.log("Erro ao obter metadados de review: " + errorData);
		});
	}

	$scope.showReviewsByRating = function(numberOfStars) {
		var limit = $scope.reviewsByRatingPagination.limit + $scope.reviewsByRatingPagination.offset
		httpNoCache.get("/petro-app-center/review/byRating?applicationId=" + $scope.appId + "&limit=" + limit + "&rating=" + numberOfStars).success(function(reviews) {
			$scope.reviewsByRating = reviews;

			//$scope.updateHasMore(reviews);			
		}).error(function(errorData) {
			console.log("Erro ao obter lista de reviews: " + errorData);
		});
	}

	$scope.editReply = function(replyId) {
		$scope.creatingReply = null;
		$scope.editingReply = replyId;
	}

	$scope.newReply = function(reviewId) {
		$scope.editingReply = null;
		$scope.creatingReply = reviewId;
	}

	$scope.cancelEdit = function() {
		$scope.editingReply = null;	
		$scope.creatingReply = null;
	}

	$scope.saveReply = function(review) {
		$scope.myReply = {}
		if (review.reply.id)
			$scope.myReply.id = review.reply.id;
		$scope.myReply.title = review.reply.title;
		$scope.myReply.text = review.reply.text;
		$scope.myReply.commentId = review.id


		if ($scope.myReply.id) {
			$http.put("/petro-app-center/reply/" +  $scope.myReply.id, $scope.myReply).success(function(data, status, headers, config) {
				if (status == 201) {
					$scope.loadMy();
					$scope.loadMostLiked();
					$scope.list();			
				}
			}).error(function(errorData) {
				console.log("Erro: " + errorData);
				alert(errorData.message);
			});
		} else {
			$http.post("/petro-app-center/reply/", $scope.myReply).success(function(data, status, headers, config) {
				if (status == 201) {
					$scope.loadMy();
					$scope.loadMostLiked();
					$scope.list();
				}
			}).error(function(errorData) {
				console.log("Erro: " + errorData);
				alert(errorData.message);
			});
		}
	}

	$scope.deleteReply = function(replyId) {
		$http.delete("/petro-app-center/reply/" + replyId).success(function() {
				$scope.loadMy();
				$scope.loadMostLiked();
				$scope.list();
			}).error(function(errorData) {
				console.log("Erro: " + errorData);
				alert(errorData.message);
			});
	}

	$scope.loadMetadata()
	
}]);