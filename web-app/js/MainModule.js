var mainModule = angular.module("mainApp", ['ui.bootstrap']);

mainModule.config([
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|notes|file):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);

mainModule.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
// http://project-fifo.net/display/PF/Cool+AngularJS+Snippets
mainModule.directive('decorate', function() {
  return {
    link: function($scope, $element, $attributes) {
      $element.bind('keydown', function(event) {
        $element.toggleClass('text-warning', warning());
        $element.toggleClass('text-danger', danger());
      });

      $element.bind('keyup', function(event) {
        $element.toggleClass('text-warning', warning());
        $element.toggleClass('text-danger', danger());
      });

      function danger() {
      	return $element.val().length >= $element.attr("maxLength");
      }

      function warning() {
      	return $element.val().length >= $element.attr("maxLength") - ($attributes.resto ? $attributes.resto : 20);
      }
    }
  };
});

mainModule.directive('deleteApp', ['$http', function($http) {
	return {
		restrict:'E',
		template: '<button type="button" id="{{\'button-delete-app\' + appId}}" data-loading-text=" " ng-click="deleteApp()" ng-class="classBtn">' +
			'</button>' +
		'<div class="modal fade" id="{{\'deleteMessageModal\' + appId}}" tabindex="-1" role="dialog" aria-labelledby="delete message" aria-hidden="true">' +
		  '<div class="modal-dialog">' +
		   ' <div class="modal-content">' +
		   		'<div class="modal-header petro-modal-header">'+
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
					'<h4 class="modal-title petro-modal-title">Aplicações Petrobras</h4>' +
				'</div>' +
		      '<div class="modal-body petro-modal-body">' +
		      	'<p>' +
		      		'<ul class="alert petro-alert alert-info petro-alert-info petro-table-info">' +
		      			'<li>Aplicação será excluída com todas as avaliações! Normalmente esse tipo de ação é feito quando a aplicação é desativada e não estará mais disponível para os usuários. Tem certeza?</li>' +
		      		'</ul>' +
		      	'</p>' +
		      	'<p>' +
		      		'<ul class="alert petro-alert alert-success petro-alert-message">' +
		      			'<li>' +
			      		'<span id="{{\'message-delete-success\' + appId}}"></span>'+
			      		'</li>' +
			      	'</ul>' +
		      '</div>' +
		      '<div class="modal-footer petro-modal-footer petro-label-success">' +
		      	'<p><button data-dismiss="modal" id="{{\'button-cancel-delete-app\' + appId}}"  type="button" class="btn btn-default petro-my-options">Cancelar</button>' +
		      	'<button id="{{\'button-confirm-delete-app\' + appId}}" data-loading-text="Excluindo.." type="button" class="btn btn-success" ng-click="confirmDelete()">Excluir</button></p>' +
		      '</div>' +
		    '</div><!-- /.modal-content -->'+
		  '</div><!-- /.modal-dialog -->' +
		'</div><!-- /.modal -->',
		scope: {
		  // same as '=customer'
		  appId: '=',
		  classBtn: '@',
		  
		},
		controller: ['$scope', '$http','$window', function($scope, $http, $window) {
			$scope.deleteApp = function() {
				$('#deleteMessageModal' + $scope.appId).on('hidden.bs.modal', function () {
					$('#button-delete-app' + $scope.appId).button('reset');
				});
				
				$('#button-delete-app' + $scope.appId).button('loading');
				$('#deleteMessageModal' + $scope.appId).modal('show');
				
			}
			$scope.confirmDelete = function () {
				$('#button-confirm-delete-app' + $scope.appId).button('loading');  
				$('#button-cancel-delete-app' + $scope.appId).prop("disabled",true);

				$http.post("/petro-app-center/application/delete/" + $scope.appId).success(function(data, status, headers, config) {
						if (status == 201) {
							$('#message-delete-success' + $scope.appId).hide().html("Aplicação excluída com sucesso... Redirecionando para a página de administração.").fadeIn('slow');
							$('#message-delete-success' + $scope.appId).parents('ul').show();
							setTimeout(function(){$window.location.href = '/petro-app-center/admin/listApplications';},4000);
						}
					}).error(function(errorData) {
						console.log("Erro ao obter dados da aplicaćão: " + errorData);
						alert(errorData.message);
					});

			};

		}]
	}
	
}]);

mainModule.filter('newlines', function () {
    return function(text) {
    	if (text != null){
    		text = $('<i></i>').text(text).html();
        	return text.replace(/\n/g, '<br/>');
        }
    }
});

// Serve para corrigir os casos em que navegadores não suportam o atributo maxlentgh
/*
//TODO: Tratar casos de paste
Caso 1
Original: 1245
Pasted: abcde
Paste na posição 3
Esperado, como deveria ser: 12a45
Como está: 12abc

Caso 2
Original: 12345
Pasted: abcde
Paste com 3 selecionado
Esperado, como deveria ser: 12a45
Como está: Nada acontece, paste está usando preventDefault
*/

mainModule.directive('hackMaxLength', function() {
	function link($scope, $element, $attributes) {
		var value;
		var maxLength = $attributes.hackMaxLength;

		var pastehandler = function(event) { 
			if ($element.val().length >= maxLength) {
				event.stopPropagation();
				event.preventDefault();
			} 
		};

		if (!elementSupportsAttribute("textarea", "maxLength")) { 
			$element.bind('input', function(event) {
		        testMaxLength(event);
      		}); 
			
			$element.bind('paste', pastehandler); 
    	} 

		function testMaxLength(event) {
			if ($element.val().length > maxLength) {
				event.stopPropagation();
	          	event.preventDefault();	          	
				$element.val($element.val().substring(0, maxLength));
			}

			value = $element.val();
		}
	}

  	return {
		link: link
	};
});


function elementSupportsAttribute(element, attribute) {
	return (attribute in document.createElement(element)) 
};

mainModule.directive('ljapModalOkCancel', function() {	
	var directiveDefinitionObject = {
		restrict:'E',
		templateUrl: '/petro-app-center/html/angular-templates/ljapModalOkCancelTemplate.html', 
		scope: {
		  modalId: '@', 
		  title: '@', 
		  text: '@', 
		  successMessage: '@', 
		  successRedirectURL: '@', 
		  loadingText: '@', 
		  okButtonLabel: '@', 
		  cancelButtonLabel: '@', 
		  okAction: '&', 
		  object: '='		  
		},
		controller: ['$scope', '$window', '$element', function($scope, $window, $element) {
			$scope.modal = $('#' + $scope.modalId);
			$scope.save = function() {
				$('#button-ok-' + $scope.modalId).button('loading');  
				$('#button-cancel-' + $scope.modalId).prop('disabled', true);

				$scope.okAction( {object: $scope.object, sucessCallback: $scope.sucessCallback, failureCallback: $scope.failureCallback} );
			}

			$scope.sucessCallback = function() { 
				$scope.successMessage = $scope.successMessage ? $scope.successMessage : "Operação realizada com sucesso!"; 

				$('#message-delete-success-' + $scope.modalId).hide().html($scope.successMessage).fadeIn('slow');
				$('#message-delete-success-' + $scope.modalId).parents('ul').show();

				if ($scope.successRedirectURL) {
					setTimeout(function(){$window.location.href = $scope.successRedirectURL;},4000); 
				} else {
					$element.modal().modal('hide');
				}
			};

			$scope.failureCallback = function(errorData) { 
				alert(errorData.message);
			};

		}],
		compile: function compile($tElement, $tAttrs, $transclude) { 
			$tAttrs.okButtonLabel = $tAttrs.okButtonLabel ? $tAttrs.okButtonLabel : "ok"; 
			$tAttrs.cancelButtonLabel = $tAttrs.cancelButtonLabel ? $tAttrs.cancelButtonLabel : "cancelar";	

			return { 
				post: function postLink($scope, $iElement, $iAttrs, $controller) { 
					$iElement.bind('show.bs.modal', function (event) { 
						$scope.loadingText = $scope.loadingText ? $scope.loadingText : "Aguarde.."; 

						$('#button-ok-' + $scope.modalId).button('reset');
						$('#button-cancel-' + $scope.modalId).button('reset');
					});

					$iElement.bind('hide.bs.modal', function (event) { 
						// hack pra remover o backdrop
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();					
					});
				}
			}
		} 
	}	
	return directiveDefinitionObject;	
});

mainModule.factory('userAppService', ['$http',function(http) {
	var service = {};
	service.add = function(appId) {
		return http.post("/petro-app-center/application/addUserApplication/" + appId);
	};
	service.remove = function(appId) {
		return http.post("/petro-app-center/application/removeUserApplication/" + appId);
	}
	return service;
}]);


mainModule.factory('httpNoCache', ['$http',function(http) {
	var service = {};
	service.get = function(url,opts){
		if(!opts){
			opts = {};
		}
		
		if(!opts.params){
			opts.params = {};
		}
		
		opts.params['_'] = new Date().getTime();
		
		return http.get(url,opts);

	 };
	 return service;

}]);

mainModule.directive('registerAccess', function () {
    return {
        link: function($scope, element, attrs) {
  			var activityUrl = '/petro-app-center/activity/proxy?type=accessapp&appid='+attrs.appid+'&where='+attrs.where+'&url=' + encodeURIComponent(attrs.href);
   		 	element.bind('mousedown', function() {
				element.attr('href',activityUrl);
				element.unbind('mousedown');
    		});		
        }
    };
});




mainModule.factory('ljapEmpty', [function() {
	var service = {};

	var isEmpty = function(val){
		return (typeof val === "undefined" || val === null || ((typeof val === "string") && val.trim().length === 0))
	}; 

	service.all = function(objs, fnIsEmpty){
		if (typeof fnIsEmpty != "function") fnIsEmpty = isEmpty; 

		if (objs.length === 0 || fnIsEmpty(objs)) return true; 

		return objs.every(function(o){
			return fnIsEmpty(o); 
		}); 
	 };

	service.none = function(objs, fnIsEmpty){
		if (typeof fnIsEmpty != "function") fnIsEmpty = isEmpty; 

		if (objs.length === 0 || fnIsEmpty(objs)) return false;

		return objs.every(function(o){
			return !fnIsEmpty(o); 
		}); 
	 };

	 return service;

}]);


mainModule.directive('allOrNone', function(ljapEmpty) {
  return {
    require: 'ngModel', 
    link: function(scope, elm, attrs, modelCtrl) {
      modelCtrl.$parsers.unshift(function(viewValue) {
        var elements = []; 
        var values = [viewValue]; 
        
        $(elm[0].form).find('[all-or-none]').each(function(i, e){
          if (elm[0]===e) return true; 
          elements.push(scope.form[e.name])
          values.push(scope.form[e.name].$viewValue)
        }); 
        
        var allEmpty = ljapEmpty.all(values, modelCtrl.$isEmpty), noneEmpty = ljapEmpty.none(values, modelCtrl.$isEmpty); 

        var wereAllTheSame = (allEmpty ^ noneEmpty);  

        if (modelCtrl.$valid !== wereAllTheSame) 
          $(elm[0].form).find('[all-or-none]').each(function(i, e){
            scope.form[e.name].$setValidity('allOrNone', wereAllTheSame);
          });

        return viewValue; 
      });
    }
  };
});
/*
<g:javascript>
	$("[data-toggle=popover]").popover()
	
	$( document ).ready( function() {
  		$('#input-date-version').datepicker({
    		format: "dd/mm/yyyy",
    		language: "pt-BR"
		}).on('changeDate', function(ev){
		    $('#input-date-version').datepicker('hide');
		  });
	});
</g:javascript>
 ng-change="hideDatePicker()"

 	$scope.hideDatePicker = function() {
		$('#input-date-version').datepicker('hide');
	}


*/
mainModule.directive('ljapDatePicker', function() {
	return {
        restrict: 'A',
		require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
        	element = element[0]; 
			$(element).datepicker({
				format: "dd/mm/yyyy", 
				language: "pt-BR"
			}).on('changeDate', function(ev){
				$(element).datepicker('hide');

				scope.$apply(function() {
					ngModelCtrl.$setViewValue(element.value);
				});	
			});
        }
    }
});
