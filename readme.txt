Para fazer o setup do projeto, basta executar o comando "grails config-project" (sem aspas).
A console pedira 3 inputs do usuario:

1) caminho do diretorio onde ficam as configuracoes de projetos (ex.: D:/Projetos/libs). Apos o setup do projeto, ao
   subir a aplicacao, ela vai procurar pelos seguintes arquivos de configuracao:
    - <caminho entrado>/ljap/config.properties (ex.: D:/Projetos/libs/ljap/config.properties)
    - <caminho entrado>/ljap/log4j.properties (ex.: D:/Projetos/libs/ljap/config.properties)
    *IMPORTANTE*: repare que o script cria o diretorio "ljap" automaticamente, basta indicar o caminho do diretorio "libs".

2) caminho do diretorio onde ficam os logs de projetos (ex.: D:/Projetos/logs). Apos o setup do projeto, ao
   subir a aplicacao, os logs serao gerados na pasta:
       - <caminho entrado>/ljap/ljap.log (ex.: D:/Projetos/logs/ljap/ljap.log)
   *IMPORTANTE*: repare que o script cria o diretorio "ljap" automaticamente, basta indicar o caminho do diretorio "logs".

3) caminho do diretorio onde ficam os arquivos de projetos (ex.: D:/Projetos/appfiles). Apos o setup do projeto, ao
   subir a aplicacao, o upload das imagens sera feito para a pasta:
       - <caminho entrado>/ljap/uploaded_images (ex.: D:/Projetos/appfiles/ljap/uploaded_images)
   Alem disso, o context root ("/") do tomcat apontara para esta mesma pasta.
   *IMPORTANTE*: repare que o script cria o diretorio "ljap" automaticamente, basta indicar o caminho do diretorio "appfiles".

Apos informar esses 3 inputs, todas as pastas e configuracoes necessarias terao sido criadas e colocadas nas pastas corretas.
A partir dai, basta subir a aplicacao usando o comando "grails run-app" (sem aspas).