import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.myrrix.client.ClientRecommender;
import net.myrrix.client.MyrrixClientConfiguration;

import org.apache.mahout.cf.taste.recommender.RecommendedItem;


public class UserRecommend {
	public static void main(String[] args) throws Exception {
		MyrrixClientConfiguration clientConfig = new MyrrixClientConfiguration();
		clientConfig.setHost("localhost");
		ClientRecommender clientRecommender = new ClientRecommender(clientConfig);

		BufferedReader reader = new BufferedReader(new FileReader("bigresult.log"));
		String line = null;
		
		Map<Long, String> users = new HashMap<Long, String>();
		Map<Long, String> apps = new HashMap<Long, String>();
		
		int i = 0 ;
		while ((line = reader.readLine()) != null) {
		    String[] tuple = line.split("\t");
		    
		    users.put(new Long( tuple[1].hashCode() ), tuple[1]);
		    apps.put(new Long( tuple[2].hashCode() ), tuple[2]);
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Digite o usuário: ");
		
		String readId ;
		while ( (readId = in.readLine()) != null ) {
			try {
				List<RecommendedItem> recommendations = clientRecommender.recommend(readId.hashCode(), 5, false, (String[])null);
				for (RecommendedItem recommendedItem : recommendations) {
					System.out.println(apps.get(new Long(recommendedItem.getItemID())));
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
			System.out.print("\nDigite o usuário: ");
		}
	} 
}
