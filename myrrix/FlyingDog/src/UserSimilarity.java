import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.mahout.cf.taste.recommender.RecommendedItem;

import net.myrrix.client.ClientRecommender;
import net.myrrix.client.MyrrixClientConfiguration;
import net.myrrix.common.collection.FastIDSet;


public class UserSimilarity {
	public static void main(String[] args) throws Exception{		
	
		MyrrixClientConfiguration clientConfig = new MyrrixClientConfiguration();
		clientConfig.setHost("10.18.114.5");
		clientConfig.setPort(8081);
		ClientRecommender clientRecommender = new ClientRecommender(clientConfig);
	
		BufferedReader reader = new BufferedReader(new FileReader("bigresult.log"));
		String line = null;
		
		Map<Long, String> users = new HashMap<Long, String>();
		Map<Long, String> apps = new HashMap<Long, String>();
		
		int i = 0 ;
		while ((line = reader.readLine()) != null) {
		    String[] tuple = line.split("\t");
		    
		    users.put(new Long( tuple[1].hashCode() ), tuple[1]);
		    apps.put(new Long( tuple[2].hashCode() ), tuple[2]);
		}
		
		FastIDSet set = clientRecommender.getAllItemIDs();
		for (long id : set) {
			List<RecommendedItem> items = clientRecommender.mostSimilarItems(id, 5);
			System.out.print(id + "\t" + apps.get(id) + " [");
			int j = 0;
			for (RecommendedItem item : items) {
				System.out.print(item.getItemID() + "\t" + apps.get(item.getItemID()) + "\t" + item.getValue() );
				if(j<4) System.out.print(",");
				j++;
			}
			System.out.print("]\n");
		}
			
			
		
		
	}
}
