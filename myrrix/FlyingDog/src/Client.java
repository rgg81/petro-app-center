import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.myrrix.client.ClientRecommender;
import net.myrrix.client.MyrrixClientConfiguration;
import net.myrrix.client.translating.TranslatedRecommendedItem;
import net.myrrix.client.translating.TranslatingClientRecommender;
import net.myrrix.common.OneWayMigrator;
import net.myrrix.common.random.MemoryIDMigrator;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.model.IDMigrator;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
public class Client {
	
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String [] args) throws Exception {
		MyrrixClientConfiguration clientConfig = new MyrrixClientConfiguration();
		clientConfig.setHost("10.18.114.5");
		clientConfig.setPort(8081);
		// These lines only needed if you enabled HTTPS
		// These lines only needed if you set a username and password
		ClientRecommender clientRecommender = new ClientRecommender(clientConfig);
		//translatingClientRecommender.setPreference("up2h", "");
		//List<TranslatedRecommendedItem> recommendations = translatingClientRecommender.recommend("up2h", 5);
		//for (int i = 0; i < recommendations.size(); i++) {
			//System.out.println(recommendations.get(i));
		//}
		File f = new File("bigresult.log");
		long size = f.length();
		BufferedReader reader = new BufferedReader(new FileReader("bigresult.log"));
		String line = null;
		
		int i = 0 ;
		while ((line = reader.readLine()) != null) {
		    String[] tuple = line.split("\t");
		    clientRecommender.setPreference(tuple[1].hashCode(), tuple[2].hashCode(),5);
		    if (tuple.length > 3)
		    	if (tuple[3] != null && !tuple[3].equals("")) {
			    	String lotacao = tuple[3].replace("/", "").toLowerCase().trim();
					clientRecommender.setUserTag(tuple[1].hashCode(), lotacao,2);
		    	}
		    if (tuple.length > 4)
		    	if (tuple[4] != null && !tuple[4].equals("")) {
		    		String funcao = tuple[4].replace("/", "").toLowerCase().trim();
			    	clientRecommender.setUserTag(tuple[1].hashCode(), funcao,3);
		    	}
		    i++;
		    if (i < 10) {
		    	System.out.println("Including: " + tuple[1].hashCode() + " - " +  tuple[2].hashCode() + " = " + tuple[1] + " - " + tuple[2]);
		    }
		    if (i % 1000 == 0)
		    	System.out.println(i);
		}
		System.out.println("Refreshing...");
		clientRecommender.refresh();
		main666(null);
	}
	
	public static void main2(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader("bigresult.log"));
		String line = null;
		
		Map<Long, String> users = new HashMap<Long, String>();
		Map<Long, String> apps = new HashMap<Long, String>();
		
		int i = 0 ;
		while ((line = reader.readLine()) != null) {
		    String[] tuple = line.split(" ");
		    
		    users.put(new Long( tuple[0].hashCode() ), tuple[0]);
		    apps.put(new Long( tuple[1].hashCode() ), tuple[1]);
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Digite o id: ");
		
		String readId ;
		while ( (readId = in.readLine()) != null ) {
			try {
				System.out.println("--> Hash = " + readId.hashCode());
				Long hash = new Long(readId);
				System.out.println("User: " + users.get(hash));
				System.out.println("App:  " + apps.get(hash));
			} catch (Exception e) {
//				e.printStackTrace();
			}
			System.out.print("\nDigite o id: ");
		}
	} 
	
	public static void main666(String[] args) throws Exception {
		MyrrixClientConfiguration clientConfig = new MyrrixClientConfiguration();
		clientConfig.setHost("10.18.114.5");
		ClientRecommender clientRecommender = new ClientRecommender(clientConfig);

		BufferedReader reader = new BufferedReader(new FileReader("bigresult.log"));
		String line = null;
		
		Map<Long, String> users = new HashMap<Long, String>();
		Map<Long, String> apps = new HashMap<Long, String>();
		
		int i = 0 ;
		while ((line = reader.readLine()) != null) {
		    String[] tuple = line.split("\t");
		    
		    users.put(new Long( tuple[1].hashCode() ), tuple[1]);
		    apps.put(new Long( tuple[2].hashCode() ), tuple[2]);
		}
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Digite o usuário: ");
		
		String readId ;
		while ( (readId = in.readLine()) != null ) {
			try {
				List<RecommendedItem> recommendations = clientRecommender.recommend(readId.hashCode(), 5, false, (String[])null);
				for (RecommendedItem recommendedItem : recommendations) {
					System.out.println(apps.get(new Long(recommendedItem.getItemID())));
				}
			} catch (Exception e) {
//				e.printStackTrace();
			}
			System.out.print("\nDigite o usuário: ");
		}
	} 
	
	public static void main3(String [] args) throws TasteException, IOException {
		MyrrixClientConfiguration clientConfig = new MyrrixClientConfiguration();
		clientConfig.setHost("localhost");
		// These lines only needed if you enabled HTTPS
		// These lines only needed if you set a username and password
		ClientRecommender clientRecommender = new ClientRecommender(clientConfig);
		IDMigrator userTranslator = new OneWayMigrator();
	    MemoryIDMigrator itemTranslator = new MemoryIDMigrator();
		TranslatingClientRecommender translatingClientRecommender = new TranslatingClientRecommender(clientRecommender,userTranslator, itemTranslator);
//		System.out.println("CBTB = " + userTranslator.toStringID(userTranslator.toLongID("CBTB")));
		System.out.println("SD2000 = " + itemTranslator.toStringID(itemTranslator.toLongID("SD2000")));
		//List<TranslatedRecommendedItem> recommendations = clientRecommender.recommend(24, 5);
		List<TranslatedRecommendedItem> recommendations = translatingClientRecommender.recommend("CBTB", 5);
		//List<TranslatedRecommendedItem> recommendations = translatingClientRecommender.mostPopularItems(5);
		for (int i = 0; i < recommendations.size(); i++) {
//			itemTranslator.
			System.out.println(itemTranslator.toStringID(Long.parseLong(recommendations.get(i).getItemID())));			
		}
	}
}
