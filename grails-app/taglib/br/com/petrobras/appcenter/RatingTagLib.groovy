package br.com.petrobras.appcenter

import grails.util.*

class RatingTagLib {

    static namespace = 'rate'

    def resources = {attrs ->
        out << "<link rel=\"stylesheet\" href=\"${createLinkTo(dir: pluginContextPath + '/css', file: 'ratings.css')}\" />"
    }

    def rating = {attrs ->
        def refreshedApp = Application.get(attrs.bean.id)
        def average
        def votes


        average = refreshedApp.averageRating ?: 0
        votes = refreshedApp.totalRatings

        out << "<div>"
            5.times {cnt ->
                def i = cnt + 1
                if (i<=average) {
                    out << """<div class="rate-star-on"></div>"""
                } else if (i > average && average > (i-1)){
                    def starWidth = 12 - (12 * (i-average))
                    out << """<div class="rate-star-off">"""
                    out << """<div style="width:${starWidth}px;"><a style="width:${starWidth}px;"></a></div></div>"""
                }
                else{
                    out << """<div class="rate-star-off"></div>"""
                }
            }
        out << "</div>"
    }
}