package br.com.petrobras.appcenter

class ApplicationTagLib {

	static namespace = "application"  
	
	def accessControlService

	def isPermitted = { attrs, body -> 		
		if(! attrs.id) throw new Exception("Identificador de application não informado!") 

		try {
			if (accessControlService.isAuthorized(namespace, attrs.id))
				out << body()
		} catch(e) {
			log.error("Erro ao verificar autorização de usuário em application. ", e)
		}
	}
}