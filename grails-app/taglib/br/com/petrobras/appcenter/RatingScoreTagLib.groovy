package br.com.petrobras.appcenter

import grails.util.*

class RatingScoreTagLib{

    static namespace = 'ratingScore'
	
	static final CONTEXT = "${this.class.name}.ratingScoreContext"
	
	def ratingScoreContext = { attrs, body ->
		// Coloca o ratingScore no contexto
		request.setAttribute(CONTEXT, getScore(attrs))
		
		out << body.call()
		
		request.removeAttribute(CONTEXT)
	}
	
	private def getScore(attrs) {
		if (attrs.bean) {
			return attrs.bean.ratingScore
		} else {
			def score = request.getAttribute(CONTEXT)
		
			if ( score ) {
				return score;
			} else {
				throw new Exception("There must be a 'bean' domain object included in the ratingScore tag.")
			}
		}
	}
	
	def total = { attrs ->
		def score = getScore(attrs)
		out << score.values().sum()
	}

	def mean = { attrs ->
		def score = getScore(attrs)
		def sum = score.values().sum()
		if ( sum != 0 )
			out << String.format("%.2f", score.collect { key, value -> key * value }.sum() / score.values().sum())
		else
			out << String.format("%.2f", 0.0)
	}

	def customDraw = { attrs, body ->
		def score = getScore(attrs)
		
		def varStars = attrs.varStars ?: "stars"
		def varRatings = attrs.varStars ?: "ratings"
		def varTotal = attrs.varTotal ?: "total"
		
		def total = score.values().sum()
		
        for( stars in 5..1 ) {
			def ratings = score[stars]
			out << body((varStars):stars, (varRatings):ratings, (varTotal): total)
        }
	}
}