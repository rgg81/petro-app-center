package br.com.petrobras.appcenter

class GreetedUserTagLib {
	
	static namespace = "greetedUser"
	
	def shiroService

	def showName = { attrs ->

		def user = shiroService.loggedUser()

		if (user) out << user.shortName
	}

}
