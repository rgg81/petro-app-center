package br.com.petrobras.appcenter

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

class ReplyService {

	def shiroService

	static transactional = false

	@Transactional
    def saveReply(reply){
    	try{
	    	Assert.notNull(shiroService.loggedUser(), "Usuario Invalido")
			Assert.isTrue(reply && reply.text,"Replica Invalida")

            def user = shiroService.loggedUser()


			Assert.notNull(reply, "Replica Inexistente")

			def admins = AdminApplication.findAllByApplication(reply.comment.application).admin
			Assert.isTrue(admins.contains(user),"Usuario nao permitido")
			Assert.isTrue(user == reply.user ,"Usuario nao permitido")
				
			def replyResult = reply.save(flush:true)
		}
		catch (Exception e)
		{
			throw new RuntimeException("Error trying to editReply id: ${reply?.id}",e)
		}
		
    }

    @Transactional
    def deleteReplyAdmin(reply) {
    	try{
            def user = shiroService.loggedUser()
 	
			Assert.notNull(reply ,"Replica Invalida")
			def admins = AdminApplication.findAllByApplication(reply.comment.application).admin
			Assert.isTrue(admins.contains(user),"Usuario nao permitido")

			deleteReply(reply)
		}
		catch (Exception e)
		{
			throw new RuntimeException("Error trying to deleteReplyAdmin id: ${reply?.id}",e)
		}
    }

   def deleteReply(reply) {
   		try {
   			Assert.notNull(shiroService.loggedUser(), "Usuario Invalido")
	   		reply.comment.reply = null
			reply.delete(flush: true)
		} catch (Exception e) {
			throw new RuntimeException("Error trying to deleteReply id: ${reply?.id}",e)
		}
	}

}
