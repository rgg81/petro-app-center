package br.com.petrobras.appcenter

import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat
import java.text.DateFormat

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile

import br.com.petrobras.security.model.log.LogAuthentication;
import grails.validation.ValidationException

import javax.imageio.ImageIO

import java.math.MathContext
import java.math.RoundingMode

import org.apache.lucene.queryparser.classic.QueryParser

class ApplicationService {
	
	static transactional = false

	static def sessionFactory
	def accessControlService
    
    @Transactional
	def saveApplication(application, File iconFile, tagNames, informations, draftApp, versions) {
    	try{
    		if (application?.adminsApp.size() == 0) {
    			log.debug "Application sem administradores, lançando exception!"
	            //TODO: O correto seria lançar uma ValidationException. Mais correto ainda seria corrigir o modelo. 
    			throw new RuntimeException("Uma aplicação deve ter ao menos um administrador. ")
    		}

			if(iconFile.exists()) {
				log.debug("icon exists")
				def imgBuffer = ImageIO.read(iconFile)

				if (application.icon.id) {
					application.icon.width = imgBuffer.width
					application.icon.height = imgBuffer.height
					application.icon.size = iconFile.size()					
				} else {
					application.icon = new Icon(size: iconFile.size(), width: imgBuffer.width, height: imgBuffer.height)
				}

				log.debug("icon width:${application.icon.width} height:${application.icon.height} size:${application.icon.size}")
			}
			
			def appIcon
			if(application.id != null) {
				AdminApplication.findAllByApplication(application).each {
					it.delete(flush:true)
				}	
				ApplicationCompatibilities.findAllByApplication(application).each {
					it.delete(flush:true)
				}
				appIcon =  new File(application?.icon?.sysAbsolutePath())
				
				log.debug("icon sys path: ${appIcon}")
				if (!iconFile.exists() && appIcon.exists()) {
					// icon not updated
					log.debug("icon not updated")
					def imgBuffer = ImageIO.read(appIcon)
					application.icon.width = imgBuffer.width
					application.icon.height = imgBuffer.height
					application.icon.size = appIcon.size()
				}
				log.debug("Admins app ${application?.adminsApp}")
				application.tags?.clear()
				application.informationsSupport?.clear()
				application.versions?.clear()
			}
			
	    	if (application.save(flush: true)) {
				if(tagNames) application.addToTagsLowerCase(tagNames*.name)
				if(informations) application.addToInformationsSupports(informations)
				if(versions) {
					def dateFormat = new SimpleDateFormat("dd/MM/yyyy")
					versions.each {
						if (it.date instanceof String)
							it.date = dateFormat.parse(it.date)
						application.addToVersions(it)
					}
				}
					 
				if ( application.save(flush: true,failOnError:true) ) {
					application?.adminsApp.each { admApp ->
						def user = User.get(admApp.admin.id)
						def oneAdminApp = new AdminApplication(admin:user, application:application)
						oneAdminApp.save(flush:true)
					}
					log.info("application?.appCompatib:${application?.appCompatib}")
					application?.appCompatib.each { comp ->
						log.info("comp:${comp}")
						def compatibility = Compatibility.get(comp.id)
						def appComp = new ApplicationCompatibilities(compatibility:compatibility, application:application)
						appComp.save(flush:true)
					}
					
					if(application instanceof Application) {
						if(draftApp) {
							if(!iconFile.exists()) iconFile = new File(draftApp.icon.sysAbsolutePath())
							deleteApplication(draftApp)
						}
					}
					else {
						appIcon =  new File(application?.icon?.sysAbsolutePath())
						if(!iconFile.exists() && !appIcon.exists()) copyFile(new File(application.application.icon.sysAbsolutePath()), application.icon.sysAbsolutePath())
					}

					if (iconFile.exists()) transferFile(iconFile,application.icon.sysAbsolutePath())

					accessControlService.updateRoleAndGrants(application)
				}
	        	return application
	        }
	    } catch (Exception e) {
			log.error("error in service application", e)
			if(application instanceof Application) application.unindex()
			throw new RuntimeException("Error trying to save an application with id: ${application?.id}",e)
		}
	      	
    }
	
	
	@Transactional
	def deleteApplication(application) {
		try {
			
			if (application instanceof Application){
				def appDraft = ApplicationDraft.findByApplication(application)
				if(appDraft) deleteApplication(appDraft)
			}
			
			AdminApplication.findAllByApplication(application).each {
				it.delete(flush:true)
			}
			ApplicationCompatibilities.findAllByApplication(application).each {
				it.delete(flush:true)
			}
			UserApplication.findAllByApplication(application).each {
				it.delete(flush:true)
			}
			
			if (application instanceof Application){
				Report.findAllByApplication(application).each {
					it.delete(flush:true)
				}
			}			
				
			application?.delete(flush: true)
			accessControlService.deleteAutorizationInformationValueInApplication(application)
		} catch (Exception e)
		{
			// Rollback somente eh feito quando existe uma runtimeexception.
			throw new RuntimeException("Error trying to delete an application with id: ${application?.id}",e)
		}
	}

    def transferFile(File originFile,String toPath){
		def oldFile = new File(toPath)
		if(oldFile.exists()) oldFile.delete()
		
		originFile.renameTo(oldFile)
    }
	
	def copyFile(File source, String toPath)  {
		File dest = new File(toPath)
		FileChannel sourceChannel = null;
		FileChannel destChannel = null;
		try {
			sourceChannel = new FileInputStream(source).getChannel();
			destChannel = new FileOutputStream(dest).getChannel();
			destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
		   }finally{
			   sourceChannel.close();
			   destChannel.close();
		   }
	}

    def popular(int offset, int limit) {
    	// Necessário ordenar pelo id de application para garantir que a ordem seja mantida entre páginas
		def results = UserApplication.createCriteria().list(max: limit, offset: offset) {
			projections {
				groupProperty("application")
				count ("application","count")
				order('count', 'desc')
				order('application', 'asc')
			}
		}
		return results.collect { it[0] }
    }
	
	def popularCount() {
		// Necessário ordenar pelo id de application para garantir que a ordem seja mantida entre páginas
		def results = UserApplication.createCriteria().get() {
			projections {
				countDistinct ("application")
			}
		}
		return results
	}

    def bestRated(int offset, int limit) {
    	def params = [max: limit, offset: offset, cache: true]
		def totalRatings = new BigDecimal(Comment.count())
		def totalAppsRated = new BigDecimal(Application.executeQuery("select count(distinct application) from Comment").head())
		def bestRatedCriteria 

		if(totalAppsRated > 0) 
			bestRatedCriteria = totalRatings.divide(totalAppsRated, new MathContext(1, RoundingMode.FLOOR)) 
		else 
			bestRatedCriteria = new BigDecimal(0)
		
		def query = "select application.id, avg(rating), count(rating) as c from Comment group by application having count(rating) >= ? order by avg(rating) desc, count(rating) desc, application.id"
        def results = Application.executeQuery(query, [bestRatedCriteria.longValue()], params)
		def countResult = Application.executeQuery(query, [bestRatedCriteria.longValue()], [cache:true]) 
		def instances = Application.withCriteria {  
			inList 'id', results.collect { it[0] } 
			cache params.cache
		}
		
		[apps: results.collect {  r-> instances.find { i -> r[0] == i.id } }, total: countResult.size()]							
    }

    def totalApplications() {
    	def drafts = ApplicationDraft.findAll();
    	def unpublished = 0
    	drafts.each {
    		if (!it.application)
    			unpublished++

    	}
    	def total = Application.count() + unpublished

    }

    def toolBeltApplicationInfo() {   	
		Float sumOfRatings = 0
		def averageAboveThree = 0
		def averageBelowThree = 0
		def numberOfRatings = 0 
		
    	def ratedApplications = Comment.createCriteria().list() {
			projections { 
				avg('rating', 'average') 
				count('rating', 'ratings') 
			}
    		groupProperty 'application'
    		order('ratings', 'desc')
    		order('average', 'desc')
    		cache true
    	}.each {
			sumOfRatings += it[0]

    		if (it[0] > 3) 
    			averageAboveThree++
    		else
    			averageBelowThree++

    		numberOfRatings += it[1]
    	}

		def averageRating = sumOfRatings/ratedApplications.size()

    	[totalApplications: totalApplications(), publishedApplications: Application.count(), pendingDrafts: ApplicationDraft.count(), averageRating: averageRating.trunc(2), averageAboveThree: averageAboveThree, averageBelowThree: averageBelowThree, ratedApplications: ratedApplications.size(), numberOfRatings: numberOfRatings]    	
    }

    def allApplications() {
    	def allApplications = BaseApplication.findAll()
  		def result = []

    	allApplications.each() {
    		def isPublished = "Não"
    		def hasDraft = "Não"
    		def averageRating = null
    		def admins = []
    		if (Application.exists(it.id)) {
    			isPublished = "Sim"

    			averageRating = Application.get(it.id).averageRating
    		}
    		if (ApplicationDraft.findByApplication(Application.get(it.id)))
    			hasDraft = "Sim"
   			admins = AdminApplication.findAllByApplication(it)*.admin
   			def adminsString = ""
   			admins.each {
   				adminsString += it.name + " - " + it.key
   				if (it != admins.last())
   					adminsString += ", "
   			}

   			if (averageRating == null)
   				averageRating = ""
   			else
   				averageRating = new Float(averageRating).round(2)

   
    		def thisApplication = [name: it.name, catalogCode: it.catalogCode, url: it.link.url, isPublished: isPublished, hasDraft: hasDraft, averageRating: averageRating, admins: adminsString]
    		result.add(thisApplication)

    	}
    	return result

    }

    def applicationAverageAndUse() {

  		def session = sessionFactory.currentSession		

		def query = session.createSQLQuery("""
			select a.id, a.name, avg(c.rating), (select count(*) from user_application where application_id = a.id) as added
			from base_application a, pcomment c, user_application u
			where a.id = c.application_id
			and a.id = u.application_id
			group by a.id, a.name
		""")

		def result = query.list()
		def applications = []
		result.each() {
			applications.add([id: it[0], name: it[1], y: new Float(it[2]).round(2), x: it[3]])
		}
		return applications
    }

    def applicationAverageAndAccess() {
    	def session = sessionFactory.currentSession		

		def query = session.createSQLQuery("""
			select a.id, a.name, avg(c.rating)
			from base_application a, pcomment c
			where a.id = c.application_id
			group by a.id, a.name
		""")

		def result = query.list()
		def applications = []
		result.each() {
			applications.add([id: it[0], name: it[1], rating: new Float(it[2]).round(2)])
		}
		return applications

    }

    def asCSVStructure(apps,locale){
    	def csv = []
        csv.push(["data ultima modificacao","nome","descrição","url","tags", "administradores", "informacoes de suporte"])

        apps.each{ it -> 
			def tags = it.tags*.name?.join(",")
			def informationsSupport = it.informationsSupport?.collect { infoSupport -> "${infoSupport.description}: ${infoSupport.url}" }?.join(",")
            def admins = AdminApplication.findAllByApplication(it)*.admin*.key.join(",")

            def row = [	DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, locale).format(it.lastUpdated),
							it.name, 
							it.description, 
							it.link, 
							tags, 
                            admins, 
							informationsSupport]
            
            csv.push(row)
        }
        return csv
    }

    def searchByDepartment(term,options){

		def escaped_d = QueryParser.escape(term)
		def apps = []
		def query = {
			match {
				department(query: escaped_d,operator : 'AND')
			}
		}

		def res = Application.search(query,[default_operator:'AND',from:options.from,size:options.size])
		res.searchResults.each {
			def app = Application.get(it.id)

			if(app!=null){
				def admins = AdminApplication.findAllByApplication(app)*.admin
				def adminsString = ""
				admins.each {
	   				adminsString += it.name + " - " + it.key
	   				if (it != admins.last())
	   					adminsString += ", "
	   			}
				apps.push([id:app.id,catalogCode:app.catalogCode,name:app.name,admins:adminsString,iconPath:"/"+app.icon.relativePath()+"?"+app?.icon?.size,department:app.department])
			}
		}

		return [apps:apps,total:res.total]
    }

    def listDistinctDepartments(){
    	Application.findAllByDepartmentIsNotNull().collect{ it.department }.unique()
    }
	
	def userApplicationsAdmins(User user) {
		
		
		def session = sessionFactory.currentSession
		
		def query = session.createSQLQuery(
			"""
				(select draftApp.id,   baseapp.id as idBaseApp, app.id as appId, baseapp.name as name from application_draft draftApp left  join base_application baseapp on draftApp.id = baseapp.id
				left join application app on app.id = draftApp.application_id where baseapp.id in (select adm.application_id from admin_application adm where adm.admin_id = :user)
				union
				select draftApp.id,  baseapp.id as idBaseApp, app.id as appId, baseapp.name as name from application app left join base_application baseapp on app.id = baseapp.id
				left join application_draft draftApp on draftApp.id = baseapp.id
				where app.id not in (select draftApp.application_id from application_draft draftApp where draftApp.application_id is not null) and 
				baseapp.id in (select adm.application_id from admin_application adm where adm.admin_id = :user)) order by name asc
			"""
			);
		query.setLong("user", user.id)
		def result = query.list()
		
		result.collect { obj ->
			def status
			if(obj[0])
				status = "Rascunho"
			else
				status = "Publicado"
			def app = BaseApplication.get(obj[1])
			[status: status, application:app]
			
		}
	}

}
