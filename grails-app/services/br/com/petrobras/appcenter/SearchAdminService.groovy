package br.com.petrobras.appcenter

import grails.transaction.Transactional
import io.searchbox.client.*
import io.searchbox.core.*
import io.searchbox.indices.*
import io.searchbox.client.config.HttpClientConfig
import io.searchbox.client.http.JestHttpClient
import io.searchbox.client.JestClient
import io.searchbox.client.JestResult
import io.searchbox.core.Search
import io.searchbox.core.SearchResult
import io.searchbox.client.JestClientFactory
import io.searchbox.indices.Stats
import javax.annotation.PostConstruct
import org.elasticsearch.action.admin.indices.mapping.get.*
import com.google.gson.JsonObject
import org.elasticsearch.* 
import org.grails.plugins.elasticsearch.mapping.SearchableClassMappingConfigurator
import org.grails.plugins.elasticsearch.mapping.SearchableClassMapping
import org.grails.plugins.elasticsearch.index.IndexRequestQueue

@Transactional
class SearchAdminService {

	static transactional = false

    def elasticSearchContextHolder
    def elasticSearchAdminService
    def searchableClassMappingConfigurator
    def elasticSearchService
    def grailsApplication

    JestClient client
    JestClientFactory factory;

    @PostConstruct
    private void init() {
        factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                        .Builder("http://localhost:9200")
                        .multiThreaded(true)
                        .connTimeout(150)
                        .readTimeout(300)
                        .build());
        client = factory.getObject();
     }
//nomes dos índices conforme configuração
    def list(indexz) {
        log.debug("indexz: $indexz")
        if (indexz) 
            elasticSearchAdminService.refresh(indexz)

        def indexes = [:]
        def indexesForReal = listIndexes()
		log.info "indexesForReal: $indexesForReal"

        elasticSearchContextHolder.mapping.each { k, v ->
            //log.info "k: $k"
            //log.info "v: $v"
            //log.info "v.indexName: $v.indexName"
            def index = indexes[v.indexName]
            if (index){
                index.classes.push([clazz: k, count: 0])
            } else {
            	//log.debug "indexesForReal[v.indexName]: ${indexesForReal[v.indexName]}"
                log.debug "indexesForReal[${v.indexName}]: ${indexesForReal[v.indexName]?.docsCount}"
                indexes[v.indexName] = [
                	classes: [[clazz: k, count: 0]], 
                	exists: (indexesForReal[v.indexName]!=null), 
                    docsCount: indexesForReal[v.indexName]?.docsCount, 
                    objectsAtDatabase: 0
                ]
            }            
        }

        indexes.each { k, v  ->
            v.classes.each { it ->
                log.debug "it: $it"
                log.debug "it.clazz.class: ${it.class}"
                def count = grailsApplication.getDomainClass(it.clazz).clazz.count()
                log.debug "count: $count"
                v.objectsAtDatabase += count
                it.count = count
            }
            log.debug "v.objectsAtDatabase: ${v.objectsAtDatabase}"
            //grailsApplication.getArtefact('Domain',it)?.getClazz()
        }
        log.debug "class: $BaseApplication"
        log.debug "count: ${BaseApplication.count()}"

//        indexes["br.com.petrobras.appcenter"].objectsAtDatabase = Application.list().inject(0) { acc, app -> acc + app.tags.size() }
            /*
            tags.size() + 2
        only = ['name', 'description', 'link', 'icon','tags','tagsBusca', 'department','isDownloadable']
        completion = [  name: 'app_suggestion', 
                        mapping: [
                                index_analyzer:'app-analyzer',
                                search_analyzer:'app-analyzer',
                                payloads: true
                        ],
                        indexing: [
                                input:{ instance -> instance.name},
                                output:{ instance -> instance.name},
                                payload:{ instance -> ['appid':instance.id,'iconPath':Icon.relativePathByApplication(instance)]}
                        ]
                     ]
                        
        name store: 'yes', boost:3.0 
        description store: 'yes'
        department store: 'yes'
        tagsBusca store: 'yes'
        link component:true
        tags component:true
        icon component:true
*/
        
        return indexes
    }

    def listIndexes() {
		Stats stats = new Stats.Builder().build();
		JestResult result = client.execute(stats);

		//log.debug("result.getJsonString(): ${result.getJsonString()}")
		//def indexes = []
		def indexes = [:]
//indexing
		result.getJsonMap().get("indices").each{ k, v->
			def total = v.get('total')
			indexes[k] = [
				docsCount: total.get("docs").get("count"), 
				indexing: total.get("indexing"), 
				docsCountp: v.get("primaries").get("docs").get("count")
			]
			/*
			indexes.push([
				indexName: k, 
				docsCount: total.get("docs").get("count"), 
				indexing: total.get("indexing"), 
				docsCountp: v.get("primaries").get("docs").get("count")
			])
*/
			//log.debug("indexing: ${v.get('total')}")
		}

		return indexes
    }

    def deleteIndex(String indexName) {
    	elasticSearchAdminService.deleteIndex(indexName)
    }

    def installMappings(String indexName) {
//SearchableClassMappingConfigurator
		log.debug "mappings for $indexName: ${elasticSearchContextHolder.mapping[indexName]}"

		List<SearchableClassMapping> mappings = []

        elasticSearchContextHolder.mapping.each { k, v ->
            log.info "k: $k"
            log.info "v: $v"
            log.info "v.indexName: $v.indexName"

            if (v.indexName == indexName)
            	mappings.push(v)
        }

        log.debug "mappings: $mappings"

		searchableClassMappingConfigurator.installMappings(mappings)
    }

    def indexDocs(classes) {
        log.debug("indexing classes $classes")
        log.debug("indexing classes ${classes[0]}")
        log.debug("indexing classes ${classes[0].clazz.class}")
        def classez = classes.collect {
        	log.debug("it.clazz: ${it.clazz}")
            log.debug("it: ${grailsApplication.getArtefact('Domain', it.clazz)?.getClazz()}")
        	
        	grailsApplication.getArtefact('Domain', it.clazz)?.getClazz()
        }

		log.debug("indexing classez $classez")
		log.debug("indexing classez ${classez.class}")
		log.debug("indexing classez ${classez*.class}")

		log.debug("${[br.com.petrobras.appcenter.Link, br.com.petrobras.appcenter.Application, br.com.petrobras.appcenter.Icon, br.com.petrobras.appcenter.Tag]*.class}")
		//elasticSearchService.index(br.com.petrobras.appcenter.Link)
		//elasticSearchService.index(classez)
/*
		classez.each {
			log.debug("it: $it")
			elasticSearchService.index(it)
		}
*/		
		elasticSearchService.index(*classez)
    }

    def docsCount(indexName) {
        Stats stats = new Stats.Builder().addIndex(indexName).build();
        JestResult result = client.execute(stats);
//http://localhost:8080/petro-app-center/searchAdmin/docsCount/br.com.petrobras.appcenter
//TODO: tratar erro ?
//15-10-09 15:42:00,226 DEBUG [http-bio-8080-exec-4] SearchAdminService:128 - result.getJsonString(): {"error":"IndexMissingException[[br.com.petrobras.petro-app-center] missing]","status":404}

        result.getJsonMap().get("indices").get(indexName).get("total").get("docs").get("count")
    }
}
