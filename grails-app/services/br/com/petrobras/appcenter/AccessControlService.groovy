package br.com.petrobras.appcenter

import org.apache.shiro.SecurityUtils
import org.springframework.transaction.annotation.Transactional

import br.com.petrobras.security.ISecurityContext
import br.com.petrobras.security.exception.ConstraintViolationException
import br.com.petrobras.security.exception.InvalidOperationException
import br.com.petrobras.security.exception.ObjectNotFoundException
import br.com.petrobras.security.model.Information
import br.com.petrobras.security.model.InformationValue
import br.com.petrobras.security.model.InformationValueClassification
import br.com.petrobras.security.model.InformationValueClassificationEnum
import br.com.petrobras.security.model.Role
import br.com.petrobras.security.model.authorization.access.UserRoleAuthorization 
import br.com.petrobras.security.model.authorization.access.RoleResourceAuthorization
import br.com.petrobras.security.model.authorization.access.UserGroupRoleAuthorization
import br.com.petrobras.security.model.authorization.configuration.RoleUseCaseAuthorization

class AccessControlService {

    static transactional = false
    
    def shiroService

	def grailsApplication

    def findUser(String login) {
        try {
			ISecurityContext.context.userManager.find(login)
        } catch(ObjectNotFoundException e) {
            throw new IllegalArgumentException("usuário não existe", e)
        } catch(Exception e) {
            throw new RuntimeException("Erro ao obter dados do usuário de chave ${login}. ", e)
        }
    }

    /*
    Concede/revoga autorização de usuários em papéis e valores de informação. Em caso de nova aplicação também cria o valor de informação correspondente. 
    */
    @Transactional
    def updateRoleAndGrants(application) {
		def initTime, endTime
		initTime = System.nanoTime()

        log.debug "application.id: ${application.id}"

        Information information = new Information("application")

        def id = application.id as String
        InformationValue informationValue = new InformationValue(id, information)
        informationValue.name = id
        informationValue.description = id
        informationValue.classification = new InformationValueClassification(InformationValueClassificationEnum.NP1)
        informationValue.enabled = true

        def userInformationValueAuthorization
        def userInformationValueAuthorizationManager = ISecurityContext.context.userInformationValueAuthorizationManager
        try {
            userInformationValueAuthorization = userInformationValueAuthorizationManager.findAllWithInformationValue(informationValue)
        } catch(ObjectNotFoundException e) {
            log.info "Valor de informação ${informationValue.name} não existe, vamos criá-lo. ", e
            try {
                ISecurityContext.context.informationValueManager.save(informationValue)        
            } catch(ConstraintViolationException ex) {
                log.info ex.message, ex
            } catch(Exception ex) {
                log.error ex.message, ex
                throw new RuntimeException("Erro ao criar valor de informação application $id.", ex)
            }
        } catch(Exception e) {
            log.error e.message, e
            throw new RuntimeException("Erro ao recuperar autorizações de usuários no valor de informação application $id.", e)
        }

        def currentAdmins = userInformationValueAuthorization.collect{ it.user?.login }
        log.debug "currentAdmins: ${currentAdmins}"

        def newAdmins = AdminApplication.findAllByApplication(application).collect { it.admin?.key }
        log.debug "newAdmins: ${newAdmins}"

        def revokees = currentAdmins - newAdmins
        def grantees = newAdmins - currentAdmins
        log.debug "revokees: ${revokees}"
        log.debug "grantees: ${grantees}"

        def userRoleAuthorizationManager = ISecurityContext.context.userRoleAuthorizationManager

        revokeAutorizationInformationValue(revokees, informationValue, userInformationValueAuthorizationManager)

        for (grantee in grantees) {
            try {
                userRoleAuthorizationManager.grant(grantee, grailsApplication.config.app.role.adminapp)
                log.debug "$grantee autorizado na role ${grailsApplication.config.app.role.adminapp}"
            } catch(InvalidOperationException e) {
                log.info e.message, e
            } catch(Exception e) {
                log.error e.message, e
                throw new RuntimeException("Erro durante concessão de acesso do usuário $grantee ao perfil ${grailsApplication.config.app.role.adminapp}.", e)
            }

            try {
                userInformationValueAuthorizationManager.grant(grantee, informationValue.information.id, informationValue.id)
                log.debug "$grantee autorizado em ${informationValue.information.id} com valor ${informationValue.id}"
            } catch(InvalidOperationException e) {
                log.info e.message, e
            } catch(Exception e) {
                log.error e.message, e
                throw new RuntimeException("Erro durante concessão de acesso do usuário $grantee ao valor ${informationValue.id} da informação ${informationValue.information.id}.", e)
            }
        }

        endTime = System.nanoTime()
        def lasted = (endTime - initTime)/(10**9)
        log.debug "updateRoleAnd demorou ${lasted} segundos. "
    }
	
	def deleteAutorizationInformationValueInApplication(application) {
		Information information = new Information("application")
		def id = application.id as String
		InformationValue informationValue = new InformationValue(id, information)
		
		def userInformationValueAuthorizationManager = ISecurityContext.context.userInformationValueAuthorizationManager
        def userInformationValueAuthorization
        try {
    		userInformationValueAuthorization = userInformationValueAuthorizationManager.findAllWithInformationValue(informationValue)
        } catch(ObjectNotFoundException e) {
            log.info e.message, e
            return
        }   
		
		revokeAutorizationInformationValue(userInformationValueAuthorization.collect { it.user?.login }, informationValue, userInformationValueAuthorizationManager)

		try {
            ISecurityContext.context.informationValueManager.remove(informationValue)
        } catch(ObjectNotFoundException e) {
            log.info e.message, e
        }	
		
	}
	
	protected def revokeAutorizationInformationValue(List<String> revokees, InformationValue informationValue, manager) {
        for (revokee in revokees) {
			try {
				manager.revoke(revokee as String, informationValue.information.id as String, informationValue.id as String)
			} catch(InvalidOperationException e) {
				log.info e.message, e
			} catch(Exception e) {
				log.error e.message, e
				throw new RuntimeException("Erro durante revogação de acesso do usuário $revokee ao valor ${informationValue.id} da informação ${informationValue.information.id}.", e)
			}
		}
	}

    def isAuthorized(informationId, informationValueId) {
        try {
            ISecurityContext.context.informationValueAuthorizer.isAuthorized(informationId as String, informationValueId as String) || SecurityUtils.subject.hasRole(grailsApplication.config.app.role.moderator)
        } catch (Exception e) {
            log.error e.message, e
            throw new RuntimeException("Erro ao verificar autorização de usuário. ", e)
        }
    }

    def listResourcesByRoles() {
        def allroles = ISecurityContext.context.roleManager.findAll()
        def rolesOutput = [:]
        allroles.each { Role role ->
            def associatedResources = []
            ISecurityContext.context.roleResourceAuthorizationManager.findAll(role).each { RoleResourceAuthorization roleResource ->
                associatedResources.add(roleResource.getResource().getName())

            }
            rolesOutput.put(role.getName(), associatedResources)
        }
        return rolesOutput
    }

    def getModeratorsEmails() {
        try{
            ISecurityContext.context.userRoleAuthorizationManager.findAllWithRole(grailsApplication.config.app.role.moderator).collect { it.user.email }
        } catch(Exception e) {
            log.error 'Erro ao recuperar lista de moderadores do controle de acesso. ', e 
            throw new RuntimeException('Erro ao recuperar lista de moderadores do controle de acesso. ', e)
        }

    }

}