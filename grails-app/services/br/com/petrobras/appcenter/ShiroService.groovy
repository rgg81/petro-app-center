package br.com.petrobras.appcenter

import org.apache.shiro.SecurityUtils

/**
 * {@author uq4m}
 * {@see http://grails.org/plugin/shiro}
 * {@see https://github.com/pledbrook/grails-shiro/blob/master/grails-app/taglib/org/apache/shiro/grails/ShiroTagLib.groovy}
 * {@see http://wiki.petrobras.com.br/display/isti/grails-petrobras-security}
 */
class ShiroService {

    def grailsApplication
    
    boolean isAuthenticated(){
        return SecurityUtils?.subject?.isAuthenticated()
    }

    User loggedUser(){
        def user = userInfo
        log.debug "******** loggedUser, chave = ${user?.login}"
        return User.findByKey(user?.login?.toUpperCase())
    }

    private getUserInfo() {
        def currentUser = SecurityUtils.subject
        return currentUser?.principals?.oneByType(br.com.petrobras.security.model.User.class)
    }

    boolean isModerator() {
        return SecurityUtils?.subject?.hasRole(grailsApplication.config.app.role.moderator) 
    }
}
