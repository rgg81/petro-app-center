package br.com.petrobras.appcenter

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import java.security.MessageDigest
import java.util.Date

class UserService {
	
	def activityService

	static transactional = true
    
    def addApplication(Application application, User user){
		//def lastUserApp = user.myApplications?.max( { a, b -> a.position <=> b.position } as Comparator )
		def lastUserApp = UserApplication.findAllByUser(user)?.max( { a, b -> a.position <=> b.position } as Comparator )
		def pos = lastUserApp ? lastUserApp.position + 1  : 0

		def userApp = new UserApplication( user: user, application: application, position: pos )
		if(!userApp.save(flush:true)){
			throw new RuntimeException("erro ao adicionar a aplicacao aos favoritos")
		}
    }

	@Transactional(readOnly = false)
    def removeApplication(Application application, User user){
		def userApp = UserApplication.findByUserAndApplication(user,application)
		userApp.delete(flush: true,failOnError:true)
		return true
		
    }
	
	@Transactional(readOnly = false)
	def updateUserApplicationPosition(UserApplication userApp, Integer position) {
		def oldUserApp = UserApplication.findByPositionAndUser(position,userApp.user)
		if(oldUserApp) {
			oldUserApp.position = (position + 100)
			oldUserApp.save(flush:true)
		}
		
		userApp.position = position
		userApp.save(flush:true)
	}

	def saveNewUser(String login) {
		def user = User.findByKeyIlike(login)

		String key = login.toUpperCase() + new Date().getTime()
		if (!user) {
			user = new User(key:login,tracking:md5(key)).save(flush:true)
		}else{
			if(!user.tracking){
				user.tracking = md5(key)
			}
			user.save(flush:true);
		}
		return user;
	}

	def md5( obj ) {
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		md5.update(obj.getBytes());
		BigInteger hash = new BigInteger(1, md5.digest());
		return hash.toString(16)
	}

	def shareUserApplications(User user) {
		try {
			user.sharesMyApplications = true
			if (user.save(flush: true)) {
				return user
			}			
		} catch (Exception e) {
			log.error("error in service user", e)
			if(application instanceof Application) application.unindex()
			throw new RuntimeException("Error trying to set user ${user.key}'s applications as public",e)
		}
	}

	def unshareUserApplications(User user) {
		try {
			user.sharesMyApplications = false
			if (user.save(flush: true)) {
				return user
			}			
		} catch (Exception e) {
			log.error("error in service user", e)
			if(application instanceof Application) application.unindex()
			throw new RuntimeException("Error trying to set user ${user.key}'s applications as public",e)
		}
	}

	def getReviews(User user) {
		println user.comments
	}

	def myMostAccessedApps(User user, int days, int limit){
		def ids = activityService.aggregatedApps(user.key, UserActivity.ACCESS_APPLICATION, days,limit);
		def apps = []
		ids.each{ it ->
			apps.push(Application.findById(it))
		}
		return apps;
	}

}
