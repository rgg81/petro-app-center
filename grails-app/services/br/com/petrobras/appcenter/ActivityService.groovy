package br.com.petrobras.appcenter

import org.springframework.transaction.annotation.Transactional

import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.common.xcontent.XContentBuilder
import org.elasticsearch.common.xcontent.XContentFactory

import io.searchbox.client.config.HttpClientConfig
import io.searchbox.client.http.JestHttpClient
import io.searchbox.core.Search
import io.searchbox.core.SearchResult
import io.searchbox.client.JestClientFactory

import javax.annotation.PostConstruct

import com.google.gson.JsonObject

class ActivityService {
    
    JestHttpClient client
    JestClientFactory factory;
    static transactional = false

    @PostConstruct
    private void init() {
        factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                        .Builder("http://localhost:9200")
                        .multiThreaded(true)
                        .connTimeout(150)
                        .readTimeout(300)
                        .build());
        client = (JestHttpClient)factory.getObject();
     }

    @Transactional
    def registerActivity(User user, Application application, String type,String where, boolean isReliable) {
        try{
        	def userActivity = new UserActivity(
                userId: user ? user.id : 0,
        		userkey: user ? user.key : "anonymous",
        		itemId:application.id,
                itemName:application.name,
        		type:type,
        		context:(where?: ""),
                ratingValue:0,
                isReliable:(isReliable?: false)
            )
        	userActivity.save(failOnError:true)
            return userActivity
        }catch (Exception e)
        {
            throw new RuntimeException("Error trying to save actitivy",e)
        }
    }

    @Transactional
    def registerRatingActivity(User user, Application application, String type, Integer value, String where,boolean isReliable) {
        try{
            def userActivity = new UserActivity(
                userId: user ? user.id : "anonymous",
                userkey: user ? user.key : "anonymous",,
                itemId:application.id,
                itemName:application.name,
                type:type,
                ratingValue:(value?: 0),
                context:(where?: ""),
                isReliable:(isReliable?: false)
            )
            userActivity.save(failOnError:true)
            return userActivity
        }catch (Exception e)
        {
            throw new RuntimeException("Error trying to save actitivy",e)
        }
    }

    def aggregatedApps(String userkey,String activity, int timespan, int limit){
        try{
            def query = '{ "aggs":{"activities":{"filter":{"bool":{"must":[{"term":{"userkey":"'+userkey+'"}},{"term":{"type":"'+activity+'"}},{"range": {"dateCreated":{"gt":"now-'+timespan+'d"}}}]}},"aggs":{"apps":{"terms":{"field":"itemId","order":{"_count":"desc"},"size":'+limit+'}}}}}}'

            def buckets = appAccessCountAggregation(query)

            def ids = []
            for(def bucket : buckets){
                ids.push(bucket.get("key").getAsLong())
            }
            
            return ids

        }catch(Exception e)
        {
            throw new RuntimeException("Error trying to get actitivy",e)
        }
    }

    def topApps(int timespan) {
        try {
            def range = ""
            if (timespan != 0) 
                range = ',{"range": {"dateCreated":{"gt":"now-'+timespan+'d"}}}'
            def query = '{"aggs":{"activities":{"filter":{"bool":{"must":[{"term":{"type":"accessapp"}}' + range + ']}},"aggs":{"apps":{"terms":{"field":"itemId","order":{"_count":"desc"},"size":15000}}}}}}'

            def buckets = appAccessCountAggregation(query)

            def ids = [:]
            for(def bucket : buckets){
                ids.put(bucket.get("key").toString(),bucket.get("doc_count").getAsLong())
            }
            return ids

        } catch(Exception e) {
            throw new RuntimeException("Error trying to get actitivy",e)
        }
    }

    def appAccessCountAggregation(String query) {
        Search search = new Search.Builder(query)
            // multiple index or types can be added.
            .addIndex("useractivity")
            .addType("userActivity")
            .build();

        SearchResult result = client.execute(search);

        def json = result.getJsonObject()
        def buckets = json.getAsJsonObject("aggregations")
                            .getAsJsonObject("activities")
                            .getAsJsonObject("apps")
                            .getAsJsonArray("buckets");
        return buckets
    }
}
