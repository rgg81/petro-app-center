package br.com.petrobras.appcenter

import java.text.SimpleDateFormat
import org.apache.commons.lang3.StringEscapeUtils

class ReplyMailService {

	def mailService
    def grailsApplication

	def sendMailNewReply(reply, host) {
        def mailParams = [subject: "[Aplicações Petrobras] - Sua avaliação foi respondida", template: "/mail/newReply"]
        sendMailToUserWhenReplyIsSaved(reply,mailParams, host)
    }

    def sendMailUpdateReply(reply, host) {
        def mailParams = [subject: "[Aplicações Petrobras] - A resposta dada à sua avaliação foi alterada", template: "/mail/updateReply"]
        sendMailToUserWhenReplyIsSaved(reply,mailParams, host)
    }

    def sendMailToUserWhenReplyIsSaved(reply, mailParams, host) {
        try {
            def userMail = reply.comment.user.key+"@petrobras.com.br"

            def application = Comment.get(reply.commentId).application;

            def toList = []

            toList.add(userMail)
            if (grailsApplication.config.faleComATIC) toList.add(grailsApplication.config.faleComATIC)

            mailService.sendMail {
                async true
                to toList
                subject mailParams.subject
                html(view: "/mail/mailShell", model: [template: mailParams.template, reviewedApplication: application, host: host])
            }


        } catch (Exception e) {
            log.error e.message, e
        }
    }

    def sendMailDeleteReviewWithReply(review, host) {
        try {
            // necessário para buscar o nome do usuário, isso depende do shiroService
            review.user.name

            def admApps = AdminApplication.findAllByApplication(review.application).collect{"${it.admin.key}@petrobras.com.br"}

            def toList = []
            toList.addAll(admApps)

            if (grailsApplication.config.faleComATIC) toList.add(grailsApplication.config.faleComATIC)

            mailService.sendMail {
                async true
                to toList
                subject "[Aplicações Petrobras] - Avaliação com resposta foi excluída"
                html(view: "/mail/mailShell", model: [template: "/mail/commentWithReplyDeleted", review: review, host: host])
            }
        } catch (Exception e) {
            log.error e.message, e
        }

    }

    def sendMailUpdateReviewWithReply(review, host) {
        try {
            // necessário para buscar o nome do usuário, isso depende do shiroService
            review.user.name

            def admApps = AdminApplication.findAllByApplication(review.application).collect{"${it.admin.key}@petrobras.com.br"}

            def toList = []
            toList.addAll(admApps)
            
            if (grailsApplication.config.faleComATIC) toList.add(grailsApplication.config.faleComATIC)

            mailService.sendMail {
                async true
                to toList
                subject "[Aplicações Petrobras] - Avaliação com resposta foi alterada"
                html(view: "/mail/mailShell", model: [template: "/mail/commentWithReplyUpdated", review: review, host: host])
            }
        } catch (Exception e) {
            log.error e.message, e
        }
    }

}
