package br.com.petrobras.appcenter

import biz.petrobras.services.ti.aplicacao._1.AplicacaoSoapService
import biz.petrobras.schemata.common.status._1.SyncReturnType
import javax.xml.ws.BindingProvider

class CatalogClientService {
    //AplicacaoPT aplicacaoPT = new AplicacaoSoapService().getAplicacaoSoapPort()
    def grailsApplication
    def catalogoAplicacaoSoapPort = null

    static transactional = false

    private def getCatalogoAplicacaoSoapPort() {
        log.debug 'catalogoAplicacaoSoapPort: $catalogoAplicacaoSoapPort'
        
        if (catalogoAplicacaoSoapPort == null) {
            catalogoAplicacaoSoapPort = new AplicacaoSoapService().getAplicacaoSoapPort()
        }

        return catalogoAplicacaoSoapPort
    }

    def fetch(String appCode) { 

        try {
            /*
            Detalhes sobre esse serviço podem ser encontrados em http://oer.petrobras.com.br/oer
            Busque por catálogo
            Nome do serviço: Catálogo de Aplicações.
            A documentação está incorreta, para conhecer a resposta use getClass().getMethods() getClass().getProperties(). 
            */
            Map<String, Object> req_ctx = ((BindingProvider)getCatalogoAplicacaoSoapPort()).getRequestContext();
            req_ctx.put(BindingProvider.USERNAME_PROPERTY, grailsApplication.config.userCatalogo);
            req_ctx.put(BindingProvider.PASSWORD_PROPERTY, grailsApplication.config.pwdCatalogo);
			req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, grailsApplication.config.catalogAppEndPointAddress);

            def response = getCatalogoAplicacaoSoapPort().buscaAplicacaoPorCodigo(appCode)

            // Caso um código sem aplicação correspondente seja informado será retornado um AplicacaoResponse com ServiceStatus.code == 601
            if (response[0].serviceStatus?.code == 601)
                throw new IllegalArgumentException("Nenhuma aplicação encontrada com o código ${appCode}.")

            def app = response[0].serviceData.aplicacao[0]

            def appGeral = app.getGeral()

            if (!["Produção", "Homologação"].contains(appGeral.getSituacao())) {
                throw new IllegalArgumentException("A situação da aplicação de código ${appCode} no catálogo de aplicações é \"${appGeral.getSituacao()}\". Apenas aplicações em Produção ou Homologação podem ser cadastradas. ")
            }

            def allTags = app.palavras.palavraChave.findAll().collect { it }

            def tags = allTags.unique()

            [name: appGeral.nome, department: appGeral.centroDeAgilidade, status: appGeral.getSituacao(), description: appGeral.descricao, tags: tags, admins: [appGeral.getLiderProjeto().getChave()]]

        } catch (IllegalArgumentException e) {
            throw e
            
        } catch (Exception e) {
            throw new RuntimeException("Erro ao buscar aplicação de código ${appCode} no catálogo de aplicações.", e)

        }
    }

 }
