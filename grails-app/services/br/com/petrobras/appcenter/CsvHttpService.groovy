package br.com.petrobras.appcenter
import au.com.bytecode.opencsv.CSVWriter
import javax.servlet.http.HttpServletResponse

class CsvHttpService {

    def render(response, csv,filename) {
        response.setHeader("Content-Type","application/vnd.ms-excel:ISO-8859-1")
        response.setHeader("Content-Disposition","attachment;filename="+filename+".csv")
        response.status = HttpServletResponse.SC_OK
        
        response.outputStream << "sep=,\n" 

        CSVWriter writer = new CSVWriter(new OutputStreamWriter(response.outputStream,"ISO-8859-1"))
        csv.each{ row ->
        	row.collect{ ((it instanceof String && it ) ? new String(it.getBytes("ISO-8859-1"),"ISO-8859-1") : it )}
        	writer.writeNext(row as String[])	
        }        
        writer.flush()
        writer.close()
    }
}
