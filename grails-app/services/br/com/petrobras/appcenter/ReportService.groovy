package br.com.petrobras.appcenter

import org.hibernate.criterion.CriteriaSpecification

class ReportService {

    def save(Report report) {
    	report.save()
    }

    def listReportsAndIssues() {
		def c = Report.createCriteria()

        def list = c.listDistinct {
            createAlias "application", "application"
            createAlias "issues", "issues", CriteriaSpecification.LEFT_JOIN
            order "application.name", "asc"
            order "dateCreated", "desc"
            order "issues.position", "asc"
        } 

        list
    }
}
