package br.com.petrobras.appcenter
import java.text.SimpleDateFormat
import org.apache.commons.lang3.StringEscapeUtils

class ReviewMailService {

	def mailService
    def grailsApplication

	def sendMailNewReview(review, host) {
		try {
            def toList = []
			def admApps
			def dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm")

            admApps = AdminApplication.findAllByApplication(review.application).collect{"${it.admin.key}@petrobras.com.br"}
			def adminAppsSeparatedByComma = admApps.inject {acc, value -> acc + "," + value}
			
			toList.addAll(admApps.toArray())
            
            if (grailsApplication.config.faleComATIC) toList.add(grailsApplication.config.faleComATIC)

            def csvInfo = "${StringEscapeUtils.escapeCsv(review.user.key)},${StringEscapeUtils.escapeCsv(review.application.catalogCode)},${StringEscapeUtils.escapeCsv(dateFormat.format(new Date()))},${StringEscapeUtils.escapeCsv(review.title + " - " + review.text)},${StringEscapeUtils.escapeCsv(review.user.name)},${StringEscapeUtils.escapeCsv(review.application.name)},${StringEscapeUtils.escapeCsv(String.valueOf(review.rating))},${StringEscapeUtils.escapeCsv(adminAppsSeparatedByComma)}"
			log.debug("sending csv info: ${csvInfo}")

            mailService.sendMail {
    			async true
    			to toList
    			subject "[Aplicações Petrobras] - Nova avaliação"
                html(view: "/mail/mailShellEvaluation", model: [template: "/mail/newReview", review: review, host: host, csvInfo : csvInfo])
    		}
        } catch (Exception e) {
            // não incomoda o usuário com erros no envio de email
            log.error e.message, e
        }
    }
	
    def sendMailDeleteReview(review, host) {
        try {
            // necessário para buscar o nome do usuário, isso depende do shiroService
            review.user.name

            mailService.sendMail {
                async true
                to "${review.user.key}@petrobras.com.br" 
                subject "[Aplicações Petrobras] - Exclusão de avaliação"
                html(view: "/mail/mailShell", model: [template: "/mail/deleteReview", review: review, host: host])
            }
        } catch (Exception e) {
            // não incomoda o usuário com erros no envio de email
            log.error e.message, e
        }
    }
}
