package br.com.petrobras.appcenter

import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import java.text.SimpleDateFormat

class ReviewsService {

	def shiroService
	def reviewMailService
	def replyMailService

	static transactional = false
 	
 	@Transactional
    def addReview(review, host){
    	try {
			Assert.isTrue(review != null, "Review Invalida")
			Assert.isTrue(!review.shouldSendMailNewReview() || host != null, "Host inválido")
	    	
	    	review.save()

            if (review.shouldSendMailNewReview()){
                log.debug "sending email review insert"
                reviewMailService.sendMailNewReview(review, host)    
            }

			return review
	    } catch (IllegalArgumentException e) {
			throw new RuntimeException("Review Invalida ou Host Inválido", e)
		} catch (Exception e) {
			throw new RuntimeException("Error trying to addReview to an application with id: ${review?.application?.id}", e)
		}
    }

    def likeReview(long reviewId) {
    	evaluateReview(reviewId, LikeEnum.Like.value)
    }
	def dislikeReview(long reviewId) {
    	evaluateReview(reviewId, LikeEnum.Dislike.value)
    }

    @Transactional
    def evaluateReview(long reviewId, Integer value){
    	try {
            def user = shiroService.loggedUser()

	    	def review = Comment.get(reviewId)

			Assert.isTrue(review != null, "Review Invalida")

			def like = Like.findOrCreateByUserAndComment(user, review)

			like.value = value

			like.save()
	    } catch (IllegalArgumentException e) {
			throw new RuntimeException("Review Invalida", e)
    	} catch (Exception e) {
    		throw new RuntimeException("Error trying to evaluateReview id: $reviewId", e)
    	}
    }
	
	@Transactional
    def editReview(reviewMap, host){
    	try {
			Assert.isTrue(reviewMap && reviewMap.id, "Avaliação inválida")

			// findBy e get não admitem fetch
			def review = Comment.findAllById(reviewMap.id, [fetch: [application: "eager", user: "eager", reply: "eager"]])?.first()

			Assert.notNull(review, "Avaliação inexistente")

			if (review.shouldSendMailNewReview() || review.reply)
				Assert.isTrue(host != null, "Host inválido")

			Assert.isTrue(review.user == shiroService.loggedUser(), "Usuario sem permissão")

			["title", "text", "rating"].each{ review[it] = reviewMap[it] }
			
			// Clean up evaluations
			review.evaluations?.clear()
			
			review.save(flush:true)
			
            if (review.reply) {
                replyMailService.sendMailUpdateReviewWithReply(review, host)
            }

            if (review.shouldSendMailNewReview()){
                reviewMailService.sendMailNewReview(review, host)
            }

			return review
	    } catch (IllegalArgumentException e) {
			throw new RuntimeException("Review ou host inválidos ou usuário alterando não é dono da review. ", e)
		} catch (Exception e) {
			throw new RuntimeException("Error trying to editReview id: ${reviewMap?.id}", e)
		}
		
    }
	
	@Transactional
    def deleteReview(reviewId, host){
    	try {
			Assert.notNull(reviewId, "Review Invalida")

			def review = Comment.findAllById(reviewId, [fetch: [application: "eager", user: "eager", reply: "eager"]])?.first();

			Assert.notNull(review, "Avaliação inexistente")

			if (review.reply)
				Assert.isTrue(host != null, "Host inválido")

			Assert.isTrue(review.user == shiroService.loggedUser() , "Usuário sem permissão")

			review.delete()

	        if (review.reply) {
	        	replyMailService.sendMailDeleteReviewWithReply(review, host)
	        }
	    } catch (IllegalArgumentException e) {
			throw new RuntimeException("Review ou host inválidos ou usuário excluindo não é dono da review. ", e)
		} catch (Exception e) {
			throw new RuntimeException("Error trying to deleteReview id: ${reviewId}", e)
		}

    }

	@Transactional
    def moderateReview(reviewId, host){
    	try {
			Assert.notNull(reviewId, "Review Invalida")

			Assert.isTrue(shiroService.isModerator() , "Usuario não é moderador")

			def review = Comment.findAllById(reviewId, [fetch: [application: "eager", user: "eager", reply: "eager"]])?.first();
			
			Assert.notNull(review, "Avaliação inexistente")

			if (review.reply)
				Assert.isTrue(host != null, "Host inválido")

			review.delete()

	        reviewMailService.sendMailDeleteReview(review, host)

	        if (review.reply) {
	        	replyMailService.sendMailDeleteReviewWithReply(review, host)
	        }
	    } catch (IllegalArgumentException e) {
			throw new RuntimeException("Review ou host inválidos ou usuário excluindo não é moderador. ", e)
		} catch (Exception e) {
			throw new RuntimeException("Error trying to deleteReview id: ${reviewId}", e)
		}

    }

    def listReviewsFromDayToDay(startDay, endDay){
    	Assert.notNull(startDay, "Data de inicio invalida")
    	Assert.notNull(endDay, "Data de fim invalida")

		def formatDay = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
		def start = formatDay.parse(startDay + " 00:00:00")
		def end = formatDay.parse(endDay + " 23:59:59")
		
		Comment.findAllByLastUpdatedBetween(start,end,[sort: 'lastUpdated', order: 'asc'])
    }

	def listMostLikedReviews(applicationId,limit) {
		BaseApplication application = BaseApplication.get(applicationId)

		Assert.notNull(application, "Application not found! with id:${applicationId}")


		if(limit <= 0) limit = 2

		application.getMostLikedReviews(limit)
	}

}