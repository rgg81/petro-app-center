<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>
	<body>
	  <div id="main" role="main" class="row">
	  		<div class="panel petro-panel-box access-url">
	  			<div class="panel-heading-access petro-box-title-url">
	  				<h3 class="pull-left petro-box-title-url">Re-indexando as classes</h3>
	  			</div>
	  			<div class="panel-body petro-panel-body">
	  				<div class="main-row">
		  				<div class="col-md-12 petro-access-url">

		  					<p>Acabamos de inicializar o processo de reindexação das classes.</p>

		  				</div>
	  				</div>
	  			</div>
	  		</div>
	  </div>
	</body>
</html>