<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />

	</head>
	<body>
		<div id="main" role="main" class="row" ng-app="homeApp">
			<!--Início Minhas Aplicações-->
				<div class="panel petro-panel-box" id="my-applications" ng-controller="MyAppsCtrl" ng-show="myApps"><a name="minhas-aplicacoes"></a>
		 			 <div class="panel-heading petro-box-title">
		 			 	<h3 class="pull-left petro-box-title">Minhas Aplicações</h3>
		 			 	
		 			 	<button data-helpref="editar-minhas-aplicacoes" type="button" id="edit-button" class="btn btn-default pull-right petro-home-buttons visible-lg petro-tooltip" title="Edite a exibição de suas aplicações.">editar</button>
		 			 	<button data-helpref="compartilhar-minhas-aplicacoes" type="button" id="share-button" class="btn btn-default pull-right petro-home-buttons petro-app-share-button visible-lg petro-tooltip" title="Compartilhe suas aplicações com outro usuário." data-toggle="modal" data-target="#shareModal"></button>
		 			 </div>
		  			 <div id="myapps" class="panel-body petro-panel-body">
						<ul ng-show-animated="myApps" class="petro-apps-drawer myApps gridly" style="position:relative;">
							<li ng-repeat="application in myApps" data-applicationid="{{application.id}}" class="col16-lg-2 col16-xxs-16 col-xs-6 col-sm-2 brick">

								<g:render template="../angular-templates/application"/>

							</li>
						</ul>
					</div>



						<div class="modal fade" id="shareModal">
						  <div class="modal-dialog">
						    <div class="modal-content">
							  <div class="modal-header petro-modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title">Compartilhar Minhas Aplicações</h4>
						      </div>
						      <div class="modal-body">
						        <div class="col-xs-12 petro-compartilhar-label" style="padding-right: 30px">
							        <div class="col-xs-8"> Deseja compartilhar suas aplicações?</div>
						       		<label class="col-xs-4 switch-light well petro-switch pull-right" onclick="">
										<input type="checkbox" name="share" ng-checked="sharesApps" ng-model="sharesApps" ng-change="shareMyApplications()">
										<span>
											<span class="petro-share-no">Não</span>
											<span class="petro-share-yes">Sim</span>
										</span>

										<a class="btn btn-primary petro-switch-button"></a>
									</label>
				       			</div>
						       	<div class="col-xs-12 petro-compartilhar-label" ng-show="sharesApps">
						       		<div class="col-xs-12"><div class="alert alert-info">Compartilhe suas aplicações com outras pessoas enviando o endereço abaixo</div></div>
						       		<div class="col-xs-12 text-center">
					        			<a class="petro-share-link" target="_blank" href="${createLink(controller: 'user', action: 'apps', id: user.key.toLowerCase(), absolute: true)}">${createLink(controller: 'user', action: 'apps', id: user.key.toLowerCase(), absolute: true)}</a>
						      		</div>
						      	</div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
						      </div>
						    </div><!-- /.modal-content -->
						  </div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
				</div>

			<!--Início Mais acessados por você-->
			<div class="panel petro-panel-box" id="my-most-accessed" ng-controller="HomeCtrl" ng-show="list.applications" ng-init="init('myMostAccessed', 'my-most-accessed-apps')"><a name="minhas-mais-acessadas-apps"></a>
				<div class="panel-heading petro-box-title">
	 				<h3 class="pull-left">Mais Acessadas Por Mim</h3>
	 			</div>			
				<div id="{{list.id}}" class="panel-body petro-panel-body">
					<ul ng-show-animated="list.applications" class="petro-apps-drawer">
						<li ng-repeat="application in list.applications" class="col16-lg-2 col16-xxs-16 col-xs-6 col-sm-2"> 
					
								<g:render template="../angular-templates/application"/>

						</li>
					</ul>
				</div>
			</div>


			<!--Início Mais Populares-->
			<div class="panel petro-panel-box" id="most-popular" ng-controller="HomeCtrl" ng-init="init('popular', 'popular-apps')"><a name="mais-populares"></a>
				<div class="panel-heading petro-box-title">
	 				<h3 class="pull-left">Mais Populares</h3>
	 			</div>			
				<div id="{{list.id}}" class="panel-body petro-panel-body">
					<ul ng-show-animated="list.applications" class="petro-apps-drawer">
						<li ng-repeat="application in list.applications" class="col16-lg-2 col16-xxs-16 col-xs-6 col-sm-2"> 
					
								<g:render template="../angular-templates/application"/>

						</li>
					</ul>

					<button data-loading-text="Carregando..." ng-click="more()" type="button" class="btn petro-show-more-apps showmore">mais aplicações</button> 
				</div>
			</div>

			<!--Início Melhor Avaliadas-->		
			<div class="panel petro-panel-box" id="best-popular" ng-controller="HomeCtrl" ng-init="init('bestRated', 'best-rated-apps')"><a name="melhor-avaliadas"></a>
				<div class="panel-heading petro-box-title">
	 				<h3 class="pull-left">Melhor Avaliadas</h3>
	 			</div>	
				<div id="{{list.id}}" class="panel-body petro-panel-body">
					<ul ng-show-animated="list.applications" class="petro-apps-drawer">
						<li ng-repeat="application in list.applications" class="col16-lg-2 col16-xxs-16 col-xs-6 col-sm-2"> 
							<g:render template="../angular-templates/application"/>

						</li>
					</ul>

					<button data-loading-text="Carregando..." ng-click="more()" type="button" class="btn petro-show-more-apps showmore">mais aplicações</button> 
				</div>				
			</div>
			<script type="text/ng-template" id="hoverApp.html">
				<div class="hoverApp">
    			<p>{{title}} - Titulo</p>
				<button ng-click="showMessage()">showMessage</button>
				</div>
			</script>
		</div> 


	</body>
</html>

