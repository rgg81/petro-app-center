
<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>

		<div class="main-row row">
			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit"><g:message code="default.list.label" args="[entityName]" /></h2>
				<div class="pull-right">
					<g:link action="listcsv" class="btn btn-default petro-review-report-loading petro-home-buttons">Exportar csv</g:link>
				</div>
			</div>
		</div>
			
		<div id="list-application" class="content scaffold-list" role="main">
			<g:if test="${flash.message}">
				<div class="alert alert-success" role="status">${flash.message}</div>
			</g:if>
			
			<div class="petro_pagination">
				<g:paginate total="${applicationInstanceTotal}"  next="&raquo;" prev="&laquo;" />
			</div>
			<div class="row">
				<g:each in="${applicationInstanceList}" status="i" var="applicationInstance">
						<div class="media">
						  <div class="col-sm-2">
							  <a href="${applicationInstance.link.url}" title="Acesso direto" target="blank">
							  	<img src="/${applicationInstance.icon.relativePath()}?${applicationInstance?.icon?.size}" style="width: 100px; height:100px" class="media-object"/>
							  	Acesso direto
							  </a>
						  </div>				
						  <div class="media-body">
						    <h4 class="media-heading"><g:link action="show" id="${applicationInstance.id}">${fieldValue(bean: applicationInstance, field: "name")}</g:link></h4>
						    ${fieldValue(bean: applicationInstance, field: "description")}
						  </div>
						</div>
				 </g:each>
			</div>
			<div class="petro_pagination">
				<g:paginate total="${applicationInstanceTotal}"  next="&raquo;" prev="&laquo;" />
			</div>
		</div>
	</body>
</html>
