var petroAppWidget = document.getElementById('petro-app-widget');
var petroAppUrl =  "${request.scheme}" + "://" + "${request.serverName}" + ":" + "${request.serverPort}" + "/petro-app-center/application/show/" + "${applicationInstance.id}";
var petroAppRatingFormatted = '<g:formatNumber number="${rating}" minFractionDigits="1" format="#,#"/>';
var petroAppRating = "${rating}";
var petroAppBackgound = "<g:resource dir="static/images" file="star.gif" absolute="true" />";
var petroAppHtml = "";

petroAppHtml += "<a href=\"" + petroAppUrl + "\" alt=\"Avaliar ${applicationInstance.name}\" target=\"_blank\" title=\"Avaliar ${applicationInstance.name}\">";
petroAppHtml += "<div style=\"background: #fafafa; border: 1px solid #eee; padding: 6px 8px 8px 8px; height: 30px; width: 170px;  font-family: Tahoma, 'Trebuchet MS', sans-serif;\">";
petroAppHtml +=	"<div style=\"float: left; padding-top: 2px\">";
petroAppHtml +=		"<div style=\"margin-bottom: 5px\"><span style=\"font-size: 11px; text-decoration: none; color:rgb(66, 139, 202);\">Avalie esta aplicação</span></div>";
petroAppHtml +=		"<div style=\"margin-top: 5px; font-size: 9px; color: #008542\">Aplicações Petrobras</div>";
petroAppHtml +=	"</div>";
petroAppHtml +=	"<div style=\"float: right; color: #999\">";
petroAppHtml +=		"<span style=\"font-size: 27px\">" + petroAppRatingFormatted + "</span></div></div></a>";

petroAppWidget.innerHTML = petroAppHtml;

