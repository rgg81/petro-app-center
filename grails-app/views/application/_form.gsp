<%@ page import="br.com.petrobras.appcenter.Application" %>
<%@ page import="br.com.petrobras.appcenter.Compatibility" %>
<%@ page import="org.springframework.validation.FieldError" %>

<!--  inicio do formulario -->
<div class="main-row  petro-edit-app"> 

	<p class="text-right">
		<span class="petro-required-indicator">* campos obrigatórios</span>
	</p>

	<h4>Informações básicas</h4>
<!--NOME APLICAÇÃO-->
		<div ng-class="{'has-error': ((form.catalogCode.$dirty && form.catalogCode.$invalid) || catalogAppNotFound || catalogAppNotUnique)}" class="row fieldcontain required">
			<div class="col-sm-4 form-group">	
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
					<input ng-disabled="application.id || application.name" type="text" class="form-control" ng-change="cleanCatalogCodeMsg()" required name="catalogCode" maxlength="4" ng-model="application.catalogCode" placeholder="Informe o código da aplicação">
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-placement="left" data-container="body" title="" id="help-name" data-html="true" data-content="Ao informar o código da aplicação e efetuar a busca, os campos 'Nome', 'Descrição' 'Tags' e 'Administradores', serão automaticamente preenchidos com os valores informados no Catálogo.<br>Quando da criação da aplicação, ao selecionar a opção para limpar, estas mesmas informações serão apagadas, bem como o código." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>
					</span>
				</div>
				<span class="help-block" ng-show="(form.catalogCode.$dirty && form.catalogCode.$invalid) || catalogAppNotFound || fetchingCatalogData || catalogAppNotUnique">
					<span ng-show="catalogAppNotFound">{{catalogMsg}}</span>
					<span ng-show="fetchingCatalogData">Pesquisando o Catálogo. Aguarde...</span>
					<span ng-show="form.catalogCode.$dirty && form.catalogCode.$error.required">Código da aplicação é obrigatório</span>
					<span class="error" ng-show="catalogAppNotUnique">Já existe uma aplicação cadastrada com este código de aplicação</span>
				</span>
			</div>	
			<div class="col-xs-6 col-sm-3 form-group">
				<button type="button" class="btn btn-success petro-admin-button-form" ng-click="fetchCatalogData()" ng-disabled="checkCatalogForm()"> buscar</button>	
			</div>
			<g:if test="${ ! editMode }">
				<div class="col-sm-3 form-group">
					<button type="button" class="btn btn-default petro-admin-button" ng-click="cleanSomeData()" title="Limpar campos importados do Catálogo de Aplicações"> limpar</button>
				</div>
			</g:if>
		</div>

		<div ng-class="{'has-error': form.name.$dirty && form.name.$invalid}" class="row fieldcontain required">

			<div class="col-sm-10 form-group">	
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
					<input type="text" ng-model="application.name" disabled="disabled" class="form-control" maxlength="150" required ng-maxlength="150" name="name" placeholder="Nome da aplicação" />
					<!--  <span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-placement="left" data-container="body" title="" id="help-name" data-html="true" data-content="Informe um nome para sua aplicação que seja de fácil entendimento para seus usuários. Evite nomes muito longos ou que sejam difíceis de memorizar.<br> O nome da aplicação pode conter até 150 caracteres." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>
					</span>-->
				</div>
				<span class="help-block" ng-show="form.name.$dirty && form.name.$invalid">
		          		<span ng-show="form.name.$error.required">Nome da aplicação é obrigatório</span>
		          		<span ng-show="form.name.$error.maxlength">Tamanho máximo de caracteres é 150</span>
		        </span>

			</div>
		</div>


<!--DEPARTAMENTO APLICAÇÃO-->
		<div ng-class="{'has-error': form.department.$dirty && form.department.$invalid}" class="row fieldcontain ${hasErrors(bean: applicationInstance, field: 'department', 'has-error')} required">	
			<div class="col-sm-10 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
					<input type="text" ng-model="application.department" disabled="disabled" class="form-control" maxlength="150" required ng-maxlength="150" name="department" placeholder="Centro de agilidade" />
				</div>
				<span class="help-block" ng-show="form.department.$dirty && form.department.$invalid">
	          		<span ng-show="form.department.$error.required">Centro de agilidade da aplicação é obrigatória</span>
	          		<span ng-show="form.department.$error.maxlength">Tamanho máximo de caracteres é 150</span>
	        	</span>
			</div>
		</div>


<!--DESCRIÇÃO APLICAÇÃO-->
		<div ng-class="{'has-error': form.description.$dirty && form.description.$invalid}" class="row fieldcontain ${hasErrors(bean: applicationInstance, field: 'description', 'has-error')} required">	
			<div class="col-sm-10 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
					<textarea class="form-control" disabled="disabled" ng-model="application.description" required name="description" ng-maxlength="4000" maxlength="4000" placeholder="Descrição" rows="5" escapeHtml="false"></textarea>
					<!--  
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-toggle="popover" data-trigger="click" data-container="body" data-placement="left" title="" id="help-name" data-html="true" data-content="Você deverá informar a descrição da aplicação, com a seguinte estrutura:<br>- Frase de impacto;<br>- Resumo da aplicação;<br>- Funcionalidades da aplicação;<br>- Não requisitos ou limitações de uso.<br>A descrição da aplicação pode conter até 4000 caracteres." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span> -->
				</div>
				<span class="help-block" ng-show="form.description.$dirty && form.description.$invalid">
	          		<span ng-show="form.description.$error.required">Descrição da aplicação é obrigatória</span>
	          		<span ng-show="form.description.$error.maxlength">Tamanho máximo de caracteres é 4000</span>
	        	</span>
			</div>
		</div>

<!--URL APLICAÇÃO-->	
		<div ng-class="{'has-error': form.url.$dirty && form.url.$invalid}" class="row fieldcontain ${hasErrors(bean: applicationInstance, field: 'link.url', 'has-error')} required">		
			<div class="col-sm-10 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
					<input ng-model="application.link.url" id="url-app" required class="form-control" name="url"  placeholder="Link da aplicação. Iniciar com http:// ou https://"  />
				</div>
				<span class="help-block" ng-show="form.url.$dirty && form.url.$invalid">
		          	<span class="error" ng-show="form.url.$error.required">Url é obrigatória</span>
		          	<span class="error" ng-show="form.url.$error.unique">Já existe uma aplicação cadastrada com esta Url</span>
		      		<span class="error" ng-show="form.url.$error.url">Url inválida</span>
		        </span>
			</div>

		</div>

		<div class="row fieldcontain required">
			<div class="col-sm-10 form-group">
				<div class="row">
					<div class="col-sm-5">
						<span class="margin-left-form-app">A aplicação vai ser diponibilizada via download?</span>
					</div>
					<div class="col-sm-4">
						<switch name="name" on="sim" off="não" ng-model="application.isDownloadable" class="green"></switch>
					</div>
				</div>
			</div>
		</div>

<!--IMAGEM APLICAÇÃO-->	
		<div class="row">
			<div class="col-sm-4 form-group">	
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-required-indicator">*</span>
  					</span>
  					<div class="thumbnail petro-required">
						<img id="icon-app" alt="Icone App" ng-src="{{iconPath()}}">
					<!-- TODO: avaliar se precisamos disso --> 
						<input class="form-control" type="hidden" name="icon" />
					</div>
				</div>
			</div>
			<div class="col-sm-6 form-group petro-img-uploader">				
			    <uploader:uploader multiple="false" params="${[ guid: UUID.randomUUID().toString()]}" url="${[controller:'imageUpload', action:'upload']}" id="yourUploaderId">
			 		<uploader:onComplete>
							if (responseJSON.success)
							{
								$('#icon-app').attr("src", '/icons/' + fileName);
								$('input[name="icon"]').val(fileName);
								//gambiarra necessaria para forcar o angular a fazer a validacao final do form
								$('#url-app').trigger('blur');
							}
							
					</uploader:onComplete>
				</uploader:uploader>
				
					<ul class="alert petro-alert alert-info petro-alert-info">
						<li>
							A imagem deverá ter o tamanho de 119 x 119 pixels e as extensões permitidas são: .jpg e .png
						</li>
					</ul>
				
		    </div>

		</div>
</div>

<!--TAGS-->	
<div class="main-row petro-edit-app">

	<h4>Tags</h4>

		<div ng-class="{'has-error': (form.key.$error.maxlength && triggertagvalidation) || (invalidtag) || (duplicatetag)}" class="row fieldcontain">	
			<div class="col-sm-4 form-group">	
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
  					</span>
					<input type="text" ng-model="tag" maxlength="200" ng-maxlength="200" class="form-control" typeahead="onetag for onetag in getTags($viewValue)" ng-change="cleanTagMsg()" name="tag" autocomplete="off" value="" placeholder="Tag"/>
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-container="body" data-html="true" data-placement="left" title="" id="help-name" data-content="As tags são palavras que servem como uma etiqueta e ajudam na hora de organizar informações, agrupando aquelas que receberam a mesma marcação, facilitando encontrar outras relacionadas.<br>Para as aplicações cujos dados foram importados do Catálogo de Aplicações, as tags são representadas aqui pelas palavras-chaves." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span>
				</div>
				<span class="help-block" ng-show="invalidtag">A tag não pode ser vazia</span>
        		<span class="help-block" ng-show="duplicatetag">Tag já existente</span>
        		<span class="help-block" ng-show="form.key.$error.maxlength && triggertagvalidation">Tamanho da tag deve ser de 200 caracteres</span>
			</div>
			<div class="col-sm-6 form-group">
				<button type="button" class="btn btn-default petro-admin-button" ng-click="addTag(tag)">
					<g:message code="user.application.button.add.label" default="Adicionar"/></button>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 form-group">

				<div class="input-group">
					<span class="input-group-addon petro-addon">
							<span class="petro-not-required"></span>
	  				</span>
					<ul class="nav-pills petro-tags-edit-nav">				
						<li ng-repeat="tag in application.tags" class="petro-tags-edit">
								<span>{{tag.name}}</span>

								<div class="petro-tags-edit-close">
								<!--início botao fechar-->
									<a href="" ng-click="removeTag(tag)" title='<g:message code="user.application.button.delete.label" default="Remover"/>'>
										<span class="glyphicon glyphicon-remove-circle"></span>
									</a>
								<!--final botao fechar-->
								</div>
						</li>			
					</ul>
				</div>

			</div>
		</div>

</div>


<!--VERSÃO -->	
<div class="main-row petro-edit-app">

	<h4>Versões</h4>
	 

		<div class="row">

		<div ng-class="{'has-error': form.versionNumber.$invalid || versionNumberRequired}" class="fieldcontain">
			<div class="col-sm-4 form-group">

				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
  					</span>	
					<input type="text" ng-model="application.currentVersion.number" ng-change="cleanVersionMsg()" class="form-control" ng-maxlength="20" maxlength="20" name="versionNumber" placeholder="0.0"  />
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-html="true" data-container="body" data-placement="left" title="" id="help-name" data-content="Informe a versão atual da aplicação. <br>A versão da aplicação pode conter até 20 caracteres." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span>				
				</div>
				<span class="help-block" ng-show="form.versionNumber.$error.maxlength">Tamanho máximo de caracteres é 20</span>
				<span class="help-block" ng-show="versionNumberRequired">Número da versão é obrigatório</span>
			</div>
		</div>

<!--DATA VERSÃO-->		
		<div ng-class="{'has-error': versionDateRequired}" class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'currentVersion.date', 'has-error')}">	
			<div class="col-sm-4 form-group">
				<div class="input-group input-append date">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
  					</span>	
					<input ng-model="application.currentVersion.date" ng-change="cleanVersionMsg()" name="versionDate" type="text" class="form-control" placeholder="dd/mm/aaaa" ljap-date-picker>
					 <span class="add-on input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-container="body" data-placement="left" title="" id="help-date-version" data-content="Informe a data em que esta versão foi implantada." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>		
					</span>
				</div>
				<span class="help-block" ng-show="versionDateRequired">Data da versão é obrigatório</span>
			</div>
		</div>	
		<!--  <button ng-cloak type="button" class="btn btn-primary" ng-click="newVersion()" ng-show="shouldShowAddVersion" ng-disabled="shouldDisableAddVersion()">Nova versão</button> -->
		<div class="col-sm-3 form-group">
				<button type="button" class="btn btn-default petro-admin-button" ng-click="newVersion()">adicionar</button>
		</div>
	</div>

<!--DESCRIÇÃO VERSÃO-->		
		<div ng-class="{'has-error': form.versionDescription.$dirty && form.versionDescription.$invalid}" class="row fieldcontain ${hasErrors(bean: applicationInstance, field: 'currentVersion.description', 'has-error')} ">		
			<div class="col-sm-8 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
  					</span>	
					<div class="input-group petro-textarea">
						<textarea class="form-control" ng-model="application.currentVersion.description" name="versionDescription" ng-maxlength="4000" maxlength="4000" hack-max-length="400" placeholder="Descrição" rows="4" escapeHtml="false" all-or-none ></textarea>
					</div>
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-html="true" data-container="body" data-placement="left" title="" id="help-name" data-content="Informe uma breve descrição da versão da aplicação, como bugs corrigidos, melhorias implementadas. Deixe seu usuário informado de como está a evolução de sua aplicação.<br> A descrição da versão da aplicação pode conter até 4000 caracteres." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span>
				</div>
			    <span ng-show="form.versionDescription.$error.maxlength">Tamanho máximo de caracteres é 4000</span>
			</div>

			<div class="col-sm-3 form-group">
				<ul class="alert petro-alert alert-info petro-alert-info">
					<li>
						É obrigatória ao menos uma versão
					</li>
				</ul>
			</div>

		</div>
		
		<div class="row nav-pills">
			<div class="col-md-4 col-sm-6 form-group">
				<div class="input-group" ng-repeat="oneVersion in application.versions">
					<span class="input-group-addon petro-addon">
							<span class="petro-not-required"></span>
	  				</span>
	  				<div class="petro-tags-edit-nav">	
						<div class="petro-tags-edit">

							<a href ng-click="editVersion(oneVersion)" title="Editar / Ver detalhes da versão">
							<g:message code="application.version.label" default="Versão" />
							{{oneVersion.number}}&nbsp;&nbsp;-&nbsp;&nbsp;{{oneVersion.date}} </a>

							<div class="petro-tags-edit-close">
							<!--início botao fechar-->					
								<a href="" ng-click="deleteVersion(oneVersion)" title='<g:message code="user.application.button.delete.label" default="Remover"/>'>
									<span class="glyphicon glyphicon-remove-circle"></span>
								</a>
							<!--final botao fechar-->
							</div>

						</div>
					</div>	
				</div>
			</div>
			<g:render template="../angular-templates/ljapModalEditVersion"/>
		</div>


</div>

<!--INFORMAÇÕES DE SUPORTE-->	
<div class="main-row petro-edit-app">

	<h4>Suporte</h4>

		<div ng-class="{'has-error': ((form.suportInfoDescription.$error.maxlength || form.suportInfoUrl.$error.maxlength) && triggersuportinfovalidation) || (invalidsuportinfo) || (duplicatesuportinfo) || (form.suportInfoUrl.$error.url && triggersuportinfovalidation)}"  class="row fieldcontain">
			<div class="col-sm-4 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
					</span>
					<input type="text" ng-model="suportInfo.description" maxlength="100" ng-maxlength="100" class="form-control" ng-change="cleanSuportInfoMsg()" name="suportInfoDescription" autocomplete="off" value="" placeholder="Nome do link"/>
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-html="true" data-container="body" data-placement="left" title="" id="help-name" data-content="Informe links que suportem os seus usuários no seu dia a dia de uso da aplicação. Exemplos: manual da aplicação, um FAQ, página de ajuda, material de treinamento." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span>
				</div>
				 <span class="help-block" ng-show="duplicatesuportinfo">
      				Informação de suporte já existente
    			</span>
    			<span class="help-block" ng-show="invalidsuportinfo">A descrição da informação de suporte não pode ser vazia</span>
    			<span class="help-block" ng-show="(form.suportInfoDescription.$error.maxlength || form.suportInfoUrl.$error.maxlength) && triggersuportinfovalidation">Tamanho deve ser de 100 caracteres</span>
			</div>
			<div class="col-sm-4 form-group">
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
					</span>
					<input type="url" ng-model="suportInfo.url" class="form-control" ng-change="cleanSuportInfoMsg()" name="suportInfoUrl" autocomplete="off" value="" placeholder="http:// ou https://"/>
				</div>
				<span class="help-block" ng-show="invalidsuportinfo">A descrição da url não pode ser vazia</span>
				 <span class="help-block" ng-show="form.suportInfoUrl.$error.url && triggersuportinfovalidation">
    				Url inválida
    			</span>
			</div>
    		<div class="col-sm-3 form-group">
				<button type="button" class="btn btn-default petro-admin-button" ng-click="addSuportInfo($event)"><g:message code="user.application.button.add.label" default="Adicionar"/></button>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 form-group">

				<div class="input-group">
					<span class="input-group-addon petro-addon">
							<span class="petro-not-required"></span>
	  				</span>
					<ul class="nav-pills petro-tags-edit-nav">				
						<li ng-repeat="oneSuportInfo in application.informationsSupport" class="petro-tags-edit">
								<span>{{oneSuportInfo.description}} - {{oneSuportInfo.url}}</span>

								<div class="petro-tags-edit-close">
									<!--início botao fechar-->
									<a href="" ng-click="removeSuportInfo(oneSuportInfo)" title='<g:message code="user.application.button.delete.label" default="Remover"/>'>
										<span class="glyphicon glyphicon-remove-circle"></span>
									</a>
									<!--final botao fechar-->
								</div>
						</li>			
					</ul>
				</div>

			</div>
		</div>

</div>

<!--COMPATIBILIDADE-->
<div class="main-row petro-edit-app">
	<h4>Compatibilidade</h4>
		<div class="row fieldcontain">			
			<div ng-class="{'petro-opacity': checkClassOpacity(oneCompatibility)}" ng-repeat="oneCompatibility in application.allcompatibilities" class="col-xs-offset-1 col-xs-12 col-sm-12 col-md-12" >
            	<div class="col-xs-3 col-sm-1 col-md-1 petro-img-compatible">
            		<a ng-click="toogleCompatibility(oneCompatibility)" href="" >
            			<img  ng-src="/petro-app-center/images/compatibilidade/{{oneCompatibility.nameImage}}" ng-attr-title="{{oneCompatibility.description}}" />
            		</a>
        		</div>
          	</div>      
		</div>
</div>


<!--ADMINISTRADORES-->
<div class="main-row petro-edit-app"> 

	<h4>Administradores</h4>

		<div ng-class="{'has-error': ((form.key.$error.minlength || form.key.$error.maxlength) && triggerkeyvalidation) || (invalidkey) || (usernotexist) || (duplicatekey)}" class="row fieldcontain">		
			<div class="col-sm-4 form-group">	
				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
  					</span>
					<input type="text" ng-model="key" maxlength="4" ng-maxlength="4" ng-minlength="4" class="form-control" ng-change="cleanKeyMsg()" name="key" autocomplete="off" placeholder="Chave do administrador" value="" placeholder=""/>
					<span class="input-group-addon petro-addon-help">
						<a href="#" class="petro-question" onclick="return false;" data-trigger="click" data-toggle="popover" data-container="body" data-placement="left" title="" id="help-name" data-content="Informe os administradores de sua aplicação. As chaves cadastradas terão acesso a editar e publicar a aplicação." data-original-title="Ajuda"><span class="glyphicon glyphicon-question-sign"></span></a>			
					</span>
				</div>
				<span class="help-block" ng-show="invalidkey">Favor preencher a chave</span>
				<span class="help-block" ng-show="duplicatekey">Chave já existente</span>
        		<span class="help-block" ng-show="(form.key.$error.minlength || form.key.$error.maxlength) && triggerkeyvalidation">
          				Tamanho da chave deve ser de quatro caracteres
        		</span>
        		<span class="help-block" ng-show="usernotexist">Usuário com a chave {{key}} não foi encontrado</span>
			</div>
						
			<div class="col-sm-2 form-group">
				<button type="button" class="btn btn-default petro-admin-button" ng-click="addAdmin(key)"><g:message code="user.application.button.add.label" default="Adicionar"/></button>
			</div>
			<div class="col-sm-4">
				<ul class="alert petro-alert alert-info petro-alert-info">
						<li>
							É obrigatório ao menos um administrador
						</li>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12 form-group">

				<div class="input-group">
					<span class="input-group-addon petro-addon">
						<span class="petro-not-required"></span>
	  				</span>
					<ul class="nav-pills petro-tags-edit-nav">				
						<li ng-repeat="adminApp in application.adminsApp" class="petro-tags-edit">
								<span>{{adminApp.admin.name}} - {{adminApp.admin.key}}</span>

								<div class="petro-tags-edit-close">
								<!--início botao fechar-->
									<a href="" ng-click="removeAdmin(adminApp)" title='<g:message code="user.application.button.delete.label" default="Remover"/>'>
										<span class="glyphicon glyphicon-remove-circle"></span>
									</a>
								<!--final botao fechar-->
								</div>
						</li>			
					</ul>
				</div>

			</div>
		</div>
	</div>

<script type="text/ng-template" id="confirmCatalogImportModal.html">
	<div class="row petro-edit-app">
		<div class="col-md-12">
		<!-- Modal -->
	    	<div class="modal-header petro-modal-header">
				<button type="button" class="close" ng-click="no()">&times;</button>
				<h4 class="modal-title petro-modal-title">
					Aplicações Petrobras
				</h4>
			</div>

			<div class="modal-body petro-modal-body">
				<p>
					<ul class="alert petro-alert alert-success">
						<li>Essa aplicação encontra-se em "Homologação" no catálogo de aplicações. Confirma que a aplicação, de fato, está em produção?</li>
					</ul>
				</p>											
			</div>

			<div class="modal-footer petro-modal-footer petro-label-success">
				<p>
			      	<button type="button" class="btn btn-default petro-my-options" ng-click="no()">Não</button> 
		      		<button type="button" class="btn btn-success" ng-click="yes()">Sim</button>
				</p>
			</div>
		</div>
	</div>
</script>

<g:javascript>
	$("[data-toggle=popover]").popover()
</g:javascript>
