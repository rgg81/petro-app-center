var petroAppWidget = document.getElementById('petro-app-widget');
var petroAppUrl =  "${request.scheme}" + "://" + "${request.serverName}" + ":" + "${request.serverPort}" + "/petro-app-center/application/show/" + "${applicationInstance.id}";
var petroAppRating = '<g:formatNumber number="${rating}" minFractionDigits="1" format="#,#"/>';
var petroAppIcon = "<g:resource dir="static/images" file="icon-petro-app.png" absolute="true" />";
var petroAppHtml = "";

petroAppHtml += "<a href=\"" + petroAppUrl + "\" target=\"_blank\" style=\"text-decoration:none;\" alt=\"Avaliar ${applicationInstance.name}\" title=\"Avaliar ${applicationInstance.name}\">";
petroAppHtml += "<div style=\"width: 60px; display: inline-block; padding: 2px; font-family: Tahoma, 'Trebuchet MS', sans-serif;\">";
petroAppHtml += "<div style=\"float: left;\">";
petroAppHtml += "<img src=\"" + petroAppIcon + "\" width=\"25 px;\"/>";
petroAppHtml += "</div>";
petroAppHtml += "<div style=\"float: right; text-align: center; padding-top: 2px; font-size: 17px;color: #999\">" + petroAppRating + "</div>";
petroAppHtml += "</div></a>";

petroAppWidget.innerHTML = petroAppHtml;