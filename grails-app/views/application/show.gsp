<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />

		<r:require module="angular-sanitize"/>
		<r:require module="bootstrapAngularUI" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="reviewControllerAngular" />
		<title>${applicationInstance?.name}</title>
	</head>
	
	<body>
		<!--a href="#show-application" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.female.label" args="[entityName]" /></g:link></li>
			</ul>
		</div-->
		<div class="row" ng-init="appId=${params.id}" ng-app="reviewsApp">
		
			<div id="show-application" class="col-md-12 panel petro-panel-box" role="main">
				<div class="main-row row petro-app-detail">
					<div class="col-sm-2 col-md-2 petro-icon-image-wrapper">
						<img src="/${applicationInstance?.icon?.relativePath()}?${applicationInstance?.icon?.size}" class="img-responsive" />
					</div>
					
					<div class="col-sm-6 col-md-8">
						<h3><g:fieldValue bean="${applicationInstance}" field="name"/></h3>

						<rate:rating active="false" bean='${applicationInstance}' id="fixo"/>

						<application:isPermitted id="${applicationInstance?.id}">
							<g:link class="btn petro-btn-moderator petro-btn-moderator-edit col-sm-3 col-md-4" action="edit" id="${applicationInstance?.id}">
								<g:message code="default.button.edit.label" default="Editar" />
							</g:link>
						</application:isPermitted> 
					</div>
					
					<div class="col-sm-4 col-md-2" ng-controller="userApplicationCtrl">

						<g:if test="${applicationInstance.isDownloadable}">
							<a  class="btn petro-btn" title="Baixe esta aplicação" href="${applicationInstance?.link?.url}" target="_blank" appId="${applicationInstance.id}" where="showapp" register-access>
								Baixar
								<span class="glyphicon glyphicon-download-alt petro-btn-accessory"></span>
							</a>
						</g:if>
						<g:else>
							<a  class="btn petro-btn" title="Acesse esta aplicação" href="${applicationInstance?.link?.url}" target="_blank" appId="${applicationInstance.id}" where="showapp" register-access>
								<g:message code="user.application.button.access.label" default="Acessar" />
								<span class="glyphicon glyphicon-play petro-btn-accessory"></span>
							</a>
						</g:else>


						<button type="button" data-helpref="detalhes-da-aplicacao" class="btn petro-btn petro-tooltip" ng-click="removeUserApplication($event)" title="Remova esta aplicação de Minhas Aplicações" ng-show="shouldShow(${user?.myApplications*.application?.contains(applicationInstance)}, 'remove')" ng-cloak>
							<span class="label-btn-add-remove"><g:message code="user.application.button.delete.label" default="Remover" /></span>
							<span class="glyphicon glyphicon-remove petro-btn-accessory"></span>
						</button>

						<div class="modal fade" id="removeUserApplicationMessageModal" tabindex="-1" role="dialog" aria-labelledby="remove application message" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
							    <div class="modal-header petro-modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title petro-modal-title">
										Aplicações Petrobras
									</h4>
							  	</div>	
							    <div class="modal-body petro-modal-body">
						      		<p>
						      			<ul class="alert petro-alert alert-success">
						      				<li>Aplicação removida com sucesso das Minhas Aplicações.</li>
						      			</ul>
									</p>
						      	</div>
							    <div class="modal-footer petro-modal-footer"></div>
						    </div><!-- /.modal-content -->
						  </div><!-- /.modal-dialog -->
						</div><!-- /.modal -->

						<g:if test="${!applicationInstance.isDownloadable}">
							<button type="button" data-helpref="detalhes-da-aplicacao" class="btn petro-btn petro-tooltip" ng-click="addUserApplication($event)" title="Adicione esta aplicação às Minhas Aplicações" ng-show="shouldShow(${user?.myApplications*.application?.contains(applicationInstance)}, 'add')" ng-cloak>
								<span class="label-btn-add-remove"><g:message code="user.application.button.add.label" default="Adicionar" /></span>
								<span class="glyphicon glyphicon-plus petro-btn-accessory"></span>
							</button>
						</g:if>
						<div class="modal fade" id="addUserApplicationMessageModal" tabindex="-1" role="dialog" aria-labelledby="add application message" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">
								<div class="modal-header petro-modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title petro-modal-title">
										Aplicações Petrobras
									</h4>
								</div>		
						      	 <div class="modal-body petro-modal-body">
						      		<p>
						      			<ul class="alert petro-alert alert-success">
						      				<li>Aplicação adicionada com sucesso às Minhas Aplicações.</li>
						      			</ul>
						      		</p>
						      	</div>
						      	<div class="modal-footer petro-modal-footer"></div>
						    </div><!-- /.modal-content -->
						  </div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
					</div>
				</div>
				
				<g:if test="${flash.message}">
					<div class="row">
						<div class="alert alert-success" role="status">${flash.message}</div>
					</div>
				</g:if>
				
				<!-- App description -->
				<div class="main-row row">
					<div class="col-md-12"> 
						<h4><g:message code="application.description.label" default="Description" /></h4>
						
						<span class="property-value petro-detail-field">
							${fieldValue(bean: applicationInstance, field: 'description').replaceAll("\n","<br>")}
						</span>

						<p class="petro-link-report">
							<a data-toggle="modal" data-helpref="informe-erros-de-cadastro" href="#report-issue" class="petro-tooltip" title="Ajude a melhorar o conteúdo da informação da aplicação.">
								<g:message code="report.create.label" default="Informar erros de cadastro" />
							</a>
							<g:render template="../angular-templates/ljapModalReport"/>
							<g:render template="../angular-templates/ljapModalAlertSuccessfullySubmittedReport"/>
						</p>						
					</div>

					<g:if test="${ applicationInstance?.tags }">
						<div class="col-md-12 petro-detail-field petro-tags">
							<label><g:message code="application.tags.label" default="Tags" />:</label>
				       		<g:each in="${applicationInstance?.tags}" var="tag">
				       			<g:link controller="searchApp" action="index" params="[q: tag.name]">
				       				<span class="petro-tag-btn">${fieldValue(bean: tag, field: 'name')}</span>
				       			</g:link>
				       		</g:each>
						</div>
					</g:if>
				</div>
				
				<div ng-controller="ReviewsCtrl">					
					<!-- Best Reviews -->
					<div class="main-row row">
						<div class="col-md-12"> 
							<h4><g:message code="application.reviews.label" default="Reviews" /></h4>
							<div class="row">
								<ratingScore:ratingScoreContext bean='${applicationInstance}'>
									<div class="col-md-4">
										<div class="petro-app-score">
											<span class="petro-app-score-value"><ratingScore:mean /></span> média								
										</div>
										<div class="petro-label-votes">
											<span class="petro-app-score-votes"><ratingScore:total /></span> <span class="petro-score-label-votes">votos</span>
										</div>
										<ratingScore:customDraw>
											<div>
												<span class="petro-score-group">
													<g:each in="${ (1..stars) }">
														<div class="star petro-star-on"></div>
													</g:each>
													<g:each in="${ (stars..<5) }">
														<div class="star petro-star-off"></div>
													</g:each>
												</span>
												<span class="petro-rating-percent">
													<g:if test="${total == 0}">
														<div class="petro-rating-percent-value" style="width: 0%"></div>
													</g:if>
													<g:if test="${total != 0}">
														<div class="petro-rating-percent-value" style="width: ${(100*ratings/total) }%"></div>
													</g:if>
												</span>
												<div class="petro-rating-total">
													<shiro:hasRole name="${ grailsApplication.config.app.role.moderator }">
														<a href="#" ng-click="showReviewsByRating(${stars})" ng-show="${ratings}" data-toggle="modal" data-target="#ratingsModal">${ratings}</a>
														<span ng-show="${!ratings}">${ratings}</span>
													</shiro:hasRole>
													<shiro:lacksRole name="${ grailsApplication.config.app.role.moderator }">
														<span>${ratings}</span>
													</shiro:lacksRole>
												</div>
											</div>
										</ratingScore:customDraw>
									</div>
								</ratingScore:ratingScoreContext>

								<div class="col-md-4 petro-review-most-liked hack-float-left-parent" ng-repeat="review in mostLiked">
									<div class="petro-review-name">
										{{review.user.name}}
									</div>
									<div class="petro-review-department">
										{{review.user.department}}
									</div>
									<div class="petro-review-rating">
										<table class="ratingDisplay">
							                <tbody>
							                	<tr>
							            			<td><div class="star on"><a></a></div></td>
							            			<td><div class="star {{(review.rating > 1) && 'on' || 'off'}}"><a></a></div></td>
							            			<td><div class="star {{(review.rating > 2) && 'on' || 'off'}}"><a></a></div></td>
							            			<td><div class="star {{(review.rating > 3) && 'on' || 'off'}}"><a></a></div></td>
							            			<td><div class="star {{(review.rating > 4) && 'on' || 'off'}}"><a></a></div></td>
							                	</tr>
							            	</tbody>
							            </table>
									</div>
									<div class="petro-review-title">
										{{review.title}}
									</div>
									<div class="petro-review-text">
										<span ng-bind-html="review.text.substring(0,270) | newlines | unsafe"></span>
										<span ng-show="review.text.length>270 || review.reply.id">
											<span ng-show="review.text.length>270">...</span> <!-- Mostra a reticência -->
											<div>
												<a data-toggle="modal" data-target="#viewReview-{{review.id}}" href="#">
													<g:message code="user.application.button.viewcomment.label" default="View comment" />
												</a>
												<div class="modal fade" id="viewReview-{{review.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													<div class="modal-dialog">
												    	<div class="modal-content">
															<div class="modal-header petro-modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																<h4 class="modal-title petro-modal-title">
																	<g:message code="user.application.button.viewcomment.label" default="View comment" />
																</h4>
															</div>
															<div class="modal-body hack-float-left-parent">
																<div class="petro-review-name">
																	{{review.user.name}}
																</div>
																<div class="petro-review-department">
																	{{review.user.department}}
																</div>
																<div class="petro-review-rating">
																	<table class="ratingDisplay">
														                <tbody>
														                	<tr>
														            			<td><div class="star on"><a></a></div></td>
														            			<td><div class="star {{(review.rating > 1) && 'on' || 'off'}}"><a></a></div></td>
														            			<td><div class="star {{(review.rating > 2) && 'on' || 'off'}}"><a></a></div></td>
														            			<td><div class="star {{(review.rating > 3) && 'on' || 'off'}}"><a></a></div></td>
														            			<td><div class="star {{(review.rating > 4) && 'on' || 'off'}}"><a></a></div></td>
														                	</tr>
														            	</tbody>
														            </table>
																</div>
																<div class="petro-review-title">
																	{{review.title}}
																</div>
																<div class="petro-review-text" ng-bind-html="review.text | newlines | unsafe">
																</div>
																<div class="petro-review-date">
																	{{review.date}}
																</div>
																<div style="margin-top:1em;">
																	<span ng-show="review.reply.id">	
																		<div class="petro-adm-answer-background">
																			<span class="petro-reply-arrow"><img src="${resource(dir: 'images', file: 'arrow-adm-reply.png')}"	
																			 title="resposta" /></span>
																			<div class="petro-reply">
																				<div class="petro-review-name">
																				Responsável pela Aplicação
																				</div>
																				<div>
																				{{review.reply.text}}
																				</div>
																				<div class="petro-review-date">
																				{{review.reply.date}}
																				</div>
																			</div>
																		</div>
																	</span>
																</div>					
															</div>

															<div class="modal-footer">
															</div>
														</div><!-- /.modal-content -->
													</div><!-- /.modal-dialog -->
												</div><!-- /.modal -->
											</div>
										</span> 
									</div>
									<div class="petro-review-date">
										{{review.date}}
									</div>
									<div class="petro-review-fromothers">
										<div class="petro-review-fromothers-like-on" ng-show="review.myevaluation > 0" >
											gostar <span>{{review.likes}}</span>
										</div>
										<div class="petro-review-fromothers-like-off" ng-show="review.myevaluation <= 0" ng-click="like(review, $event)">
											gostar <span>{{review.likes}}</span>
										</div>
										<div class="petro-review-fromothers-dislike-on" ng-show="review.myevaluation < 0">
											não gostar <span>{{review.dislikes}}</span>
										</div>
										<div class="petro-review-fromothers-dislike-off" ng-show="review.myevaluation >= 0" ng-click="dislike(review, $event)">
											não gostar <span>{{review.dislikes}}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- My comment -->
					<div class="main-row row petro-my-review">
						<div ng-show-animated="myReview" class="container">
							<h4><g:message code="user.application.button.yourcomment.label" default="Your comment" /></h4>
							<div class="petro-my-review-content">
								<div class="col-md-12">
									<div class="row hack-float-left-parent">
										<div class="petro-review-name">
											{{myReview.user.name}}
										</div>
										<div class="petro-review-rating">
											<table class="ratingDisplay">
								                <tbody>
								                	<tr>
								            			<td><div class="star on"><a></a></div></td>
								            			<td><div class="star {{(myReview.rating > 1) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(myReview.rating > 2) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(myReview.rating > 3) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(myReview.rating > 4) && 'on' || 'off'}}"><a></a></div></td>
								                	</tr>
								            	</tbody>
								            </table>
										</div>
										<div class="petro-review-title">
											{{myReview.title}}
										</div>
										<div class="petro-review-text" ng-bind-html="myReview.text | newlines | unsafe">
			
										</div>
										<div class="petro-review-date">
											{{myReview.date}}
										</div>
										<div class="petro-review-fromothers" ng-hide="myReview.text == '' || myReview.text == null">
											<div class="petro-review-fromothers-like-on" ng-show="myReview.myevaluation > 0" >
												gostar <span>{{myReview.likes}}</span>
											</div>
											<div class="petro-review-fromothers-like-off" ng-show="myReview.myevaluation <= 0" ng-click="like(myReview, $event)">
												gostar <span>{{myReview.likes}}</span>
											</div>
											<div class="petro-review-fromothers-dislike-on" ng-show="myReview.myevaluation < 0">
												não gostar <span>{{myReview.dislikes}}</span>
											</div>
											<div class="petro-review-fromothers-dislike-off" ng-show="myReview.myevaluation >= 0" ng-click="dislike(myReview, $event)">
												não gostar <span>{{myReview.dislikes}}</span>
											</div>
										</div>
									</div>
									<div style="margin-top:1em;">
										<span ng-show="myReview.reply.id">	
											<div class="petro-adm-answer-background">
												<span class="petro-reply-arrow"><img src="${resource(dir: 'images', file: 'arrow-adm-reply.png')}"	
												 title="resposta" /></span>
												<div class="petro-reply">
													<div class="petro-review-name">
													Responsável pela Aplicação
													</div>
													<div>
													{{myReview.reply.text}}
													</div>
													<div class="petro-review-date">
													{{myReview.reply.date}}
													</div>
												</div>
											</div>
										</span>
									</div>
									<div class="row petro-my-review-btns">
										<a href="" ng-click="removeMyReview()" class="btn btn-default petro-my-options">excluir</a>
										<a ng-click="editMyReview()" data-toggle="modal" href="#newReview" class="btn btn-success">editar</a>
									</div>
								</div>
							</div>	
						</div>		
				
						<div class="col-md-4 petro-my-review-addnew" ng-show-animated="!myReview">
							<a data-helpref="avaliar-aplicacao" class="btn btn-success petro-tooltip" data-toggle="modal" href="#newReview" ng-click="addReview()" title="Descreva sua experiência de uso.">
								<g:message code="user.application.button.addreview.label" default="Review and comment" />
							</a>
						</div>
						
						<!-- Modal -->
						<div class="modal fade" id="newReview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
						    	<div class="modal-content">
									<div class="modal-header petro-modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title petro-modal-title">
											<g:message code="user.application.button.addreview.label" default="Review and comment" />
										</h4>
									</div>
									<div class="modal-body petro-modal-body">
										<form name="addReviewForm" class="col-md-12" role="form">
											<p>
												<ul class="alert petro-alert alert-info petro-alert-info" role="status">
													<li>
														Descreva as impressões de sua experiência de uso desta aplicação. Sua avaliação poderá ser alterada a qualquer momento.
													</li>
													<li>
														Para erros específicos da aplicação, como indisponibilidade, erros de lógica, utilizar o <a target="_blank" href="http://atendimento.petrobras.com.br">WEB 881</a>.
													</li>
												</ul>

												<ul class="alert petro-alert petro-alert-warning" role="status" ng-show="myNewReview.errors">
													<li ng-repeat="error in myNewReview.errors">
														{{error}}
													</li>
												</ul>
											</p>

											<p>
												<div class="petro-big-rating">
													<span ng-repeat="s in stars" ng-click="rate(s, $event)"></span>
												</div>

											</p>
											<p>
								          		<input type="text" placeholder='<g:message code="application.reviews.comment.title.label" default="Título"/>' name="myNewReviewTitle" ng-model="myNewReview.title" class="form-control">
											</p>
											<p>
								          		<textarea name="myNewReviewText" placeholder='<g:message code="application.reviews.comment.text.label" default="Texto"/>' ng-model="myNewReview.text" class="form-control" rows="8" maxlength="{{metadata.text.maxSize}}" decorate>
								          		</textarea>
											</p>
										</form>
	
										<p class="text-center petro-form-control"><em>Sua avaliação será encaminhada para os administradores da aplicação.</em></p>

									</div>
									<div class="modal-footer petro-modal-footer">
										<button type="button" class="btn btn-default petro-my-options" data-dismiss="modal"><g:message code="application.reviews.button.cancelar.label" default="cancelar"/></button>
										<button type="button" class="btn btn-success" ng-click="saveNewReview()"><g:message code="application.reviews.button.save.label" default="avaliar"/></button>
									</div>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
			
					</div>

					<!-- Last reviews -->
					<div class="main-row row petro-recent-reviews" ng-show="lastReviews.length">
						<div class="col-md-12"> 
							<ul class="list-group petro-list-group">
								<li class="row list-group-item petro-list-group-item" ng-repeat="review in lastReviews">
									<div class="col-md-12 hack-float-left-parent">
					    				<shiro:hasRole name="${ grailsApplication.config.app.role.moderator }">
					    					<ljap-modal-ok-cancel modal-id="remove-review-{{review.id}}" data-keyboard="true" object="review" ok-action="remove(object, sucessCallback, failureCallback)" title="Aplicações Petrobras" text="Confirma a exclusão do comentário?" success-message="Avaliação excluída com sucesso!" ok-button-label="excluir"></ljap-modal-ok-cancel>
					    					

					    					<span class="petro-review-delete" alt="excluir avaliação" title="excluir avaliação" data-toggle="modal" data-target="#remove-review-{{review.id}}">x</span>
					    				</shiro:hasRole> 
										<div class="petro-review-name">{{review.user.name}}</div>
										<div class="petro-review-department">{{review.user.department}}</div>
										<div class="petro-review-rating">
											<table class="ratingDisplay">
								                <tbody>
								                	<tr>
								            			<td><div class="star on"><a></a></div></td>
								            			<td><div class="star {{(review.rating > 1) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(review.rating > 2) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(review.rating > 3) && 'on' || 'off'}}"><a></a></div></td>
								            			<td><div class="star {{(review.rating > 4) && 'on' || 'off'}}"><a></a></div></td>
								                	</tr>
								            	</tbody>
								            </table>
										</div>
										<div class="petro-review-title">
											{{review.title}}
										</div>
										<div class="petro-review-text" ng-bind-html="review.text | newlines | unsafe">
										</div>
										<div class="petro-review-date">
											{{review.date}}
										</div>
										<div class="petro-review-fromothers">
											<div class="petro-review-fromothers-like-on" ng-show="review.myevaluation > 0" >
												gostar <span>{{review.likes}}</span>
											</div>
											<div class="petro-review-fromothers-like-off" ng-show="review.myevaluation <= 0" ng-click="like(review, $event)">
												gostar <span>{{review.likes}}</span>
											</div>
											<div class="petro-review-fromothers-dislike-on" ng-show="review.myevaluation < 0">
												não gostar <span>{{review.dislikes}}</span>
											</div>
											<div class="petro-review-fromothers-dislike-off" ng-show="review.myevaluation >= 0" ng-click="dislike(review, $event)">
												não gostar <span>{{review.dislikes}}</span>
											</div>
										</div>
										<application:isPermitted id="${applicationInstance?.id}">
												<p ng-show="!review.reply.id && !creatingReply">
													<button type="button" title="responder para usuário" class="petro-reply-button pull-right" ng-click="newReply(review.id)"><g:message code="application.reviews.reply.send.label" default="responder"/></button>
												</p>
											</application:isPermitted>
									</div>
									<div class="col-md-12" style="margin-top:1em;">
											
										<span ng-show="review.reply.id && editingReply != review.reply.id">	
											<div class="petro-adm-answer-background">
												<span class="petro-reply-arrow"><img src="${resource(dir: 'images', file: 'arrow-adm-reply.png')}"				
												 title="resposta" /></span>
												<div class="petro-reply">
													<div class="petro-review-name">
													Responsável pela Aplicação
													</div>
													<div>
													{{review.reply.text}}
													</div>
													<div class="petro-review-date">
													{{review.reply.date}}
													</div>
													<application:isPermitted id="${applicationInstance?.id}">
													<div class="petro-adm-buttons">
														<button ng-if="${user.id} == review.reply.userId" type="button" class="petro-reply-button pull-right" ng-click="editReply(review.reply.id)"><g:message code="application.reviews.reply.update.label" default="editar resposta"/></button>													
														<button type="button" class="petro-del-reply-button pull-right" ng-click="deleteReply(review.reply.id)"><g:message code="application.reviews.reply.delete.label" default="apagar resposta"/></button>
													</div>
													</application:isPermitted>
												</div>
												
											</div>
										</span>
										<application:isPermitted id="${applicationInstance?.id}">
										<div class="col-md-12" ng-show="creatingReply == review.id || (review.reply.id && editingReply == review.reply.id)">
											<form name="replyForm">
											<p  class="col-md-12">
									        	<textarea name="text" placeholder='<g:message code="application.reviews.reply.text.label" default="Texto"/>' ng-model="review.reply.text" class="form-control petro-reply-textarea" rows="8" maxlength="{{metadata.text.maxSize}}" decorate>
									        	</textarea>
											</p>
											<p class="col-md-12">
												<button type="button" class="petro-reply-button pull-right" ng-click="saveReply(review)"><g:message code="application.reviews.reply.save.label" default="enviar resposta"/></button>
												<button type="button" class="petro-cancel-reply-button pull-right" ng-click="cancelEdit()"><g:message code="application.reviews.reply.cancel.label" default="cancelar"/></button>
											</p>
											</form>
										</div>
										</application:isPermitted>
									</div>
								</li>
							</ul>
						</div>
						<div ng-click="loadMore($event)" ng-show="hasMore">
							<!--<div class="petro-arrow-back">
								<img src="/petro-app-center/static/images/arrow.png" class="petro-arrow" alt="Ver mais comentários">
							</div>-->
							<button type="button" class="btn petro-old-reviews" title="ver mais avaliações">
								<g:message code="application.reviews.list.more.label" default="mais avaliações"/>
							</button>
						</div>
					</div>

					<shiro:hasRole name="${ grailsApplication.config.app.role.moderator }">
					<div class="modal fade" id="ratingsModal">
					 	<div class="modal-dialog">
					    	<div class="modal-content">
						  		<div class="modal-header petro-modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					        		<h4 class="modal-title">Lista de avaliações</h4>
					      		</div>
					      		<div class="modal-body">	
					       			<table class="table table-striped">
					       				<tr>
					       					<th>Avaliação</th>
					       					<th>Nome</th>
					       					<th>Lotação</th>
					       				</tr>
					       				<tr ng-repeat="review in reviewsByRating">
					       					<td><span class="petro-score-group" style="width: 100%"><div class="star petro-star-on" ng-repeat="i in numberOfStars | 	limitTo: review.stars">{{i}}</div></span></td>
					       					<td>{{review.user.name}}</td>
											<td>{{review.user.department}}</td>
					        			</tr>
				        			</table>
					      		</div>
					      		<div class="modal-footer">
					        		<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					      		</div>
					    	</div><!-- /.modal-content -->
					  	</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
					</shiro:hasRole>
		
				</div>

				
		
				<g:if test="${applicationInstance.compatibilities || applicationInstance.informationsSupport || applicationInstance.versions}">
					<div class="main-row row">
						<div class="col-md-12"> 
							<h4><g:message code="application.otherinformation.label" default="Outras Informações" /></h4>
							
							<!-- Version information -->
							<g:if test="${applicationInstance.versions}">
								<g:each in="${applicationInstance.versions}" status="index" var="version">
									<g:if test="${ index < 2 }">
										<div class="row">
											<div class="col-md-12 petro-detail-field">
												<label><g:message code="application.version.label" default="Versão" /> ${fieldValue(bean: version, field: 'number')} - <g:formatDate format="dd/MM/yyyy" date="${version.date}"/></label>
												<div>
													<span class="property-value petro-detail-field">
														${fieldValue(bean: version, field: 'description').replaceAll("\n","<br>")}
													</span>
												</div>
											</div>
										</div>	
									</g:if>
									<g:if test="${ index == 2 }">
										<div class="row" ng-show="!showMoreVersions">
											<div class="col-md-12 petro-detail-field">
												<a  ng-click="showMoreVersions = true" href>
													Ver todas as versões
												</a>
											</div>
										</div>
									</g:if>
									<g:if test="${ index >= 2 }">
										<div ng-show="showMoreVersions" class="row">
											<div class="col-md-12 petro-detail-field"">
												<label><g:message code="application.version.label" default="Versão" /> ${fieldValue(bean: version, field: 'number')} - <g:formatDate format="dd/MM/yyyy" date="${version.date}"/></label>
												<div>
													<span class="property-value petro-detail-field">
														${fieldValue(bean: version, field: 'description').replaceAll("\n","<br>")}
													</span>
												</div>
											</div>
										</div>	
									</g:if>
								</g:each>
							</g:if>

							<!-- Compatibilities -->					
							<g:if test="${applicationInstance.compatibilities}">
								<div class="row">
									<div class="petro-detail-field">
										<div class="col-md-12">
											<label><g:message code="application.compatibilities.label" default="Funciona com" /></label>
										</div>
										<ul class="col-xs-10 col-sm-12 col-md-12 petro-app-compatibility">
											<g:each in="${applicationInstance?.compatibilities?.sort { it.description }}" status="i" var="compatibility">
												<g:if test="${i == Math.ceil(applicationInstance.compatibilities.size()/2)}">

												</g:if>
												<li><em><img src="${resource(dir: 'images/compatibilidade', file: compatibility.nameImage)}"				
												 title="${compatibility.description}" /></em>${fieldValue(bean: compatibility, field: 'description')}</li>
											</g:each>
										</ul>
									</div>
								</div>
							</g:if>

							<!-- Support information -->
							<g:if test="${applicationInstance.informationsSupport}">
								<div class="row">
									<div class="col-md-12 petro-detail-field">
									<label><g:message code="application.informationssupport.label" default="Informações de Suporte" /></label>
										<div>
											<g:each in="${applicationInstance?.informationsSupport}" status="i" var="informationSupport" >
												<div>
													<span class="property-value petro-detail-field"><a href="${informationSupport.url}" target="blank">${fieldValue(bean: informationSupport, field: 'description')}</a></span>
												</div>
											</g:each>
										</div>					
									</div>
								</div>
							</g:if>

							<p class="petro-link-report">
								<a data-helpref="informe-erros-de-cadastro" data-toggle="modal" href="#report-issue" class="petro-tooltip" title="Ajude a melhorar o conteúdo da informação da aplicação">
									<g:message code="report.create.label" default="Informar erros de cadastro" />
								</a>
							</p>						
						</div>
					</div>
				</g:if>
			</div>
		</div>
	</body>
	
</html>
