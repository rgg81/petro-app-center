<%@ page import="br.com.petrobras.appcenter.Application" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />

		<r:require module="angular"/>
		<r:require module="applicationControllerAngular" />		
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapDatePicker" />
		<r:require module="bootstrapAngularUI" />
		<r:require module="angularSwitch" />
		<r:external uri="/js/MainModule.js" type="js"/>
		<title>
			<g:message code="default.create.label" args="[entityName]" />
		</title>
	</head>
	<body>

		<div id="create-application" class="row content scaffold-edit" role="main">

			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit"><g:message code="default.create.label" args="[entityName]" /></h3>
			</div>	

			<div class="col-md-12" ng-app="applicationsApp">

				<div ng-controller="ApplicationsCtrl" data-ng-init='init(${application})'>
					<form class="form" role="form" name="form" novalidate="novalidate" enctype="multipart/form-data">
					
							<g:render template="form"/>
					
						<div class="row petro-edit-app">

							<div class="col-md-12">

									<div class="col-md-4 form-group">
										<button type="button" id="button-save-app" ng-click="saveOrPublish('save')" data-loading-text="Salvando..." ng-disabled="checkForm()" class="btn btn-success save petro-admin-button-form">${message(code: 'default.button.update.label', default: 'Update')}
										</button>
									</div>
									<div class="col-md-4 col-md-offset-4 form-group">
										<button type="button" id="button-publish-app" ng-click="saveOrPublish('publish')" data-loading-text="Publicando..." ng-disabled="checkForm()" class="btn btn-success save petro-admin-button-form">Publicar
										</button>
									</div>

								<!-- Modal -->
								<div class="modal fade" id="saveMessageModal" tabindex="-1" role="dialog" aria-labelledby="save message" aria-hidden="true">
								  <div class="modal-dialog">
								    <div class="modal-content">
								    	<div class="modal-header petro-modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title petro-modal-title">
												Aplicações Petrobras
											</h4>
										</div>
								      	<div class="modal-body petro-modal-body">
								      		<p>
												<ul class="alert petro-alert alert-success">
													<li>A aplicação foi salva com sucesso e gerou um rascunho. Ao salvar a aplicação, as atualizações realizadas não estarão disponíveis para o usuário final até que a aplicação seja publicada.</li>
												</ul>
											</p>
								      	</div>
										<div class="modal-footer petro-modal-footer">
											<!--<button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>-->
										</div>
								    </div><!-- /.modal-content -->
								  </div><!-- /.modal-dialog -->
								</div><!-- /.modal -->

								<div class="modal fade" id="publishMessageModal" tabindex="-1" role="dialog" aria-labelledby="save message" aria-hidden="true">
								  <div class="modal-dialog">
								    <div class="modal-content">
								      <div class="modal-header petro-modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title petro-modal-title">
												Aplicações Petrobras
											</h4>
									  </div>		
								      <div class="modal-body petro-modal-body">
								      		<p>
								      			<ul class="alert petro-alert alert-success">
													<li>Sua aplicação foi publicada com sucesso.</li>
												</ul>
											</p>
								      </div>
								      <div class="modal-footer petro-modal-footer">
											<!--<button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>-->
									  </div>
								    </div><!-- /.modal-content -->
								  </div><!-- /.modal-dialog -->
								</div><!-- /.modal -->
							</div>			
						</div>	
					</form>
				</div>			
			</div>
	  </div>
	  <script src="${resource(dir: 'js', file: 'fileuploader.js')}" charset="UTF-8"></script>
	</body>
</html>
