var petroAppWidget = document.getElementById('petro-app-widget');
var petroAppUrl =  "${request.scheme}" + "://" + "${request.serverName}" + ":" + "${request.serverPort}" + "/petro-app-center/application/show/" + "${applicationInstance.id}";
var petroAppStar = "<g:resource dir="static/images" file="star.gif" absolute="true" />";
var petroAppRatingFormatted = '<g:formatNumber number="${rating}" minFractionDigits="1" format="#,#"/>';
var petroAppIcon = "<g:resource dir="static/images" file="icon-petro-app.png" absolute="true" />";
var petroAppRating = "${rating}";
var petroAppStarsHtml = "";
var petroAppHtml = ""
for(var i = 1; i <= 5; i++){
	if(i <= petroAppRating){
		petroAppStarsHtml +=  "<div style=\"background: url('" + petroAppStar + "') no-repeat 0 -24px; display:inline-block; height:12px; width: 12px;\"></div>";	
	}
	else if(i > petroAppRating && petroAppRating > (i-1)){
		var petroAppStarWidth = 11.5 - (12 * (i-petroAppRating)); 
		petroAppStarsHtml += "<div style=\"background: url('" + petroAppStar + "') no-repeat 0 0; display:inline-block; height:12px; width: 12px;\">";
		petroAppStarsHtml += "<div style=\"width:" + petroAppStarWidth + "px;\"><a style=\" display:inline-block; height:12px; width:" + petroAppStarWidth + "px;background: url('" + petroAppStar + "') no-repeat 0 -24px;\"></a></div></div>";
	}
	else{
		petroAppStarsHtml += "<div style=\"background: url('" + petroAppStar + "') no-repeat 0 0; display:inline-block; height:12px; width: 12px;\"></div>";
	}
}

petroAppHtml += "<a href=\"" +  petroAppUrl + "\" target=\"_blank\" style=\"text-decoration:none;\" alt=\"Avaliar ${applicationInstance.name}\" title=\"Avaliar ${applicationInstance.name}\">";
petroAppHtml +=	"<div style=\"width: 103px; padding: 2px; font-family: Tahoma, 'Trebuchet MS', sans-serif;\">";
petroAppHtml +=     "<div style=\"float: left;\">";
petroAppHtml += 		"<img src=\"" + petroAppIcon + "\" width=\"38 px;\"/>";
petroAppHtml +=		"</div>";
petroAppHtml +=		"<div style=\"float: right; text-align: center; padding-top:2px;\">";
petroAppHtml +=			"<div style=\"font-size: 18px;color: #999\">" + petroAppRatingFormatted + "</div>";
petroAppHtml +=			"<div style=\"height: 12px\">";
petroAppHtml += 			petroAppStarsHtml;			
petroAppHtml += 		"</div></div></div></a>";
petroAppWidget.innerHTML = petroAppHtml;