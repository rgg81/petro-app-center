<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<r:require module="angular"/>
		<r:require module="applicationControllerAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapDatePicker" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>		
		<title>All apps</title>
	</head>
	<body>

		<g:javascript>
			var applicationsApp = angular.module("allApps", ['mainApp']);

			applicationsApp.controller('AllAppsCtrl', ['$scope', '$http', '$window', '$modal', function($scope, $http, $window, $modal) {
				$scope.init = function(applications) {
					$scope.applications = applications;
				};	
				
				$scope.updateAll = function() {
					var arrayLength = $scope.applications.length;
					for (var i = 0; i < arrayLength; i++) {
					    var app = $scope.applications[i]
					    $scope.updateFromCatalog(app.id)
					}
				}
				$scope.updateFromCatalog = function(id) {
					$http.post("/petro-app-center/accessControl/updateFromCatalogById/" + id).success(function(data, status, headers, config) {
						$('#app-' + id).text('ok');
						
					}).error(function(errorData) {
						console.log("Erro ao obter dados da aplicação: " + errorData);
						$('#app-' + id).text('erro');
					});
				}
			}]);
		</g:javascript>
		<div id="edit-application" class="row content scaffold-edit" role="main">

			<div class="col-md-12" ng-app="allApps">

				<div ng-controller="AllAppsCtrl" data-ng-init='init(${allAppsJson})'>
					<div>
						<button ng-click="updateAll()" class="btn btn-info">Atualizar tudo</button>
					</div>
					<div ng-repeat="app in applications">
						<div class="row">
							<div class="col-md-8">{{app.name}}</div><div class="col-md-3"><button class="btn btn-success" ng-click="updateFromCatalog(app.id)">atualizar do cat</button><span id='app-{{app.id}}'></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
