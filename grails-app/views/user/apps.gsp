<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />

	</head>
	<body>
		<div id="main" role="main" class="row" ng-app="homeApp">
			<!--Início Minhas Aplicações-->
			<g:if test="${user}">
				<g:if test="${user.sharesMyApplications}">
					<div ng-controller="MyAppsCtrl" style="display: none"></div>
					<div class="panel petro-panel-box" id="user-applications" ng-controller="HomeCtrl" ng-init="init('userapps', 'userapps', 'there-is-no-limit!', '${user.key}')"><a name="userapplist"></a>
						<div class="panel-heading petro-box-title">
			 				<h3 class="pull-left">Aplicações de ${user.shortName}</h3>
			 			</div>			
						<div id="{{list.id}}" class="panel-body petro-panel-body">
							<ul ng-show-animated="list.applications" class="petro-apps-drawer">
								<li ng-repeat="application in list.applications" class="col16-lg-2 col16-xxs-16 col-xs-6 col-sm-2">

									<g:render template="../angular-templates/application"/>

								</li>
							</ul>
						</div>
					</div>
				</g:if>
			</g:if>
			<g:if test="${user}">
				<g:if test="${!user.sharesMyApplications}">
					<div class="panel petro-panel-box access-url">
			  			<div class="panel-heading-access petro-box-title-url">
			  				<h3 class="pull-left petro-box-title-url">Acesso não autorizado</h3>
			  			</div>
			  			<div class="panel-body petro-panel-body">
			  				<div class="main-row">
				  				<div class="col-md-12 petro-access-url">
				  					<p align="center"><g:img uri="/images/img-broken-link.png"/></p>

				  					<p>O usuário informado não compartilhou suas aplicações.<br/>Se achar necessário, solicite ao mesmo que ative o compartilhamento na seção "Minhas Aplicações" na primeira página.</p>

				  					<p><a href="/petro-app-center" title="Retornar para a página inicial"><strong>Continue navegando...</strong></a></p>
				  				</div>
			  				</div>
			  			</div>
			  		</div>
	  			</g:if>
			</g:if>
		</div>		
	</body>
</html>