<div class="navbar navbar-static-top " id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/petro-app-center/toolbelt" class="navbar-brand">Início</a>
    </div>
    <nav role="navigation">
      <ul class="nav navbar-nav">
        <li>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gráficos <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/petro-app-center/review/report">Avaliações por dia</a></li>
            <li><a href="/petro-app-center/toolbelt/averagexuse">Aplicações: Vezes adicionada X Avaliações</a></li>
            <li><a href="/petro-app-center/toolbelt/averagexaccess">Aplicações: Acessos X Avaliações</a></li>
          </ul>
        </li>
        <shiro:hasRole name="${grailsApplication.config.app.role.admin}">
          <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Relatórios <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/petro-app-center/toolbelt/applications">Todas as aplicações</a></li>
            </ul>
          </li>
        </shiro:hasRole>
      </ul>
    </nav>
  </div>
</div>