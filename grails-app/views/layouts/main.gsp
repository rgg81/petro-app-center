<!DOCTYPE html>
<html data-placeholder-focus="false">
<head>
	<title><g:layoutTitle default="Aplicações Petrobras1"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta charset="UTF-8">
	<g:layoutHead/>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Bootstrap -->
	<link href="${resource(dir: 'css', file: 'bootstrap.min.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'toggle-switch.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'typeahead.js-bootstrap.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'styles-1.0.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'grid16.css')}" rel="stylesheet" media="screen">
   	<link href="${resource(dir: 'css', file: 'ratings.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'mediaqueries.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'uploader.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'datepicker.css')}" rel="stylesheet" media="screen">
	<link href="${resource(dir: 'css', file: 'tooltipster.css')}" rel="stylesheet" media="screen">


	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">

	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">

   	<r:layoutResources />

</head>
<body>

<!--  -->
<header class="container">
	<div class="row">
		<nav class="navbar navbar-default petro-navbar" role="navigation">
		  	<!-- Brand and toggle get grouped for better mobile display -->
		  	<div  class="navbar-header col-sm-7 col-md-7 petro-navbar-background">
		    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			      	<span class="sr-only">Ativar navegação</span>
			        <span class="icon-bar petro-icon-bar"></span>
			        <span class="icon-bar petro-icon-bar"></span>
			        <span class="icon-bar petro-icon-bar"></span>
		    	</button>
	    		<a href="/petro-app-center" class="petro-image-title"  title="Ir para página inicial." >
					<strong>APLICAÇÕES PETROBRAS<br>
					<span class="petro-image-bottom-bar">—</span></strong>
				</a>
				<img src="${resource(dir: 'images', file: 'bg-header1.png')}" class="petro-image hidden-xs img-responsive">
		  	</div>	

			<div class="collapse col-sm-5 col-md-5 navbar-collapse navbar-ex1-collapse navbar-right">

	    		<div class="petro-search-box">

	    			<!--INÍCIO LOGIN-->
	    			<div class="pull-left logado">
				    		<shiro:isLoggedIn>
			          			<span class="glyphicon glyphicon-user"></span> 
			          			<greetedUser:showName />
				         	</shiro:isLoggedIn>
							<shiro:isNotLoggedIn>
								<a href="/" title="Faça seu login" class="dropdown-toggle" data-toggle="dropdown">Login <b class="caret petro-user-menu"></b></a>
							</shiro:isNotLoggedIn>
					</div>

					<div class="pull-right petro-portal-petrobras">
						<a href="http://portalpetrobras.petrobras.com.br:80/PetrobrasPortal/appmanager/portal/desktop?_nfpb=true&_pageLabel=home_a_petrobras" target="_blank" title="Ir para o Portal Petrobras">Portal Petrobras</a>
					</div>


			  		<g:form url='[controller: "searchApp", action: "index"]' class="petro-search-form" id="searchableForm" name="searchableForm" method="get">
			  			<div class="petro-search">
	        				<g:textField name="q" id="search-input" data-provide="typeahead" autocomplete="off" class="petro-form-control petro-search-field" placeholder="Buscar aplicações" value="${params.q}" size="50"/>
	        				
	        				<g:javascript>
	        				/* Inicializa autocomplete do bootstrap na busca */
	        				var apps = new Bloodhound({
							  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
							  queryTokenizer: Bloodhound.tokenizers.whitespace,
							  //prefetch: '../data/films/post_1960.json',
							  remote: '/petro-app-center/searchApp/autocomplete?q=%QUERY'
							});
							 
							apps.initialize();
							 
							$('#search-input').typeahead(null, {
								name: 'autocomplete-apps',
								displayKey: function (datum) {
										return datum.name;
								},
								source: apps.ttAdapter(),
								templates: {
								    suggestion: function (datum) {
										return '<img src="/' + datum.icon + '"><p>'+datum.highlight+'</p>'
									}
								}
							});

							$('#search-input').bind('typeahead:selected', function(obj, datum) {
	        					window.location.href = datum.url;        
							});
						
	        				// para evitar erros decorrentes de sessão expirada
							setInterval(function(){
								console.log("heartBeat");
								$.ajax({
									type: "GET", url: "/petro-app-center/heartBeat/isAlive"
								}).done(function(data, textStatus, jqXHR) { 
									if (!data.isAlive) {
										console.log("User must authenticate!");
										window.location.reload(true);
									}
								}).fail(function(jqXHR, textStatus, textStatus) {
									console.log("jqXHR: " + jqXHR);
									console.log("textStatus: " + textStatus);
									console.log("textStatus: " + textStatus);
								}); 
							// 29 minutos: 1740000 = 29*60*1000 (sessão de 30 minutos)
							},1740000);
							//4 minutos: 240000 = 4*60*1000 (sessão de 5 minutos)
							//},240000);

							if (window.location.hash == '#termsAndconditionsMessageModal') {
								$(window).load(function(){
								    $('#termsAndconditionsMessageModal').modal('show');
								});	
							}

	        				</g:javascript>
	        				<button type="submit" data-helpref="buscar-aplicacoes"  class="btn petro-back-search petro-search-button petro-tooltip" title="Encontre a aplicação que procura."></button>
        				</div>
    				</g:form>


	    			<ul class="nav navbar-nav navbar-right petro-top-menu">			
		    			<li>
		    				<shiro:hasRole name="${grailsApplication.config.app.role.watcher}">
				        		<a href="/petro-app-center/toolbelt" title="Acesse os relatórios"><span class="glyphicon glyphicon-tasks"></span> Relatórios </a>				   
							</shiro:hasRole>
						</li>
		    			<li>
		    				<shiro:hasRole name="${grailsApplication.config.app.role.adminapp}">
				        		<a href="/petro-app-center/admin/listApplications" title="Faça sua administração"><span class="glyphicon glyphicon-cog"></span> Administração </a>				   
							</shiro:hasRole>
						</li>

		    			<li class="petro-link-help">
				    		  <a href="/petro-app-center/help" title="Posso ajudar?"><span class="glyphicon glyphicon-question-sign"></span> Ajuda </a>
						</li> 
		    		</ul> 
            

	  			</div>

		    </div>
		</nav>
	</div>
</header>
	
	<div class="petro-page-content container">
		<g:layoutBody/>
	</div>
    <footer id="footer">
  		<div class="container col-md-12 petro-footer">
			<p class="navbar-text pull-right petro-privacy">	
				<a href="#" data-toggle="modal" data-target="#termsAndconditionsMessageModal" title="Acessar Termos e condições de uso">Termos e condições de uso</a>&nbsp;&nbsp;| @ 2014 Petrobras
					<div class="modal fade" id="termsAndconditionsMessageModal" tabindex="-1" role="dialog" aria-labelledby="terms and conditions message" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header petro-modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title petro-modal-title">
									Termos e condições de uso
								</h4>
						  </div>		
					      <div class="modal-body petro-modal-body">
					      		<p>
					      			O conteúdo deste site se destina a oferecer informações relativas às aplicações disponíveis para uso na <strong>Petrobras</strong>, permitindo a seus usuários que as encontrem rapidamente, facilitando seu uso diário, não se responsabilizando pelo correto funcionamento das aplicações, sendo apenas um direcionador para as mesmas.
								</p>
								<p>
									Permite ainda que as aplicações sejam avaliadas através de notas e que sejam acrescentadas opiniões sobre qualquer aspecto relativo à aplicação (usabilidade, sugestão de melhorias, etc).
								</p>
								<p>
									Tais manifestações são permitidas livremente ao usuário, porém, ao realizá-las, o usuário concorda com os seguintes termos:
								</p>
								<ol>
									<li>
										Não são permitidas manifestações que contrariem o <a href="http://portalpetrobras.petrobras.com.br/PetrobrasPortal/appmanager/portal/desktop?_nfpb=true&_pageLabel=petr_landing_lista_codigo_de_etica_a_petrobras&idConteudo=petro_land_lista_titulo_ce_000001&areaAtual=a_petrobras&portalpath=portal" target="_blank"><strong>Código de Ética do Sistema Petrobras</strong></a> e/ou desrespeitem as <a href="http://rjln202/03256BD6007448CD/0/684FB55D694F9BE283257C3C0054798C?OpenDocument" target="_blank">Diretrizes de Segurança da Informação</a>;
									</li>
									<li>
										Não são permitidas manifestações que possam causar danos à imagem de pessoas pertencentes à força de trabalho, concorrentes, parceiros, fornecedores da Petrobras, outras empresas, órgãos ou quaisquer outras instituições;	
									</li>
									<li>
										Não são permitidas acusações ou denúncias, sendo a <a href="http://ouvidoria.petrobras.com.br/" target="_blank">Ouvidoria</a> o caminho adequado para tal;
									</li>
									<li>
										Não é permitida a utilização da manifestação para veicular propagandas, mensagens de cunho religioso, político-partidário e/ou pornográfico, bem como para realizar promoção pessoal e a comercialização de bens e serviços;
									</li>
									<li>
										Todas as leis vigentes devem ser seguidas também neste site, não sendo permitida a prática de condutas ilícitas, como crimes contra a honra (injúria, calúnia, difamação), violação aos direitos de propriedade intelectual e ao sigilo funcional;
									</li>
									<li>
										Os textos publicados devem ser de autoria própria ou ter a sua divulgação devidamente autorizada pelo autor, respeitando-se ainda a imagem, a honra e a privacidade de terceiros;
									</li>
									<li>
										A publicação de informações pessoais de terceiros, como telefones, endereços, e-mail ou imagens, dependem da autorização do proprietário do conteúdo;
									</li>
									<li>
										Manifestações publicadas que violem estes termos e condições, bem como as demais políticas e normas da <strong>Petrobras</strong>, poderão ser removidas sem consulta ou autorização prévia do autor;
									</li>
								</ol>
								<p>
									<strong>A Petrobras</strong> se reserva o direito de monitorar o uso do site, com o objetivo de proteger suas informações, seus recursos de informação, bem como de verificar a observância das normas internas e da legislação vigente.
								</p>
								<p>
									A informação contida neste site, em regra, é classificada como NP-2, devendo ser observadas as normas estabelecidas no <a target="_blank" href="http://rjln202/03256BD6007448CD/0/4E2935547F01FB7F83256C050064BB15?OpenDocument"><strong>Padrão PG-0V3-00054</strong></a> no que diz respeito à reprodução, divulgação, guarda, transporte e descarte das informações.
								</p>
								<p>
									A <strong>Petrobras</strong> se reserva o direito de modificar a qualquer momento, de forma unilateral, os presentes <strong>Termos e Condições de Uso</strong>. Ao navegar por este site, você aceita os Termos e Condições vigentes na data, e, portanto, deve verificá-los previamente cada vez que visitá-lo.
								</p>
						  </div>
						  <div class="modal-footer petro-modal-footer"></div>
					    </div><!-- /.modal-content -->
					  </div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</p>
  		</div>
    </footer>
      
	<script src="${resource(dir: 'js', file: 'jquery.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'respond.min.js')}"></script>
	<script src="${resource(dir: 'js', file: 'typeahead.js')}"></script>
	<script src="${resource(dir: 'js', file: 'yahoo-dom-event.js')}" charset="UTF-8"></script>
	<script src="${resource(dir: 'js', file: 'connection.js')}" charset="UTF-8"></script>
	<script src="${resource(dir: 'js', file: 'ratings.js')}" charset="UTF-8"></script>
	<script src="${resource(dir: 'js', file: 'modernizr-ljap.js')}" charset="UTF-8"></script>
	<script src="${resource(dir: 'js', file: 'placeholder-enhanced.min.js')}" charset="UTF-8"></script>
	<script src="/petro-app-center/static/js/jquery.tooltipster.min.js" type="text/javascript"></script>

   <script>
			 $(document).ready(function() {

			 $('.petro-tooltip').tooltipster({
							   functionInit : function(origin, content) {
								   	return $('<span>' + $(origin).attr('title') + '</span><div class="petro-more-help"><a href="/petro-app-center/help#' + $(origin.context).data('helpref')+ '">Posso ajudar?</a></div>');
								},

						       animation: 'fade',
							   delay: 200,
							   theme: 'tooltipster-default',
							   touchDevices: true,
							   trigger: 'hover',
							   interactive: 'boolean'
	        });
		});

    </script>



	<!-- START OF SmartSource Data Collector TAG -->
	<!-- Copyright (c) 1996-2014 Webtrends Inc.  All rights reserved. -->
	<!-- Version: 9.4.0 -->
	<!-- Tag Builder Version: 4.1  -->
	<!-- Created: 5/14/2014 6:17:09 PM -->
	<script src="/petro-app-center/static/js/webtrends.js" type="text/javascript"></script>
	<!-- ----------------------------------------------------------------------------------- -->
	<!-- Warning: The two script blocks below must remain inline. Moving them to an external -->
	<!-- JavaScript include file can cause serious problems with cross-domain tracking.      -->
	<!-- ----------------------------------------------------------------------------------- -->
	<script type="text/javascript">
		//<![CDATA[
		var _tag=new WebTrends();
		_tag.dcsGetId();
		//]]>

		//<![CDATA[
		_tag.dcsCustom=function(){
		// Add custom parameters here.
		//_tag.DCSext.param_name=param_value;
		}
		_tag.dcsCollect();
		//]]>
	</script>

	<noscript>
	<script>	
		var url = window.location.href;
		var nohttp = url.split('//')[1];
		var hostPort = nohttp.split('/')[0]

		if (hostPort == 'aplicacoespetrobras.petrobras.com.br'){
			document.write('<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://wt-sdc.petrobras.com.br/dcsa6m2wbg0ei7mkauwosi9qo_3l7l/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.4.0&amp;dcssip=www.aplicacoespetrobras.petrobras.com.br"/></div>');
		} else {
			document.write('<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://wt-sdc-desenv.petrobras.com.br/dcsdtu9y1g0ei7arnffz5g9qo_3h6f/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=9.4.0&amp;dcssip=www.aplicacoespetrobrasd.petrobras.com.br"/></div>');
		}
	</script>
	</noscript>
	<!-- END OF SmartSource Data Collector TAG -->

	<g:javascript library="application"/>
	<r:layoutResources />
</body>
</html>
