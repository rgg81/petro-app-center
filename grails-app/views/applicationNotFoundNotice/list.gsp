<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="applicationNotFoundNoticeController" />
		<r:require module="showHideAnimatedAngular" />
		<r:external uri="/js/ShowHideModule.js" type="js"/>
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>
	</head>
	<body>
		<div class="table-responsive col-md-12" role="main" class="row" ng-app="applicationNotFoundNoticeApp">
			<table class="table table-striped petro-table-striped" ng-controller="applicationNotFoundNoticeCtrl">
				<thead>
					<th class="col-md-1">Usuário</th>
					<th class="col-md-1">Data</th>
					<th class="col-md-2">URL</th>
					<th class="col-md-5">Descrição</th>
				</thead>
				
				<tbody>
					<tr ng-repeat="notice in notices">
						<td>{{notice.user.name}} ({{notice.user.key}})</td>
						<td>{{notice.dateCreated}}</td>
						<td>{{notice.url}}</td>
						<td>{{notice.description}}</td>
					</tr>
				</tbody>
			</table>

		</div>

	</body>
</html>