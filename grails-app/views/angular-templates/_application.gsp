<div hover-app ng-cloak class="petro-app front" style="backface-visibility: hidden;" ontouchstart="this.classList.toggle('hover');">	
	<a href="" ng-click="removeUserApplication(application.id,$event)" title="Remova de Minhas Aplicações"><span class="glyphicon petro-remove-button" style="display:none;"></span></a>
	<a href='{{application.url}}' appId="{{application.id}}" where="{{list.action}}" register-access  title='Acesse esta aplicação' target="_blank">
		<div class='petro-icon-crop'>
			<img ng-src='{{application.iconPath}}' alt='Acesse esta aplicação' class='petro-icon-image'
			target="_blank"/>
		</div>
	</a>
	<div class='petro-app-text'>
		<div class="petro-app-rating">
			<table class="ratingDisplay">
	            <tbody>
	            	<tr>
	        			<td ng-repeat="i in [0,1,2,3,4]">
	        				 <div class="star {{(application.averageRating > i) && 'on' || 'off'}}">
	        				 	<a style="width:{{(application.averageRating > i && (application.averageRating < (i + 1 ))) ? (100 * (application.averageRating - i)) + '%': '100%'}}"></a>
	        				 </div>	
	        			</td>
	            	</tr>
	        	</tbody>
	        </table>
		</div>

		<div class='petro-app-title'>
			<a href='{{application.url}}' title='Acesse esta aplicação' appId="{{application.id}}" where="{{list.action}}" register-access target="_blank">{{application.name}}</a>
		</div>
		<div class="petro-app-minibuttons slide visible-md visible-lg">			
			<a help-app data-helpref="detalhes-da-aplicacao" ng-href="/petro-app-center/application/show/{{application.id}}" title="Veja todos os detalhes desta aplicação."><button class="petro-app-button glyphicon glyphicon-eye-open" ></button></a>
			<a href="{{application.url}}" appId="{{application.id}}" where="{{list.action}}" register-access target="_blank">
				<button ng-class="{'petro-app-button glyphicon glyphicon-play': !application.isDownloadable, 'petro-app-button glyphicon glyphicon-download-alt': application.isDownloadable}"  ng-attr-title="{{application.tooltip}}"></button>
			</a>
			<button help-app data-helpref="minhas-aplicacoes" ng-hide="hashMyApps[{{application.id}}] || application.isDownloadable" class="petro-app-button glyphicon glyphicon-plus" ng-click="addUserApplication(application.id,$event)" title="Adicione esta aplicação às Minhas Aplicações."></button>
			<div class="petro-app-minibuttons-bg"></div>
		</div>
		<span class="limit-fade"></span>
	</div>
	<a href="{{application.url}}"  appId="{{application.id}}" where="mobile-{{list.action}}" register-access>
		<div class="petro-app-mobile-launch visible-xs">
			<button ng-class="{'petro-app-button glyphicon glyphicon-play': !application.isDownloadable, 'petro-app-button glyphicon glyphicon-download-alt': application.isDownloadable}" ng-attr-title="{{application.tooltip}}"></button>
		</div>
	</a>
	<div class="petro-app-added animated hidden">
		<span class="glyphicon glyphicon-ok"></span>
		Aplicação adicionada às Minhas Aplicações		
	</div>
</div>
