<div ng-controller="reportCtrl" class="modal fade" id="report-issue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
			<div class="modal-header petro-modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 id="myModalLabel" class="modal-title petro-modal-title">
					<!--<g:message code="user.application.button.addreview.label" default="Review and comment" />-->Erros de informação
				</h4>
			</div>
			<div class="modal-body petro-modal-body">
				<form name="addReportForm" class="col-md-12" role="form">
					<p>
						<ul class="alert petro-alert alert-info petro-alert-info" role="status">
							<li>
								<!--<g:message code="application.reviews.comment.edit.warning" default="Ao editar sua avaliação as opiniões sobre a mesma serão descartadas."/>-->Este espaço destina-se somente a ajuste das informações visualizadas no cadastro dessa aplicação.
							</li>
						</ul>

						<ul class="alert petro-alert petro-alert-warning" role="status" ng-show="errors">
							<li ng-repeat="error in errors">
								{{error}}
							</li>
						</ul>
					</p>

					<div class="checkbox" ng-repeat="issue in issues">
		          		<label><input type="checkbox" ng-model="issue.checked">{{issue.description}}</label>
					</div>

	          		<!--<textarea name="comment" placeholder='<g:message code="application.reviews.comment.text.label" default="Se o problema que você deseja relatar não está na lista acima, por favor, descreva-o aqui. "/>' ng-model="comment" rows="8" class="form-control">
	          		</textarea>-->
	          		<textarea name="description" placeholder='Se o problema que deseja relatar não está na lista acima ou se deseja complementar a opção selecionada, por favor, descreva aqui.' ng-model="description" rows="8" class="form-control">
	          		</textarea>
				</form>
			</div>
			<div class="modal-footer petro-modal-footer">
				<button type="button" class="btn btn-default petro-my-options" data-dismiss="modal"><!--<g:message code="application.reviews.button.cancelar.label" default="cancelar"/>-->cancelar</button>
				<button type="button" class="btn btn-success" ng-click="save()" data-loading-text="Enviando..." ng-disabled="isFormInvalid()"><!--<g:message code="application.reviews.button.save.label" default="avaliar"/>-->Enviar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->