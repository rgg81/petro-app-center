<div class="modal fade" id="{{modalId}}" tabindex="-1" role="dialog" aria-labelledby="remove-review-{{modalId}}" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header petro-modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title petro-modal-title">{{title}}</h4>
			</div>
			<div class="modal-body petro-modal-body">
		      	<p><ul class="alert petro-alert alert-info petro-alert-info petro-table-info"><li>{{text}}</li></ul></p>
		      	<p><ul class="alert petro-alert alert-success petro-alert-message"><li><span id="message-delete-success-{{modalId}}"></span></li></ul></p>
			</div>
			<div class="modal-footer petro-modal-footer petro-label-success">
				<p>
			      	<button data-dismiss="modal" id="button-cancel-{{modalId}}" type="button" class="btn btn-default petro-my-options">{{cancelButtonLabel}}</button> 
		      		<button id="button-ok-{{modalId}}" data-loading-text="{{loadingText}}" type="button" class="btn btn-success" ng-click="save()">{{okButtonLabel}}</button>
				</p>
			</div>
		</div>
	</div>
</div>