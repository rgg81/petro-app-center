<tr>
	<td>
		<br/>
		<p>Olá,</p>
		<p><strong>O administrador</strong> da aplicação <span style="color:#008542; font-weight:bold;">${reviewedApplication.name}</span> alterou a resposta dada à sua avaliação.</p>    
	</td>
</tr>
<tr>
	<td align="right"><strong><a target="_blank" style="text-decoration:underline; color:#428bca; cursor:pointer; font-size:14px;" href="http://${host}/petro-app-center/application/show/${reviewedApplication.id}" title="Veja os detalhes">Acesse para visualizar os detalhes da aplicação.</a></strong><br/></td>
</tr>