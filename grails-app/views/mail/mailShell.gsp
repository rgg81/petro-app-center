<html>
	<body style="font-size:16px; line-height: 25px; font-family:Trebuchet MS, Tahoma, sans-serif;">
		<table width="540px" align="center" border="0" style="background-color: #F8F8F8; border:1px solid #d6d6d6;">
			<tr>
				<td>  
					<table border="0" cellspacing="10" cellpadding="10" width="100%" align="center">
						<tr>
							<td height="70px" align="left" style="background-color: #008542;">
								<p style="font-size: 22px; color:#fff;">APLICAÇÕES PETROBRAS<br/><span style="color: #FFCC30;">&minus;</span></a></p>
							</td>
						</tr>
						<g:render template="${template}" />
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>