<tr>
	<td>
		<br/>
		<p>Olá,</p>
		<p><strong>${review.user.name}</strong>, chave <strong>${review.user.key}</strong>, realizou a seguinte avaliação sobre a aplicação <span style="color:#008542; font-weight:bold;">${review.application.name}</span></p>    
	</td>
</tr>
<tr>
	<td><span style="color:#666666;">nota:</span> <strong>${review.rating}</strong><br/><br/></td>
</tr>
<tr>
	<td><strong>${review.title?.encodeAsHTML()?:""}<strong></td>
</tr>
<tr>
	<td>${review.text?.encodeAsHTML()?.replaceAll("\n", "<br>")?:""}<br/><br/></td>
</tr>
<tr>
	<td align="right"><strong><a target="_blank" style="text-decoration:underline; color:#428bca; cursor:pointer; font-size:14px;" href="http://${host}/petro-app-center/application/show/${review.application.id}" title="Veja os detalhes">Acesse para visualizar os detalhes da aplicação.</a></strong><br/></td>
</tr>