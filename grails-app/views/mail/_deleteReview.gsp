<tr>
    <td>
        <br/>
        <p>Olá, <strong>${review.user.name}</strong></p>
        <p>Sua avaliação, realizada sobre a aplicação <span style="color:#008542; font-weight:bold;">${review.application.name}</span>, foi excluída por não atender às determinações estabelecidas nos <strong>Termos e Condições de Uso</strong>. </p>    
    </td>
</tr>
<tr>
    <td>
        <p>Contamos com sua compreensão,<br/>
        <span style="color:#008542; font-weight:bold;">Aplicações Petrobras</span></p>
    </td>
</tr>
<tr>
    <td align="right"><strong><a target="_blank" style="font-size:14px; text-decoration:underline; color:#428bca; cursor:pointer;" href="http://${host}/petro-app-center#termsAndconditionsMessageModal" title="Acessar Termos e condições de uso">Consulte os Termos e Condições de Uso.</a></strong><br/></td>
</tr>