<tr>
	<td>
		<br/>
		<p>Olá,</p>
		<p><strong>${report.user.name}</strong> (${report.user.key}) relatou os seguintes erros de informação para a aplicação <strong><a target="_blank" style="text-decoration:underline; color:#428bca; cursor:pointer; font-size:14px;" href="http://${serverNameWithPort}/petro-app-center/application/show/${report.application.id}" title="Veja os detalhes">${report.application.name}</a></strong>
		</p>    
	</td>
</tr>
<tr>
	<td>
		<g:if test="${report.sortedIssues?.size() > 0}"> 
			<p><strong>Erros:</strong> </p>
			<ul>
				<g:each in="${report.sortedIssues}">
				    <li>${it.description}</li>
				</g:each>		    
			</ul>
		</g:if>

		<g:if test="${report.description?.size() > 0}"> 
			<p><strong>Observação:</strong> ${report.description.encodeAsHTML()?.replaceAll("\n", "<br>")}</p>
		</g:if>
	</td>
</tr>