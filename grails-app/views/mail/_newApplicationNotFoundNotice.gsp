<tr>
	<td>
		<br/>
		<p>Olá,</p>
		<p><strong>${applicationNotFoundNotice.user.name}</strong> (${applicationNotFoundNotice.user.key}) informou não ter encontrado uma aplicação. Segue o conteúdo de sua notificação: 
		</p>    
	</td>
</tr>
<tr>
	<td>
		<g:if test="${applicationNotFoundNotice.url}"> 
			<p><strong>URL:</strong> ${applicationNotFoundNotice.url}</p>
		</g:if>

		<p><strong>Descrição:</strong> ${applicationNotFoundNotice.description.encodeAsHTML()?.replaceAll("\n", "<br>")}</p>
	</td>
</tr>