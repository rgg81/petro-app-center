<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="reportControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:external uri="/js/ShowHideModule.js" type="js"/>
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>
	</head>
	<body>
		<div class="table-responsive col-md-12" role="main" class="row" ng-app="reportApp">
			<table class="table table-striped petro-table-striped" ng-controller="reportCtrl">
				<thead>
					<th class="col-md-3">Aplicação</th>
					<th class="col-md-1">Usuário</th>
					<th class="col-md-1">Data</th>
					<th class="col-md-3">Observação</th>
					<th class="col-md-4">Erros</th>
				</thead>
				
				<tbody>
					<tr ng-repeat="report in reports">
						<td>{{report.application.name}}</td>
						<td>{{report.user.name}} ({{report.user.key}})</td>
						<td>{{report.dateCreated}}</td>
						<td ng-bind-html="report.description | newlines | unsafe"></td>						
						<td>
							<ul>
								<li ng-repeat="issue in report.issues">{{issue.description}}</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>

		</div>

	</body>
</html>