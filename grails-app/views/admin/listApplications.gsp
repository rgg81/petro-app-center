
<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<r:require module="angular"/>
		<r:require module="showHideAnimatedAngular" />
		<r:external uri="/js/ShowHideModule.js" type="js"/>
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-application" class="row content scaffold-list petro-edit-app" ng-app="mainApp" role="main">

			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit">Administrar Aplicação</h3>
		 		<g:link controller="application" action="create">
					<button type="button" class="btn btn-default pull-right petro-admin-new" title="criar nova aplicação">
		 			 		 nova aplicação </button>
		 		</g:link>
		 		<g:link controller="admin" action="listApplicationsByDepartment">
					<button type="button" class="btn btn-default pull-right petro-admin-new" title="criar nova aplicação">
		 			 		 aplicações por agilidade</button>
		 		</g:link>
			</div>

			<div class="col-md-12 petro-admin">
			
				<g:if test="${flash.message}">
					<div class="alert petro-alert alert-success" role="status">${flash.message}</div>
				</g:if>	

				 <!--<div class="row petro-edit-app">
					<div class="col-md-4 col-md-offset-8">
						<g:link controller="application" action="create">
							<button id="" class="btn btn-success save petro-admin-button-form">nova aplicação</button>
						</g:link>
					</div>
				</div>-->

				<div class="main-row">
					<div class="table-responsive col-md-12 petro-admin-app">
						<table class="table table-striped petro-table-striped">
							<thead>
								<th class="col-md-1">Ícone</th>
								<th class="col-sm-3 col-md-5">Aplicação</th>
								<th>Última atualização</th>
								<th>Situação</th>
								<th class="col-md-2">Ações</th>
							</thead>
							
							<tbody>
								<g:each in="${appsAndStatus}" status="i" var="appAndStatus">
									<tr>
										<td><img src="/${appAndStatus.application.icon.relativePath()}?${appAndStatus.application?.icon?.size}" class="admin-list-app-icon" alt="Ver detalhes" /></td>
										<td>
											<g:if test="${appAndStatus.status != 'Rascunho' }">
												<g:link controller="application" action="show" id="${appAndStatus.application.id}">${fieldValue(bean: appAndStatus.application, field: "name")}</g:link>
											</g:if>
											<g:else>
												${fieldValue(bean: appAndStatus.application, field: "name")}	
											</g:else>
										</td>
										<td><g:formatDate format="dd/MM/yyyy" date="${appAndStatus.application.lastUpdated}"/></td>
										<td>${appAndStatus.status }</td>
										<td>
											<div class="btn-group btn-group-sm">
												<g:link class="btn petro-btn-moderator petro-btn-moderator-edit" controller="application" action="edit" id="${(appAndStatus.application.hasProperty('application') && appAndStatus.application.application) ? appAndStatus.application.application.id : appAndStatus.application.id}"></g:link>
												
												<shiro:hasRole name="${ grailsApplication.config.app.role.moderator }">
													<delete-app app-id="${(appAndStatus.application.hasProperty('application') && appAndStatus.application.application) ? appAndStatus.application.application.id : appAndStatus.application.id}"  class-btn="btn petro-btn-moderator petro-btn-moderator-delete"  />
												</shiro:hasRole>
										    </div>
										</td>
									</tr>
								</g:each>
							</tbody>
						</table>
					</div>
		  	   </div>

			</div>
		</div>
	</body>
</html>
