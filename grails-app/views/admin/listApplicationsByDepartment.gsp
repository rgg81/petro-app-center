
<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<r:require module="angular"/>
		<r:require module="showHideAnimatedAngular" />
		<r:external uri="/js/ShowHideModule.js" type="js"/>
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="list-application" class="row content scaffold-list petro-edit-app" ng-app="mainApp" role="main">

			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit">Aplicações por agilidade</h3>
				<g:link controller="admin" action="listApplications">
					<button type="button" class="btn btn-default pull-right petro-admin-new" title="criar nova aplicação">
		 			 		 administrar aplicação</button>
		 		</g:link>
				<g:link controller="admin" action="listApplicationsByDepartment" params="[format: 'csv', d: params.d]">
					<button type="button" class="btn btn-default pull-right petro-admin-new" title="criar nova aplicação">
		 			 		 exportar csv</button>
		 		</g:link>
			</div>
			<div class="col-md-12">
				<g:form url='[controller: "admin", action: "listApplicationsByDepartment"]' class="petro-search-form" id="departmentSearch" name="departmentSearch" method="get">
  			
  				<div class="row">
  					<div class="col-xs-9 col-md-5">
						<g:textField name="d" id="department-input" data-provide="typeahead" autocomplete="off" class="petro-department-field" 
						placeholder="Digite a lotação de uma agilidade. Ex: TIC/TIC-CORP/TIC-JCAA" value="${params.d}" size="150"/>
						
						<g:javascript>
						
						$.get("/petro-app-center/application/department", function(data, status){
							var departments = data;
		        				/* Inicializa autocomplete do bootstrap na busca */
							var deps = new Bloodhound({
							  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
							  queryTokenizer: Bloodhound.tokenizers.whitespace,
							  local: $.map(departments, function(dep) { return { value: dep }; })
							});
							 
							deps.initialize();

							$('#department-input').typeahead({
							  hint: true,
							  highlight: true,
							  minLength: 1
							},
							{
							  name: 'departments',
							  displayKey: 'value',
							  source: deps.ttAdapter()
							});
							 
							$('#department-input').bind('typeahead:selected', function(obj, datum) {
								window.location.href = '/petro-app-center/admin/listApplicationsByDepartment?d=' + encodeURIComponent(datum.value);
							});
		    			});

					

						</g:javascript>
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn btn-success" title="Encontre a aplicação que procura.">Buscar</button>
					</div>
				</div>
				</g:form>
			</div>
			

			<div class="col-md-12 petro-admin">
			
				<g:if test="${flash.message}">
					<div class="alert petro-alert alert-success" role="status">${flash.message}</div>
				</g:if>	
				<g:if test="${apps.size() > 0}">
					<div class="main-row">
						<div class="table-responsive col-md-12 petro-admin-app"><table class="table table-striped petro-table-striped">
								<thead>
									<th class="col-md-1">Ícone</th>
									<th class="col-md-4">Aplicação</th>
									<th class="col-md-1">Código</th>
									<th class="col-md-2">Centro de Agilidade</th>
									<th class="col-md-4">Administradores</th>
								</thead>
								
								<tbody>
									<g:each in="${apps}" status="i" var="app">
										<tr>
											<td><img src="${app.iconPath}" class="admin-list-app-icon" alt="Ver detalhes" /></td>
											<td>
												<g:link controller="application" action="show" id="${app.id}">${app.name}</g:link>
											</td>
											<td>${app.catalogCode}</td>
											<td>${app.department}</td>
											<td>${app.admins}</td>
										</tr>
									</g:each>
								</tbody>
							</table>
						</div>
						<g:if test="${total > 10 && apps.size > 0}">
							<div class="outer-center">
								<div class="inner-center petro_pagination">
								<g:paginate total="${total}"  next="\u003Cspan class=\'glyphicon glyphicon-chevron-right'\u003E\u003C/span\u003E" prev="\u003Cspan class=\'glyphicon glyphicon-chevron-left'\u003E\u003C/span\u003E" params="[d:params.d]"/>
								</div>
							</div>
						</g:if>
			  	   </div>
			  	</g:if>
			  	<g:else>
					<p class="petro-you-search">
						<span class="petro-without-result">Nenhuma aplicação encontrada com o centro de agilidade especificado.</span> 
					</p>
					<div class="col-md-12 col-offset-1 petro-you-search">
						<label>Sugestões:</label>
						<ul class="list-unstyled">
							<li>- Certifique-se de que a lotação esteja escrita corretamente.</li>
							<li>- Utilize o autocomplete.</li>
						</ul>
					</div>
			  	</g:else>
			</div>
		</div>
	</body>
</html>
