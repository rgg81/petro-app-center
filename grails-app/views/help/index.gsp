
<%@ page import="br.com.petrobras.appcenter.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<r:require module="angular"/>
		<r:require module="showHideAnimatedAngular" />
		<r:external uri="/js/ShowHideModule.js" type="js"/>
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<r:external uri="/js/MainModule.js" type="js"/>
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>
	<body>


	  <div id="help-application" class="row content"  role="main">

	  		<div class="panel petro-panel-box petro-help">

	  			 <div class="panel-heading petro-box-title">
	  				<h3 class="pull-left petro-box-title">Ajuda</h3>
	  			</div>

	  			<div class="panel-body">
<!-- 
	  				<div class="row">

	  					<div class="col-md-12">

	  						<div class="col-md-4">
	  							<button type="button" class="btn btn-lg btn-block petro-help-type"><span class="glyphicon glyphicon-question-sign petro-help-icon-app"></span> Conhecendo o Aplicações</button>
	  						</div>
	  						<div class="col-md-4 col-md-offset-4">
								<button type="button" class="btn btn-lg btn-block petro-help-type-off"><span class="glyphicon glyphicon-question-sign petro-help-icon-app"></span> Administrador</button>
							</div>
						</div>

	  				</div>
 -->
	  				<div class="row">

	  					<div class="col-md-12 petro-help-question">

	  						<!--Begins collapse-->
	  						<div class="panel-group" id="accordion">

							  <div class="panel panel-default petro-help-panel">
							    <div id="buscar-aplicacoes" class="panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" title="Ver conteúdo">
							          Buscar Aplicações <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseOne" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							        <p>É possível realizar a busca de uma determinada aplicação fornecendo, no campo de busca, o termo desejado.</p>
									<p>Conforme o termo vai sendo digitado, são exibidas, logo abaixo do campo de busca, as aplicações cujos nomes se iniciam com esses termos (o que chamamos de autocomplete) e, a seguir, as sugestões de aplicações que contenham termos com o mesmo radical do termo já digitado.</p>
									<p>Se a aplicação que você procura for apresentada, ao clicar sobre seu nome, você verá sua página.</p> 
									<p>Caso a aplicação não seja apresentada pelo autocomplete, você pode ainda solicitar a busca, clicando no botão ao lado do campo ou pressionando a tecla <em>"Enter"</em>.</p>
									<p>Neste caso, são exibidas as aplicações nas quais o termo fornecido foi encontrado no nome e/ou na descrição e/ou nas tags da aplicação.</p>
									<p>Nas buscas com mais de um termo, são trazidas nos resultados as aplicações em que foram encontrados todos os termos fornecidos no campo da busca.</p>
									<p>São exibidas até 10 aplicações por página, contendo cada uma seu ícone, seu título, um pequeno resumo da aplicação, a média das notas a ela atribuídas, suas tags e o botão para acesso à aplicação.</p>
									<p>Neste momento, você poderá escolher entre acessar diretamente a aplicação, clicando no botão <em>“acessar”</em> ou visualizar sua página de detalhes, clicando em seu título.</p>
									<p>Se, após fornecer os termos que você julgou adequados para a busca, nenhum resultado apresentar a aplicação que você procurava, você pode selecionar a opção <strong>Fale conosco</strong> ao final da página, na qual você deverá fornecer detalhes da aplicação que procura e seu endereço eletrônico, caso você o conheça.</p> 
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default petro-help-panel">
							    <div id="minhas-aplicacoes" class="panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" title="Ver conteúdo">
							          Minhas Aplicações <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseTwo" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							        Existem várias maneiras para adicionar uma aplicação na área <strong>Minhas Aplicações</strong> e poder acessá-la diretamente da página inicial.
									<p>Se a aplicação que você deseja adicionar já está sendo exibida nas áreas <strong>Mais Populares</strong> ou <strong>Melhor Avaliadas</strong> da página inicial, basta posicionar o mouse sobre ela e clicar no botão indicado pelo sinal gráfico <strong>mais (+)</strong>. Automaticamente ela passará a fazer parte da área Minhas Aplicações.</p>
									<p>Se você quiser obter mais detalhes sobre a aplicação, repita o procedimento de passar o mouse sobre a aplicação e, desta vez, clique no botão representado pelo símbolo gráfico de um <strong>olho</strong>.</p>
									<p>Na página de detalhes da aplicação, você encontrará dois botões à direita do nome. Um destes botões, chamado <em>"adicionar"</em>, ao ser clicado, adiciona a aplicação para a área <strong>Minhas Aplicações</strong>.</p>
									<p>Se a aplicação desejada não estiver presente na página inicial, realize uma busca, por exemplo, pelo nome da aplicação. Ao selecioná-la nos resultados, você visualizará sua página de detalhes, podendo proceder da mesma forma descrita acima.</p>
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default petro-help-panel">
							    <div id="compartilhar-minhas-aplicacoes"  class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" title="Ver conteúdo">
							          Compartilhar Minhas Aplicações <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseThree" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							        Para compartilhar com outro usuário suas aplicações favoritas, clique no botão com o símbolo <em>“compartilhar”</em>, presente na área <strong>Minhas Aplicações</strong>. <br/>
							        Será apresentada uma URL para você enviar a quem desejar.
							      </div>
							    </div>
							  </div>

							  <div class="panel panel-default petro-help-panel">
							    <div id="editar-minhas-aplicacoes" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" title="Ver conteúdo">
							          Editar Minhas Aplicações <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseFour" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	<p>É possível reorganizar a área <strong>Minhas Aplicações</strong>, reordenando as aplicações ou mesmo excluindo as que não são mais de seu interesse.<br/>
									Para tal, clique no botão <em>“editar”</em>, na área <strong>Minhas Aplicações</strong>.<br/> 
									Em seguida, esta área será destacada e as aplicações nela contidas serão exibidas de uma forma mais atrativa, permitindo que você realize duas ações:</p>
									<p><strong>- Mover as aplicações</strong> <br/>
									 Troque as aplicaçoes de posição clicando com o mouse sobre a aplicação que deseja mover, mantendo o mouse pressionado. <br/>
									 Arraste a aplicação, posicionando-a no local desejado e solte o mouse. Faça isso com todas as aplicações que desejar.</p>
									<p><strong>- Remover aplicações</strong> <br/>
									Clique com o mouse sobre a imagem <strong>(X)</strong> no canto superior direito da aplicação. No mesmo instante a aplicação será removida.</p>
									<p>Para que as alterações que você realizou sejam mantidas, clique no botão <em>“sair”</em>.</p>
							      </div>
							    </div>
							  </div>

							  <div class="panel panel-default petro-help-panel">
							    <div id="detalhes-da-aplicacao" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" title="Ver conteúdo">
							          Detalhes da Aplicação <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseFive" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	<p>Apresenta todas as informações obtidas automaticamente do Catálogo de Aplicações quando seu cadastro foi realizado, além de outras adicionais:</p>
									<p><strong>- Nome da aplicação e a imagem criada para representá-la.</strong></p>
									<p><strong>- Opções para acessá-la, adicioná-la ou removê-la da área Minhas Aplicações.</strong><br/>
									Se você faz uso frequente de uma aplicação e deseja acessá-la de forma mais rápida diretamente da página inicial, selecione a opção <em>“adicionar”</em> e automaticamente sua aplicação será incluída na área <strong>Minhas Aplicações</strong>. Ao mesmo tempo, a opção <em>“adicionar”</em> será substituída por <em>“remover”</em>, permitindo que você retire a aplicação de sua área <strong>Minhas Aplicações</strong> sempre que não for mais necessária.</p>
									<p><strong>- Descrição da aplicação</strong></p>
									<p><strong>- Tags</strong><br/>
									Correspondem às palavras chaves que fazem referência à aplicação, portanto facilitam a busca pela mesma.</p>
									<p><strong>- Avaliações</strong><br/>
									Nesta área é apresentado um placar das notas dadas pelos usuários à aplicação, representado graficamente de 1 (uma) a 5 (cinco) estrelas, segundo a quantidade de usuários votantes.<br/> 
									Também é possível fazer a sua avaliação sobre a aplicação, selecionando a opção <em>“avaliar”</em>. Você deverá fornecer uma nota para a aplicação e, se desejar, acrescentar um texto com as suas impressões de uso da aplicação.<br/> 
									Cada avaliação realizada com comentários é vista pelos demais usuários. Eles podem ainda dar suas opiniões sobre o que você escreveu, selecionando uma das opções <em>“gostar”</em> ou <em>“não gostar”</em>.<br/>
									As duas avaliações que possuam a maior quantidade de opiniões <em>“gostar”</em> se apresentam ao lado do placar que exibe a média da avaliação.<br/>
									Para mais detalhes, veja o item de ajuda <strong>Avaliar Aplicação</strong>.</p>
									<p><strong>- Outras Informações</strong><br/>
									São informações adicionais sobre a aplicação, tais como:<br/>
									<p>. Identificação da versão em que se encontra a aplicação, seguida da data de sua implantação.<br/>
									. Descrição da versão em que se encontra a aplicação, contendo informações como melhorias e alterações efetuadas para a versão.<br/>
									. Informações de compatibilidade com os diversos navegadores.<br/>
									. Informações de suporte, que são informações de apoio ao uso da aplicação.</p>
									
									É possível melhorar o conteúdo da informação da aplicação exibidas na página, selecionando a opção <em>"erros de informação"</em>.
									Para mais informações, veja o item de ajuda <strong>Erros de informação</strong>.</a></p>
									</div>
							    </div>
							  </div>

							   <div class="panel panel-default petro-help-panel">
							    <div id="acessar-aplicacao" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" title="Ver conteúdo">
							          Acessar Aplicação <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseSix" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	<p>Você pode acessar uma aplicação estando na página inicial, na página de detalhes da aplicação ou mesmo na página de resultados de uma busca.<p>
									<p>- Na <strong>página inicial</strong>, o acesso à aplicação é possível ao clicar no nome, na imagem ou no ícone de seta que é exibido, quando posicionamos o mouse na mesma.</p>
									<p>- Na <strong>página de detalhes</strong> e <strong>página de resultados da busca</strong>, o acesso é através do botão <em>“acessar”</em>.</p>
									<p>Em todos os casos, ao selecionar a opção <em>“acessar”</em>, você será direcionado para o endereço da aplicação que foi cadastrado.</p>
							      </div>
							    </div>
							  </div>

							  <div class="panel panel-default petro-help-panel">
							    <div id="meus-acessos" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" title="Ver conteúdo">
							          Meus Últimos Acessos <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseSeven" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	Esta área apresenta até 08 aplicações mais acessadas pelo usuário nos últimos 10 dias. Desta forma, você pode identificar a necessidade de inserir novas aplicações na área Minhas Aplicações.
							      </div>
							    </div>
							  </div>

							   <div class="panel panel-default petro-help-panel">
							    <div id="mais-populares" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" title="Ver conteúdo">
							          Mais Populares <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseEight" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	São aplicações que foram mais vezes acrescentadas à área Minhas Aplicações por todos os usuários. Quanto maior a quantidade de associações feitas, mais popular é a aplicação, sendo exibida nas primeiras posições. 
							      </div>
							    </div>
							  </div>

							   <div class="panel panel-default petro-help-panel">
							    <div id="melhor-avaliadas" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" title="Ver conteúdo">
							          Melhor Avaliadas <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseNine" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	São as aplicações consideradas as melhores, ou seja, aquelas com maior nota dada (quantidade de estrelas) pelos usuários e que tenham tido uma boa quantidade de avaliações. 
							      </div>
							    </div>
							  </div>

 							<div class="panel panel-default petro-help-panel">
							    <div id="informe-erros-de-cadastro" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" title="Ver conteúdo">
						          Erros de Informação <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseTen" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	<p>Se, ao ler as informações na página de detalhes da aplicação, você considerar que uma ou mais não estão corretas, você pode utilizar a opção <strong>Erros de informação</strong>, para relatar estes erros.</p> 
							       	<p>Os erros podem ser de várias naturezas, como por exemplo, ortográficos ou falta de informações. Ao fazer o informe, você já encontrará várias opções para seleção, mas caso o erro não esteja presente em nenhuma das opções, você ainda dispõe de um campo para digitação. Você também pode optar por utilizar o campo de digitação para complementar a opção selecionada.</p>
									<p>Cabe ressaltar que este é um canal apenas para erros das informações relacionadas à página de detalhes da aplicação. Para erros encontrados após o acesso à aplicação, o canal correto para este reporte é o <strong>Web 881</strong>.</p>
							      </div>
							    </div>
							  </div>

							   <div class="panel panel-default petro-help-panel">
							    <div id="avaliar-aplicacao" class="panel-heading petro-help-panel-heading">
							      <h4 class="panel-title petro-help-panel-title-off">
							        <a data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" title="Ver conteúdo">
							          Avaliar Aplicação <span class="glyphicon glyphicon-chevron-down petro-help-collapse-off"></span>
							        </a>
							      </h4>
							    </div>
							    <div id="collapseEleven" class="panel-collapse collapse petro-panel-collapse">
							      <div class="panel-body">
							       	Ao selecionarmos a opção para avaliar a aplicação, podemos expressar as nossas impressões de experiência de duas formas:
									<p><strong>- Atribuindo uma nota à aplicação que pode ser de 1 (uma) a 5 (cinco) estrelas.</strong><br/>
									As notas dadas à aplicação por todos os usuários resultam na média que é exibida na página de detalhes, na página inicial e nos resultados da busca. Sendo assim, de qualquer ponto do site você fica sabendo como a aplicação está sendo avaliada.</p>
									<p><strong>- Atribuindo uma nota à aplicação e adicionando seus comentários.</strong><br/>  
									A nota dada à aplicação pode ser enriquecida com comentários sobre o que você considera bom ou o que pode ser melhorado.</p>
									<p>Sua avaliação é encaminhada aos administradores da aplicação que avaliam seus comentários e podem utilizá-los como referência para uma eventual troca de versão, por exemplo.</p>
									<p>Cada avaliação realizada com comentários é exibida na página de detalhes da aplicação, permitindo que os demais usuários a vejam. Eles podem ainda dar suas opiniões sobre o que você escreveu, selecionando uma das opções “gostar” ou “não gostar”.</p>
									<p>As duas avaliações feitas por usuários que possuam a maior quantidade de opiniões “gostar” se apresentam ao lado do placar que exibe a média da avaliação da aplicação.</p>
									<p>As avaliações feitas para cada uma das aplicações, podem apresentar réplicas de seus Administradores e, quando existentes, são exibidas abaixo de cada avaliação.</p>
									<p>A avaliação pode estar sendo exibida ao lado do placar e, neste caso, apresentam suas réplicas apenas depois de clicar no link "Ver avaliação completa".</p>
									<p>Você pode alterar sua avaliação, tanto a nota como os comentários, a qualquer momento. No entanto, ao fazer isso, todas as opiniões dadas anteriormente (quantidade de pessoas que gostaram ou que não gostaram de sua avaliação) serão descartadas. Ao alterar sua avaliação com comentários, a réplica feita pelo Administrador, caso haja, deixará de ser apresentada.</p>
									Você tem ainda a opção de excluir a sua avaliação. Neste caso, caso haja uma réplica, ela também deixará de ser apresentada.
							      </div>
							    </div>
							  </div>

							</div>
							<!--Ends Collpase-->

				  		</div>

	  				</div>

	  			</div>

	  		</div>
	  			
	  </div>
	  <g:javascript>
	  		var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
	  		if($('#' + hash).offset()){
      			$('#' + hash).next().addClass('in');
	      		$('html, body').animate({
	                  scrollTop: $('#' + hash).offset().top
	              }, 1000);
	      	}
	  </g:javascript>

	</body>
</html>