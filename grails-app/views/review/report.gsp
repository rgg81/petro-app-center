<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="bootstrapAngularUI" />
		<r:require module="highcharts" />
		<r:require module="bootstrapRangeDatePicker" />
		<r:require module="reviewReportController" />
		
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>

	<body>
	<g:applyLayout name="tools-menu"/>
	<div ng-app="trendApp" id="reviewsReportCtrl" ng-controller="reviewsReportCtrl" class="main-row row"  role="main">
		<div class="panel-heading-edit petro-box-title-edit">
			<h3 class="pull-left petro-box-title-edit">Relatório de Avaliação</h3>
			<div class="pull-right">
					<a ng-show="!loading && url" ng-href="{{urlCsv}}" class="btn btn-default petro-review-report-loading petro-home-buttons">Exportar csv</a>
					<div id="reportrange" class="petro-datarange">
        				<span>Selecione o Período</span>
        				<b ng-show="!loading" class="caret" style="position: relative"></b>
        				<div ng-show="loading" id="review-report-loading" class="petro-review-report-loading"></div>
    				</div>
				</div>
		</div>
	
		
		<div class="col-md-12">
			<div class="row">
				<div id="graph"></div>
			</div>
		</div>


		<!-- Last reviews -->
		
		<div class="col-md-12" ng-show="data[day].count"> 
			<ul class="list-group petro-list-group">
				<li class="row list-group-item petro-list-group-item" ng-repeat="review in data[day].reviews">
					<div class="col-md-12 hack-float-left-parent">
						<div class="petro-review-name">{{review.user.name}}</div>
						<div class="petro-review-department">{{review.user.department}}</div>
						<div class="petro-review-rating">
							<table class="ratingDisplay">
				                <tbody>
				                	<tr>
				            			<td><div class="star on"><a></a></div></td>
				            			<td><div class="star {{(review.rating > 1) && 'on' || 'off'}}"><a></a></div></td>
				            			<td><div class="star {{(review.rating > 2) && 'on' || 'off'}}"><a></a></div></td>
				            			<td><div class="star {{(review.rating > 3) && 'on' || 'off'}}"><a></a></div></td>
				            			<td><div class="star {{(review.rating > 4) && 'on' || 'off'}}"><a></a></div></td>
				                	</tr>
				            	</tbody>
				            </table>
						</div>
						<div class="petro-review-application">
							<a  target="_blank" ng-href="/petro-app-center/application/show/{{review.app_id}}">{{review.app}}</a>
						</div>
						<div class="petro-review-title">
							{{review.title}}
						</div>
						<div class="petro-review-text" ng-bind-html="review.text | newlines | unsafe">
						</div>
						<div class="petro-review-date">
							{{review.date}}
						</div>
					</div>
				</li>
			</ul>
		</div>		
	</div>


		<g:javascript>

			$(document).ready(function() {

				var onDateChange = function(start, end) {
			    	console.log("combo changed");
			    	var fstart = start.format('DD/MM/YYYY');
			    	var fend = end.format('DD/MM/YYYY');
			    	$('#reportrange span').html(fstart + ' - ' + fend);
			    	angular.element('#reviewsReportCtrl').scope().list(fstart,fend);
			    };
				$('#reportrange').daterangepicker({
					locale : {cancelLabel : "Cancelar", applyLabel : "Aplicar", fromLabel : "De", toLabel: "Até", customRangeLabel: "Manual",
						daysOfWeek: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se','Sa'],
						monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
					},
					format : 'DD/MM/YYYY',
					opens : 'left',
					ranges: {
						'Últimos 7 dias': [moment().subtract('days', 6), moment()],
						'Últimos 30 Dias': [moment().subtract('days', 29), moment()],
						'Este Mês': [moment().startOf('month'), moment().endOf('month')],
						'Mês Anterior': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
					},
					startDate: moment().subtract('days', 6),
					endDate: moment()
					}, 
					onDateChange
				);
				onDateChange(moment().subtract('days', 6),moment());
			});
		</g:javascript>
	</body>
</html>