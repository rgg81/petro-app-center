<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />
		<r:require module="highcharts" />
		<r:require module="toolbeltController" />
		<r:require module="dynatableCss" />
		<r:require module="dynatableJs" />
		

	</head>
	<body>
		<g:applyLayout name="tools-menu"/>
		<div ng-app="toolbeltApp" id="reviewsReportCtrl" ng-controller="averageXUseCtrl" class="main-row row"  role="main">
			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit">Aplicações: Vezes adicionada X Avaliações<span class="petro-toolbelt-title-loading" id="toolbelt-title-loading"></span></h3>
			</div>
		</div>
		<div class="row" style="padding: 10px">
			<div id="scatter-chart"></div>
		</div>
		<script>

		</script>	
	</body>
</html>