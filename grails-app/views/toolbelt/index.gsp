<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />
		<r:require module="highcharts" />
		<r:require module="toolbeltController" />
		

	</head>
	<body>
		<g:applyLayout name="tools-menu"/>		
		<div class="container" ng-app="toolbeltApp">
			<div style="width 100%; display: inline-block; margin: 10px 0"><h3 class="pull-left petro-box-title-edit">Aplicações<span class="petro-toolbelt-title-loading" id="toolbelt-title-loading"></span></h3></div>
			<div class="row" ng-controller="toolbeltCtrl" style="visibility: hidden" id="applications-panel">				
				<div class="col-sm-8 col-xs-12 no-padding-left no-padding-right">
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default">
						 	<div class="panel-heading petro-toolbelt-title centered">Cadastradas</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.totalApplications}}</span>
								</div>
						  	</div>
						</div>
					</div>
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default ">
						 	<div class="panel-heading petro-toolbelt-title centered">Publicadas</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.publishedApplications}}</span>
								</div>
						  	</div>
						</div>
					</div>
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default ">
						 	<div class="panel-heading petro-toolbelt-title centered">Com Rascunho</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.pendingDrafts}}</span>
								</div>
						  	</div>
						</div>
					</div>
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default ">
						 	<div class="panel-heading petro-toolbelt-title centered">Total de avaliações</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.numberOfRatings}}</span>
								</div>
						  	</div>
						</div>
					</div>
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default ">
						 	<div class="panel-heading petro-toolbelt-title centered">Avaliadas</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.ratedApplications}}</span>
								</div>
						  	</div>
						</div>
					</div>
					<div  class="col-sm-4 col-xs-6">
						<div class="panel panel-default ">
						 	<div class="panel-heading petro-toolbelt-title centered">Avaliação Média</div>
							<div class="panel-body" >
								<div class="petro-app-score">
									<span class="petro-app-score-value">{{infos.averageRating}}</span>
								</div>
						  	</div>
						</div>
					</div>	
				</div>
				<div class="col-sm-4 col-xs-12 no-padding-left no-padding-right">
					<div  class="col-xs-12" >
						<div class="panel panel-default ">
							<div class="panel-heading petro-toolbelt-title centered">Médias acima e abaixo de 3</div>
							<div class="panel-body">
								<div id="averageChart"  style="height: 16em" ></div>
						  	</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<g:javascript>

			$(document).ready(function() {
				
			});
		</g:javascript>
	</body>

</html>