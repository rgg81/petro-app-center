<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />
		<r:require module="highcharts" />
		<r:require module="toolbeltController" />
		<r:require module="dynatableCss" />
		<r:require module="dynatableJs" />
		

	</head>
	<body>
		<g:applyLayout name="tools-menu"/>
		<div ng-app="toolbeltApp" id="reviewsReportCtrl" ng-controller="allApplicationsCtrl" class="main-row row"  role="main">
			<div class="panel-heading-edit petro-box-title-edit">
				<h3 class="pull-left petro-box-title-edit">Aplicações cadastradas<span class="petro-toolbelt-title-loading" id="toolbelt-title-loading"></span></h3>
				<div class="pull-right">
					<a ng-show="!loading && url" ng-href="{{urlCsv}}" class="btn btn-default petro-review-report-loading petro-home-buttons">Exportar csv</a>
				</div>
			</div>
		</div>
		<div class="row" style="padding: 10px">
			<table id="my-final-table" class="table table-striped petro-table-striped petro-table-sortable">
			  <thead>
			    <th data-dynatable-column="name">Nome</th>
			    <th data-dynatable-column="catalogCode">Código</th>
			    <th data-dynatable-column="url">URL</th>
			    <th data-dynatable-column="isPublished">Publicada</th>
			    <th data-dynatable-column="hasDraft">Tem rascunho</th>
			    <th data-dynatable-column="averageRating">Média</th>
			    <th data-dynatable-column="admins">Admins</th>
			  </thead>
			  <tbody>
			  </tbody>
			</table>
		</div>
		<script>

		</script>	
	</body>
</html>