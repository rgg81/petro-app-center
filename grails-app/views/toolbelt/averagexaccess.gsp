<!DOCTYPE html>
<%@page import="org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="angular"/>
		<r:require module="angular-sanitize"/>
		<r:require module="homeControllerAngular" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="bootstrapAngularUI" />
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
		<r:require module="dragAndDropGrid" />
		<r:require module="hoverIntent" />
		<r:require module="highcharts" />
		<r:require module="toolbeltController" />
		<r:require module="dynatableCss" />
		<r:require module="dynatableJs" />
		

	</head>
	<body>
		<g:applyLayout name="tools-menu"/>
		<div ng-app="toolbeltApp" ng-controller="ratingXAccessCtrl">
			<div class="main-row row" role="main">
				<div class="panel-heading-edit petro-box-title-edit">
					<h3 class="pull-left petro-box-title-edit">Aplicações: Acessos x Avaliações<span class="petro-toolbelt-title-loading" id="toolbelt-title-loading"></span></h3>				
				</div>
				
				
			</div>
			<div class="row" style="padding: 10px">
				<div id="scatter-chart"></div>				
			</div>
			<div class="row text-center">
				<button ng-click="loadHomeInfo(7)" class="btn btn-default petro-review-report-loading petro-home-buttons">Últimos 7 dias</button>
				<button ng-click="loadHomeInfo(30)" class="btn btn-default petro-review-report-loading petro-home-buttons" >Últimos 30 dias</button>
				<button ng-click="loadHomeInfo()" class="btn btn-default petro-review-report-loading petro-home-buttons">Tudo</button>
			</div><br>
		</div>
	</body>
</html>