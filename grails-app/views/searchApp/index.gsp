<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="br.com.petrobras.appcenter.Icon"%>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require module="bootstrapAngularUI" />
		<r:require module="searchAppController" />

		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>
	<body>
		<div ng-app="searchApp">
		<div class="row main-row petro-search-result">
			<div class="col-md-12 panel petro-panel-box petro-disable-col-padding">
	 			<div class="panel-heading petro-box-title">
	 				<h3 class="pull-left petro-box-title">Resultado da Busca: <span class="petro-search-result-title">${params.q}</span></h3>	 
	 			</div>
	 			<div class="row petro-disable-row-margin">
					<g:if test="${result?.searchResults?.size > 0}">
						<g:each in="${result?.searchResults}" status="i" var="app">
							<div class="petro-search-result-item">
								<div class="col-xs-10 col-sm-9 col-md-10">
									<img class="petro-icon-image-wrapper" src="/${Icon.relativePathByApplication(app)}" alt="">
									<div class="petro-search-result-item-detail">
										<g:link controller="application" action="show" id="${app.id}" title='Ver detalhes'>
									        <h4>${app.name}</h4>
									        <p class="hidden-xs">
								        		${app.description}
									       	</p>
								       	</g:link>
								       	
								       	<rate:rating active="false" bean='${app}' id="fixo"/>
								       	
								       	<g:if test="${app.tags}">

									       	<p class="petro-tags hidden-xs">
									       		<b>Tags: </b>
									       		<g:each in="${app.tags}"  var="tag">
									       			<g:link controller="searchApp" action="index" params="[q: tag.key]">
									       				<span class="petro-tag-btn">${tag.name}</span>
									       			</g:link>
									       		</g:each>
									       	</p>
									    </g:if>
							       	
									</div>
								</div>
								<span class="hidden-xs col-sm-3 col-md-2">
						    		<a  class="btn petro-btn" href="${app?.link?.url}" target="_blank" appId="${app.id}" where="searchresult" register-access>
										<g:if test="${app.isDownloadable}">
											Baixar
											<span class="glyphicon glyphicon-download-alt petro-btn-accessory"></span>
										</g:if>
										<g:else>
											<g:message code="user.application.button.access.label" default="Acessar" />
											<span class="glyphicon glyphicon-play petro-btn-accessory"></span>
										</g:else>

									</a>
								</span>

								<div class="visible-xs petro-app-search-mobile-launch col-xs-2 petro-disable-col-padding">
									<a  class="" href="${app?.link?.url}" target="_blank" appId="${app.id}" where="searchresult" register-access>
									<span class="glyphicon glyphicon-circle-arrow-right"></span>
									</a>								
								</div>
							</div>
						</g:each>


		 				<div class='petro-you-search'>
		 					<p class="petro-you-search">
								Não achou a aplicação que estava procurando? Faça uma nova busca ou <a data-toggle="modal" href="#application-not-found">Fale conosco</a>. 
			 				</p>
			 			</div>

			 			
					</g:if>
 					<g:else>


		 				<p class="petro-you-search">
							<span class="petro-without-result"><g:message code="application.search.noneresult" default="Nenhuma aplicação encontrada com o termo"/></span>. 
							<br/>Não achou a aplicação que estava procurando? Faça uma nova busca ou <a data-toggle="modal" href="#application-not-found">Fale conosco</a>.
						</p>
						<div class="col-md-12 col-offset-1 petro-you-search">
		 					<label>Sugestões:</label>
		 					<ul class="list-unstyled">
			 					<li>- Certifique-se de que todas as palavras estejam escritas corretamente.</li>
			 					<li>- Tente palavras-chave diferentes.</li>
		 					</ul>
	 					</div>

					</g:else>

					<g:if test="${result?.total > 10 && result.searchResults.size > 0}">
						<div class="outer-center">
							<div class="inner-center petro_pagination">
						        <g:paginate controller="searchApp" action="index" params="[q: params.q]" 
						                    total="${result.total}" next="\u003Cspan class=\'glyphicon glyphicon-chevron-right'\u003E\u003C/span\u003E" prev="\u003Cspan class=\'glyphicon glyphicon-chevron-left'\u003E\u003C/span\u003E"/>
							</div>
						</div>
					</g:if>
				</div>
						
			</div>
		</div>
		<div >
			<g:render template="../angular-templates/ljapModalApplicationNotFoundNotice"/>
			<g:render template="../angular-templates/ljapModalAlertSuccessfullySubmittedApplicationNotFoundNotice"/>
		</div>
		</div>
	</body>
</html>