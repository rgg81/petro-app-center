<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="layout" content="main"/>

		<r:require module="angular-sanitize"/>
		<r:require module="bootstrapAngularUI" />
		<r:require module="showHideAnimatedAngular" />
		<r:require module="mainModuleAngular" />
		<r:require module="searchAdminControllerAngular" />

		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>
	<body>
		<div id="main" role="main" class="row" ng-app="searchAdminApp">
			<div class="panel petro-panel-box access-url">
				<div class="panel-heading-access petro-box-title-url">
					<h3 class="pull-left petro-box-title-url">Re-indexando as classes</h3>
				</div>
				<div class="panel-body petro-panel-body">
					<div class="main-row" ng-controller="searchAdminCtrl">
						<p style="background-color: yellow;">{{message}}</p>
						<div class="table-responsive">
							<table class="table table-striped petro-table-striped">
								<thead>
									<th class="col-md-3">index</th>
									<th class="col-md-2">docsCount (stats)</th>
									<th class="col-md-1"></th>
									<th class="col-md-1"></th>
									<th class="col-md-1"></th>
									<th class="col-md-1">exists</th>
									<th class="col-md-3">Classes</th>
								</thead>
								<tbody>
									<tr ng-repeat="(index, value) in indexes">
										<td>{{index}}</td>
										<td>{{value.docsCount}}</td>
										<td><button ng-disabled="!value.exists" ng-click="deleteIndex(index)">delete</button></td>
										<td><button ng-disabled="value.exists" ng-click="installMappings(index)">create</button></td>
										<td><button ng-disabled="!value.exists" ng-click="indexDocs(index, value.classes)">index</button></td>
										<td>{{value.exists}}</td>
										<td><ul><li ng-repeat="c in value.classes">{{c.count}}: {{c.clazz}}</li></ul></td>
										<!--
										<td>{{value.classes.join(separator=", ")}}</td>
										-->
									</tr>
								</tbody>
							</table>
							<!--
							incluir snapshot com data, restauração
							?
							-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>