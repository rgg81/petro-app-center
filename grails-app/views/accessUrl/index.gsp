<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="layout" content="main"/>
		<title><g:message code="default.title" default="Aplicações Petrobras"/></title>
	</head>
	<body>
	  <div id="main" role="main" class="row">
	  		<div class="panel petro-panel-box access-url">
	  			<div class="panel-heading-access petro-box-title-url">
	  				<h3 class="pull-left petro-box-title-url">Sem Acesso</h3>
	  			</div>
	  			<div class="panel-body petro-panel-body">
	  				<div class="main-row">
		  				<div class="col-md-12 petro-access-url">

		  					<div class="main-row row">
		  						<div class="col-md-12"> 
		  							<div class="row petro-acess-url-content">			
				  						<div class="col-sm-4 col-md-4 petro-icon-image-broken-url"><img src="images/icon-broken-url.png" class="img-responsive"></div>
				  						<div class="col-sm-8 col-md-8">
				  						  <h3>A aplicação que você deseja acessar não possui URL.</h3>
				  						</div>
		  							</div>

				  					<div class="row petro-acess-url-close-window">
				  						<a href="javascript:void(0)" onclick="window.close();" class="btn btn-success">Fechar janela</a>
				  					</div>
				  				</div>
		  					</div>

	  					</div>
	  				</div>
	  			</div>
	  		</div>
	  </div>
	</body>
</html>