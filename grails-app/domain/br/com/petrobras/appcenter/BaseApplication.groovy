package br.com.petrobras.appcenter

import grails.util.GrailsNameUtils;

import static br.com.petrobras.appcenter.Comment.*


class BaseApplication implements Serializable {

	
	Date dateCreated
	Date lastUpdated
	
	String name
    String description
    String department
    String catalogCode
	Set<AdminApplication> adminsApp
	Set<Compatibility> appCompatib
	String[] tagsBusca
	Boolean isDownloadable = false

    static constraints = {
    	name blank:false
    	description blank:false
		link nullable:false
    	icon nullable:false
    	department  nullable: true		
		catalogCode nullable: false, validator: { val, obj ->
            if(obj.id) {
				def ids = []
				def liveApp, draftApp
				
				if (obj instanceof ApplicationDraft) {
					draftApp = obj
					liveApp = obj.application
				}
				else {
					liveApp = obj
					draftApp = ApplicationDraft.findByCatalogCode(val)
				}
				
				if(liveApp) ids += liveApp.id
				if(draftApp) ids += draftApp.id

				def baseApps = BaseApplication.withCriteria(uniqueResult: true) {
					eq("catalogCode", val)
					not {'in'("id", ids)}
				}
				if (baseApps) ['unique']
			}	       
        } 
    }
	
    static hasOne = [link:Link,icon:Icon]
	static hasMany = [userApplications:UserApplication, tags:Tag, comments: Comment, informationsSupport:InformationSupport, versions:Version]
	
	static mapping = {
		informationsSupport sort:'description', order:"asc"
		description type: "text"
		tags cascade: "all-delete-orphan"
		tags sort:'name', order:"asc"
		informationsSupport cascade: "all-delete-orphan"
		tablePerHierarchy false
		versions sort:'date', order:'desc', cascade:'all-delete-orphan'
	}
	
	static transients = ['adminsApp', 'appCompatib','tagsBusca']
	
	Set<AdminApplication> getAdminsApp() {
		
		if (adminsApp == null)
			return new HashSet<AdminApplication>()
		else
			return adminsApp
		
	}

	String[] getTagsBusca() {
		tagsBusca = tags*.name
		return tagsBusca
	}
	
	Set<Compatibility> getCompatibilities() {
		if (this?.id != null)
			ApplicationCompatibilities.findAllByApplication(this).collect { appComp ->
				appComp.compatibility
			}.sort{it.description}
		else
			new HashSet<Compatibility>()
	}

	def getRatingScore()  {
		def results = Comment.executeQuery(
		"""
			select  max(comment.rating) as s, count(*) as c
			from Comment comment
			where comment.application.id = ?
				group by comment.rating order by s asc
		"""
		, [this.id])

		def score = [1:0,2:0,3:0,4:0,5:0]
 		for(def item in results) {
			score[(int)item[0]] = item[1]
		}
		return score
	}

	def addToTagsLowerCase(tagNames){
		// Remove tags duplicadas e ignora case
		def lowercaseUniqueTags = tagNames*.toLowerCase().unique()
		
		// Adiciona as tags
		lowercaseUniqueTags?.each { tagName -> 
			this.addToTags(name: tagName)
		}
	}

	def addToInformationsSupports(lista){

		lista?.each { information ->
			this.addToInformationsSupport(information)
		}
	}
	
	def addCompatibilities(lista){
		
		lista?.each { information ->
			this.addToCompatibilities(information)
		}
	}
	

	def getComment(User user) {
 		for(def comment in comments) {
			if (comment.user.key == user.key)
				return comment
		}
	}
	
	

	def getMostLikedReviews(Integer limit){

		def results = executeQuery(
		"""
			select  c,sum(eval.value) as sv
			from Comment as c join c.evaluations eval
			where c.application.id = ? 
			and c.text != null
			group by c.id, c.version, c.application, c.dateCreated, c.lastUpdated, c.text, c.title, c.user, c.rating 
			having sum(eval.value) > 0
 			order by sv desc
				
		"""
		, [this.id] , [max: limit])

		return results.collect{it[0]}
	}
	
	def beforeInsert() {
		catalogCode = catalogCode?.toUpperCase()
	}
	
	def beforeUpdate() {
		catalogCode = catalogCode?.toUpperCase()
	}

    def getAverageRating() {
		return Comment.createCriteria().get {
			projections { avg 'rating' }
			eq "application", this
			cache true
		}
    }

	def getTotalRatings() {
		this.comments.size()
	}

}