package br.com.petrobras.appcenter


class Icon {

	def grailsApplication
	
    int width
    int height
    long size
	
	static belongsTo = [application:BaseApplication] 

	static searchable = {
		root false
		only = ['width','height','size']
	}

	
	static mapping = {
		size column: "imgSize"	
	}
	
	static constraints = {
		size min: 1L
	}

	def relativePath() {
    	return "icons/icon_${application.id}.jpg"
    }
	
	static relativePathByApplication(Application app) {
		return "icons/icon_${app.id}.jpg?${app.icon.size}"
	}

    def sysAbsolutePath(){
    	return "${grailsApplication.config.imageDir}/icons/icon_${application.id}.jpg"
    }

}
