package br.com.petrobras.appcenter

class ApplicationNotFoundNotice {

	String description
    String url	
	Date dateCreated

	static belongsTo = [user: User]

    static constraints = {
    	user nullable: false
    	url nullable: true
	    description blank: false
    }

	static mapping = {
	}
}
