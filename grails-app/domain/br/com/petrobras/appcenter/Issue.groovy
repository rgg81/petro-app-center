package br.com.petrobras.appcenter

class Issue {

    String description
    
	int position

    static constraints = {
    	position unique: true
    	description unique: true
    	description blank: false
    }

}
