package br.com.petrobras.appcenter

class Comment {

	String title
	String text
	Date dateCreated, lastUpdated
	Integer rating

	// like / dislike
	static hasMany = [evaluations: Like]

	static hasOne = [user:User, application:BaseApplication, reply: Reply]
	static belongsTo = [User, BaseApplication]

    static constraints = {
    	title nullable: true, blank: true, validator: { val, obj -> 
			return (!obj.text?.length() || val?.length()) ?: 'blank'
		}
        text nullable: true, maxSize: 1000,  validator: { val, obj ->
			return (!obj.title?.length() || val?.length()) ?: 'blank'
		}
		reply nullable:true

    	application(unique: ['user'])

    	rating validator: { val -> 
    		return val in (1I..5I)
    	}
    }

    static mapping = { 
    	table 'pcomment'
		evaluations cascade: "all-delete-orphan"
    }

    def getLikes() {
		return evaluations.findAll{ e -> e.value == LikeEnum.Like.value }
    }

    def getDislikes() {
		return evaluations.findAll{ e -> e.value == LikeEnum.Dislike.value }
    }

    def shouldSendMailNewReview() {
    	return (title?.size() > 0 && text?.size() > 0)
    }

}