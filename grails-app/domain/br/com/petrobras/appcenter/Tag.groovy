package br.com.petrobras.appcenter

class Tag implements Serializable {
	
	String name
	
	static belongsTo = [application:BaseApplication] 
	
	static mapping = {
		version: false
	}

	static searchable = {
		root true
		only = ['name']
	}
	
	String toString() {
		this.name;
	}
		
}
