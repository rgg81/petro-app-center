package br.com.petrobras.appcenter

class InformationSupport {

	String url
	String description
	static belongsTo = [application:BaseApplication] 
	
    static constraints = {
		url blank:false,nullable:false
		description blank: false, nullable:false
    }
}
