package br.com.petrobras.appcenter

class AdminApplication implements Serializable {

	User admin
	BaseApplication application
	
    static constraints = {
		admin nullable: false
		application nullable: false
		admin unique: 'application'
    }
	
	static mapping = {
		version: false
	}
	
	
}