package br.com.petrobras.appcenter

import java.io.Serializable;
import java.util.Set;


class Compatibility implements Serializable{

	String nameImage
	String description

	Set<ApplicationCompatibilities> appCompatib
	
	static constraints = {
		nameImage blank: false, unique: true, nullable: false
		description blank: false, nullable:false
    }
	
	static transients = ['appCompatib']
}
