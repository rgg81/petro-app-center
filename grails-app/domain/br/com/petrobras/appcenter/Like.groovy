package br.com.petrobras.appcenter

public class Like {

	Integer value

	static hasOne = [user:User, comment: Comment]
	static belongsTo = [User, Comment]

    static constraints = {
    	user(unique: ['comment'])
    	value inList: [LikeEnum.Like.value,LikeEnum.Dislike.value]
    }

    static mapping = { 
    	table 'plike'
    }

}
