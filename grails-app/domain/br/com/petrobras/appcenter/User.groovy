package br.com.petrobras.appcenter
import java.util.Set;

import groovy.lang.MissingPropertyException

class User implements Serializable {

	// TODO: Avaliar o uso de category com mixin para name, shortName e department
	String key, name, shortName,tracking
	Boolean sharesMyApplications = false
	def department
	def accessControlService
	private def userFromAccessControl
	Set<AdminApplication> adminsteredApp

    static constraints = {
		key blank:false, unique: true
		tracking nullable: true
    }
	
	static hasMany = [myApplications:UserApplication, comments: Comment, evaluations: Like, replies: Reply]

	static mapping = {
		table 'userdomain'
		myApplications sort:'position'
		key column: "usrKey"
		sharesMyApplications defaultValue: false
	}
	
	static transients=['accessControlService', 'userFromAccessControl', 'name', 'shortName', 'department', 'adminsteredApp']
	
	void setUserFromAccessControl(user) {
		userFromAccessControl = user
	}

	private def getUserFromAccessControl() {
		if (!userFromAccessControl) {
			userFromAccessControl = accessControlService.findUser(key)
		}
		return userFromAccessControl
	}

	String getShortName() {
		capitalize(getUserFromAccessControl()?.shortName)
	}
	
	Set<AdminApplication> getAdminsteredApp() {
		if (adminsteredApp == null) {
			adminsteredApp = new HashSet<AdminApplication>()
		}
		return adminsteredApp
		
	}

	String getName() {
		log.debug "name: ${name}\n"
		//capitalize(getUserFromAccessControl()?.name)
		if (!name) {
			name = capitalize(getUserFromAccessControl()?.name)
		}

		log.debug "name: ${name}\n"
		return name
	}

	def getDepartment() {
		def user = getUserFromAccessControl()

		try {
			return user.department			

		// Necessário para os usuáros de teste, são br.com.petrobras.security.model.ExternalUser
		} catch(MissingPropertyException e) {			
			return [acronym: "TRANSPETRO/DTM/TM/OP1/GETRAM4/SBHOLANDA"];

		} catch(Exception e) {
			log.info "key: ${key}"
			log.error e.message, e
			return null;
		}
	}

	private capitalize(string) {
		string.split().collect { it.trim().toLowerCase().capitalize() }.join(" ")
	}

}