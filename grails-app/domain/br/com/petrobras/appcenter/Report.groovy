package br.com.petrobras.appcenter

class Report {

	String description
	Date dateCreated

	static hasMany = [issues: Issue]

	static belongsTo = [user: User, application: Application]

    static constraints = {
    	application nullable: false
    	user nullable: false
	    description nullable: true, validator: {val, obj ->  if (!val && !obj.issues) return 'report.must.have.description.or.issue'}
	    issues nullable: true, validator: {val, obj ->  if (!val && !obj.description) return 'report.must.have.description.or.issue'}
    }

	static mapping = {
		table 'reportdomain'
	}

}
