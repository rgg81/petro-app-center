package br.com.petrobras.appcenter

class UserActivity {

	final static ADD_APPLICATION = "addapp"
	final static REMOVE_APPLICATION = "removeapp"
    final static ACCESS_APPLICATION = "accessapp"
    final static RATING_APPLICATION = "ratingapp"

    final static MYAPPS_WHERE = "myapps"
    final static POPAPPS_WHERE = "popularapps"
    final static SHOWAPP_WHERE = "showapp"
    final static SEARCH_RESULT_WHERE = "searchresult"
    final static POPB_WHERE = "popbapps"

    static constraints = { 
    }

    static mapping = {
        version false
        autoTimestamp true
        isReliable defaultValue: true
    }

    static searchable = {
        userId store: 'yes' , index : 'not_analyzed'
        userkey  store: 'yes', index : 'not_analyzed'
        itemId store: 'yes', index : 'not_analyzed'
        itemName store: 'yes', index : 'not_analyzed'
        type  store: 'yes', index : 'not_analyzed'
        context store: 'yes', index : 'not_analyzed'
        ratingValue store: 'yes', index : 'not_analyzed'
        dateCreated store: 'yes', index : 'not_analyzed'
        isReliable store: 'yes', index : 'not_analyzed'
        indexName 'useractivity'
    }


    Long userId
    String userkey 
    Long itemId
    String itemName
    String type 
    String context
    Integer ratingValue
    boolean isReliable
    Date dateCreated

    /* usuario uq4n avaliou app 21 com valor 5 aplicacao 21, através do minhas aplicações
     {user: "uq4n", activity : "rating" , application: "21",where: "myapps", ratingValue: 5} 
     usuario uq4n acessou a app 21, atraves do aplicacoes mais populares
     {user: "uq4n", activity : "accessapp" , application: "21",where: "popularapps" } */
}
