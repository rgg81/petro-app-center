package br.com.petrobras.appcenter

public enum LikeEnum {

	Like(1), Dislike(-1)

	private final Integer value

	LikeEnum(Integer value) {
		this.value = value
	}

	Integer getValue() {
		value
	}

	String getId() { value } 

}