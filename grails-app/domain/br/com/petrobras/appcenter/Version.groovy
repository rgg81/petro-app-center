package br.com.petrobras.appcenter

class Version {

	String number
	String description
	Date date
	
	static belongsTo = [application:BaseApplication] 
	
	static constraints = {
		number(blank: false)
		description nullable: true, maxSize: 4000
		date blank: false
    }

    static mapping = {
		date column: "last_updated"
		number column: "pnumber"
	}
		
}
