package br.com.petrobras.appcenter

class Reply {

	String text
	Date dateCreated, lastUpdated
	Comment comment

	static hasOne = [user:User]
	static belongsTo = [User]

    static constraints = {
        text maxSize: 1000,  nullable: false, blank: false
    }
}
