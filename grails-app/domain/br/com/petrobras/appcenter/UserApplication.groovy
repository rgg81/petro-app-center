package br.com.petrobras.appcenter

class UserApplication implements Serializable {
	int position

	static hasOne = [user:User, application:BaseApplication]
	static belongsTo = [User]
	
    static constraints = {
		user nullable: false
		application nullable: false
		position unique: ['user']
    }
	
	static mapping = {
		id composite: ['user', 'application']
		version: false
	}
	
	@Override
	public boolean equals(Object obj) {
		obj instanceof UserApplication && 
		obj.user == this.user
		obj.application == this.application
	}
	
	@Override
	public int hashCode() {
		int hash = 31
		if ( user ) hash ^= user.hashCode()
		if ( application ) hash ^= application.hashCode() 
		return hash
	}
}
