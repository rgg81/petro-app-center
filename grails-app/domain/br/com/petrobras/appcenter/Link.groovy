package br.com.petrobras.appcenter

class Link {
	
    String url
	
	static belongsTo = [application:BaseApplication] 

	static searchable = {
		root true
		only = ['url']
	}
	
	static constraints = {
		url blank:false
	}

    String toString() {
    	this.url;
    }

}
