package br.com.petrobras.appcenter

import grails.util.GrailsNameUtils;


import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

class Application extends BaseApplication {


	static searchable = {
		only = ['name', 'description', 'link', 'icon','tags','tagsBusca', 'department','isDownloadable']
		completion = [	name: 'app_suggestion', 
						mapping: [
								index_analyzer:'app-analyzer',
								search_analyzer:'app-analyzer',
								payloads: true
						],
						indexing: [
								input:{ instance -> instance.name},
								output:{ instance -> instance.name},
								payload:{ instance -> ['appid':instance.id,'iconPath':Icon.relativePathByApplication(instance)]}
						]
					 ]
					 	
		name store: 'yes', boost:3.0 
		description store: 'yes'
		department store: 'yes'
		tagsBusca store: 'yes'
		link component:true
		tags component:true
		icon component:true

		
	}

}

/**
 * Essa classe foi criada devido a um erro que ocorre na deserialização do json de application devido a herança com base application.
 * O erro é apresentado devido as duas classes terem o atributo errors, foi-se necessário remover este atributo pois não era utilizado na deserialização
 * @author UPN1
 *
 */
class ApplicationExclusionEstrategy implements ExclusionStrategy {
	
	public boolean shouldSkipField(FieldAttributes arg0) {
		arg0.name == "errors" || arg0.name == "application"
	}



	@Override
	public boolean shouldSkipClass(Class<?> arg0) {
		return false;
	}

}