package br.com.petrobras.appcenter

class ApplicationCompatibilities {

    Compatibility compatibility
	BaseApplication application
	
    static constraints = {
		compatibility nullable: false
		application nullable: false
		compatibility unique: 'application'
    }
	
	static mapping = {
		version: false
	}
}
