databaseChangeLog = {

	changeSet(author: "UPN1 (generated)", id: "1389290847081-1") {
		createTable(tableName: "application_draft") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "application_dPK")
			}

			column(name: "application_id", type: "number(19,0)")
		}
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-2") {
		createTable(tableName: "base_application") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "base_applicatPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "catalog_code", type: "varchar2(255 char)")

			column(name: "description", type: "clob") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-7") {
		dropForeignKeyConstraint(baseTableName: "ADMIN_APPLICATION", baseTableSchemaName: "LJAP", constraintName: "FK4798E860BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-8") {
		dropForeignKeyConstraint(baseTableName: "APPLICATION_COMPATIBILITIES", baseTableSchemaName: "LJAP", constraintName: "FK8BA2B51FBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-9") {
		dropForeignKeyConstraint(baseTableName: "ICON", baseTableSchemaName: "LJAP", constraintName: "FK313C79BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-10") {
		dropForeignKeyConstraint(baseTableName: "INFORMATION_SUPPORT", baseTableSchemaName: "LJAP", constraintName: "FK9FD6AFBCBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-11") {
		dropForeignKeyConstraint(baseTableName: "LINK", baseTableSchemaName: "LJAP", constraintName: "FK32AFFABDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-12") {
		dropForeignKeyConstraint(baseTableName: "PCOMMENT", baseTableSchemaName: "LJAP", constraintName: "FKAB298FEFBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-13") {
		dropForeignKeyConstraint(baseTableName: "TAG", baseTableSchemaName: "LJAP", constraintName: "FK1BF9ABDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-14") {
		dropForeignKeyConstraint(baseTableName: "USER_APPLICATION", baseTableSchemaName: "LJAP", constraintName: "FKFFA4F6DCBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-15") {
		dropForeignKeyConstraint(baseTableName: "VERSION", baseTableSchemaName: "LJAP", constraintName: "FK14F51CD8BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-26") {
		dropColumn(columnName: "CATALOG_CODE", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-27") {
		dropColumn(columnName: "DESCRIPTION", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-28") {
		dropColumn(columnName: "NAME", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-29") {
		dropColumn(columnName: "VERSION", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-16") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "admin_application", constraintName: "FK4798E86051F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-17") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "application_compatibilities", constraintName: "FK8BA2B51F51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-18") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "application_draft", constraintName: "FK91A5F2BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-19") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "icon", constraintName: "FK313C7951F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-20") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "information_support", constraintName: "FK9FD6AFBC51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-21") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "link", constraintName: "FK32AFFA51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-22") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "pcomment", constraintName: "FKAB298FEF51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-23") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "tag", constraintName: "FK1BF9A51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-24") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "user_application", constraintName: "FKFFA4F6DC51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-25") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "version", constraintName: "FK14F51CD851F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
}
