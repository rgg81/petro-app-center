databaseChangeLog = {

	changeSet(author: "flvs (generated)", id: "1381155913281-1") {
		createTable(tableName: "application") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "applicationPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-2") {
		createTable(tableName: "application_compatibilities") {
			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "compatibility_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-3") {
		createTable(tableName: "compatibility") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "compatibilityPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "name_image", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-4") {
		createTable(tableName: "icon") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "iconPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "height", type: "number(10,0)") {
				constraints(nullable: "false")
			}

			column(name: "imgSize", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "width", type: "number(10,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-5") {
		createTable(tableName: "information_support") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "information_sPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-6") {
		createTable(tableName: "link") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "linkPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-7") {
		createTable(tableName: "pcomment") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "pcommentPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "date") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "date") {
				constraints(nullable: "false")
			}

			column(name: "text", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-8") {
		createTable(tableName: "plike") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "plikePK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "comment_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "value", type: "number(10,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-9") {
		createTable(tableName: "rating") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "ratingPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "date") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "date") {
				constraints(nullable: "false")
			}

			column(name: "rater_class", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "rater_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "stars", type: "double precision") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-10") {
		createTable(tableName: "rating_link") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "rating_linkPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "rating_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "rating_ref", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "type", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-11") {
		createTable(tableName: "tag") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "tagPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "normalized_name", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-12") {
		createTable(tableName: "test_domain") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "test_domainPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-13") {
		createTable(tableName: "test_rater") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "test_raterPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-14") {
		createTable(tableName: "user_application") {
			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "position", type: "number(10,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-15") {
		createTable(tableName: "userdomain") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "userdomainPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "usrKey", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-16") {
		createTable(tableName: "version") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "versionPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "date") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255)") {
				constraints(nullable: "false")
			}

			column(name: "pnumber", type: "varchar2(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-17") {
		addPrimaryKey(columnNames: "application_id, compatibility_id", tableName: "application_compatibilities")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-18") {
		addPrimaryKey(columnNames: "user_id, application_id", constraintName: "user_applicatPK", tableName: "user_application")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-33") {
		createIndex(indexName: "name_image_uniq_1", tableName: "compatibility", unique: "true") {
			column(name: "name_image")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-34") {
		createIndex(indexName: "application_id_uniq_1", tableName: "icon", unique: "true") {
			column(name: "application_id")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-35") {
		createIndex(indexName: "url_uniq_1", tableName: "information_support", unique: "true") {
			column(name: "url")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-36") {
		createIndex(indexName: "application_id_uniq_2", tableName: "link", unique: "true") {
			column(name: "application_id")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-37") {
		createIndex(indexName: "unique_application_id", tableName: "pcomment", unique: "true") {
			column(name: "user_id")

			column(name: "application_id")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-38") {
		createIndex(indexName: "unique_user_id", tableName: "plike", unique: "true") {
			column(name: "comment_id")

			column(name: "user_id")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-39") {
		createIndex(indexName: "unique_position", tableName: "user_application", unique: "true") {
			column(name: "user_id")

			column(name: "position")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-40") {
		createIndex(indexName: "application_id_uniq_3", tableName: "version", unique: "true") {
			column(name: "application_id")
		}
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-19") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "application_compatibilities", constraintName: "FK8BA2B51FBDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-20") {
		addForeignKeyConstraint(baseColumnNames: "compatibility_id", baseTableName: "application_compatibilities", constraintName: "FK8BA2B51FD4F72947", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "compatibility", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-21") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "icon", constraintName: "FK313C79BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-22") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "information_support", constraintName: "FK9FD6AFBCBDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-23") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "link", constraintName: "FK32AFFABDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-24") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "pcomment", constraintName: "FKAB298FEFBDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-25") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "pcomment", constraintName: "FKAB298FEFB86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-26") {
		addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "plike", constraintName: "FK65CF80780033CA7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "pcomment", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-27") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "plike", constraintName: "FK65CF807B86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-28") {
		addForeignKeyConstraint(baseColumnNames: "rating_id", baseTableName: "rating_link", constraintName: "FK1827315C45884E64", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "rating", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-29") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "tag", constraintName: "FK1BF9ABDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-30") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "user_application", constraintName: "FKFFA4F6DCBDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-31") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_application", constraintName: "FKFFA4F6DCB86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "1381155913281-32") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "version", constraintName: "FK14F51CD8BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "flvs (generated)", id: "hibernate-sequence") {
        sql("CREATE SEQUENCE  HIBERNATE_SEQUENCE")
    }
	
	changeSet(author: "flvs (generated)", id: "grants-1") {
	    sql("""
		grant all on APPLICATION to ljap_aplicacao;
		grant all on APPLICATION_COMPATIBILITIES to ljap_aplicacao;
		grant all on ICON to ljap_aplicacao;
		grant all on LINK to ljap_aplicacao;
		grant all on RATING to ljap_aplicacao;
		grant all on RATING_LINK to ljap_aplicacao;
		grant all on TEST_DOMAIN to ljap_aplicacao;
		grant all on TEST_RATER to ljap_aplicacao;
		grant all on USER_APPLICATION to ljap_aplicacao;
		grant all on USERDOMAIN to ljap_aplicacao;
		grant all on TAG to ljap_aplicacao;
		grant all on PCOMMENT to ljap_aplicacao;
		grant all on PLIKE to ljap_aplicacao;
		grant all on APPLICATION_COMPATIBILITIES to ljap_aplicacao;
		grant all on COMPATIBILITY to ljap_aplicacao;
		grant all on INFORMATION_SUPPORT to ljap_aplicacao;
		grant all on VERSION to ljap_aplicacao;
	  	grant all on HIBERNATE_SEQUENCE to ljap_aplicacao;
	  	""")
	}

	changeSet(author: "y1r4", id: "Comment.text.size.increase.to.1000.chars") {
		sql("""
		ALTER TABLE PCOMMENT MODIFY (TEXT VARCHAR2(1000)); 
		delete from databasechangelog where id = '1381268110635-2';
		""")
	}

	changeSet(author: "up2h", id: "Change.description.column.type.from.varchar.to.clob") {
		sql("""
		ALTER TABLE APPLICATION RENAME COLUMN DESCRIPTION to DESCRIPTION_OLD
		""")
		sql("""
		ALTER TABLE APPLICATION ADD (DESCRIPTION CLOB)
		""")
		sql("""
		UPDATE APPLICATION SET DESCRIPTION=DESCRIPTION_OLD
		""")
		sql("""
		ALTER TABLE APPLICATION DROP COLUMN DESCRIPTION_OLD 
		""")
	}
	
	changeSet(author: "uq4e", id: "Comment.titleAndText.canbe.null") {
		sql("""
		ALTER TABLE PCOMMENT  
		MODIFY (TEXT NULL);
		
		ALTER TABLE PCOMMENT  
		MODIFY (TITLE NULL);
		""")
	}

	changeSet(author: "y1r4", id: "Tag.drop.field.normalizedName") {
		sql("""
		ALTER TABLE TAG DROP COLUMN NORMALIZED_NAME 
		""")
	}
	
	changeSet(author: "y1qx", id: "Add.column.catalogcode") {
		sql("""
		ALTER TABLE APPLICATION ADD (CATALOG_CODE VARCHAR2(4))
		""")
	}

	changeSet(author: "y1r4 (generated)", id: "1386616262554-1") {
		createTable(tableName: "admin_application") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "admin_applicaPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "admin_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "y1r4 (generated)", id: "1386616262554-9") {
		dropIndex(indexName: "URL_UNIQ_1", tableName: "INFORMATION_SUPPORT")
	}

	changeSet(author: "y1r4 (generated)", id: "1386616262554-10") {
		createIndex(indexName: "unique_admin_id", tableName: "admin_application", unique: "true") {
			column(name: "application_id")

			column(name: "admin_id")
		}
	}

	changeSet(author: "y1r4 (generated)", id: "1386616262554-7") {
		addForeignKeyConstraint(baseColumnNames: "admin_id", baseTableName: "admin_application", constraintName: "FK4798E860876B93A9", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "y1r4 (generated)", id: "1386616262554-8") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "admin_application", constraintName: "FK4798E860BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "y1r4", id: "grants-synonym-admin-application") {
	    sql("""
		grant all on ADMIN_APPLICATION to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."ADMIN_APPLICATION" FOR "LJAP"."ADMIN_APPLICATION";
	  	""")
	}

	changeSet(author: "y1r4", id: "grants-synonym-databasechangelog") {
	    sql("""
		grant all on DATABASECHANGELOG to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."DATABASECHANGELOG" FOR "LJAP"."DATABASECHANGELOG";
	  	""")
	}

	changeSet(author: "y1r4", id: "atualizacao.application.compatibilities") {
		sql("""
		alter table application_compatibilities drop PRIMARY KEY;
		delete from application_compatibilities;
		alter table application_compatibilities add (id number(19,0) NOT NULL PRIMARY KEY, version number(19,0) not null);
		alter table application_compatibilities add constraint app_compatibilities_unique unique (application_id, compatibility_id);
		""")
	}
	
	changeSet(author: "UPN1 (generated)", id: "1389290847081-1") {
		createTable(tableName: "application_draft") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "application_dPK")
			}

			column(name: "application_id", type: "number(19,0)")
		}
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-2") {
		createTable(tableName: "base_application") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "base_applicatPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "catalog_code", type: "varchar2(255 char)")

			column(name: "description", type: "clob") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-7") {
		dropForeignKeyConstraint(baseTableName: "ADMIN_APPLICATION", baseTableSchemaName: "LJAP", constraintName: "FK4798E860BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-8") {
		dropForeignKeyConstraint(baseTableName: "APPLICATION_COMPATIBILITIES", baseTableSchemaName: "LJAP", constraintName: "FK8BA2B51FBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-9") {
		dropForeignKeyConstraint(baseTableName: "ICON", baseTableSchemaName: "LJAP", constraintName: "FK313C79BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-10") {
		dropForeignKeyConstraint(baseTableName: "INFORMATION_SUPPORT", baseTableSchemaName: "LJAP", constraintName: "FK9FD6AFBCBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-11") {
		dropForeignKeyConstraint(baseTableName: "LINK", baseTableSchemaName: "LJAP", constraintName: "FK32AFFABDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-12") {
		dropForeignKeyConstraint(baseTableName: "PCOMMENT", baseTableSchemaName: "LJAP", constraintName: "FKAB298FEFBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-13") {
		dropForeignKeyConstraint(baseTableName: "TAG", baseTableSchemaName: "LJAP", constraintName: "FK1BF9ABDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-14") {
		dropForeignKeyConstraint(baseTableName: "USER_APPLICATION", baseTableSchemaName: "LJAP", constraintName: "FKFFA4F6DCBDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-15") {
		dropForeignKeyConstraint(baseTableName: "VERSION", baseTableSchemaName: "LJAP", constraintName: "FK14F51CD8BDC89C87")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-26") {
		dropColumn(columnName: "CATALOG_CODE", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-27") {
		dropColumn(columnName: "DESCRIPTION", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-28") {
		dropColumn(columnName: "NAME", tableName: "APPLICATION")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-29") {
		dropColumn(columnName: "VERSION", tableName: "APPLICATION")
	}
	
	changeSet(author: "upn1", id: "delete-admin_application") {
		sql("""
		delete from admin_application;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-16") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "admin_application", constraintName: "FK4798E86051F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-application_compatibilities") {
		sql("""
		delete from application_compatibilities;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-17") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "application_compatibilities", constraintName: "FK8BA2B51F51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-18") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "application_draft", constraintName: "FK91A5F2BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-icon") {
		sql("""
		delete from icon;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-19") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "icon", constraintName: "FK313C7951F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-information_support") {
		sql("""
		delete from information_support;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-20") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "information_support", constraintName: "FK9FD6AFBC51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-link") {
		sql("""
		delete from link;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-21") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "link", constraintName: "FK32AFFA51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-plike") {
		sql("""
		delete from plike;
	  	""")
	}
	
	changeSet(author: "upn1", id: "delete-pcomment") {
		sql("""
		delete from pcomment;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-22") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "pcomment", constraintName: "FKAB298FEF51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}

	changeSet(author: "upn1", id: "delete-tag") {
		sql("""
		delete from tag;
	  	""")
	}
	changeSet(author: "UPN1 (generated)", id: "1389290847081-23") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "tag", constraintName: "FK1BF9A51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	
	changeSet(author: "upn1", id: "delete-user_application") {
		sql("""
		delete from user_application;
	  	""")
	}

	changeSet(author: "UPN1 (generated)", id: "1389290847081-24") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "user_application", constraintName: "FKFFA4F6DC51F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-version") {
		sql("""
		delete from version;
	  	""")
	}


	changeSet(author: "UPN1 (generated)", id: "1389290847081-25") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "version", constraintName: "FK14F51CD851F5276", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "base_application", referencesUniqueColumn: "false")
	}
	
	changeSet(author: "upn1", id: "delete-application") {
		sql("""
		delete from application;
	  	""")
	}
	
	changeSet(author: "upn1", id: "delete-ratinglink") {
		sql("""
		delete from rating_link;
	  	""")
	}
	
	changeSet(author: "upn1", id: "delete-rating") {
		sql("""
		delete from rating;
	  	""")
	}
	
	changeSet(author: "upn1", id: "change-version-description-column-size") {
		sql("""
		Alter table version  
		MODIFY description varchar2(4000);
	  	""")
	}
	
	changeSet(author: "upn1", id: "grants-synonym-baseapplication") {
		sql("""
		grant all on BASE_APPLICATION to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."BASE_APPLICATION" FOR "LJAP"."BASE_APPLICATION";
	  	""")
	}
	
	changeSet(author: "upn1", id: "grants-synonym-applicationdraft") {
		sql("""
		grant all on APPLICATION_DRAFT to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."APPLICATION_DRAFT" FOR "LJAP"."APPLICATION_DRAFT";
	  	""")
	}
	
	
	changeSet(author: "UPN1 (generated)", id: "create-column-date-created-in-application") {
		addColumn(tableName: "base_application") {
			column(name: "date_created", type: "timestamp") 
		}
	}
	
	changeSet(author: "UPN1 (generated)", id: "addnotnull-date-created-in-application") {
		addNotNullConstraint(tableName: "base_application", columnDataType: "timestamp", columnName: "date_created")
	
	}

	changeSet(author: "UPN1 (generated)", id: "create-column-last-updated-in-application") {
		addColumn(tableName: "base_application") {
			column(name: "last_updated", type: "timestamp") 
		}
	}
	
	changeSet(author: "UPN1 (generated)", id: "addnotnull-last-updated-in-application") {
		addNotNullConstraint(tableName: "base_application", columnDataType: "timestamp", columnName: "last_updated")
	
	}
	
	changeSet(author: "UPN1 (generated)", id: "addnotnull-catalogcode-in-application") {
		addNotNullConstraint(tableName: "base_application", columnDataType: "varchar2(255 char)", columnName: "catalog_code")
	
	}
	
	
	changeSet(author: "upn1", id: "create-all-synonym") {
		sql("""
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."ADMIN_APPLICATION" FOR "LJAP"."ADMIN_APPLICATION";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."APPLICATION" FOR "LJAP"."APPLICATION";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."APPLICATION_COMPATIBILITIES" FOR "LJAP"."APPLICATION_COMPATIBILITIES";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."APPLICATION_DRAFT" FOR "LJAP"."APPLICATION_DRAFT";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."BASE_APPLICATION" FOR "LJAP"."BASE_APPLICATION";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."COMPATIBILITY" FOR "LJAP"."COMPATIBILITY";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."DATABASECHANGELOG" FOR "LJAP"."DATABASECHANGELOG";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."DATABASECHANGELOGLOCK" FOR "LJAP"."DATABASECHANGELOGLOCK";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."ICON" FOR "LJAP"."ICON";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."INFORMATION_SUPPORT" FOR "LJAP"."INFORMATION_SUPPORT";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."LINK" FOR "LJAP"."LINK";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."PCOMMENT" FOR "LJAP"."PCOMMENT";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."PLIKE" FOR "LJAP"."PLIKE";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."RATING" FOR "LJAP"."RATING";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."RATING_LINK" FOR "LJAP"."RATING_LINK";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."TAG" FOR "LJAP"."TAG";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."TEST_DOMAIN" FOR "LJAP"."TEST_DOMAIN";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."TEST_RATER" FOR "LJAP"."TEST_RATER";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."USER_APPLICATION" FOR "LJAP"."USER_APPLICATION";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."USERDOMAIN" FOR "LJAP"."USERDOMAIN";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."VERSION" FOR "LJAP"."VERSION";
	  	""")
	}

	changeSet(author: "y1r4", id: "create-synonym-for-hibernate-sequence") {
		sql("""
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."HIBERNATE_SEQUENCE" FOR "LJAP"."HIBERNATE_SEQUENCE";
	  	""")
	}

	changeSet(author: "y1r4", id: "information-support-url-unrestricted-size") {
		sql("""
			alter table information_support rename column url to url_old;
			alter table information_support add url clob null;
			update information_support set url = url_old;
			alter table information_support modify url not null;
			alter table information_support drop column url_old;
	  	""")
	}

	changeSet(author: "Y1R4 (generated)", id: "1402516834314-1") {
		createTable(tableName: "issue") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "issuePK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-1") {
		createTable(tableName: "REPORTDOMAIN") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "reportdomainPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "application_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "varchar2(255 char)")

			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-2") {
		createTable(tableName: "reportdomain_issue") {
			column(name: "report_issues_id", type: "number(19,0)")

			column(name: "issue_id", type: "number(19,0)")
		}
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-8") {
		addForeignKeyConstraint(baseColumnNames: "application_id", baseTableName: "reportdomain", constraintName: "FK40B072F8BDC89C87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "application", referencesUniqueColumn: "false")
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-9") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "reportdomain", constraintName: "FK40B072F8B86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-10") {
		addForeignKeyConstraint(baseColumnNames: "issue_id", baseTableName: "reportdomain_issue", constraintName: "FK6A3664D28DB0E1A7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "issue", referencesUniqueColumn: "false")
	}

	changeSet(author: "Y1R4 (generated)", id: "1402519103769-11") {
		addForeignKeyConstraint(baseColumnNames: "report_issues_id", baseTableName: "reportdomain_issue", constraintName: "FK6A3664D2E56487BC", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "reportdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "y1r4", id: "create-report-issue-synonym") {
		sql("""
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."ISSUE" FOR "LJAP"."ISSUE";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."REPORTDOMAIN" FOR "LJAP"."REPORTDOMAIN";
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."REPORTDOMAIN_ISSUE" FOR "LJAP"."REPORTDOMAIN_ISSUE";
	  	""")
	}

	changeSet(author: "y1r4", id: "grant-report-issue-synonym") {
		sql("""
		grant all on ISSUE to LJAP_APLICACAO;
		grant all on REPORTDOMAIN to LJAP_APLICACAO;
		grant all on REPORTDOMAIN_ISSUE to LJAP_APLICACAO;
	  	""")
	}

	changeSet(author: "Y1R4 (generated)", id: "1403102367706-1") {
		addColumn(tableName: "reportdomain") {
			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "y1r4", id: "limitless-report-description") {
		sql("""
			alter table reportdomain rename column description to description_old;
			alter table reportdomain add description clob null;
			update reportdomain set description = description_old;
			alter table reportdomain drop column description_old;
	  	""")
	}

	changeSet(author: "y1r4", id: "issues-positioning") {
		sql("""
			ALTER TABLE issue ADD position NUMBER(10,0) NULL;
			UPDATE issue SET position = ID*100;
			alter table issue modify position not null;
	  	""")
	}

	changeSet(author: "Y1R4 (generated)", id: "1404742844318-7") {
		createIndex(indexName: "description_uniq_1404742793314", tableName: "issue", unique: "true") {
			column(name: "description")
		}
	}

	changeSet(author: "Y1R4 (generated)", id: "1404742844318-8") {
		createIndex(indexName: "position_uniq_1404742793314", tableName: "issue", unique: "true") {
			column(name: "position")
		}
	}

	changeSet(author: "UP2H", id: "userdomain-share-my-applications") {
		sql("""
			ALTER TABLE userdomain ADD shares_my_applications NUMBER(1,0) DEFAULT 0 NOT NULL;
	  	""")
	}

	changeSet(author: "leandro (generated)", id: "1408558730828-1") {
		createTable(tableName: "application_not_found_notice") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "application_nPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "clob") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "varchar2(255 char)") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "leandro (generated)", id: "1408558730828-8") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "application_not_found_notice", constraintName: "FKE62CBB30B86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "y1r4", id: "create-application_not_found_notice-synonym") {
		sql("""
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."APPLICATION_NOT_FOUND_NOTICE" FOR "LJAP"."APPLICATION_NOT_FOUND_NOTICE";
	  	""")
	}

	changeSet(author: "y1r4", id: "grant-application_not_found_notice-synonym") {
		sql("""
		grant all on APPLICATION_NOT_FOUND_NOTICE to LJAP_APLICACAO;
	  	""")
	}

	changeSet(author: "leandro (generated)", id: "1408631396252-3") {
		dropNotNullConstraint(columnDataType: "varchar2(255 char)", columnName: "URL", tableName: "APPLICATION_NOT_FOUND_NOTICE")
	}
	
	changeSet(author: "upn1", id: "change-column-type-version-description-to-clob") {
		sql("""
		ALTER TABLE version ADD tmp_name CLOB;
		UPDATE version SET tmp_name=description;
		ALTER TABLE version DROP COLUMN description;
		ALTER TABLE version RENAME COLUMN tmp_name to description;
	  	""")
	}

	changeSet(author: "up2h (generated)", id: "1416592007327-1") {
		createTable(tableName: "reply") {
			column(name: "id", type: "number(19,0)") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "replyPK")
			}

			column(name: "version", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "comment_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "timestamp") {
				constraints(nullable: "false")
			}

			column(name: "text", type: "clob") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "number(19,0)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "up2h (generated)", id: "1416592007327-15") {
		createIndex(indexName: "comment_id_uniq_1416592002254", tableName: "reply", unique: "true") {
			column(name: "comment_id")
		}
	}

	changeSet(author: "up2h (generated)", id: "1416592007327-12") {
		addForeignKeyConstraint(baseColumnNames: "comment_id", baseTableName: "reply", constraintName: "FK67612EA80033CA7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "pcomment", referencesUniqueColumn: "false")
	}

	changeSet(author: "up2h (generated)", id: "1416592007327-13") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "reply", constraintName: "FK67612EAB86A09AD", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "userdomain", referencesUniqueColumn: "false")
	}

	changeSet(author: "up2h", id: "grants-synonym-reply") {
	    sql("""
		grant all on REPLY to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."REPLY" FOR "LJAP"."REPLY";
	  	""")
	}


	changeSet(author: "y1r4", id: "zero.or.n.versions.per.base_application") {
		sql("""
		DROP INDEX APPLICATION_ID_UNIQ_3;
		""")
	}
	

	changeSet(author: "uq4n", id: "user-activity-database-create") {
		sql("""
		CREATE TABLE USER_ACTIVITY (id NUMBER(19,0) NOT NULL, 
			context VARCHAR2(255 char) NOT NULL, 
			date_created TIMESTAMP NOT NULL, 
			item_id NUMBER(19,0) NOT NULL, 
			item_name VARCHAR2(255 char) NOT NULL, 
			rating_value NUMBER(10,0) NOT NULL, type VARCHAR2(255 char) NOT NULL, 
			user_id NUMBER(19,0) NOT NULL, 
			userkey VARCHAR2(255 char) NOT NULL, 
		CONSTRAINT user_activityPK PRIMARY KEY (id));
		""")
	}

	changeSet(author: "uq4n", id: "grants-synonym-user-activity") {
	    sql("""
		grant all on USER_ACTIVITY to ljap_aplicacao;
		CREATE OR REPLACE SYNONYM "LJAP_APLICACAO"."USER_ACTIVITY" FOR "LJAP"."USER_ACTIVITY";
	  	""")
	}

	
    changeSet(author: "uq4n", id: "user_activity-is-reliable") {
            sql("ALTER TABLE user_activity ADD is_reliable NUMBER(1,0) DEFAULT 1 NOT NULL")
    }

    changeSet(author: "uq4n", id: "userdomain-tracking") {
            addColumn(tableName: "userdomain") {
                    column(name: "tracking", type: "varchar2(255 char)")
            }
    }
        changeSet(author: "uq4n", id: "1423070767176-1") {
                addColumn(tableName: "base_application") {
                        column(name: "department", type: "varchar2(255 char)")
                }
        }

	changeSet(author: "roberto (generated)", id: "1434561970159-1") {
		sql("""
		ALTER TABLE base_application ADD (is_downloadable NUMBER(1,0) default 0 NOT NULL);
	  	""")
	}

	changeSet(author: "leandro (generated)", id: "1441143558483-1") {
		addColumn(tableName: "pcomment") {
			column(name: "rating", type: "number(10,0)", defaultValue: 1) {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "y1r4", id: "ratings-migration") {
		sql("""
			update pcomment c set rating = 0; 

			update pcomment c set rating = (select r.stars from rating_link rl, rating r where rl.rating_id = r.id and rl.rating_ref = c.application_id and r.rater_id = c.user_id);
		""")
	}

}

