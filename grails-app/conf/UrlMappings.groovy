class UrlMappings {
// http://grails.org/doc/2.2.4/guide/theWebLayer.html#urlmappings
	static mappings = {

		"/listuserapps/$key"(controller:"application", action: "listUserApplications")
		"/user/$action/$key"(controller:"user")

		//"mostLiked","recent","like","dislike","my"
	    "/review/$action"(controller:"review") {
			constraints {
				action(inList:["mostLiked","recent","byRating","like","dislike","my","all","metadata","list","report","groupedByDay"])
			}
	    }

	    // delete, update,insert
	    "/review/$id?"(controller:"review") {
	    	action = [DELETE:"delete",PUT:"update",POST:"insert"]
	    	constraints {
			}
	    }

	    // delete, update,insert
	    "/reply/$id?"(controller:"reply") {
	    	action = [DELETE:"delete",PUT:"update",POST:"insert"]
	    	constraints {
			}
	    }
		
		"/url/$action/$field?"(controller:"url") {
			constraints {
			}
		}

		"/$lang/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

	    "/mail/newReview"(view: "/mail/newReview")
	    "/mail/deleteReview"(view: "/mail/deleteReview")
	    "/mail/newReport"(view: "/mail/newReport")
	    "/mail/mailShell"(view: "/mail/mailShell")
		"/mail/mailShellEvaluation"(view: "/mail/mailShellEvaluation")

		"/index.gsp" (controller:"home")
		"/" (controller:"home")
		"500"(view:'/error')
	}
}