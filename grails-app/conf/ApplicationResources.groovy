modules = {

	angular {
		resource url:'/js/angular-1.2.11.js'
	}

	"angular-sanitize" {
		dependsOn "angular"
		resource url:'/js/angular-sanitize-1.2.11.js'
	}

    application {
        resource url:'js/application.js'
    }
	reviewControllerAngular {
		dependsOn "angular"
		resource url:'/js/ReviewsCtrl.js'
	}
	applicationControllerAngular {
		dependsOn "angular"
		resource url:'/js/ApplicationsCtrl-1.0.js'
	}
	showHideAnimatedAngular {
		dependsOn "angular"
		resource url:'/js/ShowHideModule.js'
	}
	dragAndDropGrid {
		resource url:'/js/jquery.gridly.js'
		resource id:'css', url:'/css/jquery.gridly.css'
	}
	mainModuleAngular {
		dependsOn "angular"
		resource url:'/js/MainModule.js'
	}
	homeControllerAngular {
		dependsOn "mainModuleAngular"
		resource url:'/js/HomeCtrl.js'
	}

	angularSwitch {
		dependsOn "mainModuleAngular"
		resource url:'/js/angular-ui-switch.min.js'
		resource url: '/css/angular-ui-switch.css'
	}

	bootstrapAngularUI {
		dependsOn "angular"
		resource url:'/js/ui-bootstrap-tpls-0.11.0.min.js'
	}
	bootstrapDatePicker {
		resource url:'/js/bootstrap-datepicker.js'
		resource url:'/js/locales/bootstrap-datepicker.pt-BR.js'
	}

	/*angularStrapPopover {
		dependsOn "angular"
		resource url:'/js/angular-strap-popover.js'
	}*/

	hoverIntent {
		resource url:'/js/jquery.hoverIntent.js'
	}
	
    overrides {
        fileuploader {
            resource id: 'css', url:'/css/styles-1.0.css'
        }
    } 	
	reportControllerAngular {
		dependsOn "angular"
		resource url:'/js/ReportCtrl.js'
	}

	highcharts {
		resource url:'/js/highcharts.js'
		resource url:'/js/data.js'
		resource url:'/js/report-jquery-chart.js'
		
	}

	bootstrapRangeDatePicker {
		resource url:'/js/moment.js'
		resource url:'/js/daterangepicker.js'
		resource url:'/css/daterangepicker-bs3.css'
	}


	reviewReportController {
		dependsOn "angular-sanitize"
		dependsOn "mainModuleAngular"
		resource url:'/js/ReviewsReportCtrl.js'
	}

	toolbeltController {
		dependsOn "angular-sanitize"
		dependsOn "mainModuleAngular"
		resource url:'/js/ToolbeltCtrl.js'
	}

	dynatableJs {
		resource url: '/js/jquery.dynatable.js'
	}

	dynatableCss {
		resource url: '/css/jquery.dynatable.css'
	}

	searchAppController {
		dependsOn "mainModuleAngular"
		resource url:'/js/SearchAppCtrl.js'
	}

	applicationNotFoundNoticeController {
		dependsOn "mainModuleAngular"
		resource url:'/js/ApplicationNotFoundNoticeCtrl.js'
	}

	searchAdminControllerAngular {
		dependsOn "angular"
		resource url:'/js/SearchAdminCtrl.js'
	}

}