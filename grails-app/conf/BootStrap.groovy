import grails.converters.*
import br.com.petrobras.appcenter.*
import org.springframework.web.context.support.WebApplicationContextUtils
import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder
import org.codehaus.groovy.grails.orm.hibernate.cfg.Mapping

class BootStrap {
	def activityService
//	def searchableService
	//def customObjectMarshallers

    def init = { servletContext ->
    	try {
			log.debug("initializing petro-app-center")
			insertCompatibilities()
			insertIssues()
	
	    	environments {
	    		mobile {
	
	    		}
				development {
					populateModel()
					//new User(key:'y1r4').save(flush: true, failOnError:true)
				}
				developmentMysql {
					populateModel()
				}
			}
	
			// Manually start the mirroring process to ensure that it comes after the automated migrations.
//		    searchableService.reindex()
//		    searchableService.startMirroring()
	
			//customObjectMarshallers.register()
			new ApplicationMarshaller().register()
			new UserMarshaller().register()
			Locale.setDefault(new Locale("pt", "BR"))
    	}
		catch (Exception e) {
			log.error("error initializing app", e)
			throw e
		}
    }

    def destroy = {
    }
	
	def insertCompatibilities(){
		if (Compatibility.count() > 0) return

		new Compatibility(id: 1, nameImage:"compatibility_ie.png", description: "Internet Explorer").save(flush:true, failOnError: true)
		new Compatibility(id: 2, nameImage:"compatibility_firefox.png", description: "Mozilla Firefox").save(flush:true, failOnError: true)
		new Compatibility(id: 3, nameImage:"compatibility_chrome.png", description: "Google Chrome").save(flush:true, failOnError: true)
		new Compatibility(id: 4, nameImage:"compatibility_safari.png", description: "Safari").save(flush:true, failOnError: true)
		new Compatibility(id: 5, nameImage:"compatibility_notes.png", description: "Lotus Notes").save(flush:true, failOnError: true)
		new Compatibility(id: 6, nameImage:"compatibility_iphone-web.png", description: "iOS").save(flush:true, failOnError: true)
		new Compatibility(id: 7, nameImage:"compatibility_android-web.png", description: "Android").save(flush:true, failOnError: true)
	}

	def insertIssues(){
		if (Issue.count() > 0) return

		new Issue(description:"Aplicação não conhecida por esse título", position: 100).save(flush:true, failOnError: true)
		new Issue(description:"Endereço da aplicação errado", position: 200).save(flush:true, failOnError: true)
		new Issue(description:"Descrição da aplicação está errada", position: 300).save(flush:true, failOnError: true)
		new Issue(description:"Funcionalidades importantes não estão na descrição", position: 400).save(flush:true, failOnError: true)
		new Issue(description:"Tag inapropriada", position: 500).save(flush:true, failOnError: true)
		new Issue(description:"Descrição da versão está errada", position: 600).save(flush:true, failOnError: true)
		new Issue(description:"Navegador não suportado", position: 700).save(flush:true, failOnError: true)
		new Issue(description:"Endereço da informação de suporte errado", position: 800).save(flush:true, failOnError: true)
		new Issue(description:"Aplicação desativada", position: 900).save(flush:true, failOnError: true)
		new Issue(description:"Erros ortográficos", position: 1000).save(flush:true, failOnError: true)

	}

	private Boolean loadAllData() {
		System.getProperty("loadAllData")
     }

    def populateModel(){
		if (!loadAllData()) return
				
    	/* BEGIN: MOCKING USERS AND APPLICATIONS */
    	def user1 = new User(key:'UQ4E').save(flush: true, failOnError:true)
		def user2 = new User(key:'UP2H').save(flush: true, failOnError:true)
		def user3 = new User(key:'UQ4N').save(flush: true, failOnError:true)
		def user4 = new User(key:'UPN1').save(flush: true, failOnError:true)
		def user5 = new User(key:'AB9W').save(flush: true, failOnError:true)
		def user6 = new User(key:'Y2AQ').save(flush: true, failOnError:true)
		def user7 = new User(key:'Y1R4').save(flush: true, failOnError:true)
		def user8 = new User(key:'HACM').save(flush: true, failOnError:true)
		def user9 = new User(key:'UPMQ').save(flush: true, failOnError:true)
		def user10 = new User(key:'Y19X').save(flush: true, failOnError:true)
		def user11 = new User(key:'UP2Y').save(flush: true, failOnError:true)
		def user12 = new User(key:'NTLH').save(flush: true, failOnError:true)
		def user13 = new User(key:'Y775').save(flush: true, failOnError:true)
		def user14 = new User(key:'AC3F').save(flush: true, failOnError:true)
		def user15 = new User(key:'Y1B7').save(flush: true, failOnError:true)
		def user16 = new User(key:'UQ4M').save(flush: true, failOnError:true)
		def user17 = new User(key:'Y1QX').save(flush: true, failOnError:true)

		def users = [user1,user2,user3,user4,user5, user6, user7, user8, user9, user10, user11, user12, user13, user14, user15, user16, user17]
		
		def departments =['TIC','TIC/CPSW/IST-II/SP','TIC/CPSW','TIC/TIC-CORP/TIC-JCAA','TIC/TIC-E&P/SNEP']

		def apps = []
		for(i in 1..60){
			
			def ver = new Version(number:"${i}",description:"descricao da versao 1.0",date:new Date())
			
			def app = new Application(name:"Petrobras Aplicação ${i}. Avalie, comente e favorite as aplicações mais utilizadas por você, desse modo você estará dando feedback e ajudando o ",	
			 	description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
			 	link:new Link(url:"http://www.google.com.br/#q=${i}"),
			 	icon:new Icon(size:1), catalogCode: UUID.randomUUID().toString().substring(0,4))			
			
			['medico', 'medicos', 'efetivacao', 'efetivar'].each { value ->
				app.addToTags(new Tag(name:value))
			}

			app.department = departments[i % 5]

			app.addToVersions(ver)

			def savedApp = app.save(flush:true, failOnError:true)	
			//Sets compatibility for the first 6 applications
			if(i <=5){
				Compatibility.list().subList(0,i).each { comp ->
					def appComp = new ApplicationCompatibilities(application: app, compatibility:comp).save(flush:true)
				}
			}

			for (int j = 0; j < (i%5); j++) {
				new AdminApplication(admin: User.get((i-j)%17+1), application: savedApp).save(flush:true)
			}

			apps.add(savedApp)			
		}		
		
		//InformationSupport
		apps.eachWithIndex { Application application, i ->
			for (j in 1..5) {
				def information = new InformationSupport(url:"http://google.com/${j}/${i}", application:application, description:"Informacao Suporte ${i} ${j}")
				information.save(flush:true, failOnError:true)
			}
		}
		
		Random rnd = new Random()
										
		apps[0,3,6,9].eachWithIndex { Application app, i ->
			users[0..14].each{ User user ->
				app.addToComments(title: "Titulo Comentario", text: 'Texto Comentario' * (5 + rnd.nextInt(40)), date: new Date(), user: user, rating: 5-i);
				activityService.registerActivity(user, app, "accessapp","myapps", true)
				app.save(flush:true);
			}
		}

		apps[1,4,7,10].eachWithIndex { Application app, i ->
			users[0..2].each{ User user ->
				app.addToComments(title: "Titulo Comentario", text: 'Texto Comentario' * (5 + rnd.nextInt(40)), date: new Date(), user: user, rating: 5-i);
				activityService.registerActivity(user, app, "accessapp","myapps", true)
				app.save(flush:true);						
			}
		}

		apps[2,5,8,11].eachWithIndex { Application app, i ->
			users[0].each{ User user ->
				app.addToComments(title: "Titulo Comentario", text: 'Texto Comentario' * (5 + rnd.nextInt(40)), date: new Date(), user: user, rating: 5-i);
				activityService.registerActivity(user, app, "accessapp","myapps", true)
				app.save(flush:true);
			}
		}
		
		users[0,1].eachWithIndex { User user, i ->
			apps[12].addToComments(title: "Titulo Comentario", text: 'Texto Comentario' * (5 + rnd.nextInt(40)), user: user, rating: 5-i);
			apps[12].save(flush:true);
		}
		
		users.eachWithIndex{User user, j ->
			if (user.key != "UP2H") {
				apps.eachWithIndex { Application app, i ->
					new UserApplication( user: user, application: app, position: i).save()
				}
				apps.remove(apps.first())
			}
			else {
				log.debug ("bootstrap: ta aparecendo alguma coisa igual a minha chave")
			}
		}

		Comment.list().each { Comment comment -> 
			users[0..2].each{ User user ->		
				new Like(user: user, value: LikeEnum.Like.value, comment: comment).save(flush:true)				
			}
		
			users[3..6].each{ User user ->		
				new Like(user: user, value: LikeEnum.Dislike.value, comment: comment).save(flush:true)
			}
		}
			//new Reply(user: users[0], text: "Replica texto texto texto", comment: comment).save(flush:true)
			
		
		def verDraft = new Version(number:"2",description:"descricao da versao 1.0",date:new Date())
		def appDraft = new ApplicationDraft(name:"Draft App",
			description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum",
			link:new Link(url:"http://www.google.com.br/#q=222222222"),
			icon:new Icon(size:1), catalogCode: UUID.randomUUID().toString().substring(0,4))
		
		appDraft.addToVersions(verDraft)
		
		appDraft.save(flush:true)


		//assert UserApplication.count() == users.size() * apps.size()

		/* END: MOCKING USERS AND APPLICATIONS */ 
    }

}
