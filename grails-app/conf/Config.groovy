// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

appCode = "ljap"
app.role.moderator = "Moderator"
app.role.watcher = "Watcher"
app.role.adminapp = "AdminApp"
app.role.admin = "Admin"
app.role.everyone = "Everyone"

grails.config.locations = ["classpath:${appCode}/config.properties"]

grails.project.groupId = "br.com.petrobras.appcenter" // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*','/fonts/*']
grails.resources.adhoc.includes = [
    '/images/**', '/css/**', '/js/**', '/plugins/**','/fonts/**'
]

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

grails.plugins.springsecurity.providerNames = ['preAuthenticatedAuthenticationProvider', 'anonymousAuthenticationProvider']

grails.mail.default.from="Aplicações Petrobras <noreply@petrobras.com.br>"

environments {
    development {
        grails.logging.jul.usebridge = true
		grails.mail.disabled=true
    }
	test {
		grails.mail.disabled=true
	}
    production {
        grails.logging.jul.usebridge = false
		grails.mail.jndiName = "mailer"
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

grails.gorm.failOnError=true

grails.gorm.default.mapping = {
    autoTimestamp true //or false based on your need
}

imageUpload {
    temporaryFile = {"/tmp/temp.file"} // Path to where files will be uploaded
 }

// https://github.com/robfletcher/grails-gson#readme
grails.converters.gson.domain.include.class = true
//grails.converters.gson.serializeNulls = true
grails.converters.gson.datePattern = "dd/MM/yyyy"
//grails.plugin.databasemigration.changelogFileName='desenvolvimento-diff.groovy'

log4j = {
    
	appenders {
		'null' name: "stacktrace"
	}
}

//elasticsearch plugin
elasticSearch{
    datastoreImpl = 'hibernateDatastore'
    bootstrap.config.file = 'classpath:${appCode}/elasticsearch.json'
    disableAutoIndex = false
    client.mode = 'local'
    bulkIndexOnStartup = false
    path.data = System.getenv('ELASTICSEARCH_PATH') ?: System.getProperty('ELASTICSEARCH_PATH') 
}


environments {
    development{
        elasticSearch{
            client.transport.sniff = true
            bulkIndexOnStartup = true    
        }
    }
    test {
        elasticSearch {
            client.transport.sniff = true
            index.store.type = 'memory'
            path.data = "./data"
        }
  
    }  
    production {
        elasticSearch{
            client.mode = 'dataNode'
        }
    }
}



if(elasticSearch.path.data){
    print "Using ELASTICSEARCH_PATH : ${elasticSearch.path.data}\n"
}

tomcat.scan.enabled=true


