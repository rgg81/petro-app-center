dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    //cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory'
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
    flush.mode = 'manual'
}


// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
			logSql = true
        }
    }

    developmentMysql {
        dataSource {
			dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
            dbCreate = "" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:mysql://localhost:3306/petro-app-center"
            username = "root"
            password = ""
            logSql = true
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
        }
    }

    test {
        dataSource {
            dbCreate = "create-drop"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
			logSql = true
        }
    }
    mobile {
        dataSource {
            dbCreate = ""
            jndiName = "java:comp/env/jdbc/petro-app-center"
        }
    }
    production {
        dataSource {
            dbCreate = ""
            jndiName = "ljapDS"
        }
    }

	desenvolvimento {
		dataSource {
			dbCreate = ""
			username = "ljap"
			password = "lgs1_yvsd13"
			driverClassName = "oracle.jdbc.driver.OracleDriver"
			dialect = "org.hibernate.dialect.Oracle10gDialect"
			url = "jdbc:oracle:thin:@//nestor.petrobras.com.br:1521/prtlcrpd"
			logSql = true
		}
	}

    desenvolvimentoapp {
        dataSource {
            dbCreate = ""
            username = "ljap_aplicacao"
            password = "lpes1_yvsd13"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//nestor.petrobras.com.br:1521/prtlcrpd"
            logSql = true
        }
    }

    teste {
        dataSource {
            dbCreate = ""
            username = "ljap"
            password = "jsIesay_01"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//glaucius.petrobras.com.br:1521/prtlcrpt"
            logSql = true
        }
    }

    testeapp {
        dataSource {
            dbCreate = ""
            username = "ljap_aplicacao"
            password = "wqpslGt_35"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//glaucius.petrobras.com.br:1521/prtlcrpt"
            logSql = true
        }
    }

    homologacao {
        dataSource {
            dbCreate = ""
            username = "ljap"
            password = "jsIesay_01"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrpha.petrobras.com.br:1521/prtlcrph.petrobras.com.br"
            logSql = true
        }
    }

    homologacaoapp {
        dataSource {
            dbCreate = ""
            username = "ljap_aplicacao"
            password = "wqpslGt_35"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrpha.petrobras.com.br:1521/prtlcrph.petrobras.com.br"
            logSql = true
        }
    }

    producao {
        dataSource {
            dbCreate = ""
            username = "ljap"
            password = "jsIesay_01"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrppa.petrobras.com.br:1521/prtlcrpp.petrobras.com.br"
            logSql = true
        }
    }

    producaoapp {
        dataSource {
            dbCreate = ""
            username = "ljap_aplicacao"
            password = "wqpslGt_35"
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = "org.hibernate.dialect.Oracle10gDialect"
            url = "jdbc:oracle:thin:@//vipprtlcrppa.petrobras.com.br:1521/prtlcrpp.petrobras.com.br"
            logSql = true
        }
    }

}
