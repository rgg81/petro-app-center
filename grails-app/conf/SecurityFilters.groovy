// http://grails.org/doc/2.2.4/guide/theWebLayer.html#filters
class SecurityFilters {
    def filters = {
        authenticated(controller: 'application|heartBeat|activity', action: 'listUserApplications|isAlive|proxy', invert: 'true') { 
            before = {
                if (controllerName == 'application' && (actionName == 'widget' || actionName == 'rating')) return true
                if (!controllerName) return true

                // Access control by convention.
                try {
                    accessControl()
                } catch (e) {
                    log.error("Erro ao processar verificacao de seguranca", e)
                    return false
                }
            }
        }

    }
}