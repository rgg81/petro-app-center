
class NoCompatibilityModeFilters {
    def filters = {
		all(controller: '*', action: '*') {
			after = {
               	response.setHeader('X-UA-Compatible', 'IE=EmulateIE8')
            }
		}
    }
}