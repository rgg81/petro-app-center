/*grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "target/${appName}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "debug" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()

        mavenRepo "http://nexus.petrobras.com.br/nexus/content/groups/public/"
        mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/releases/"
        mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/snapshots/"
        mavenRepo "https://oss.sonatype.org/content/groups/public/"

        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.

        //runtime 'mysql:mysql-connector-java:5.1.26'

        //------ dependencias do CAv4

        compile('br.com.petrobras.security:security-web:4.6.0.1') {
            // O Grails usa a versao 3.0 do Spring, por isto precisa desta exclusao
            excludes 'spring-beans', 'spring-context', 'spring-core', 'spring-web', 'spring-aop',
                    'aspectjweaver', 'log4j'
        }

        compile 'net.sf.opencsv:opencsv:2.0'

        compile 'br.com.petrobras.security:security-shiro:4.3.2'
		
		compile 'org.apache.commons:commons-lang3:3.3.2'
		
		compile ('org.apache.shiro:shiro-core:1.2.2',
			'org.apache.shiro:shiro-web:1.2.2',
			'org.apache.shiro:shiro-spring:1.2.2',
			'org.apache.shiro:shiro-ehcache:1.2.2',
			'org.apache.shiro:shiro-quartz:1.2.2') {
			excludes 'ejb', 'jsf-api', 'servlet-api', 'jsp-api', 'jstl', 'jms',
			   'connector-api', 'ehcache-core', 'slf4j-api', 'commons-logging'
			}
        //------ fim das dependencias do CAv4
        compile 'io.searchbox:jest:0.1.3'   
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        runtime ":resources:1.2.1"
        runtime ':angularjs-resources:1.2.11'

        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.5"

        build ":tomcat:$grailsVersion"

        compile ":database-migration:1.3.6"

        compile ':cache:1.0.1'
		compile ":yui:2.8.2"
		compile ":executor:0.3"

		compile ":ajax-uploader:1.1"
        test ":plastic-criteria:1.0"
        compile ":cxf-client:1.5.7"
        // https://github.com/robfletcher/grails-gson#readme
        compile ":gson:1.1.4"
		compile ":mail:1.0.4"
        compile ":include:0.3"
		compile ":cache-headers:1.1.6"
		runtime ":cached-resources:1.0"
        compile ':elasticsearch:0.0.3.6.br.1.0.0'     

		build ':release:2.2.1', ':rest-client-builder:1.0.3', {
			export = false
		}
    }
}*/
grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.project.war.file = "target/${appName}.war"

grails.project.fork = [
    // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
    //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

    // configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    //run: false,
    // configure settings for the run-war JVM
    war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
    // configure settings for the Console UI JVM
    console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

	try {
		repositories {
			inherits true // Whether to inherit repository definitions from plugins
	
			grailsPlugins()
			grailsHome()
			mavenLocal()
			def envVariables = new ConfigSlurper().parse(new File("${userHome}/ljap-env.groovy").toURI().toURL())
			def proxyconfig = {
				proxy 'inet-sys.gnet.petrobras.com.br', 8080, auth([username: "${envVariables.petrobras.proxy.user}", password:"${envVariables.petrobras.proxy.password}"])
			}
	
			grailsCentral proxyconfig
			mavenCentral proxyconfig
	
			mavenRepo "http://nexus.petrobras.com.br/nexus/content/groups/public/"
			mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/releases/"
			mavenRepo "http://nexus.petrobras.com.br/nexus/content/repositories/snapshots/"
	
			mavenRepo ("https://oss.sonatype.org/content/groups/public/") {
				 proxyconfig
			}
			
		}
		
	} catch (Exception e) {
		println "Verificar se as configuracoes do proxy foram realizadas com sucesso na criação do ambiente (grails config-project). Veja o arquivo ljap-env.groovy no seu diretorio home."
		throw e;
	}

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        // runtime 'mysql:mysql-connector-java:5.1.29'
        // runtime 'org.postgresql:postgresql:9.3-1101-jdbc41'
        test "org.grails:grails-datastore-test-support:1.0.2-grails-2.4"

        def versionSecurity = '4.6.0.1'
        compile("br.com.petrobras.security:security-core:${versionSecurity}",
                "br.com.petrobras.security:security-command:${versionSecurity}",
                "br.com.petrobras.security:security-extension-web:${versionSecurity}",
                "br.com.petrobras.security:security-consume-api:${versionSecurity}",
                "br.com.petrobras.security:security-context-api:${versionSecurity}",
                "br.com.petrobras.security:security-consume-soap:${versionSecurity}",
                "br.com.petrobras.security:security-context-spring:${versionSecurity}",
                "br.com.petrobras.security:security-utility:${versionSecurity}",
                "br.com.petrobras.security:security-configuration:${versionSecurity}",
                "br.com.petrobras.security:security-annotation-aspectj:${versionSecurity}",
                "br.com.petrobras.security:security-exception:${versionSecurity}",
                "commons-codec:commons-codec:1.4") {
            // O Grails usa a versao 3.0 do Spring, por isto precisa desta exclusao
            excludes 'spring-beans', 'spring-context', 'spring-core', 'spring-web', 'spring-aop',
                    'aspectjweaver', 'log4j'
        }

        compile 'net.sf.opencsv:opencsv:2.0'

        compile 'br.com.petrobras.security:security-shiro:4.3.2'
        
        compile 'org.apache.commons:commons-lang3:3.3.2'
        
        compile ('org.apache.shiro:shiro-core:1.2.2',
            'org.apache.shiro:shiro-web:1.2.2',
            'org.apache.shiro:shiro-spring:1.2.2',
            'org.apache.shiro:shiro-ehcache:1.2.2',
            'org.apache.shiro:shiro-quartz:1.2.2') {
            excludes 'ejb', 'jsf-api', 'servlet-api', 'jsp-api', 'jstl', 'jms',
               'connector-api', 'ehcache-core', 'slf4j-api', 'commons-logging'
            }
        //------ fim das dependencias do CAv4
        compile 'io.searchbox:jest:0.1.5'

        provided "ojdbc:ojdbc:14"

        //compile "javax.validation:validation-api:1.1.0.Final"
        //runtime "org.hibernate:hibernate-validator:5.0.3.Final"
    }

    plugins {
        // plugins for the build system only
        build ":tomcat:7.0.55"

        // plugins for the compile step
        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.8'
        //compile ":asset-pipeline:1.9.9"

        // plugins needed at runtime but not for compilation
        //runtime ":hibernate4:4.3.6.1" 
        runtime ":hibernate:3.6.10.18"
        runtime ":database-migration:1.4.0"
        runtime ":jquery:1.11.1"

        runtime ":resources:1.2.14"
        compile ":yui:2.8.2"
        compile ":executor:0.3"
        test ":plastic-criteria:1.5.1"
        compile ":cxf-client:1.5.7"
        compile ":gson:1.1.4"
        compile ":mail:1.0.7"
        //TODO compile ":include:0.4"
        compile ":cache-headers:1.1.6"
        runtime ":cached-resources:1.0"
        compile ':elasticsearch:0.0.3.6.br.1.0.1'   
        compile ":shiro:1.2.1"  

        build ':release:2.2.1', ':rest-client-builder:1.0.3', {
            export = false
        }

    }
}



