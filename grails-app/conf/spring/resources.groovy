import org.apache.lucene.analysis.br.BrazilianAnalyzer;

import grails.util.Environment;
import br.com.petrobras.appcenter.CustomObjectMarshallers
import br.com.petrobras.appcenter.ApplicationMarshaller
import br.com.petrobras.appcenter.UserMarshaller;


// Place your Spring DSL code here
beans = {

    cacheManager(org.apache.shiro.cache.ehcache.EhCacheManager)

    petrobrasSecurityRealm(br.com.petrobras.security.shiro.PetrobrasSecurityRealm) {
        cacheManager = ref("cacheManager")
    }

	if(Environment.current != Environment.TEST ){
		println "Configuring log4j ..."
		log4jConfigurer(org.springframework.beans.factory.config.MethodInvokingFactoryBean)
		{
			targetClass = "org.springframework.util.Log4jConfigurer"
			targetMethod = "initLogging"
			arguments = "${grailsApplication.config.log4jpath}"
		}
	}

	customObjectMarshallers( CustomObjectMarshallers ) {
		marshallers = [
			new ApplicationMarshaller(),
			new UserMarshaller()
		]
	}

	localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
		defaultLocale = new Locale("pt","BR")
		java.util.Locale.setDefault(defaultLocale)
	}
}