package br.com.petrobras.appcenter

import grails.converters.*
import grails.plugin.gson.converters.GSON
import javax.servlet.http.HttpServletResponse

class HeartBeatController {

	def shiroService

    def isAlive() {
		def isAlive = !(shiroService.loggedUser() == null)

		log.debug "$isAlive"

		return render(text:[isAlive: isAlive] as JSON, contentType:'application/json', status: HttpServletResponse.SC_OK, encoding: "UTF-8") 
    }

}
