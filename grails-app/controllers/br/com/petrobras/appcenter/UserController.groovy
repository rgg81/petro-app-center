package br.com.petrobras.appcenter

import grails.converters.*

import javax.servlet.http.HttpServletResponse

class UserController extends BaseController {

	def userService
    
    def myApplications() { 

    	secure({
			tryClosure({
				def user = shiroService.loggedUser()
				log.debug "Usuario [chave : ${user?.key}] apps [count ${user?.myApplications}]"
				def userApps = user?.myApplications*.application
				

				render text: [applications: ApplicationHelper.applicationTile(userApps)] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"

			})
		})
    }

    def mostAccessedApps() { 

        secure({
            tryClosure({
                def user = shiroService.loggedUser()
                def mostAccessedApps = userService.myMostAccessedApps(user,10, 8)
                render text: [applications: ApplicationHelper.applicationTile(mostAccessedApps)] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"

            })
        })
    }

    def share() {
    	secure({
    		tryClosure({
    			def user = shiroService.loggedUser()
    			log.debug "Usuario [chave : ${user?.key}] compartilhando as suas aplicações"
    			userService.shareUserApplications(user)
				def text = [user: user.key,  
                        	shareUserApplications: user.sharesMyApplications]
                
                log.debug "text: ${text}"
                render text: text as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
    			
    		})

    	})
    }

    def unshare() {
    	secure({
    		tryClosure({
    			def user = shiroService.loggedUser()
    			log.debug "Usuario [chave : ${user?.key}] parando de compartilhar as suas aplicações"
    			userService.unshareUserApplications(user)
				def text = [user: user.key,  
                        	shareUserApplications: user.sharesMyApplications]
                
                log.debug "text: ${text}"
                render text: text as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
    			
    		})

    	})
    }

    def checkShare() {
    	secure({
    		tryClosure({
    			def user = shiroService.loggedUser()
    			def text
    			if (user.sharesMyApplications)
    				text = [user: user.key, shareUserApplications: user.sharesMyApplications]
    			else
    				text = [user: user.key, shareUserApplications: user.sharesMyApplications]
    			render text: text as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
    		})
    	})
    }

    def userApps(String key) {
    	secure({
			tryClosure({
				def user = User.findByKey(key.toUpperCase())
				// Necessário para que outros domínios possam consumir o serviço
				if (user.sharesMyApplications) {
					response.setHeader('Access-Control-Allow-Origin', '*')
					def userApps = user?.myApplications*.application;
					render	text: [applications: ApplicationHelper.applicationTile(userApps)] as JSON
				} else {
					render	text: JSON.parse('{"message" : "Good try, better luck next time!" }') as JSON
					log.debug "Usuario [chave : ${user?.key}] nao compartilha as suas aplicações"
				}
				
				
			})
		})
	}

	def apps(String key) {
		secure({
			def user = User.findByKey(key.toUpperCase())
			[user: user]
		})
	}
}
