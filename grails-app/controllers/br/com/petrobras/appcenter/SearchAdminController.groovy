package br.com.petrobras.appcenter

import javax.servlet.http.HttpServletResponse
import grails.converters.*

class SearchAdminController {

	def searchAdminService

	static allowedMethods = [list: "GET", deleteIndex: "DELETE"]

    def index() { }

    def list() {
        log.debug("params: ${params}")
        log.debug("params?.index: ${params?.index}")
    	render text: [indexes: searchAdminService.list(params?.index&&params?.index!='undefined'?params?.index:null)] as JSON, status: HttpServletResponse.SC_OK, contentType: "application/json"	
    }

    def deleteIndex(String id) {
    	log.debug("excluindo índice $id")

        searchAdminService.deleteIndex(id)

    	render status: HttpServletResponse.SC_OK
    }

    def installMappings(String id) {
        log.debug("installing mappings for index $id")

        searchAdminService.installMappings(id)
        
        render status: HttpServletResponse.SC_OK
    }

    def indexDocs() {
        searchAdminService.indexDocs(request.JSON)

        render status: HttpServletResponse.SC_OK
    }

    def docsCount(String id) {
        render text: searchAdminService.docsCount(id), status: HttpServletResponse.SC_OK
    }

}