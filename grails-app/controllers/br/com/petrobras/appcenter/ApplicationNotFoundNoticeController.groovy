package br.com.petrobras.appcenter

import javax.servlet.http.HttpServletResponse
import grails.plugin.gson.converters.GSON
import grails.converters.*

class ApplicationNotFoundNoticeController extends BaseController {

	def mailService
    def accessControlService

	static allowedMethods = [save: "POST", list: "GET"]

	def save() {
	    secure({
    		tryClosure({
				def applicationNotFoundNotice = new ApplicationNotFoundNotice(
					user: shiroService.loggedUser(), 
					url: request.GSON.url?.getAsString(), 
					description: request.GSON.description?.getAsString()
				)

				applicationNotFoundNotice.save()

				sendMailNewApplicationNotFoundNotice applicationNotFoundNotice

		    	render status: HttpServletResponse.SC_CREATED 
	    	})
    	})	
	}

    def list() {
        []
    }
    
	def all(){
		secure({
			tryClosure({ 
                def list = ApplicationNotFoundNotice.list()

                list = list.collect{
                    [user: it.user, url: it.url, description: it.description, dateCreated: it.dateCreated.format("dd/MM/yy")]
                }

                render text: list as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
			})
		})
	}

    private def sendMailNewApplicationNotFoundNotice(applicationNotFoundNotice) {
        try {
            applicationNotFoundNotice.user.name = applicationNotFoundNotice.user.name

            mailService.sendMail {
                async true
                to "${applicationNotFoundNotice.user.key}@petrobras.com.br"
                bcc accessControlService.getModeratorsEmails()
                subject "[Aplicações Petrobras] - Notificação de aplicação não encontrada"
                html(view: "/mail/mailShell", model: [template: "/mail/newApplicationNotFoundNotice", applicationNotFoundNotice: applicationNotFoundNotice])
            }
        } catch (Exception e) {
            // não incomoda o usuário com erros no envio de email
            log.error e.message, e
        }
    }

}
