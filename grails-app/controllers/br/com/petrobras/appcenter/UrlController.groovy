package br.com.petrobras.appcenter

import grails.converters.JSON;

class UrlController {

    def check() {
		def unique
		
		log.debug(request.JSON.val)
		log.debug(request.JSON.applicationId)

		if (request.JSON.val.contains("petro-app-center/accessUrl")) {
			unique = true	
		}
		else if (request.JSON.applicationId) {
			def apps = []
			def app = BaseApplication.get(request.JSON.applicationId as Integer)
			def liveApp = Application.get(app.id)
			
			def draftApp = ApplicationDraft.get(app.id)
			if(draftApp) liveApp = draftApp?.application
			
			if(liveApp) apps += liveApp
			if(draftApp) apps += draftApp
			
			def links = Link.withCriteria(uniqueResult: true) {
				eq("url", request.JSON.val)
				not {'in'("application", apps)}
			}
			unique = ! (links)

		} else {
			unique = ! (Link.findByUrl(request.JSON.val) as Boolean)			
		}

		render(text:[isUnique:unique] as JSON, contentType:'application/json', encoding: "UTF-8")
	}
}
