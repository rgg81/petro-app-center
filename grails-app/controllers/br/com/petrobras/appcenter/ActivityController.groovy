package br.com.petrobras.appcenter

import org.springframework.util.Assert
import javax.servlet.http.Cookie

class ActivityController extends BaseController {

	def activityService

    def proxy() {
			tryClosure({
				Assert.notNull(params.type, "Parameter type not found")
    			Assert.notNull(params.where, "Parameter where not found")
    			Assert.notNull(params.appid, "Parameter appid not found")
    			Assert.notNull(params.url, "Parameter url not found")
				
				def app = Application.findById(params.appid)
				
				Assert.notNull(app, "Application not found")

				def isReliable = false //con

				Cookie[] cookies = request.getCookies()
                Cookie trackingCookie
                cookies.each{ cookie -> 
                    if(cookie.getName() == 'PETRO_TRACKING'){
                        trackingCookie = cookie
                    }
                }

				def user = shiroService.loggedUser()
                if(user){
                		isReliable = true;
                }else if(trackingCookie){
                	def trackedUser = User.findByTracking(trackingCookie.getValue())
                	if(trackedUser){
	                	if(params.userkey){
	                		if(trackedUser.key == params.userkey){	                
	                			user = trackedUser;
	                			isReliable = false
	                		}else{
	                			isReliable = true
	                		}
	                	}else{
	                		isReliable = false
	                		user = trackedUser;
	                	}
	                }else{
	                	isReliable = true;
	                	// user null
	                	// deslogado e tracking cookie invalido -> usuario desconhecido (mesmo que tenha params.userkey)
	                }
                }else{
                	isReliable = true;
                	// user null
                	// deslogado e sem tracking cookie-> usuario desconhecido (mesmo que tenha params.userkey)
                }

				

				activityService.registerActivity(user,app,params.type,params.where,isReliable)
				redirect(url: params.url)
			})
    }
}
