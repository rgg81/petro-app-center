package br.com.petrobras.appcenter

import grails.converters.*
import javax.servlet.http.HttpServletResponse
import grails.plugin.gson.converters.GSON

class ReportController extends BaseController {

	def reportService
	def gsonBuilder
	def mailService

    static allowedMethods = [listIssues: "GET", save: "POST", fetch: "GET"]

    def listIssues() {
	    secure({
    		tryClosure({
		    	render text: Issue.listOrderByPosition() as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
	    	})
    	})	
    }
    
    def save() {
	    secure({
    		tryClosure({
				def report = new Report(
					application: Application.get(request.GSON.applicationId.getAsLong()), 
					user: shiroService.loggedUser(), 
					description: request.GSON.description?.getAsString())

				def gson = gsonBuilder.create()
    			request.GSON.issues.each() {
    				report.addToIssues(gson.fromJson(it, Issue.class))
    			}    			 

				reportService.save report

				sendMailNewReport report

		    	render status: HttpServletResponse.SC_CREATED 
	    	})
    	})	
    }
    
    private def sendMailNewReport(report) {
        report.metaClass.sortedIssues = report.issues?.sort({a, b -> a.position <=> b.position})

        try {
            report.user.name = report.user.name

            def toArray = AdminApplication.findAllByApplication(report.application).collect{"${it.admin.key}@petrobras.com.br"}
            toArray.add("${report.user.key}@petrobras.com.br")

            mailService.sendMail {
                async true
                to toArray.toArray() 
                subject "[Aplicações Petrobras] - Notificação de erros de informação"
                html(view: "/mail/mailShell", model: [template: "/mail/newReport", report: report, serverNameWithPort: "${request.serverName}:${request.serverPort}"])
            }
        } catch (Exception e) {
            // não incomoda o usuário com erros no envio de email
            log.error e.message, e
        }
    }

    def list() {
        []
    }
    
    def all() {
        secure({
            tryClosure({
                def list = reportService.listReportsAndIssues()                               

                def application

                list = list.collect{
                    application = [name: it.application.name, url: it.application.link.url]

                    [application: application, user: it.user, issues: it.issues, description: it.description, dateCreated: it.dateCreated.format("dd/MM/yy")]
                }

                render text: list as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
            })
        })  
    }

}
