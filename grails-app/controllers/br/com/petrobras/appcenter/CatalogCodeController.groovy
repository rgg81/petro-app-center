package br.com.petrobras.appcenter
import grails.converters.JSON;

class CatalogCodeController {

    def check() {
		def unique

		if (request.JSON.applicationId) {
			def apps = []
			def app = BaseApplication.get(request.JSON.applicationId as Integer)
			def liveApp = Application.get(app.id)
			
			def draftApp = ApplicationDraft.get(app.id)
			if(draftApp) liveApp = draftApp?.application
			
			if(liveApp) apps += liveApp
			if(draftApp) apps += draftApp
			
			def appsNot = BaseApplication.withCriteria(uniqueResult: true) {
				eq("catalogCode", request.JSON.val?.toUpperCase())
				not {'in'("application", apps)}
			}
			unique = ! (appsNot)

		} else {
			unique = ! (BaseApplication.findByCatalogCode(request.JSON.val?.toUpperCase()) as Boolean)			
		}

		render(text:[isUnique:unique] as JSON, contentType:'application/json', encoding: "UTF-8")
	}
}
