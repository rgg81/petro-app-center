package br.com.petrobras.appcenter
import org.apache.lucene.queryparser.classic.QueryParser

class SearchAppController extends BaseController{


    def index() {
		if (!params.q?.trim()) {
			return [:]
		}

		def escaped_q = QueryParser.escape(params.q);

		try {
			
			def highlightSettings = {
			  field 'name'
			  field 'description', 300,1
			  field 'tagsBusca'
			  preTags '<b>'
			  postTags '</b>'
			}

			/*def mysuggest = [
				type:"term", 
				name:"mysuggest", 
				settings: 
				{	text escaped_q
					field 'name'
				}
			]*/
	
			def query = {
				match {
					_all(query: escaped_q,operator : 'and')
				}
			}

			def res = Application.search(query,[highlight: highlightSettings, from:params.offset?:0,size:params.max?:10])
			
			res.searchResults.eachWithIndex { it, index ->
				def fragments = res?.highlight[index]?.name?.fragments
				it.name = fragments?.size() ? fragments[0] : it.name

				if(it.description.length() > 300){
					it.description = it.description.substring(0,300)
				}

				fragments = res?.highlight[index]?.description?.fragments
				it.description = fragments?.size() ? fragments[0] : it.description

				fragments = res?.highlight[index]?.tagsBusca?.fragments

				it.tags.each{ tag ->
					def tagName = tag.name
					tag.metaClass.getKey = {tagName} 
					fragments?.each { frag ->
						if(frag.string() == ('<b>' + tagName + '</b>')){
							tag.name = frag.string();
						}
					}
				}

			}

			return [result: res,totalPages:res.searchResults.size() > 0 ? Math.ceil(res.total / res.searchResults.size()) : 0]
		} catch (Exception ex) {
			throw ex
		}
	}

	def autocomplete() {
		if (!params.q?.trim()) {
			return [:]
		}
		
		def escaped_q = QueryParser.escape(params.q);

		def highlightSettings = {
			field 'name', 300,1
			preTags '<b>'
		  	postTags '</b>'
		}
	
		def mysuggest = [
			type:"completion", 
			name:"mysuggest", 
			settings: 
			{	text escaped_q
				field 'app_suggestion'
			}
		]

		def numberOfSuggestion = 5;


		def query = {
			match {
				name(query: escaped_q,operator : 'or')
			}
		}


		def res = Application.search(query,[suggests:[mysuggest],highlight:highlightSettings,from:0,size:numberOfSuggestion,default_operator:'OR'])

		// getting autocomplete
		def apps = []
		res.suggest.each{ suggestion ->
			suggestion.each{ entry ->
				entry.options.eachWithIndex{ it, index ->
					def name = it.text.string()
					def iconPath = it.payloadAsMap.iconPath
					def appid = (Long)it.payloadAsMap.appid
					apps[index] =
					[ 	name: name,
						highlight: "<i>" + name + "</i>", // ainda nao tem highlight no autocomplete do elastci search, alternativa usr edge-ngram analyzer?
						value: appid,
						url: g.createLink(controller:'application', action:'show', id:appid),
						icon: iconPath
				 	]
				}
			}
		}

		if(apps.size > 5){
			apps = apps[0..4]
		}
		// getting normal search
		if(apps.size < numberOfSuggestion){
			res.searchResults.eachWithIndex { it, index ->
				def fragments = res?.highlight[index]?.name?.fragments
				def highlight = fragments?.size() ? fragments[0].string() : it.name

				if(!apps*.value.contains(it.id)){
					apps.push([
					 	name: it.name,
						highlight: highlight, // ainda nao tem highlight no autocomplete do elastci search, alternativa usr edge-ngram analyzer?
						value: it.id,
						url: g.createLink(controller:'application', action:'show', id:it.id),
						icon: Icon.relativePathByApplication(it)
					])
				}
			}
		}

		if(apps.size > 5){
			apps = apps[0..4];
		}

		render(contentType: "application/json") { apps }
	}

}
