package br.com.petrobras.appcenter

import grails.converters.*
import grails.plugin.gson.converters.GSON
import javax.servlet.http.HttpServletResponse
import grails.validation.ValidationException
import org.springframework.dao.DataIntegrityViolationException
import java.text.ParseException
import java.text.SimpleDateFormat
import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil
import com.google.gson.JsonNull;
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

//TODO: LIMPAR
import org.apache.shiro.SecurityUtils

import org.springframework.web.servlet.support.RequestContextUtils
import com.google.gson.reflect.TypeToken

class ApplicationController extends BaseController {

    def applicationService
    def userService
    def sessionFactory
	def gsonBuilder
	def accessControlService
	def csvHttpService
	def grailsLinkGenerator

    static allowedMethods = [listUserApplications: "GET", publish: "POST", save: "POST", update: "POST", delete: "POST", index: "GET", list: "GET", create: "GET", show: "GET", addUserApplication: "POST", removeUserApplication: "POST"]

	def beforeInterceptor = [action: this.&checkPermission, only: ['save', 'publish', 'edit', 'create']]
	
	private checkPermission() {
        log.debug "SecurityUtils.subject: ${SecurityUtils.subject}\n\n\n\n\n"
	    log.debug "controllerName: $controllerName\n\n\n\n\n"
	    log.debug "actionName: $actionName\n\n\n\n\n"
	    log.debug "actionUri: $actionUri\n\n\n\n\n"
	    log.debug "actionUri: $params\n\n\n\n\n"
	    
	    //TODO: Corrigir os casos save e publish
	    if (actionName in ['edit'] && params?.id != null) {
	    	if (!accessControlService.isAuthorized(controllerName, params?.id)) {
	    		log.info "Usuário NÃO autorizado em $controllerName id ${params?.id}. "
				redirect(action: 'index', controller: 'home')
				return false
	    	}
	
		    log.info "Usuário autorizado em $controllerName id ${params?.id}. "
	    }
	}

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        secure({
            tryClosure({
		        params.max = Math.min(max ?: 10, 100)

		        [applicationInstanceList: Application.list(params), applicationInstanceTotal: Application.count()]
            })
        })   
    }
	
	def department() {
		def deps = applicationService.listDistinctDepartments()
		render text: deps as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
	}

   def listcsv() {
        secure({
            tryClosure({

            	def csv = applicationService.asCSVStructure(Application.list(sort: "lastUpdated", order: "desc"),RequestContextUtils.getLocale(request));
	    		csvHttpService.render(response, csv,"aplicacoes")
            })
        })   
    }

    def create() {		

        def adminsApp = [[admin: [key: shiroService.loggedUser()?.key, name: shiroService.loggedUser()?.name, id: shiroService.loggedUser()?.id, class: shiroService.loggedUser()?.class]]]

        def application = new Application()
		
		application.adminsApp = adminsApp

        log.debug "application as JSON: ${application as JSON}\n"

        /*
        Precisamos do atributo name de User (em application.adminsApp.admin) que só é inicializado a partir do getter. 
        JSON e GSON não usam o getter, por isso foi usado um custom marshaller para Application. 
        */

        [applicationInstance: new Application(params), application: application as JSON]
    }
	
	private def mergeAppFromJson(applicationInstance, request, gson) {
		def dateFormat = new SimpleDateFormat("dd/MM/yyyy")
		applicationInstance.description = request.GSON.description.getAsString()
		applicationInstance.name = request.GSON.name.getAsString()
		applicationInstance.department = request.GSON.department?.getAsString()
		applicationInstance.icon.width = request.GSON.icon.width.asInt
		applicationInstance.icon.height = request.GSON.icon.height.asInt
		applicationInstance.icon.size = request.GSON.icon.size.asLong
		applicationInstance.link.url = request.GSON.link.url.asString
		log.debug "request.GSON.catalogCode.asString: ${request.GSON.catalogCode.asString}"
		applicationInstance.catalogCode = request.GSON.catalogCode.asString
		log.debug "applicationInstance.catalogCode: ${applicationInstance.catalogCode}"
		applicationInstance.isDownloadable = request.GSON.isDownloadable.asBoolean

		return applicationInstance
	}
	
    def save() {	
		secure({
			tryClosure({
				def applicationInstance,tagsParsed,infoSuportParsed,versionsParsed
				def gson = gsonBuilder
					.setExclusionStrategies(new ApplicationExclusionEstrategy())
					.setDateFormat("dd/MM/yyyy")
					.create()

				if (request.GSON.id == JsonNull.INSTANCE || request.GSON.id == null) {
					applicationInstance = gson.fromJson(request.GSON, ApplicationDraft.class)
				} else {
					log.debug("updating apps......")
					applicationInstance = ApplicationDraft.get(request.GSON.id.getAsLong())
					if(!applicationInstance) {
						def activeApp = Application.get(request.GSON.id.getAsLong())
						applicationInstance = new ApplicationDraft(icon:new Icon(), link:new Link(), application:activeApp)
					}
					
					// http://spring.io/blog/2010/07/02/gorm-gotchas-part-2/
					log.debug("user apps: ${applicationInstance.userApplications*.position}")
					applicationInstance = mergeAppFromJson(applicationInstance, request, gson)
					
					tagsParsed = gson.fromJson(request.GSON.tags, new HashSet<Tag>().class)
					infoSuportParsed = gson.fromJson(request.GSON.informationsSupport, new HashSet<InformationSupport>().class)
					versionsParsed = gson.fromJson(request.GSON.versions, new HashSet<Version>().class)
				}

				assignCompatibilityAndAdmin(applicationInstance, gson)
				
                log.debug "request.GSON: ${request.GSON}"	

                def iconFile = new File("${iconPath()}${request.GSON.icon.name?.asString}")
				
        		log.debug "applicationInstance.catalogCode: ${applicationInstance.catalogCode}"

        		applicationInstance = applicationService.saveApplication(applicationInstance, iconFile, tagsParsed, infoSuportParsed, null, versionsParsed) 
                
                log.debug "applicationInstance.id: ${applicationInstance.id}"
                log.debug "applicationInstance.catalogCode: ${applicationInstance.catalogCode}"
               
                def uri = g.createLink(controller: 'application', action: 'edit', id: applicationInstance.application?.id?:applicationInstance.id, absolute: true)

                def text = [application: applicationInstance, uri: uri, 
                    message: message(code: 'default.created.message', 
                        args: [message(code: 'application.label'), applicationInstance.name])]
                
                log.debug "text: ${text}"
                render text: text as JSON, status: HttpServletResponse.SC_CREATED, contentType: "text/json" 
			
			})
		})
          
    }
	
	private def assignCompatibilityAndAdmin = { applicationInstance, gson ->

		def adminsParsed = gson.fromJson(request.GSON.adminsApp, new TypeToken<HashSet<AdminApplication>>(){}.getType())
		def compatibilitiesParsed = gson.fromJson(request.GSON.compatibilities, new TypeToken<HashSet<Compatibility>>(){}.getType())
		log.info("compatibilitiesParsed:${compatibilitiesParsed}")
		log.debug("adminsParsed:${adminsParsed}")
		applicationInstance?.adminsApp = adminsParsed
		applicationInstance?.appCompatib = compatibilitiesParsed
		
	} 
	
	def publish() {
		secure({
			tryClosure({
				def applicationInstance,tagsParsed,infoSuportParsed,draftApp,versionsParsed
				def gson = gsonBuilder
					.setExclusionStrategies(new ApplicationExclusionEstrategy())
					.setDateFormat("dd/MM/yyyy")
					.create()

				if (request.GSON.id == JsonNull.INSTANCE || request.GSON.id == null) {
				   applicationInstance = gson.fromJson(request.GSON, Application.class)
				} else {
					log.debug("updating apps......")
					draftApp = ApplicationDraft.get(request.GSON.id.getAsLong())
					if(draftApp) {
						if(draftApp.application) {
							applicationInstance = Application.get(draftApp.application.id)
						}
						else {
							applicationInstance = new Application(icon:new Icon(), link:new Link())
						}
					}
					else applicationInstance = Application.get(request.GSON.id.getAsLong())
					
					// http://spring.io/blog/2010/07/02/gorm-gotchas-part-2/
					log.debug("user apps: ${applicationInstance.userApplications*.position}")
					applicationInstance = mergeAppFromJson(applicationInstance, request, gson)
					
					tagsParsed = gson.fromJson(request.GSON.tags, new HashSet<Tag>().class)
					infoSuportParsed = gson.fromJson(request.GSON.informationsSupport, new HashSet<InformationSupport>().class)
					versionsParsed = gson.fromJson(request.GSON.versions, new HashSet<Version>().class)
				}

				assignCompatibilityAndAdmin(applicationInstance, gson)
				
				log.debug "request.GSON: ${request.GSON}"

				def iconFile = new File("${iconPath()}${request.GSON.icon.name?.asString}")
				
				applicationInstance = applicationService.saveApplication(applicationInstance, iconFile, tagsParsed, infoSuportParsed, draftApp,versionsParsed)
				
				log.debug "applicationInstance.id: ${applicationInstance.id}"
			   
				def uri = g.createLink(controller: 'application', action: 'show', id: applicationInstance.id, absolute: true)

				def text = [application: applicationInstance, uri: uri,
					message: message(code: 'default.created.message',
						args: [message(code: 'application.label'), applicationInstance.name])]
				
				log.debug "text: ${text}"
				render text: text as JSON, status: HttpServletResponse.SC_CREATED, contentType: "text/json"
			
			})
		})
		  
	}
	
    def show(Long id) {
        def applicationInstance = Application.get(id)

		//TODO ajustar para quando for fazer o rating glogal, colocar em um lugar onde se possa ser usado por todos os controllers.
		def user = shiroService.loggedUser()
		session["user"] = user
        if (!applicationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'application.label', default: 'Application'), id])
            redirect(action: "list")
            return
        }

        [applicationInstance: applicationInstance, user: user]
    }

    def edit(Long id) {
		secure({
			tryClosure({
				def appJson
				def liveApp = Application.get(id)
				if(liveApp) {
					def draftApp = ApplicationDraft.findByApplication(liveApp)
					if(draftApp) {
						flash.appStatus = message(code: 'draft')
						appJson = BaseApplication.get(draftApp.id)
					}
					else appJson = liveApp
				}
				else {
					flash.appStatus = message(code: 'draft')
					appJson = BaseApplication.get(id)
				}
		
		        if (!appJson) {
		            flash.message = message(code: 'default.not.found.message', args: [message(code: 'application.label', default: 'Application'), id])
		            redirect(action: "list")
		            return
		        }
				log.info("edit application json ${appJson as JSON}")
		        [applicationInstance: new BaseApplication(params), appJson: appJson as JSON, editMode: true, applicationName: liveApp?.name]
			})
		})
                         
    }

	private def iconPath() {"${grailsApplication.config.imageDir}/icons/"}
	
	
    def delete(Long id) {
		secure({
			tryClosure({
				def applicationInstance = Application.get(id)
		        if (!applicationInstance) {
		            applicationInstance = ApplicationDraft.get(id)
					if (!applicationInstance)
						throw new IllegalArgumentException("application not found")
		        }
				applicationService.deleteApplication(applicationInstance)
				return render(text:[success:true] as JSON, contentType:'application/json', status: HttpServletResponse.SC_CREATED, encoding: "UTF-8")
			})
		})
    }

    def addUserApplication(Long id){
		secure({
			tryClosure({
				def applicationInstance = Application.get(id)
		        if (!applicationInstance) {
		            throw new IllegalArgumentException("application not found")
		        }
		        userService.addApplication(applicationInstance, shiroService.loggedUser())
				return render(text:[success:true] as JSON, contentType:'application/json', status: HttpServletResponse.SC_CREATED, encoding: "UTF-8")
			})
		})
    }
	
	//Essa action não exige autenticação
	def listUserApplications(String key) {
		tryClosure({
			def user = User.findByKey(key.toUpperCase())

			// Necessário para que outros domínios possam consumir o serviço
			response.setHeader('Access-Control-Allow-Origin', '*')

			render (user?.myApplications*.application.collect {
				[appName: it.name, appImage: "http://${request.serverName}:${request.serverPort}/${it.icon.relativePath()}", applink: it.link.url]
			} as JSON)
			
		})
	}

     def removeUserApplication(Long id){
		 secure({
			 tryClosure({
				def applicationInstance = Application.get(id)
		        if (!applicationInstance) {
		            throw new IllegalArgumentException("application not found")
		        }
		        userService.removeApplication(applicationInstance, shiroService.loggedUser())
				return render(text:[success:true] as JSON, contentType:'application/json', status: HttpServletResponse.SC_CREATED, encoding: "UTF-8")
			 })
		 })
    }

    private def pagination() {
        tryClosure({
            def offset = params.offset as Integer

            if(offset <= 0) offset = 0

            def limit = params.limit as Integer

            if(limit <= 0) limit = 10

            return [offset: offset, limit: limit]
        })
    }
	
	private forceClearCacheResponseForIE() {
		response.setHeader("Cache-Control","no-cache, no-store, max-age=0, must-revalidate")
		response.setHeader("Pragma","no-cache")
		response.setHeader("Expires", "Fri, 01 Jan 1990 00:00:00 GMT")
	}

    def popular() {
        secure({
            tryClosure({
                def pagination = pagination()

                def popularApplications = applicationService.popular(pagination.offset, pagination.limit)
				forceClearCacheResponseForIE()
				
                render text: [applications: ApplicationHelper.applicationTile(popularApplications), count: applicationService.popularCount()] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        }) 
    }

    def bestRated() {
        secure({
            tryClosure({
                def pagination = pagination()

                def bestRatedApplications = applicationService.bestRated(pagination.offset, pagination.limit)
				forceClearCacheResponseForIE()
                render text: [applications: ApplicationHelper.applicationTile(bestRatedApplications.apps), count: bestRatedApplications.total] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        }) 
    }

	def widget(Long id) {
    	def applicationInstance = Application.get(id)
    	def rating = applicationInstance.averageRating.round(2)
    	def view = "widget" + params.type
   		render (contentType:"application/javascript", view: view, model:[applicationInstance: applicationInstance, rating: rating])
    }

	
    def rating(Long id){
		def applicationInstance = Application.get(id)
    	def rating = applicationInstance.averageRating.round(2)
		def appLink = grailsLinkGenerator.link(absolute:true, controller: "application", action: "show", id: id)
    	render text:[page: appLink, rating: rating, totalRatings: applicationInstance.totalRatings] as JSON, status: HttpServletResponse.SC_OK, contentType: "application/json"
    }

}
