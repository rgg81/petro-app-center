package br.com.petrobras.appcenter

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.*
import javax.servlet.http.HttpServletResponse
import grails.plugin.gson.converters.GSON

class ReplyController extends BaseController {

    def replyService
    def replyMailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def update() {
        secure({
            tryClosure({
                def replyInstance = Reply.findById(request.GSON.id.getAsLong())
                replyInstance.text = request.GSON.text.getAsString()

                replyService.saveReply replyInstance
                replyMailService.sendMailUpdateReply replyInstance, "${request.serverName}:${request.serverPort}"
                render status: HttpServletResponse.SC_CREATED
            })
        })  
    }

    def insert() {
        secure({
            tryClosure({

                def replyInstance = new Reply()
                replyInstance.comment = Comment.get(request.GSON.commentId.getAsLong())
                replyInstance.text = request.GSON.text.getAsString()
                replyInstance.user = shiroService.loggedUser()

                replyService.saveReply replyInstance
                replyMailService.sendMailNewReply replyInstance, "${request.serverName}:${request.serverPort}"
                render status: HttpServletResponse.SC_CREATED 
            })
        })  
    } 

    def delete() {
        secure({
            tryClosure({
               def replyId = params["id"]
               def replyInstance = Reply.get(params["id"])
               

               replyService.deleteReplyAdmin(replyInstance)

                render text: ["Delete OK"] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        })  
    } 

}
