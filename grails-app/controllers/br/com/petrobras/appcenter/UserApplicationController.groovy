package br.com.petrobras.appcenter

import grails.converters.JSON;

class UserApplicationController extends BaseController {
	
	def userService
	
    def updateAppPosition() {
		secure({
			tryClosure({
				log.debug(request.JSON)
				request.JSON.each {
					def application = BaseApplication.get(it.applicationId.toLong())
					def thisUserApp = UserApplication.findByApplicationAndUser(application, shiroService.loggedUser())
					if(thisUserApp)
						userService.updateUserApplicationPosition(thisUserApp, it.position.toInteger())
				}
				render(text:[success:true] as JSON, contentType:'application/json', encoding: "UTF-8")
			})
		})
	}
}
