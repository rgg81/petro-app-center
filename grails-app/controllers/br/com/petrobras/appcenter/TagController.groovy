package br.com.petrobras.appcenter
import grails.converters.*

class TagController {

	
    def index() { }

    def find() {
        def tagsByName = Tag.findAllByNameLike("%" + params.name + "%")*.name.unique()
        def combined = tagsByName.flatten()
		
		def converter = new JSON(combined.collect{[value:it,tokens:it.tokenize(" ")]})
		converter.render(response)
    }	
}
