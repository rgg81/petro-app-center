package br.com.petrobras.appcenter

import grails.converters.*
import grails.validation.ValidationException
import javax.servlet.http.HttpServletResponse

class BaseController {

    def shiroService

    private def secure(Closure closure){
        if(!shiroService.loggedUser()){
            log.error "Unauthorized access!"
			render text: [message: "Unauthorized access!"] as JSON, status: HttpServletResponse.SC_FORBIDDEN, 
                    contentType: "text/json"  
            return
        }
        closure()
    }

    private def tryClosure(Closure closure) {
        try{
            closure()
        }catch(IllegalArgumentException e){
            log.error e.message, e
            render text: [message: e.message , errors: [e.message]] as JSON, status: HttpServletResponse.SC_BAD_REQUEST, contentType: "text/json"
            return
        }catch(RuntimeException e){
            //IllegalArgumentException
            if (e.cause?.class == IllegalArgumentException) {
                log.error e.cause.message, e.cause
                render text: [message: e.cause.message , errors: [e.cause.message]] as JSON, status: HttpServletResponse.SC_BAD_REQUEST, contentType: "text/json"
                return 
            }
			// ValidationException
            if (e.cause?.class == ValidationException) {
                log.error e.cause.message, e.cause
				
				def errorsMsgs = e.cause.errors.allErrors.collect {
					message(error:it, encodeAs:'JavaScript')
				}
				
				render text: [message: errorsMsgs.join(';\n') + '.', errors: errorsMsgs] as JSON, status: HttpServletResponse.SC_BAD_REQUEST, contentType: "text/json"
                return 	
            }
            log.error e.message, e
            render text: [message: e?.message?:e?.cause?.message] as JSON, status: HttpServletResponse.SC_INTERNAL_SERVER_ERROR, contentType: "text/json"
            return
        }catch(Exception e){
            log.error e.message, e
            render text: [message: e.message] as JSON, status: HttpServletResponse.SC_INTERNAL_SERVER_ERROR, contentType: "text/json"
            return
        }

    }

}
