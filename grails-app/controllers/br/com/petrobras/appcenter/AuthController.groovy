package br.com.petrobras.appcenter

import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.UsernamePasswordToken
import br.com.petrobras.security.extension.web.filter.logic.AuthenticationVerifier

import org.apache.shiro.web.util.WebUtils
import br.com.petrobras.security.ISecurityContext
import javax.servlet.http.Cookie

class AuthController {
    def index = { redirect(action: 'login', params: params) }
    
    def userService
    def accessControlService

    def login = {
        try {
			log.debug("AuthController.login - begin")
			def targetUri = (params.targetUri ?: "").decodeURL()
	        def returnUrl = getReturnUrl(request, targetUri)
	
	        def verifier = new AuthenticationVerifier()
	
	        if (verifier.isAuthenticated(request, response, returnUrl)) {
	            def user = ISecurityContext.context.currentLoggedUser
	            if (user != null) {
	                // Usuario esta autenticado pelo CAv4, cria a sessao do shiro.
	                log.debug "Usuario logado. Criando sessao no Shiro."
	                def token = new UsernamePasswordToken(user.login)
	                SecurityUtils.subject.login(token)

                    def savedUser = userService.saveNewUser(user.login)
                    
                    log.debug "trackingCookie:" + savedUser.tracking

                    Cookie cookie = new Cookie("PETRO_TRACKING",savedUser.tracking)
                    cookie.maxAge = 365 * 24 * 60 * 60
                    cookie.path = '/petro-app-center/'
                    response.addCookie(cookie)
	            }
	            log.debug "Redirecionando para '${targetUri}'."
	            return redirect(uri: targetUri)
	        } else {
	            // Usuario nao autenticado ou cookie nao verificado, AuthenticationVerifier ja redirecionou
	            log.debug "Usuario nao esta autenticado no CA. Ira redirecionar para a tela de login (espero)"
	            return false
	        }
        }
		catch (Exception e) {
			log.error("erro na autenticação", e)
			throw e
		}
    }

    private String getReturnUrl(request, targetUri) {
        log.debug "targetUri='${targetUri}'"
        // Handle requests saved by Shiro filters.
        def savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }
        log.debug "targetUri+savedRequest='${targetUri}'"

        def returnUrl
        def hostName = request.getHeader('host')
        log.debug "hostName=${hostName}"
        if (hostName) {
            def protocol = request.isSecure() ? "https" : "http"
            returnUrl = "${protocol}://${hostName}${request.forwardURI}?targetUri=${targetUri}"

        } else {
            returnUrl = g.createLink(action: 'login', params: [targetUri: targetUri], absolute: true).decodeURL()
        }
        log.debug "returnUrl=${returnUrl}"

        return returnUrl
    }

    def signOut = {
        // Log the user out of the application.
        SecurityUtils.subject?.logout()

        // Redireciona para o named mapping "indexJsp", caso esteja declarado no UrlMappings do projeto.
        // Senao redireciona para o index deste controller.
        redirect(url: g.createLink(mapping:'indexJsp', absolute: true))
    }

    def unauthorized = {
        
		redirect(url: g.createLink(mapping:'indexJsp', absolute: true))
    }
}
