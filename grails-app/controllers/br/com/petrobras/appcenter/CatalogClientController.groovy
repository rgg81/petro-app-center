package br.com.petrobras.appcenter

import grails.converters.*
import javax.servlet.http.HttpServletResponse

class CatalogClientController extends BaseController {
	def catalogClientService

    def fetch() { 
    	secure({
    		tryClosure({
	        	render text: catalogClientService.fetch(params.appCode) as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
    		})
    	})
    }
}
