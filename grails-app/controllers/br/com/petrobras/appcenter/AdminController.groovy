package br.com.petrobras.appcenter

class AdminController extends BaseController {

	def applicationService
	def csvHttpService

    def listApplications() { 
		secure {
	    	def returnValues = [:]
			def user = shiroService.loggedUser()
			def appsAndStatus = applicationService.userApplicationsAdmins(user)
			[appsAndStatus: appsAndStatus, applicationAdminInstanceTotal: appsAndStatus?.size]
		}

    }

    def listApplicationsByDepartment() { 
		secure {
	    	def returnValues = [:]
			def user = shiroService.loggedUser()
			

			if(!params.containsKey('d')){
				params.d = user.department.acronym
			}

			
			

			withFormat {
				html {
					def result = applicationService.searchByDepartment(params.d,[from:params.offset?:0,size:params.max?:10])
					[apps: result.apps, user: user, total: result.total]
				}
				csv {
					def result = applicationService.searchByDepartment(params.d,[from:0,size:5000])
					def csv=[['id','código', 'aplicação', 'administradores', 'centro de agilidade']]
					result.apps?.each{
						csv.push([it.id,it.catalogCode,it.name,it.admins,it.department])
					}
					csvHttpService.render(response, csv,"aplicacoes_por_centro_de_agilidade")
				}
			}
		}
    }
}
