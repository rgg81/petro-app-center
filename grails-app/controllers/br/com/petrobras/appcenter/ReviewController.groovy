package br.com.petrobras.appcenter

import grails.converters.*
import java.text.DateFormat
import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.support.RequestContextUtils
import org.springframework.util.Assert
import au.com.bytecode.opencsv.CSVWriter
import groovy.time.TimeCategory
import java.text.SimpleDateFormat

class ReviewController extends BaseController {

    def reviewsService

    static allowedMethods = [mostLiked: "GET", recent: "GET", byRating: "GET", my:"GET", insert: "POST", update: "PUT", delete: "DELETE", like: "PUT", dislike: "PUT", metadata: "GET", moderate: "DELETE"]

	private def assembleReview(Comment comment, Double rating) {
        def user = shiroService.loggedUser()
        
        Like evaluation

        for (eval in comment.evaluations) {
            if (eval.user.key == user.key) {
                log.debug "eval: $eval" 
                evaluation = eval
                break
            }
        }

        def review = [
            user: [
                key: comment.user.key, 
                name: comment.user.name, 
                department: comment.user.department?.acronym
                ], 
            myevaluation: (evaluation?.value ?: 0),
            rating: rating, 
            date: DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, RequestContextUtils.getLocale(request)).format(comment.lastUpdated?:new Date()),
            likes: comment.likes.size(), 
            dislikes: comment.dislikes.size(), 
            title: comment.title ? comment.title : "", 
            text: comment.text ? comment.text : "", 
            id: comment.id,
            app: comment.application.name,
            app_id: comment.application.id,
            reply: [
                id: comment.reply?.id?:null,
                text: comment.reply?.text?: null,
                userId: comment.reply?.user?.id,
                date: comment.reply ? DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, RequestContextUtils.getLocale(request)).format(comment.reply.lastUpdated) : null,
            ]
        ]

        return review
    }

    def mostLiked() {
        secure({
            tryClosure({
                def limit = params.limit as Integer
                def mostLiked = reviewsService.listMostLikedReviews(params.applicationId,limit).collect{
                    assembleReview(it, it.rating)
                }
                /*
                Tratado sobre cache: http://www.mnot.net/cache_docs/#PRAGMA
                Serve para garantir que a lista esteja sempre atualizada (o valor em max-age está em segundos)
                */
                response.setHeader("Cache-Control", "max-age=1")

                render text: mostLiked as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json" 
            })
        }) 
    }

    def recent() {
        secure({
            tryClosure({
                BaseApplication application = BaseApplication.get(params.applicationId)
                Assert.notNull(application, "Application not found!")
                
                def offset = params.offset as Integer

                if(offset <= 0) offset = 0

                def limit = params.limit as Integer

                if(limit <= 0) limit = 10

                def recent = Comment.createCriteria().list(max: limit, offset: offset, sort: 'lastUpdated', order: 'desc') {
                    isNotNull("text")
                    eq("application", application)
                }.collect{ 
                    assembleReview(it, it.rating)
                }
       
                /* 
                Tratado sobre cache: http://www.mnot.net/cache_docs/#PRAGMA
                Serve para garantir que a lista esteja sempre atualizada (o valor em max-age está em segundos)
                */
                response.setHeader("Cache-Control", "max-age=1")

                render text: recent as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        }) 
    }

    def byRating() {
        secure({
            tryClosure({
                def user = shiroService.loggedUser()

                if (shiroService.isModerator()) {
                    BaseApplication application = BaseApplication.get(params.applicationId)
                    Assert.notNull(application, "Application not found!")

                    def offset = params.offset as Integer

                    if(offset <= 0) offset = 0

                    def limit = params.limit as Integer

                    if(limit <= 0) limit = 10

                    def rating = params.rating as Integer

                    def reviewsRating = Comment.findAllByApplicationAndRating(application, rating).collect {
                        [stars: it.rating, user: [key: it.user.key, name: it.user.name, department: it.user.department.acronym] ]
                    }

                    /* 
                    Tratado sobre cache: http://www.mnot.net/cache_docs/#PRAGMA
                    Serve para garantir que a lista esteja sempre atualizada (o valor em max-age está em segundos)
                    */
                    response.setHeader("Cache-Control", "max-age=1")

                    render text: reviewsRating as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
                } else {
                    render  text: JSON.parse('{"message" : "Good try, better luck next time!" }') as JSON
                }
             })
        })            
        
    }

    JSON all() {
        secure({
            tryClosure({
                BaseApplication application = BaseApplication.get(params.applicationId)
                Assert.notNull(application, "Application not found!")

                def allReviews = Comment.createCriteria().list(sort: 'lastUpdated', order: 'desc') {
                    isNotNull("text")
                    createAlias("application", "app")
                    eq("app.id", application.id)            
                }.collect{ [comment : it , rating: it.rating] }                

                render text: allReviews as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        }) 
    }

    def insert() {
        secure({
            tryClosure({

                Comment review = new Comment(request.JSON)
                review.user = shiroService.loggedUser()
                def myReview = reviewsService.addReview(review, "${request.serverName}:${request.serverPort}")
                render text: assembleReview(myReview, myReview.rating) as JSON, status: HttpServletResponse.SC_CREATED, contentType: "text/json"
            })   

        })      

    }

    def like() {
        log.debug "params.review: " + params
        secure({
            tryClosure({
                def review = request.JSON

                Assert.isTrue(review && review.id, "Parametro de review invalido")

                reviewsService.likeReview(review.id)

                render text: [user: shiroService.loggedUser().key, review:review.id , message: "Like OK"]  as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        })
    }

    def dislike() {
        log.debug "params.review: " + params
        secure({
            tryClosure({
                def review = request.JSON
                Assert.isTrue(review && review.id, "Parametro de review invalido")
                reviewsService.dislikeReview(review.id)
                render text: [user: shiroService.loggedUser().key, review:review.id , message: "Dislike OK"]  as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })
        })
    }

    def update() {
        secure({
            tryClosure({
                def review = reviewsService.editReview(request.JSON, "${request.serverName}:${request.serverPort}")

                render text: assembleReview(review, review.rating) as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })   
        })
    }

    def delete() {
        secure({
            tryClosure({
                Assert.hasLength(params["id"],"Parametro reviewId Invalida")

                reviewsService.deleteReview(params["id"].toLong(), "${request.serverName}:${request.serverPort}")

                render text: ["Delete OK"] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })   
        })
    }

    def moderate() {
        secure({
            tryClosure({
                Assert.hasLength(params["id"],"Parametro reviewId Invalida")

                reviewsService.moderateReview(params["id"].toLong(), "${request.serverName}:${request.serverPort}")

                render text: ["Delete OK"] as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })   
        })
    }

    def my() {
        secure({
            tryClosure({
                Assert.notNull(params.applicationId, "Applicacao id invalido.")
                BaseApplication application = BaseApplication.get(params.applicationId)
                Assert.notNull(application, "Applicacao nao existe.")
                
                def review = Comment.findByApplicationAndUser(application, shiroService.loggedUser())
                
                if(review == null){
                    render status: HttpServletResponse.SC_NO_CONTENT
                    return
                }
                
                /* 
                Tratado sobre cache: http://www.mnot.net/cache_docs/#PRAGMA
                Serve para garantir que a lista esteja sempre atualizada (o valor em max-age está em segundos)
                */
                response.setHeader("Cache-Control", "max-age=1")

                render text: assembleReview(review, review.rating) as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })   
        })
    }

    def metadata() {
        secure({
            //TODO: generalizar
            //TODO: criar teste ??
            def metadata = [
                text: [maxSize: Comment.constraints.text.maxSize]
            ]

            render text: metadata as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
        })
    }

    //TODO teste
    def groupedByDay(){
        secure({
            tryClosure({
                def accept = request.getHeader("Accept")
                def startDate = (params.start ? params.start : null)
                def endDate = (params.end ? params.end : null)

                def reviews = reviewsService.listReviewsFromDayToDay(startDate,endDate)

                withFormat {
                    json { 
                        def reportByDay = [:]

                        fillReportWithZeroValues(startDate,endDate,reportByDay)

                        reviews.each{ review ->                            
                            def dateKey = review.lastUpdated.format('dd/MM/yyyy')
                            if(!reportByDay[dateKey]){
                                reportByDay[dateKey] = [count:0, reviews:[]]
                            } 
                            
                            reportByDay[dateKey].count++;
                            reportByDay[dateKey].reviews.push(assembleReview(review, review.rating));                            
                        }

                        render text: reportByDay as JSON, status: HttpServletResponse.SC_OK, contentType: "application/json"
                    }
                    csv {
                        renderReviewsAsCSV(response, request, reviews)
                    }
                }
            })
        })   
    }

    def report() {
        secure({
            tryClosure({

            })
        })
    }

    private def fillReportWithZeroValues(startDay,endDay,reportByDay){
        def formatDay = new SimpleDateFormat("dd/MM/yyyy")
        def start = formatDay.parse(startDay)
        def end = formatDay.parse(endDay)

        def duration = end - start

        for(i in 0..duration){
            def dateKey =(start + i).format('dd/MM/yyyy');
            reportByDay[dateKey] = [count:0, reviews:[]] 
        }
    }

    private def renderReviewsAsCSV(response, request, reviews){        
        response.setHeader("Content-Type","application/vnd.ms-excel:ISO-8859-1")
        response.setHeader("Content-Disposition","attachment;filename=avaliacoes.csv")
        response.status = HttpServletResponse.SC_OK
        
        response.outputStream << "sep=,\n" 

        def headers = ["chave usuario","nome usuario","lotacao usuario","nota","id avaliacao","titulo","texto",
                        "data ultima modificacao","gostaram", "desgostaram","id aplicacao", "codigo aplicacao", "nome aplicacao", "admins"]      

        CSVWriter writer = new CSVWriter(new OutputStreamWriter(response.outputStream,"ISO-8859-1"))
        
        writer.writeNext(headers as String[])

        reviews.each{ review -> 
            def admins = AdminApplication.findAllByApplication(review.application)*.admin*.key.join(",")

            def params = [  review.user.key,
                            review.user.name,
                            review.user.department?.acronym,
                            review.rating,
                            review.id,
                            review.title,
                            review.text,
                            DateFormat.getDateTimeInstance(DateFormat.LONG, 
                                DateFormat.MEDIUM, 
                                RequestContextUtils.getLocale(request)).format(review.lastUpdated?:new Date()),
                            review.likes?.size(),
                            review.dislikes?.size(),
                            review.application.id,
                            review.application.catalogCode,
                            review.application.name,
                            admins]
            
            params.collect{ ((it instanceof String && it ) ? new String(it.getBytes("ISO-8859-1"),"ISO-8859-1") : it )}
            writer.writeNext(params as String[])

        }
        writer.flush()
        writer.close()
    }

}
