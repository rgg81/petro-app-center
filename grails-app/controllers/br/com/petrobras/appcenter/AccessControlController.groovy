package br.com.petrobras.appcenter


import grails.converters.*
import grails.validation.ValidationException
import org.springframework.util.Assert
import javax.servlet.http.HttpServletResponse
import br.com.petrobras.security.ISecurityContext
import br.com.petrobras.security.model.*
import br.com.petrobras.security.configuration.*
import br.com.petrobras.security.exception.*
import br.com.petrobras.security.extension.web.bean.AuthenticationManagedBean
import br.com.petrobras.security.authentication.IUserAuthenticator
import javax.servlet.http.HttpServletResponse
import org.springframework.beans.BeanWrapper
import org.springframework.beans.PropertyAccessorFactory
import org.codehaus.groovy.grails.commons.DefaultGrailsControllerClass
import java.lang.reflect.Method
import grails.web.Action
import br.com.petrobras.security.management.access.IResourceManager
import grails.converters.*
import br.com.petrobras.security.authorization.*
import br.com.petrobras.security.management.authorization.IUserRoleAuthorizationManager
import br.com.petrobras.security.model.authorization.access.UserRoleAuthorization 
import br.com.petrobras.security.model.authorization.access.RoleResourceAuthorization
import br.com.petrobras.security.model.authorization.access.UserGroupRoleAuthorization
import br.com.petrobras.security.model.authorization.configuration.RoleUseCaseAuthorization
import br.com.petrobras.security.model.Role
import br.com.petrobras.security.exception.ConstraintViolationException
import groovy.json.JsonSlurper

class AccessControlController extends BaseController {

	def accessControlService
	def catalogClientService

    def findUser() {
        secure({
            tryClosure({
                Assert.notNull(params.key, "Chave inválida.")

				def userFromCA = accessControlService.findUser(params.key)
				
				def domainUser = User.findByKey(userFromCA.login)
				if(domainUser == null) {
					domainUser = new User(key: userFromCA.login)
					domainUser.save(flush:true)
				}
                render text: domainUser as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
            })   
        })
    }

/*
Métodos utilitários, sem cobertura de teste. 
*/

	def numUsers = 1

// os usuários são compartilhados entre os ambientes
	def createExternalUsers() {

		log.info "create/update users \n\n\n\n\n\n\n\n\n\n"
		//br.com.petrobras.security.extension.web.listener.SecurityConfigListener
		def manager = ISecurityContext.context.externalUserManager
		ExternalUser user
		Boolean isNew
		def numUsers = 10

		for (int i = 0; i < numUsers; i++) {
			log.info "usuário: ljap_teste${i} \n\n\n\n\n\n\n\n\n\n"
			try {
				user = manager.find("ljap_teste${i}")	
				isNew = false
			} catch(ObjectNotFoundException e) {
				isNew = true				
				log.info "novo usuário: ${isNew}"
				user = new ExternalUser()
				user.login = "ljap_teste${i}"
			}

			user.name = "ljap_teste${i} name"
			user.shortName = "ljap_teste${i} shortName"				
			user.email = "ljap@mailinator.com"
			user.gender = br.com.petrobras.security.model.Gender.MALE
			user.setPreferredLanguage(new java.util.Locale("pt_BR"))
			user.birthDate = new Date()
			user.documentNumber = "11111111"
			user.address = "a"
			user.districtAddress = "a"
			user.cityAddress = "a"
			user.countryAddress = "AFG"
			user.stateAddress = "a"

			isNew ? manager.save(user) : manager.update(user)
		}

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 
	}
// localhost:8080/petro-app-center/ca/update?login=ljap_teste0&name=Joao%20das%20Couves&shortName=Caldo%20Verde
	def updateExternalUsers() {

		log.info "create/update users \n\n\n\n\n\n\n\n\n\n"
		//br.com.petrobras.security.extension.web.listener.SecurityConfigListener
		def manager = ISecurityContext.context.externalUserManager
		ExternalUser user
		Boolean isNew
		def numUsers = 10

		log.info "Alteraćão de usuário externo\n\n\n\n\n"
		log.info "name: ${params.login}"
		log.info "name: ${params.name}"
		log.info "name: ${params.shortName}"
		
		user = manager.find(params.login)	

		user.name = params.name
		user.shortName = params.shortName
		user.email = "ljap@mailinator.com"
		user.gender = br.com.petrobras.security.model.Gender.MALE
		user.setPreferredLanguage(new java.util.Locale("pt_BR"))
		user.birthDate = new Date()
		user.documentNumber = "11111111"
		user.address = "a"
		user.districtAddress = "a"
		user.cityAddress = "a"
		user.countryAddress = "AFG"
		user.stateAddress = "a"

		manager.update(user)

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 
	}

// Ainda não funciona
	def updateExternalUsersPassword() {

		log.info "Update users' passwords \n\n\n\n\n\n\n\n\n\n"
		//br.com.petrobras.security.extension.web.listener.SecurityConfigListener
		def manager = ISecurityContext.context.externalUserManager
		ExternalUser user
		Boolean isNew

		def senhas = [
		"yDEVPj1C", 
		"f4FOQIMH", 
		"xVt7NOVz", 
		"wJSHEHs1", 
		"lEl8pCzn", 
		"r0SQD5AX", 
		"mXE7tB72", 
		"l2buG24Q", 
		"nJxBd02L", 
		"uE95Gsvk"]

		for (int i = 0; i < numUsers; i++) {
			user = manager.find("ljap_teste${i}")	

			try {
				manager.updatePassword("ljap_teste${i}", senhas[i], "12345678")
			} catch(Exception e) {
				log.info e.message, e
				render text: "Not Ok", status: HttpServletResponse.SC_INTERNAL_SERVER_ERROR, contentType: "text/html" 
				return 
			}
		}

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 
	}

	def listaActions() {
		log.info "\n\n\n\n\n\n"
// stackoverflow.com/questions/2956294/reading-out-all-actions-in-a-grails-controller
		// keys are logical controller names, values are list of action names  
		// that belong to that controller
		def controllerActionNames = [:]		
		//def _package = params.package ? params.package : 'br.com.petrobras.appcenter'
		//log.info "params.package: ${params.package}"
		//log.info "_package: ${_package}"

		grailsApplication.controllerClasses.each { DefaultGrailsControllerClass controller ->

		    Class controllerClass = controller.clazz

		    // skip controllers in plugins
		    //if (controllerClass.name.startsWith(_package)) {
		    if (!controllerClass.name.startsWith('br.com.petrobras.appcenter')) {
//		    if (controllerClass.name) {
		        String logicalControllerName = controller.logicalPropertyName

		        // get the actions defined as methods (Grails 2)
		        controllerClass.methods.each { Method method ->

		            if (method.getAnnotation(Action)) {
		                def actions = controllerActionNames[logicalControllerName] ?: []
		                actions << method.name

		                controllerActionNames[logicalControllerName] = actions
		            }
		        }
		    }
		}

		def manager = ISecurityContext.context.externalUserManager

		for (controllerInfo in controllerActionNames) {
			log.info "controller: ${controllerInfo.key}"
			log.info "actions: "
			for (action in controllerInfo.value) {
				log.info "${action}"
			}
		}

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 		

	}

	def createResources() {
		log.info "\n\n\n\n\n\n ********************** createResources *********************** \n\n\n\n\n\n"
// stackoverflow.com/questions/2956294/reading-out-all-actions-in-a-grails-controller
		// keys are logical controller names, values are list of action names  
		// that belong to that controller
		def controllerActionNames = [:]

		grailsApplication.controllerClasses.each { DefaultGrailsControllerClass controller ->

		    Class controllerClass = controller.clazz

		    // skip controllers in plugins
		    if (controllerClass.name.startsWith('br.com.petrobras.appcenter')) {
		        String logicalControllerName = controller.logicalPropertyName

		        // get the actions defined as methods (Grails 2)
		        controllerClass.methods.each { Method method ->

		            if (method.getAnnotation(Action)) {
		                def actions = controllerActionNames[logicalControllerName] ?: []
		                actions << method.name

		                controllerActionNames[logicalControllerName] = actions
		            }
		        }
		    }
		}

		def key
		Resource parent

		def ljap = addResource("ljap", null, params.update)

		for (controllerInfo in controllerActionNames) {
			//log.info "controller: ${controllerInfo.key}"
			//log.info "actions: "
			
			parent = addResource(controllerInfo.key, ljap, params.update)

			for (action in controllerInfo.value) {
				//log.info "${action}"

				key = "${controllerInfo.key}:${action}"

				addResource("${controllerInfo.key}:${action}", parent, params.update)
			}
		}
		
		//add list user applications resource
		addResource("listuserapps", ljap, params.update)

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 		

	}

	private def addResource(key, parent, update) {
		def manager = ISecurityContext.context.resourceManager		
		Resource resource
		ResourceType type

		try {
			def types = manager.findAllTypes()
			for (t in types) {
				if (t.id == "MODULE") {
					type = t
					break
				}
			}
			
			//log.info "type.id: ${type.id}"

			resource = manager.find(key)			
			if (update) {
				log.info "\n\n\n\n\n ******************** UPDATING RESOURCE ******************** \n\n\n\n\n"
				log.info "resource.id: ${resource.id}"
				if (parent) resource.parent = parent
				manager.update(resource)
			}			
		} catch(ObjectNotFoundException e) {
			log.info e.message, e
			resource = new Resource(key, key, key, key, type)

			if (parent) resource.parent = parent
			manager.save(resource)
		} catch(Exception e) {
			log.error e.message, e
		}

		manager.find(key)
	}

	def listResources() {
		log.info "\n\n\n\n\n\n"

		def manager = ISecurityContext.context.resourceManager

		def resources = manager.findAll()

		def htmlOut = "<html><head></head><body><table border=1><thead><th>Id</th><th>Parent Id</th>"
		for (resource in resources) {
			htmlOut = htmlOut + "<tr>"

			log.info "resource.id: ${resource.id}"
			log.info "resource.parent: ${resource.parent?.id}"
			log.info "resource.name: ${resource.parent?.name}"
			log.info "resource.nameTranslations: ${resource.nameTranslations}"

			htmlOut = htmlOut + "<td>" + resource.id + "</td>"
			htmlOut = htmlOut + "<td>" + resource.parent?.id + "</td>"

			htmlOut = htmlOut + "</tr>"
		}
		htmlOut = htmlOut + "</table></body>"

		render text: htmlOut, status: HttpServletResponse.SC_OK, contentType: "text/html" 		
	}

	def listaTypes() {
		log.info "\n\n\n\n\n\n"

		def manager = ISecurityContext.context.resourceManager

		for (type in manager.findAllTypes()) {
			log.info "type.id: ${type.id}"
			log.info "type.name: ${type.name}"
		}

		render text: "Ok", status: HttpServletResponse.SC_OK, contentType: "text/html" 		

	}

	def listUserAuthorizedRoles() {
		IRoleAuthorizer authorizer = ISecurityContext.context.roleAuthorizer;

		// Obtém os papéis autorizados para o usuário logado desconsiderando o contexto.
		// Uma melhor leitura para essa consulta seria: me dê todos os papéis que o usuário logado pode assumir na "Sala A" da "Plataforma P-51".
		List<Role> roles = authorizer.findAllAuthorized();

		def htmlOut = "<html><head></head><body><table border=1><thead><th>Id</th><th>Name</th>"
		for (role in roles) {
			htmlOut = htmlOut + "<tr>"

			htmlOut = htmlOut + "<td>" + role.id + "</td>"
			htmlOut = htmlOut + "<td>" + role.name + "</td>"

			htmlOut = htmlOut + "</tr>"
		}
		htmlOut = htmlOut + "</table></body>"

		render text: htmlOut, status: HttpServletResponse.SC_OK, contentType: "text/html" 		

	}

	def listUserAuthorizedResources() {
		log.info "\n\n\n\n\n\n"

		IResourceAuthorizer authorizer = ISecurityContext.context.resourceAuthorizer;

		def resources = authorizer.findAll()

		def htmlOut = "<html><head></head><body><table border=1><thead><th>Id</th><th>Parent Id</th>"
		for (resource in resources) {
			htmlOut = htmlOut + "<tr>"

			log.info "resource.id: ${resource.id}"
			log.info "resource.parent: ${resource.parent?.id}"
			log.info "resource.name: ${resource.parent?.name}"
			log.info "resource.nameTranslations: ${resource.nameTranslations}"

			htmlOut = htmlOut + "<td>" + resource.id + "</td>"
			htmlOut = htmlOut + "<td>" + resource.parent?.id + "</td>"

			htmlOut = htmlOut + "</tr>"
		}
		htmlOut = htmlOut + "</table></body>"

		render text: htmlOut, status: HttpServletResponse.SC_OK, contentType: "text/html" 		
	}

	def usersHTMLTable(users, title) {
		def htmlOut = "<html><head></head><body><h3>${title}</h3><table border=1><thead><th>login</th><th>name</th>"
		for (user in users) {
			htmlOut = htmlOut + "<tr>"

			log.info "user.login: ${user.login}"
			log.info "user.name: ${user.name}"

			htmlOut = htmlOut + "<td>" + user.login + "</td>"
			htmlOut = htmlOut + "<td>" + user.name + "</td>"

			htmlOut = htmlOut + "</tr>"
		}
		htmlOut = htmlOut + "</table></body>"
	}

	/*
	Ao criar app, devem ser criados: 
	- Resource correspondente
	- Perfil correspondente
	- Incluir criador no perfil
	- Dar acesso ao perfil no Resource
	*/

	def createInitialRoles() {
		log.info "\n\n\n\n\n\n ********** createInitialRoles ***************** \n\n\n\n\n\n"

		Role role

		def manager = ISecurityContext.context.roleManager;
		
		def types = manager.findAllTypes()
		def type
		def rolesWithError = []

		for (t in types) {
			if (t.id == "COMMON") {
				log.info "t: ${t.id}"
				type = t
				break
			}
		}

		log.info "type: ${type?.id}"

/*
    PermissionDeniedException - Usuário não tem autorização para realizar a operação. 
    SecurityException - Exceção genérica de segurança.
*/
		def applications = Application.findAll()

		//applications = [[id: 1]]
		for (application in applications) {			
			def roleId = accessControlService.getApplicationAdminRole(application.id)

			role = new Role(roleId, roleId, roleId, true, type)

			try {
				manager.save(role)
/*
    InvalidArgumentException - Papel é nulo ou vazio. 
    PermissionDeniedException - Usuário não tem autorização para realizar a operação. 
    ConstraintViolationException - Papel já existe ou não possui todos os dados obrigatórios. 
    SecurityException - Exceção genérica de segurança.
*/
			} catch(ConstraintViolationException e) {
				log.error e.message, e
				rolesWithError.push roleId
			} catch(Exception e) {
				log.error e.message, e
			}
		}
	
		if (rolesWithError.size > 0) {
			render text: "Algumas roles já existiam: " + rolesWithError, status: HttpServletResponse.SC_BAD_REQUEST, contentType: "text/plain" 								
		} else {
			render text: "Roles criadas. ", status: HttpServletResponse.SC_OK, contentType: "text/plain" 								
		}
	
	}

	/*
	Esse método serve para inicializar o CA quando a base de dados já existe. Basta executar uma vez para o ambiente. 
	Se precisar usar remova o private. 

	Esse método NAO faz: 
	- Criar papel Admin
	- Grant de Admin em recursos
	- Grant usuários em Admin 
	- Criar papel Everyone
	- Grant de Everyone em recursos
	*/
	def initializeca() {
		// cria recursos
		createResources()

		// cria informação
		Information applicationI = new Information("application")
		applicationI.name = "application"
		applicationI.description = "application"
		applicationI.sharingType = new InformationSharingType(InformationSharingTypeEnum.NONE)
		applicationI.enabled = true

		try{
			ISecurityContext.context.informationManager.save(applicationI)
		} catch(ConstraintViolationException e) {}
		
		def commonRoleType = ISecurityContext.context.roleManager.findAllTypes().findAll { type -> type.id == "COMMON" } [0]
		Role role
		
		// Cria papéis Moderator e AdminApp
		[grailsApplication.config.app.role.adminapp, grailsApplication.config.app.role.moderator].each() { roleId -> 
			log.info "${roleId}"
	
			role = new Role()
			role.id = roleId
			role.name = roleId
			role.description = roleId
			role.enabled = true
			role.type = commonRoleType
			log.info "role.type: ${role.type.id}"

			try{
				ISecurityContext.context.roleManager.save(role)
			} catch(ConstraintViolationException e) {}
		}


		// Concede Moderator a um grupo de chaves
		["Y1R4", "Y1MB", "Y1B7", "Y775", "UPN1", "AB9W"].each() { userLogin -> 
			log.info "${userLogin}"
	
			try{
				ISecurityContext.context.userRoleAuthorizationManager.grant(userLogin, grailsApplication.config.app.role.moderator)
			} catch(InvalidOperationException e) {}
		}


		// autoriza AdminApp em create, delete, edit e save application
		["admin:listApplications", "application:create", "application:delete", "application:edit", "application:list", "application:publish", "application:save", "catalogClient:fetch", "catalogCode:check", "imageUpload:upload", "url:check"].each() { resourceId -> 
			log.info "${resourceId}"
			try {
				ISecurityContext.context.roleResourceAuthorizationManager.grant(grailsApplication.config.app.role.adminapp, resourceId)
			} catch (InvalidOperationException e) {}				
		}


		// autoriza Moderator em moderate review
		["review:groupedByDay", "review:moderate", "review:report", "report:all", "report:list", "applicationNotFoundNotice:all", "applicationNotFoundNotice:list"].each() { resourceId -> 
			log.info "${resourceId}"
			try {
				ISecurityContext.context.roleResourceAuthorizationManager.grant(grailsApplication.config.app.role.moderator, resourceId)
			} catch (InvalidOperationException e) {}				
		}


		// autoriza AdminApp nos casos de uso
		[ UseCase.GRANT_INFORMATION_VALUE_TO_USER_USE_CASE_ID, UseCase.REVOKE_INFORMATION_VALUE_FROM_USER_USE_CASE_ID, UseCase.SAVE_INFORMATION_VALUE_USE_CASE_ID, UseCase.REMOVE_INFORMATION_VALUE_USE_CASE_ID , UseCase.GRANT_ROLE_TO_USER_USE_CASE_ID, UseCase.REVOKE_ROLE_FROM_USER_USE_CASE_ID ].each() { useCaseId -> 
			log.info "${useCaseId}"
			try {
				ISecurityContext.context.roleUseCaseAuthorizationManager.grant(grailsApplication.config.app.role.adminapp, useCaseId)
			} catch (InvalidOperationException e) {}				
		}


		// pra cada aplicação, cria valor de informação e grants
		BaseApplication.findAll().each() {  
			log.info "\n\n\n\n\n\n"
			log.info "application.id: ${it.id}"
			it.adminsApp = AdminApplication.findAllByApplication(it)
			log.info "adminsApp: ${it.adminsApp}"
			log.info "\n\n\n\n\n\n"
			try {
				accessControlService.updateRoleAndGrants(it)
			} catch(Exception e) {
				log.error e.message, e
			}

		}

		render text: "Criados e grantados! ", status: HttpServletResponse.SC_OK, contentType: "text/plain" 
	}

	/*
	ALTERE O AMBIENTE CONFORME DESEJADO!!!
	Limpa autorizações e cadastros que usamos em ljap. 
	*/
	def cleanca() {
		log.info "\n\n\n\n\n"
		log.info "iniciando cleanca"

		def allroles = ISecurityContext.context.roleManager.findAll()
		def resources = ["application:delete:", "application:edit:", "application:save:"]

		allroles.each() { role -> 
			log.info "\n\n\n\n\n"
			log.info "role id: ${role.id}"
			try {
				def roleUseCaseAuthorization = ISecurityContext.context.roleUseCaseAuthorizationManager.findAll(role.id)
	
				roleUseCaseAuthorization.each() {
					log.info "usecase id: ${role.useCase.id}"
					try {
						ISecurityContext.context.roleUseCaseAuthorizationManager.revoke(role.role, role.useCase)	
					} catch (InvalidOperationException e) {}
					
				}
			} catch(ObjectNotFoundException e) {}

			ISecurityContext.context.roleResourceAuthorizationManager.findAll(role).each() { roleResourceAuthorization -> 
				log.info "${roleResourceAuthorization.resource.id}"
				if ( resources.findAll{ roleResourceAuthorization.resource.id.contains(it) } ) {
					log.info "${roleResourceAuthorization.resource.id} in resources array!"
					try {
						// Revoga autorizações no recurso
						ISecurityContext.context.roleResourceAuthorizationManager.revoke(role.id, roleResourceAuthorization.resource.id)
					} catch (InvalidOperationException e) {}				
				}
			}

			if (!role.id in [grailsApplication.config.app.role.admin, grailsApplication.config.app.role.everyone]) {
				try {
					ISecurityContext.context.userRoleAuthorizationManager.findAllWithRole(role).each() {
						// revoga os direitos do usuário no papel
						ISecurityContext.context.userRoleAuthorizationManager.revoke(it.user, it.role)
					}
				} catch (ObjectNotFoundException e) {}				

				try {
					// Exclui o papel
					ISecurityContext.context.roleManager.remove(role)
				} catch (ObjectNotFoundException e) {}				
			}
		}

		log.info "\n\n\n\n\n"
		log.info "excluindo recursos"

		ISecurityContext.context.resourceManager.findAll().each() { resource -> 
			log.info "${resource.id}"
			if ( resources.findAll{ resource.id.contains(it) } ) {
				log.info "${resource.id} in resources array!"
	
				// Exclui recurso
				ISecurityContext.context.resourceManager.remove(resource)
			}
		}

		/*
		IRoleUseCaseAuthorizationManager
		java.util.List<RoleUseCaseAuthorization>	findAll(java.lang.String roleId) 
		revoke(java.lang.String roleId, java.lang.String useCaseId) 

IRoleInformationValueAuthorizationManager
java.util.List<RoleInformationValueAuthorization>	findAllWithInformationValue(InformationValue informationValue) 

IInformationValueManager
remove(InformationValue informationValue) 
          Remove um valor de informação.
		*/
		log.info "finalizando cleanca"
		log.info "\n\n\n\n\n"

		render text: "Tá limpo! ", status: HttpServletResponse.SC_OK, contentType: "text/plain" 
	}

	// Atualiza aplications com informações do catálogo
	
	def allApps() {
		secure({
			tryClosure({
				
				[allAppsJson: BaseApplication.findAll() as JSON]
			})
		})
	}
	
	def updateFromCatalogById(Long id) {
		secure({
			tryClosure({
				
				def application = BaseApplication.get(id)
				def catalogData = catalogClientService.fetch(application.catalogCode)
				application.name = catalogData.name
				application.description = catalogData.description
				application.department = catalogData.department
	
				application.save(flush:true, failOnError:true)
				
				[success: true as JSON]
			})
		})
	}

	def updateFromCatalog() {
		runAsync {
			def catalogData
			def applications = BaseApplication.findAll()
			
			for (application in applications) {
				Application.withTransaction { status ->
					log.debug "application.catalogCode: ${application.catalogCode}"
					try {
						catalogData = catalogClientService.fetch(application.catalogCode)
						log.debug "catalogData: $catalogData"
						
						log.debug "catalogData.name: ${catalogData.name}"
						log.debug "catalogData.description: ${catalogData.description}"
			
						log.debug "application.name: ${application.name}"
						log.debug "application.description: ${application.description}"
			
						application.name = catalogData.name
						application.description = catalogData.description
						application.department = catalogData.department
			
						application.save(flush:true, failOnError:true)
						log.info "application.id saved success: ${application.id}"
					} catch (Exception e) {
						log.debug "Continue ... "
						log.info "error saving application.id: ${application.id}"
						log.error e.message, e
					}
				}
			}
		}
		render text: "Viva a preguiça!!! ", status: HttpServletResponse.SC_OK, contentType: "text/plain" 
	}

	def resourcesByRoles() {
		render accessControlService.listResourcesByRoles() as JSON
	}

	def diffAccessControl() {
		def inputFile = grailsApplication.mainContext.getResource('/WEB-INF/accessControl.json').file
		def inputJSON = new JsonSlurper().parseText(inputFile.text)

		def rolesOutput = accessControlService.listResourcesByRoles()

		render makeDiff(inputJSON,rolesOutput).sort {it.key} as JSON	
	}

	def diffJSON() {
		def inputFile = grailsApplication.mainContext.getResource('/WEB-INF/accessControl.json').file
		def inputJSON = new JsonSlurper().parseText(inputFile.text)

		def rolesOutput = accessControlService.listResourcesByRoles()

		render makeDiff(rolesOutput,inputJSON).sort {it.key} as JSON	
	}

	def makeDiff(jsonMap1, jsonMap2) {
		def inputDiff = [:]
		jsonMap1.each{ 
			def inputDiffResources = []
			def role = it.getKey()
			if (jsonMap2.containsKey(it.getKey())) {				
				it.getValue().each {
					if (!jsonMap2.get(role).contains(it))
						inputDiffResources.add(it)
				}
				if (inputDiffResources.size() > 0) {

					inputDiff.put(role,inputDiffResources.sort())
				}
			}
			else {
				inputDiff.put(role, it.getValue())
			}
		}
		return inputDiff
	}

	def removeOldAdminAppRoles() {
		def allroles = ISecurityContext.context.roleManager.findAll()
		def removedHtmlOut = ""
		def notRemovedHtmlOut = ""
		def removedCounter = 0;
		def notRemovedCounter = 0;
		allroles.each { Role role -> 
			if (role.getName().matches("AdminApp\\d.*")){
				def roleName = role.getName()
				def roleId = role.getId()
				ISecurityContext.context.userRoleAuthorizationManager.findAllWithRole(role).each { UserRoleAuthorization userRole ->
					ISecurityContext.context.userRoleAuthorizationManager.revoke(userRole.getUser(), role)
				}
				ISecurityContext.context.roleResourceAuthorizationManager.findAll(role).each { RoleResourceAuthorization roleResource ->
					ISecurityContext.context.roleResourceAuthorizationManager.revoke(role, roleResource.getResource())
				}
				ISecurityContext.context.roleUseCaseAuthorizationManager.findAll(role).each { RoleUseCaseAuthorization roleUseCase ->
					ISecurityContext.context.roleUseCaseAuthorizationManager.revoke(role, roleUseCase.getUseCase())
				}
				ISecurityContext.context.userGroupRoleAuthorizationManager.findAllWithRole(role).each { UserGroupRoleAuthorization userGroupRole ->
					ISecurityContext.context.userGroupRoleAuthorizationManager.revoke(userGroupRole.getUserGroup(), role)
				}
				try {
					ISecurityContext.context.roleManager.remove(role)
					removedCounter++;
					removedHtmlOut = removedHtmlOut + roleName + "<br>"
				} catch (ConstraintViolationException e) {
					log.error e.message, e
					notRemovedCounter++;
					notRemovedHtmlOut = notRemovedHtmlOut + roleName + "<br>"
				}
			}
		}
		render "<h1>${removedCounter} Roles Removidas</h1><span style='font-weight: bold'>" + removedHtmlOut + "<h1 style='color: red'>${notRemovedCounter} Roles NÃO Removidas</h1><span style='color: red; font-weight: bold'>" + notRemovedHtmlOut
	}
}