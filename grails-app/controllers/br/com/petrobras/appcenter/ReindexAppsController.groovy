package br.com.petrobras.appcenter

class ReindexAppsController {

	def elasticSearchService

	def unindex() {
		elasticSearchService.unindex(Application)
		render text: "unindex ok", status: 200
	}
    def index() {
		elasticSearchService.index(Application)
		render text: "reindex ok", status: 200
	}

	def indexUserActivity() {
		elasticSearchService.index(UserActivity)
		render text: "index UserActivity ok", status: 200
	}

	def unindexUserActivity() {
		elasticSearchService.unindex(UserActivity)
		render text: "unindex UserActivity ok", status: 200
	}
}
