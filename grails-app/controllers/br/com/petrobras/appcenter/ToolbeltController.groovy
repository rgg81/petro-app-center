package br.com.petrobras.appcenter

import grails.converters.*
import au.com.bytecode.opencsv.CSVWriter
import javax.servlet.http.HttpServletResponse

class ToolbeltController extends BaseController {
  def applicationService
  def userService
  def sessionFactory
  def gsonBuilder
  def accessControlService
  def activityService


    def index() { 
      secure({
        tryClosure({

        })
      })
   	}

   	def ratings(String key) {

   	}

   	def ratingRanking() {

   	}

   	def mostApplicationsAdded() {

   	}

   	def indexInfo() {
      secure({
        tryClosure({
       	 render text: applicationService.toolBeltApplicationInfo() as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"	
        })
      })

   	}

    def allApplications() {
      secure({
        tryClosure({
			def apps = applicationService.allApplications()
			withFormat {
				json {
					render text: apps as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"
				}
				csv {
					response.setHeader("Content-Type","application/vnd.ms-excel:ISO-8859-1")
					response.setHeader("Content-Disposition","attachment;filename=avaliacoes.csv")
					response.status = HttpServletResponse.SC_OK
					
					response.outputStream << "sep=,\n"
			
					def headers = ["Nome Aplicação","Código","URL","Publicada","Tem rascunho","Média","Admins"]
			
			
					CSVWriter writer = new CSVWriter(new OutputStreamWriter(response.outputStream,"ISO-8859-1"))
					
					writer.writeNext(headers as String[])
			
					apps.each{
						
			
						def params = [  it.name,
										it.catalogCode,
										it.url,
										it.isPublished,
										it.hasDraft,
										it.averageRating,
										it.admins]
						
						params.collect{ ((it instanceof String && it ) ? new String(it.getBytes("ISO-8859-1"),"ISO-8859-1") : it )}
						writer.writeNext(params as String[])
			
					}
					writer.flush()
					writer.close()
				}
			}   
        })
      })
    }

    def applications() {
      secure({
        tryClosure({

        })
      })

    }

    def averagexuse() {
      secure({
        tryClosure({

        })
      })
    }

    def averagexaccess() {
      secure({
        tryClosure({

        })
      })
    }


    def applicationAverageAndUse() {
      secure({
        tryClosure({
          render text: applicationService.applicationAverageAndUse() as JSON, status: HttpServletResponse.SC_OK, contentType: "text/json"   
        })
      })
    }

    def applicationAverageAndAccess() {
     secure({
        tryClosure({
            def appsWithRating = applicationService.applicationAverageAndAccess()
            def apps = []
            def id = ""
            def timespan = 0
            def today = new Date()
            def endDate = String.format('%td/%<tm/%<tY',today)
            def startDate
            if (params.timespan) { 
              timespan = params.timespan.toInteger()
              startDate = String.format('%td/%<tm/%<tY',today - timespan)
            }
           
            def idsAndCounts = activityService.topApps(timespan)
            appsWithRating.each { app ->
              def count = 0
              if (idsAndCounts[app.id.toString()])
                count = idsAndCounts[app.id.toString()]
              apps.push([id:app.id,name:app.name,count:count,rating:app.rating,x:count,y:app.rating])
            }
            def result = [
              startDate: startDate,
              endDate: endDate,
              apps: apps
            ]

           render result as JSON          
        })
      })
   }
}
