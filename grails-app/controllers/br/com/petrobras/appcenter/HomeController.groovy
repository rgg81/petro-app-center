package br.com.petrobras.appcenter

import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsHibernateUtil;

class HomeController {

	def applicationService
    def activityService

	def shiroService
	
	static maxAppsInList = 16

    def index() {

    	def returnValues = [:]
    	if(shiroService.isAuthenticated()){
			def user = shiroService.loggedUser()
			log.debug "Usuario [chave : ${user?.key}] apps [count ${user?.myApplications}]"
			def userApps = user?.myApplications*.application
    		returnValues.put("apps",userApps)
    		returnValues.put("user",user)
    	}

    	return returnValues
    }

}
