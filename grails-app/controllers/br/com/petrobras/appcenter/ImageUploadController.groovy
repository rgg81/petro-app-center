package br.com.petrobras.appcenter

import grails.converters.JSON

import org.springframework.http.HttpStatus

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.springframework.web.multipart.MultipartFile

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest

class ImageUploadController {

	def upload = {
		try {
			def filename
			if (params.qqfile instanceof CommonsMultipartFile) {
				filename = params.qqfile.originalFilename
			}else {
				filename = params.qqfile
			}
			
			File uploaded = createTemporaryFile(filename)
			InputStream inputStream = selectInputStream(request)
			def imgBuffer = ImageIO.read(inputStream)

			def	index = filename.lastIndexOf('.')
			def	extension = filename.substring(index + 1)
			
			if (extension?.toLowerCase() != 'jpg' && extension?.toLowerCase() != 'png')
			{
				return render(text: [success:false, message: "Extensão não permitida"] as JSON, contentType:"text/html")
			}
			
			if (imgBuffer.width == 119 && imgBuffer.height == 119)
			{
				log.debug("params.guid=${params.guid}")
				if (session[params.guid]) {
					session[params.guid].delete()
				}
				ImageIO.write(imgBuffer, extension, uploaded)
				session[params.guid] = uploaded
				return render(text:[success:true] as JSON, contentType:'text/html', encoding: "UTF-8")
			}
			else
			{
				log.debug("Image doesnt fit the with and height")
				return render(text:[success:false, message: 'Dimensão da imagem está diferente (119x119)'] as JSON, contentType:'text/html', encoding: "UTF-8")
			}			
		} catch (Exception e) {

			log.error("Failed to upload file.", e)
			return render(text:[success:false, message: e.message] as JSON, contentType:'text/html', encoding: "UTF-8")
		}

	}

	private InputStream selectInputStream(HttpServletRequest request) {
		if (request instanceof MultipartHttpServletRequest) {
			MultipartFile uploadedFile = ((MultipartHttpServletRequest) request).getFile('qqfile')
			return uploadedFile.inputStream
		}
		return request.inputStream
	}

	private File createTemporaryFile(String filename) {
		File uploaded
		uploaded = new File("${grailsApplication.config.imageDir}/icons/${filename}")
		
		return uploaded
	}

}

