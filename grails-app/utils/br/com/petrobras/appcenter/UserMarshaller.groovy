package br.com.petrobras.appcenter
 
import grails.converters.JSON

// http://compiledammit.com/2012/08/16/custom-json-marshalling-in-grails-done-right/ 
class UserMarshaller {
 
	void register() {
		JSON.registerObjectMarshaller(User) { User user ->
	    	return [
	    		class: user.class, 
	    		id: user.id, 
	    		name: user.name, 
	    		key: user.key	    		
	    	]
		}
	}
}