package br.com.petrobras.appcenter
 
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;

import org.codehaus.groovy.grails.web.json.JSONObject;

import com.google.gson.JsonObject;

import grails.converters.JSON
import groovy.json.JsonOutput;

// http://compiledammit.com/2012/08/16/custom-json-marshalling-in-grails-done-right/ 
class ApplicationMarshaller {
 
	void register() {
		JSON.registerObjectMarshaller(BaseApplication) { BaseApplication application ->
			log.debug "application.adminsApp: ${application.adminsApp}\n"
			log.debug "application.adminsApp.admin.key: ${application.adminsApp.admin.key}\n"
			log.debug "User.class: ${User.class}\n"
			def dateFormat = new SimpleDateFormat("dd/MM/yyyy")
			def adminsApp = AdminApplication.findAllByApplication(BaseApplication.get(application?.id)).collect { adminApp -> 
				[class: AdminApplication.class, id: adminApp?.id, application: [id: adminApp?.application?.id], version: adminApp?.version, 
					admin: [key: adminApp?.admin?.key, name: adminApp?.admin?.name, id: adminApp?.admin?.id, class: adminApp?.admin?.class]
				]
			}
			def allCompatibilities = Compatibility.list()?.collect { comp ->
				[class: Compatibility.class, id:comp?.id, nameImage:comp?.nameImage, description:comp?.description]
			}.sort{it.description}
			
			def compatibilities = application.compatibilities?.collect { comp ->
				[class: Compatibility.class ,id:comp?.id, nameImage:comp?.nameImage, description:comp?.description]
				
			}
			def validEscape = { value -> if(value) value?.encodeAsHTML().replace("&quot;","\"")}
			
			def tags = application.tags?.collect { tag ->
				[name: validEscape(tag.name)]
			}

			def icon = [
				width: application?.icon?.width, 
				height: application?.icon?.height, 
				size: application?.icon?.size,
				id: application?.icon?.id,
				relativePath: application?.icon?.relativePath()
			]

			
			def informationsSupport = application.informationsSupport?.collect { infoSupport ->
				[class: InformationSupport.class, id: infoSupport.id, url: infoSupport.url, description:validEscape(infoSupport.description)]
				
			}

			def allversions = application.versions?.collect { version ->
				[class:Version.class, id: version?.id, number: validEscape(version?.number), description: validEscape(version?.description), date: dateFormat.format(version?.date?:new Date())]  
			}
			
	    	return [
	    		class: application.class, 
	    		id: application.id,
				isDownloadable: application.isDownloadable,
	    		name: validEscape(application.name), 
	    		description: validEscape(application.description),
	    		department: validEscape(application.department),
				catalogCode: application.catalogCode, 
	    		link: application.link, 
	    		icon: icon, 
	    		versions: allversions?:new HashSet<Version>(), 
	    		adminsApp: adminsApp?:application.adminsApp?:new HashSet<AdminApplication>(), 
	    		tags: tags?:new HashSet<Tag>(), 
				informationsSupport: informationsSupport?:new HashSet<Tag>(), 
				compatibilities: compatibilities?:new HashSet<Compatibility>(),	 
				allcompatibilities: allCompatibilities?:new HashSet<Compatibility>(), 
	    		catalogCode: application.catalogCode

	    	]
		}
	}
}